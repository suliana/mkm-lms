<?php
/**
 * @author      Danny Froberg <danny@hackix.com>
 * @name        App_Controller_Plugin_LangSelector
 * @filesource  library/App/Controller/Plugin/LangSelector.php
 * @tutorial    Instantiate in application.ini with;
 *              resources.frontController.plugins.LangSelector =
 *              "App_Controller_Plugin_LangSelector"
 * @desc        Takes the lang parameneter when set either via a
 *              route or get/post and switches Locale, This depends
 *              on the main initTranslate function in Bootstrap.php
 *              to set the initial Zend_Translate object.
 *              Inspiration from ZendCasts LangSelector.
 */


class Epic_LangSelector extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$registry = Zend_Registry::getInstance();
		// Get our translate object from registry.
		$translate = $registry->get('Zend_Translate');
		$currLocale = $translate->getLocale();
		
	
		if(!isset($currLocale) || empty($currLocale) || $currLocale == 'en_US'){
			$currLocale = 'en';
		}
		// Create Session block and save the locale
		$session = new Zend_Session_Namespace('language');
		$session->lang;
		$session->lang_dir;
		//var_dump($session->lang);
		$lang = $request->getParam('lang','');
		if(!isset($lang) || empty($lang) || !isset($session->lang)){
			$currLocale = 'en';
		}
		
		//Get language_configurer white list 
		$available_language_list = $this->Get_All_Language_List();
		
		//Check if available in white list
		if(!array_key_exists($lang, $available_language_list)){
			$langLocale = isset($session->lang) ? $session->lang : $currLocale;
			
		}else{
			$langLocale = $lang;
			
		}
		
		//Assign to session variable
		$session->lang = $langLocale;
		$session->lang_dir = $available_language_list[$langLocale][1];

		$locale = new Zend_Locale($langLocale);
		$file = $this->Get_Language_Set_By_ID($available_language_list[$langLocale][2]);
		$translate = new Zend_Translate('array',
		$file, $locale,
		array(
		'disableNotices' => true,    // This is a very good idea!
		'logUntranslated' => false,  // Change this if you debug
		)
		);

		$newLocale = new Zend_Locale();
		$newLocale->setLocale($langLocale);
		$registry->set('Zend_Locale', $newLocale);

		$translate->setLocale($langLocale);
		// Save the modified translate back to registry
		$registry->set('Zend_Translate', $translate);
	}

	
	public function Get_All_Language_List(){
		$languageModel = new Languageset_Model_Configurer();
		$languageConfigurerData = $languageModel->fetchAll();
		$_arrayLanguage  = array();
		if(count($languageConfigurerData) > 0){
			foreach($languageConfigurerData as $language){
				$_arrayLanguage[$language['Code']][0] = $language['Name']; 
				$_arrayLanguage[$language['Code']][1] = $language['Direction'];
				$_arrayLanguage[$language['Code']][2] = $language['ID'];
			}
			return $_arrayLanguage;
		}else{
			return false;	
		}
		
	}
	/**
	 * List all 
	 * @param integer $id
	 * @return array
	 */
	public function Get_Language_Set_By_ID($id = null){
		$id = (int) $id;
		
		$languageSetModel = new Languageset_Model_Set();
		$languageSetData = $languageSetModel->fetchByLang($id);
		if(count($languageSetData) > 0){
			$langSet = array();
			foreach($languageSetData as $languageSet){
				$key = $languageSet['varname'];
				$value = ($languageSet['meaning'] == '' ? $key : $languageSet['meaning']);
				$langSet[$key] = $value;
			}
		}
		return $langSet;
	}
	
}

//class Epic_LangSelector extends Zend_Controller_Plugin_Abstract
//{
//	public function preDispatch(Zend_Controller_Request_Abstract $request)
//	{
//		$registry = Zend_Registry::getInstance();
//		// Get our translate object from registry.
//		$translate = $registry->get('Zend_Translate');
//		$currLocale = $translate->getLocale();
//
//		// Create Session block and save the locale
//		$session = new Zend_Session_Namespace('session');
//		$session->lang;
//
//		$lang = $request->getParam('lang','');
//
//		// Register all your "approved" locales below.
//		
//		switch($lang) 
//		{
//			case "ms":
//				$langLocale = 'ms_MY'; break;
//			case "en":
//				$langLocale = 'en_US'; break;
//			default:
//				/**
//                 * Get a previously set locale from session or set
//                 * the current application wide locale (set in
//                 * Bootstrap)if not.
//                 */
//				$langLocale = isset($session->lang) ? $session->lang : $currLocale;
//
//		}
//
//		//set new session
//		$session->lang = $langLocale;
//
//		switch($session->lang)
//		{
//			case "ms_MY":
//				$locale = new Zend_Locale('ms_MY');
//				$file = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR . "ms_MY.php";
//
//				break;
//			case "en_US":
//				$locale = new Zend_Locale('en_US');
//				$file = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR . "en_US.php";
//
//				break;
//		}
//
//		$translate = new Zend_Translate('array',
//		$file, $locale,
//		array(
//		'disableNotices' => true,    // This is a very good idea!
//		'logUntranslated' => false,  // Change this if you debug
//		)
//		);
//
//		$newLocale = new Zend_Locale();
//		$newLocale->setLocale($langLocale);
//		$registry->set('Zend_Locale', $newLocale);
//
//		$translate->setLocale($langLocale);
//		// Save the modified translate back to registry
//		$registry->set('Zend_Translate', $translate);
//	}
//
//	
//	public function Language_List(){
//		$languageModel = new Languageset_Model_Configurer();
//		$languageData = $languageModel->fetchAll();
//		$_arrayLanguage  = array();
//		if(count($languageData) > 0){
//			foreach($languageData as $language){
//				$_arrayLanguage[$language['Code']][0] = $language['Name']; 
//				$_arrayLanguage[$language['Code']][1] = $language['Direction'];
//			}
//			return $_arrayLanguage;
//		}else{
//			return false;	
//		}
//		
//	}
//	
//}








