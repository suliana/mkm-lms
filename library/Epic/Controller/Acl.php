<?php
class Epic_Controller_Acl extends Zend_Controller_Plugin_Abstract
{
	/**
     *
     * @var Zend_Auth
     */
	protected $_auth;
	protected $_acl;
	protected $_action;
	protected $_controller;
	protected $_module;
	protected $_currentRole;
	protected $_courseid;

	public function __construct (Zend_Acl $acl, array $options = array())
	{
		//$storage = new Zend_Auth_Storage_Session();
		//$this->_auth = $storage->read();
		
		$this->_auth = Zend_Registry::get('auth')->getStorage()->read();
		
		//$this->_auth = Zend_Auth::getInstance();
		$this->_acl = $acl;
	}
	
	
	public function preDispatch (Zend_Controller_Request_Abstract $request)
	{		
		$exception = array( 'firstlogin', 'register' );
		
		// force login
		if ( !Zend_Registry::get('auth')->hasIdentity() )
		{
			if ( $request->getControllerName() == 'index' && in_array($request->getActionName(), $exception) )
			{
				// we should ignore this since this is a first time login page
			}
			else
			{
				$request->setModuleName('default');
				$request->setControllerName('index');
	        	$request->setActionName('index');
			}	
		}
		
		$this->_init($request);
		
		// if the current user role is not allowed to do something
		$resource = $this->_controller;
		if ($this->_module != "default" && $this->_module != "index")
		$resource = strtolower($this->_module) . '_' . $this->_controller;
		
		if (! $this->_acl->isAllowed($this->_currentRole, $resource,
		$this->_action)) 
		{
			$request->setModuleName('default');
			$request->setControllerName('error');
			$request->setActionName('na');
		} 
		else
		{

			$this->_courseid = $request->getParam('courseid',0);
			//echo $this->_courseid;
			if ($this->_courseid)
			{
				//$storage = new Zend_Auth_Storage_Session ();
				//$data = $storage->read ();
				
				$data = $this->_auth;

				if ($this->_currentRole=="facilitator"){

					$grader = new Users_Model_Users ();

					$role=$grader->checkgraderpriviledge($data->username,$this->_courseid);
					
					if($role){
						$canaccess='Yes';
					} else {
						$canaccess='No';
					}
				} 
				else if ($this->_currentRole=="premium")
				{

					$coordinator = new Users_Model_Users ();

					$role=$coordinator->checkcoordinatorpriviledge($data->username,$this->_courseid);
					if($role){
						$canaccess='Yes';
					} else {
						$canaccess='No';
					}
				}
				else if ($this->_currentRole=="administrator")
				{
					$canaccess='Yes';
				}
				if ($canaccess=='No')
				{
					//if ($this->_action=="modify" || $this->_action=="upload" || $this->_action=="delete"){
					$request->setModuleName('default');
					$request->setControllerName('error');
					$request->setActionName('na');
					//}
				}


			}
		}
	}
	protected function _init ($request)
	{
		$this->_action = $request->getActionName();
		$this->_module = $request->getModuleName();
		//echo  $this->_module;
		$this->_controller = $request->getControllerName();
		//echo  $this->_controller;
		$this->_currentRole = $this->_getCurrentUserRole();
		//echo $this->_courseid = $request->getParam('courseid',0);


	}
	protected function _getCurrentUserRole ()
	{
		if ($this->_auth) {
			$authData = $this->_auth;
			//echo $authData->privilage;
			//exit();
			if ($authData->privilage == 'STUDENT')
			$role = 'basic';
			if ($authData->privilage == 'ADMIN')
			$role = 'administrator';
			if ($authData->privilage == 'COURSE COORDINATOR')
			$role = 'premium';
			if ($authData->privilage == 'EOS')
			$role = 'facilitator';
		} else {
			$role = 'guest';
		}
		return $role;
	}
	public function navi ($module, $controller, $action)
	{
		$this->_currentRole = $this->_getCurrentUserRole();
		// if the current user role is not allowed to do something
		$resource = $controller;
		if ($module != "default" && $module != "index")
		$resource = strtolower($module) . '_' . $controller;
		if (! $this->_acl->isAllowed($this->_currentRole, $resource, $action)) {
			return 0;
		} else
		if ($this->_acl->isAllowed($this->_currentRole, $resource, $action)) {
			return 1;
		}
	}

	public function navicourse ($module, $controller, $action, $username, $courseid)
	{
		/*echo $module;
		echo $controller;
		echo $action;
		echo $username;
		echo $courseid;*/
		
		$this->_currentRole = $this->_getCurrentUserRole();
		
		// if the current user role is not allowed to do something
		$resource = $controller;
		if ($module != "default" && $module != "index")
		$resource = strtolower($module) . '_' . $controller;
		if (! $this->_acl->isAllowed($this->_currentRole, $resource, $action)) {
			return 0;
		} else
		if ($this->_acl->isAllowed($this->_currentRole, $resource, $action)) {
			

			if ($this->_currentRole=="facilitator"){

				$grader = new Users_Model_Users ();

				$role=$grader->checkgraderpriviledge($username,$courseid);
				
				if($role){
					$canaccess='Yes';
				} else {
					$canaccess='No';
				}
			} else if ($this->_currentRole=="premium"){

				$coordinator = new Users_Model_Users ();

				$role=$coordinator->checkcoordinatorpriviledge($username,$courseid);
				if($role){
					$canaccess='Yes';
				} else {
					$canaccess='No';
				}
			} else if ($this->_currentRole=="administrator"){
				$canaccess='Yes';
			}
			if ($canaccess=='Yes'){
				return 1;
			}
			else {
				return 0;
			}

		}
	}
}
?>
