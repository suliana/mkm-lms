signup = 
{ 
	check : function (url, target, text, to)
	{
		var html = $.ajax({
						   type: "GET",
						   url: url,
						   async: false,
						   global: true
						 }).responseText;
		
		if ( html != '<span class="green"><span class="icon icon-check"></span></span>' )
		{
			$('#'+to).addClass('wrong');
		}
		else
		{
			$('#'+to).removeClass('wrong');
		}

		$('#'+target).html(html);
	},
	pass1 : function () 
	{
		var pw1 = $('#pw1').val();

		if (pw1.length < 6 || pw1.length > 25) 
		{
			if (pw1.length < 6)
			{
				signup.error('pw1','Password must be at least 6 characters');
			}
			else
			{
				signup.error('pw1','Password cannot be longer than 25 characters');
			}
		} 
		else
		{
			$('#pw1check').html('<span class="green"><span class="icon icon-check"></span></span>');
		}		
	},
	pass2 : function ()
	{
		var pw2 = $('#pw2').val();
		
		if (pw2.length < 6 || pw2.length > 25) 
		{
			if (pw2.length < 6) 
			{
				signup.error('pw2','Password must be at least 6 characters');
			} 
			else 
			{
				signup.error('pw2','Password cannot be longer than 25 characters');
			}
		}
		else 
		{
			if ( $("#pw1").val() != $("#pw2").val() )
			{
				signup.error('pw2','Passwords do not match.');
			} 
			else 
			{
				$('#pw2check').html('<span class="green"><span class="icon icon-check"></span></span>');
			}
		}
	},
	error : function(to,text)
	{
		$("#"+to+'check').html('<span class="red">'+text+'</span>');
		$("#"+to).addClass('wrong');
	},
	checkform : function()
	{
		var errors = $("#registerpage form .red").length;
		
		if ( errors > 0 )
		{
			alert('There are still errors, please fix them before submitting.');
			return false;
		}
		
		return true;
	},
	username : function ()
	{
		var val = $("#username").val();

		if (val.length < 3 || val.length > 25) 
		{
			if (val.length < 3) 
			{
				signup.error('username','Username must be at least 3 characters');
			}
			else
			{
				signup.error('username','Username cannot be longer than 25 characters');
			}
		}
		else
		{
			signup.check(_path + '/index/register/ajax/username/value/'+val,'usernamecheck','Checking..','username');
		}
	},
	icno : function ()
	{
		var val = $("#icno").val();

		if (val == '' ) 
		{
			signup.error('icno','You must enter your valid IC number');
		}
		else
		{
			signup.check(_path + '/index/register/ajax/icno/value/'+val,'icnocheck','Checking..','icno');
		}
	},
	check_email : function check_email(e)
	{
		ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";
		for(i=0; i < e.length ;i++){ if(ok.indexOf(e.charAt(i))<0){  return (false); } }   if (document.images) { re = /(@.*@)|(\.\.)|(^\.)|(^@)|(@$)|(\.$)|(@\.)/; re_two = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/; if (!e.match(re) && e.match(re_two)) { return (-1); }   }
	},
	email : function () 
	{
		var val = $("#email").val();

		if (val.length < 6 || val.length > 49) 
		{
			if (val.length < 6) 
			{
				signup.error('email','Email is too short');
			}
			else
			{
				signup.error('email','Email cannot be longer than 50 characters');
			}
		}
		else
		{
			if (signup.check_email(val)) 
			{
				signup.check(_path+'/index/register/ajax/email/value/'+val,'emailcheck','Checking..','email');
			} 
			else
			{
				signup.error('email','Invalid Email');
			}
		}
	},
	pwmatch : function ()
	{
		var pw2 = $("#pw2").val();
		if ( pw2.length == 0)
		{
			return;
		}

		if ( $("#pw1").val() != pw2 )
		{
			$("#pw2check").html('<span class="red">Passwords do not match</span>');
			$("#pw1").addClass("wrong");
			$("#pw2").addClass("wrong");
		} 
		else
		{
			$('#pw2check').html('<span class="green"><span class="icon icon-check"></span></span>');
		}
	}
}