$(document).ready(function()
{
	$("#announcements a").fancybox({minWidth:800});
});


function sure(msg)
{
	if (msg == undefined)
	{
		var msg = 'Are you sure?';
	}

	var ask = confirm(msg);
	if (ask == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
