<?php
//PHP 5
require_once ("/data/lms/htdocs/lib/adodb/adodb.inc.php");


function my_connect(){
	$conn = NewADOConnection('mysql');
	$conn->PConnect('localhost', 'root', 'admin123','epic');

	return $conn;
}

function ora_connect(){


	$cstr = "(DESCRIPTION =
			(FAILOVER = ON)
		    (ADDRESS = (PROTOCOL = TCP)(HOST = mycampusdb.oum.edu.my)(PORT = 1521))
		    (ADDRESS = (PROTOCOL = TCP)(HOST = mycampusdb1.oum.edu.my)(PORT = 1521))
		    (LOAD_BALANCE = yes)
		    (CONNECT_DATA =
		      (SERVER = DEDICATED)
		      (SERVICE_NAME = icems)
		      (FAILOVER_MODE =
		        (TYPE = SELECT)
		        (METHOD = BASIC)
		        (RETRIES = 180)
		        (DELAY = 5)
		      )
		    )
		  )";

	$conn = NewADOConnection('oci8');

	$conn->Connect($cstr,'icem_mmls','icem_mmls');
	return $conn;

}


$cronjobstat=2;//for auto insert

$conn=my_connect();
//get intake id & semesterid
$sqlintake="SELECT id,intakecode FROM  intake WHERE intakestatus='CURRENT'";
$rsintake=$conn->Execute($sqlintake);
$intakeid=$rsintake->fields["id"];
$semester=$rsintake->fields["intakecode"];

$date=date('Y-m-d');
$taggedby="system";

//get all courses
//$sql_coursemain="select * from coursemain where courseid='HBEF2703'";
$sql_coursemain="select * from coursemain";
$rs_coursemain=$conn->Execute($sql_coursemain);

while (!$rs_coursemain->EOF) {
	$courseid=$rs_coursemain->fields["courseid"];

	echo $courseid."<br>";
	$sql_delete_tagginguser="delete from tagginguser where courseid='$courseid' and intakeid=$intakeid and status=$cronjobstat";
	$rs_delete_tagginguser=$conn->Execute($sql_delete_tagginguser);

	$sql_delete_coursefacilitator="delete from coursefacilitator where courseid='$courseid' and intakeid=$intakeid and status=$cronjobstat";
	$rs_delete_coursefacilitator=$conn->Execute($sql_delete_coursefacilitator);

	$sql_delete_coursestudent="delete from coursestudent where courseid='$courseid' and intakeid=$intakeid and status=$cronjobstat";
	$rs_delete_coursestudent=$conn->Execute($sql_delete_coursestudent);


     $ora_db=ora_connect();
	//get facilitators
	$sql="SELECT VTGE_GRADER_NAME, VTGE_GRADER_UNAME, VTGE_GRADER_IC, VTGE_GRADER_ID FROM VIEW_TUTOR_GRADER_EPIC where VTGE_GRADER_SUBJECT='$courseid' AND VTGE_SEM='$semester' AND VTGE_GRADER_UNAME is not null";
	$rsgrader=$ora_db->Execute($sql);
	$ora_db->close();

	foreach ($rsgrader as $g){
		$fullname=$g["VTGE_GRADER_NAME"];
		$guname=$g["VTGE_GRADER_UNAME"];
		$gic=$g["VTGE_GRADER_IC"];
		$VTGE_GRADER_ID=$g["VTGE_GRADER_ID"];

		$conn=my_connect();
		//check faci's existance in tagging setup
		$sql_check="select * from taggingsetup where courseid='$courseid' and intakeid=$intakeid and tutorname='$guname' and tutoric='$gic'";
		$rs_check=$conn->Execute($sql_check);

		if ($rs_check->fields["id"]!=""){
			$id=$rs_check->fields["id"];
		}else{
			//insert new facilitators
			$sql_insertinto_setup="INSERT INTO taggingsetup (courseid,fullname,tutorname,tutoric,datecreated,intakeid,taggedby, status) VALUES ('$courseid','".mysql_real_escape_string($fullname)."','$guname','$gic','$date',$intakeid,'$taggedby',$cronjobstat)";
			$rs_insertinto_setup=$conn->Execute($sql_insertinto_setup);
			$id=$conn->Insert_ID();
		}

		$sql_insertFACIinto_taguser="INSERT INTO tagginguser (groupID,userID,courseid,intakeid,status) VALUES ('$id','$guname','$courseid','$intakeid','$cronjobstat')";
		$rs_insertFASIinto_taguser=$conn->Execute($sql_insertFACIinto_taguser);

		$sql_insert_coursefacilitator="INSERT INTO coursefacilitator (username, courseid, intakeid, status) VALUES ('$guname','$courseid','$intakeid','$cronjobstat')";
		$rs_insert_coursefacilitator=$conn->Execute($sql_insert_coursefacilitator);

		$selectusersgrader="select * from users where username='$guname'";
		$rsusersgrader=$conn->Execute($selectusersgrader);

		if ($rsusersgrader->fields["username"]==""){
			$passwordg=md5($guname);
			$sql_insert_users_eos="INSERT INTO users (firstname,studentID,STUD_ID,username,password,privilage) VALUES ('".mysql_real_escape_string($fullname)."','$gic','$VTGE_GRADER_ID','$guname','$passwordg','EOS')";
			$rs_insert_users_eos=$conn->Execute($sql_insert_users_eos);
		}
		else {
			if ($rsusersgrader->fields["STUD_ID"]==null)
			$sql_update_users_eos="UPDATE users set STUD_ID='$VTGE_GRADER_ID' where username='".$rsusersgrader->fields["username"]."'";
			$conn->Execute($sql_update_users_eos);
			
			
		}
	}


	$ora_db=ora_connect();
	//get students in course and intake/semester
	//$sqlstudent="select * from view_student_grader_epic where VSGE_GRADER_SUBJECT='$courseid' and VSGE_SEM='$semester' AND VSGE_GRADER_UNAME is not null";
	$sqlstudent="select view_student_grader_epic.VSGE_GRADER_UNAME, view_student_grader_epic.VSGE_STUD_UNAME, view_student_grader_epic.VSGE_STUD_IC, view_student_grader_epic.VSGE_STUD_ID, view_student_grader_epic.VSGE_STUD_NAME
	from view_student_grader_epic
	inner join VIEW_TUTOR_GRADER_EPIC
	on view_student_grader_epic.VSGE_GRADER_SUBJECT=VIEW_TUTOR_GRADER_EPIC.VTGE_GRADER_SUBJECT
	and view_student_grader_epic.VSGE_SEM=VIEW_TUTOR_GRADER_EPIC.VTGE_SEM
	and view_student_grader_epic.VSGE_GRADER_IC=VIEW_TUTOR_GRADER_EPIC.VTGE_GRADER_IC
	and view_student_grader_epic.VSGE_GRADER_UNAME=VIEW_TUTOR_GRADER_EPIC.VTGE_GRADER_UNAME
	and view_student_grader_epic.VSGE_GRADER_SUBJECT='$courseid'
	AND view_student_grader_epic.VSGE_SEM='$semester'
	AND view_student_grader_epic.VSGE_GRADER_UNAME is not null";
	$rsstudent=$ora_db->Execute($sqlstudent);
	$ora_db->close();

	foreach ($rsstudent as $stud){
		$graderid=$stud["VSGE_GRADER_UNAME"];
		$studentid=$stud["VSGE_STUD_UNAME"];
		$studentIC=$stud["VSGE_STUD_IC"];
		$VSGE_STUD_ID=$stud["VSGE_STUD_ID"];
		$studentname=$stud["VSGE_STUD_NAME"];
		
		$conn=my_connect();
		$selecttaggingsetup="select * from taggingsetup where courseid='$courseid' and intakeid='$intakeid' and tutorname='$graderid' and status=$cronjobstat";
		$rstaggingsetup=$conn->Execute($selecttaggingsetup);

		$groupID=$rstaggingsetup->fields["id"];

		$sql_insertinto_taguser="INSERT INTO tagginguser (groupID,userID,courseid,intakeid,status) VALUES ('$groupID','$studentid','$courseid','$intakeid','$cronjobstat')";
		$rs_insertinto_taguser=$conn->Execute($sql_insertinto_taguser);

		$sql_insert_coursestudent="INSERT INTO coursestudent (username,courseid,intakeid,status) VALUES ('$studentid','$courseid','$intakeid','$cronjobstat')";
		$rs_insert_coursestudent=$conn->Execute($sql_insert_coursestudent);

		$selectusersstud="select * from users where username='$studentid'";
		$rsusersstud=$conn->Execute($selectusersstud);

		if ($rsusersstud->fields["username"]==""){
			$password=md5($studentid);
			$sql_insert_users_student="INSERT INTO users (firstname,studentID,STUD_ID,username,password,privilage) VALUES ('".mysql_real_escape_string($studentname)."','$studentIC','$VSGE_STUD_ID','$studentid','$password','STUDENT')";
			if ( $studentid=="chophisear"){
				echo $sql_insert_users_student;
			}
			$rs_insert_users_student=$conn->Execute($sql_insert_users_student);
		} else {
			if ($rsusersstud->fields["STUD_ID"]==null)
			$sql_update_users_student="UPDATE users set STUD_ID='$VSGE_STUD_ID' where username='".$rsusersstud->fields["username"]."'";
			$conn->Execute($sql_update_users_student);
			
			
		}

	}

	//exit();

	$rs_coursemain->movenext();
}

?>