Rad Upload Plus 4.12
====================

Drag and Drop File upload applet.


Install.txt is the guide to installation and docs/index.html file
describes the configuration parameters that can be passed to the
applet.

More information and online documentation is available at
http://www.radinks.com/upload/docs.php



