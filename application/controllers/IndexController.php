<?php
ini_set("soap.wsdl_cache_enabled", "0");
class IndexController extends Zend_Controller_Action
{
	public function init ()
	{
		/* Initialize action controller here */
		
		// get courseinfo
		//$courseinfo = Application_Model_Course::fetch($id);
	}
	
	public function indexAction ()
	{
		
		$this->_helper->layout->setLayout('login');
		
		$request = $this->getRequest();
		$form = new Application_Form_Index( array('redir' => $request->getRequestUri() ));
		
		
		//$db = $this->_getParam('db');
		$db = Zend_Registry::get('db');
		
		if ($this->getRequest()->isPost()) 
		{
			if ($form->isValid($request->getPost())) 
			{
				$username = $form->getValue('username');
				$password = $form->getValue('password');

				//nie kalo create manual
				$adapter = new Zend_Auth_Adapter_DbTable($db,'users', 'username', 'password');
				$adapter->setIdentity($form->getValue('username'));
				$adapter->setCredential(md5($form->getValue('password')));
				
				//$auth = Zend_Auth::getInstance();
				$auth = Zend_Registry::get('auth');
				
				$result = $auth->authenticate($adapter);
				
				if ($result->isValid()) 
				{
					//$storage = new Zend_Auth_Storage_Session();
					//$storage->write($adapter->getResultRowObject());
					
					$auth->getStorage()->write($adapter->getResultRowObject());
						
					$storage2 = new Zend_Auth_Storage_Session("myvleemail");
					$email= new stdClass();
					$email->passwd=base64_encode($form->getValue('password'));
					$storage2->write($email);
					
					//$data = $storage2->read();
					//print_r($data);
					
					//$auth->getStorage()->write($adapter->getResultRowObject());

					$user = new Users_Model_Users();
					$rs = $user->checkusername($username);
				
					if ($rs) 
					{
						$data["last_login"]=date("Y-m-d H:i:s");
						$user->modify($data,$rs['id']);
						
						include_once("browserdetection.php");
						
						$sdata['userID'] = $username;
						$sdata['userIP'] = $_SERVER['REMOTE_ADDR'];
						$sdata['userBrowser'] = browser_detection( 'browser_working' )." ".browser_detection( 'browser_number' );
						$sdata['userOS'] = browser_detection( 'os' )." ".browser_detection( 'os_number' );
						$sdata['userlogin'] = $data["last_login"];
						$user->tracklogin($sdata);

						$announcement = new Application_Model_Announcement();
						$rscourse_announcement=$announcement->listannouncement();
						$this->view->announcement = $rscourse_announcement;


						//if coordinator
						if ($rs['privilage']=="COURSE COORDINATOR")
						{
							//delete course eos kalo ada
							$grader = new Course_Model_Grader ();
							$grader->delete($username);

							//delete course student kalo ada
							$student = new Course_Model_Student ();
							$student->delete($username);
						}
						
						//end coordinator assign subject
						//if eos
						if ($rs['privilage']=="EOS")
						{
							//delete course coordinator kalo ada
							$coordinator = new Course_Model_Coordinator ();
							$coordinator->delete($username);

							//delete course student kalo ada
							$student = new Course_Model_Student ();
							$student->delete($username);
						}
						
						//end eos assign subject
						//if student
						if ($rs['privilage']=="STUDENT")
						{
							//delete course coordinator kalo ada
							$coordinator = new Course_Model_Coordinator ();
							$coordinator->delete($username);

							//delete course eos kalo ada
							$grader = new Course_Model_Grader ();
							$grader->delete($username);
						}
						
						//end student assign subject
					}

					//$this->_redirect(str_replace('/lms/public','',$form->getValue('redirect')));
					$this->_redirect('index/home');
					//
				} 
				else
				{
					$this->view->errorMessage = $this->view->translate('login_error');
				}
				
			}
		}
		
		
		$this->view->form = $form;
		$announcement = new Application_Model_Announcement();
		$rscourse_announcement=$announcement->listannouncement();
		$this->view->announcement = $rscourse_announcement;	
	}
	
	/*
	 * Register Page
	 */
	public function registerAction()
	{
		// if already logged in, why bother
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if ($data)
		{
			$this->_redirect('index/home');
		}
		
		if($this->getRequest()->isXmlHttpRequest())
	  	{
		    //Disable the view/layout
		    $this->_helper->layout->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(TRUE);
		
		    $ajax = $this->_getParam('ajax');
		    $value = $this->_getParam('value');
			
		    if ( $ajax != '' )
		    {
			    $model = new Application_Model_Register();
			    $return = '';
			    
			    switch( $ajax )
			    {
			    	case "username":
			    		$result = $model->checkUsername($value);
			    		$return =  is_array($result) ? '<span class="red">Username already taken</span>' : '<span class="green"><span class="icon icon-check"></span></span>';
			    	break;
			    	
			    	case "email":
			    		$result = $model->checkEmail($value);
			    		$return =  is_array($result) ? '<span class="red">Email already taken</span>' : '<span class="green"><span class="icon icon-check"></span></span>';
			    	break;
			    	
			    	case "icno":
			    		$result = $model->checkICNo($value);
			    		$return =  is_array($result) ? '<span class="red">IC No already in use</span>' : '<span class="green"><span class="icon icon-check"></span></span>';
			    	break;
			    	
			    }
			   	
			    $this->getResponse()->appendBody( $return );
		    }
		}
		else
		{
			// other stuff
			$this->_helper->layout->setLayout('login');
			
			$request = $this->getRequest();
			$formLogin = new Application_Form_Register();
			$this->view->formLogin = $formLogin;
			
			//process registration
			if ($this->getRequest()->isPost()) 
			{
				if ($formLogin->isValid($request->getPost())) 
				{
					$data = $this->getRequest()->getPost();
					
					$user = new Users_Model_Users();
					$check_user = $user->fetch($data['username'], 'username');
					$check_email = $user->fetch($data['email'], 'email');
					
					if ( is_array($check_user) )
					{
						$this->view->errorMessage = $this->view->translate('Username already exists');
						return;
					}
					
					if ( is_array($check_email) )
					{
						$this->view->errorMessage = $this->view->translate('Email already exists');
						return;
					}
			
					//check for illegal characters
					if ( preg_match("/[^a-zA-Z0-9]+/",$data['username']) )
					{
						$this->view->errorMessage = $this->view->translate('Username contains illegal characters. Try again. Only <span class="help" title="characters containing letters and digits.">alphanumeric</span> characters are allowed.');
						return;
					}
					
					if ( ( strlen($data['username']) < 3 || strlen($data['username'] > 25) )) 
					{
						if ( strlen($_POST['username']) < 3 )
						{
							$this->view->errorMessage = $this->view->translate('Username is too short. You need to have atleast 3 characters.');
							return;
						}
						else
						{
							$this->view->errorMessage = $this->view->translate('Username is too long. Your username cannot be longer than 25 characters.');
							return;
						}
					}
					
					//check length
					if ( strlen($data['pw1']) < 6 )
					{
						$this->view->errorMessage = $this->view->translate('Password must be atleast 6 characters.');
					}
						

					//all good?
					$userdata = array(
										'email'			=> 	$data['email'],
										'username'		=>	$data['username'],
										'password'		=> md5($data['pw1']),
										'privilage'		=> 'STUDENT',
										'datecreated' 	=> new Zend_Db_Expr('NOW()'),
										'firstname'		=> $data['firstname'],
										'lastname'		=> $data['lastname'],
										'icno'			=> $data['icno']
								);
				
					$user->upload( $userdata );
					
					//nie kalo create manual
					$adapter = new Zend_Auth_Adapter_DbTable(Zend_Registry::get('db'),'users', 'username', 'password');
					$adapter->setIdentity($data['username']);
					$adapter->setCredential(md5($data['pw1']));
					
					$auth = Zend_Registry::get('auth');
					
					$result = $auth->authenticate($adapter);
					
					if ($result->isValid()) 
					{	
						$auth->getStorage()->write($adapter->getResultRowObject());
					}
					
					$this->_redirect('index/home/?new=1');
				} // isValid
			}
			
			
		}
	}
	
	/*
	 * First Time Login
	 */
	public function firstloginAction ()
	{
		// if already logged in, why bother
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if ($data)
		{
			$this->_redirect('index/home');
		}
		
		//ajax stuff
		if($this->getRequest()->isXmlHttpRequest())
	  	{
		    //Disable the view/layout
		    $this->_helper->layout->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(TRUE);
		
		    $id = $this->_getParam('id');
		
		    $model = new Application_Model_Firstlogin();
		    $result = $model->checkStudent($id);
			
		   	$return = is_array($result) ? '1' : '0';
		   	
		    $this->getResponse()->appendBody( $return );
		}
		else
		{
			// other stuff
			$this->_helper->layout->setLayout('login');
			
			$request = $this->getRequest();
			$form = new Application_Form_Firstlogin();
			$this->view->form = $form;
			
			//$db = $this->_getParam('db');
			
			if ($this->getRequest()->isPost()) 
			{
				if ($form->isValid($request->getPost())) 
				{
					$data = $this->getRequest()->getPost();
					
					$user = new Users_Model_Users();
					
					$check = $user->fetch($data['student_id'], 'studentID');

					
					// username already set
					if ( $check['username'] != '' )
					{
						$this->view->errorMessage = $this->view->translate('firstlogin_already_set');
						return;
					}
					else
					{
						//username
						$check_user = $user->fetch($data['username'], 'username');
						if ( is_array($check_user) )
						{
							$this->view->errorMessage = $this->view->translate('firstlogin_username_exists');
							return;
						}
						
						//email
						$check_email = $user->fetch($data['email'], 'email');
						if ( is_array($check_email) )
						{
							$this->view->errorMessage = $this->view->translate('firstlogin_email_exists');
							return;
						}
						
						//all good?
						$newdata = array(
											'email'		=> 	$data['email'],
											'username'	=>	$data['username'],
											'password'	=> md5($data['password'])					
									);
						
						echo 'destination reached';
						//$user->modify( $newdata, array('studentID = ?' => $check['id']) );
						
						
						//all done
						$this->_redirect('index/home');
						
						
					} //user check
					
					
					
				} // valid post
			}
			
			
			
			$announcement = new Application_Model_Announcement();
			$rscourse_announcement=$announcement->listannouncement();
			$this->view->announcement = $rscourse_announcement;	
		}
	}

	public function logoutAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();

		$logindate=$data->last_login;
		$username=$data->username;

		$user = new Users_Model_Users();
		$sdata["userlogout"]=date("Y-m-d H:i:s");
		$user->modifytracklogin($sdata,$username,$logindate);

		$storage->clear();


		$this->_redirect('index/index');
	}
	
	/*
	 * Home
	 */
	public function homeAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		
		//print_R($_SESSION);
		$this->view->page_title = $this->view->translate('home');
		$this->view->title = $this->view->translate('e_learning')." > ". $this->view->translate('home'). " >";		
		$this->view->username=$data->username;
		$this->view->lastlogin=$data->last_login;
		
		$this->view->new = $this->_getParam('new');
		
		$course = new Application_Model_Course ();
		
		if ($this->_request->getParam('keywords') || $this->getRequest()->isPost())
		{
//			echo "if";
			$keywords = $this->_request->getParam('keywords',"");
			$this->view->keywords = strip_tags(trim($keywords));
			
			$rscourse = $course->searchcourse($keywords);
		}
		else
		{
//			echo "else";
			//$rscourse = $course->listcourse();
			$rscourse = $course->myallcourse();
		} 
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $rscourse );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keywords'=>$keywords);
	}

	public function coursepageAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$this->view->privilage=$data->privilage;

		$this->view->username=$data->username;


		//echo PUBLIC_PATH;
		//exit();
		$id = $this->_getParam('id', 0);



		//check grader ker
		// Kalau sama ngan grader gi page grader
		if($data->privilage=="EOS"){
			$this->_redirect('index/graderpage/id/'.$id);
		}
		//

		$course = new Application_Model_Course();
		$rscourse=$course->listcourse($id);

		$this->view->course = $rscourse;

		$intakeid = "";
		//tambahan for student
		if ($data->privilage=="STUDENT"){
			$assignsubject= new Course_Model_Student();
			$rs=$assignsubject->fetch($data->username,$id);
			if ($rs)
			$intakeid=$rs["intakeid"];
			else
			$intakeid='0';
			//echo $intakeid;
			//exit();

			$this->view->semid = $intakeid;
		}
		//end tambahan



		$sample = new Sample_Model_Sample ();
		$rssample=$sample->sampledownload($id,$intakeid);

		$this->view->sampledownload = $rssample;



	}

	public function movieAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}

	public function menuAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$this->view->act = $this->_getParam('act', 0);

		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();

		$announcement = new Application_Model_Announcement();
		$rscourse_announcement=$announcement->listannouncement();

		//echo $data->username;

		$this->view->username=$data->username;
		$this->view->lastlogin=$data->last_login;
		$this->view->course = $rscourse;
		$this->view->announcement = $rscourse_announcement;
		$privilage = $data->privilage;
		$this->view->privilage=$privilage;
		//		echo $rscourse_announcement;
	}

	public function smecontentAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}
	public function activitiesAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}
	public function assignmentAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}
	public function resourcesAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}

	public function graderpageAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$this->view->privilage=$data->privilage;
		$this->view->username=$data->username;

		$id = $this->_getParam('id', 0);


		//check assign subjek
		//jika semua expired tak boleh masuk page
		//jika salah satu expired boleh masuk page
		//jika semua start date dan end date sama dengan "0000-00-00 00:00:00" tak boleh masuk page

		//Kesimpulan kalo ada yang ok boleh masuk
		$assignsubject= new Course_Model_Grader();
		$rs=$assignsubject->fetchsemua($data->username,$id);
		$today=date("Y-m-d H:i:s");

		$coursedetails = new Admin_Model_Course();

		$access="not ok";
		foreach($rs as $row){

			$rsdetails=$coursedetails->checkdetails($row['courseid'],$row['intakeid']);
			/*echo $rsdetails[0]['start_date'];
			echo $rsdetails[0]['end_date'];
			echo $today;*/
			if ($today>=$rsdetails[0]['start_date'] && $today<=$rsdetails[0]['end_date']){

				$access="ok";
			}
		}
		/*echo $access;
		exit();*/
		$this->view->access = $access;

		$course = new Application_Model_Course();
		$rscourse=$course->listcourse($id);

		$this->view->course = $rscourse;


	}

	public function feedbackpageAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$username=$data->username;
		$firstname=$data->firstname;
		$telephonenumber=$data->telephone_number;

		$this->view->username = $username;

		$form = new Application_Form_Feedback();

		$this->view->form = $form;

		if ($this->getRequest ()->isPost ()) {
			if ($form->isValid ( $_POST )) {
				$data2 = $form->getValues ();
				$config = array(
				'ssl' => 'ssl',
				'port' => 465,
				'auth' => 'login',
				'username' => 'assignment_admin@oum.edu.my',
				'password' => 'admin123' );


				$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

				// Set From & Reply-To address and name for all emails to send.
				Zend_Mail::setDefaultFrom($username.'@'.MAIL_DOMAIN, $firstname);
				Zend_Mail::setDefaultReplyTo($username.'@'.MAIL_DOMAIN,$firstname);



				$mail = new Zend_Mail();
				$mail->setBodyHtml("Fullname : ".$firstname."<br>Email : ".$username."@oum.edu.my<br> Telephone Number : ".$telephonenumber."<br><br>".$data2['message']);


				//$mail->setFrom($username.'@'.MAIL_DOMAIN, $firstname);
				$mail->addTo('epic_feedback@oum.edu.my');
				$mail->setSubject($data2['subject']);
				//$mail->setReturnPath($username.'@'.MAIL_DOMAIN);
				//$mail->getFrom($username.'@'.MAIL_DOMAIN);
				//$mail->setReplyTo($username.'@'.MAIL_DOMAIN);
				//$mail->send($transport);

				if(!$mail->send($transport)) {
					?>
				    <script>
				    alert("Error Sent Message");
					</script>
					<?php

				} else {
					?>
				    <script>
				    alert("Message has been sent");
					</script>
					<?php


				}

				// Reset defaults
				Zend_Mail::clearDefaultFrom();
				Zend_Mail::clearDefaultReplyTo();


				/*include_once(APPLICATION_PATH . "/../public"."/PHPMailer/class.phpmailer.php");
				include_once(APPLICATION_PATH . "/../public"."/PHPMailer/class.smtp.php");

				$mail=new PHPMailer();

				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
				$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 465;                   // set the SMTP port


				$mail->Username   = "assignment_admin@oum.edu.my";  // GMAIL username
				$mail->Password   = "admin123";           // GMAIL password

				$mail->From       = $username.'@'.MAIL_DOMAIN;
				$mail->FromName   = $firstname;

				$mail->Subject    = $data2['subject'];


				$mail->Body       = "Fullname : ".$firstname."<br>Email : ".$username."@oum.edu.my<br> Telephone Number : ".$telephonenumber."<br><br>".$data2['message'];//HTML Body


				$mail->WordWrap   = 5000; // set word wrap


				$mail->AddAddress("epic_feedback@oum.edu.my","Epic Feedback");
				$mail->AddReplyTo($username.'@'.MAIL_DOMAIN,$firstname);



				$mail->IsHTML(true); // send as HTML

				if(!$mail->Send()) {
				?>
				<script>
				alert("Mailer Error: <?php echo $mail->ErrorInfo;?>");
				</script>
				<?php

				} else {
				?>
				<script>
				alert("Message has been sent");
				</script>
				<?php

				}*/

			}
		}

	}
	public function youtubeAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		
		$this->view->username=$data->username;
		$this->view->lastlogin=$data->last_login;
		
		$act = $this->_getParam ( 'act', 0 );
		$this->view->act=$act;
		$txtkeyword = $this->_getParam ( 'q', 0 );
		$this->view->txtkeyword=$txtkeyword;
	}


	public function embedyoutubeAction ()
	{
		$this->_helper->layout->disableLayout();
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$vidid = $this->_getParam ( 'vidID', 0 );
		$this->view->vidid=$vidid;
	}

	public function addreadlistAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$vidid = $this->_getParam ( 'vidID', 0 );

		$read = new Application_Model_Readinglist ();

		$datas["link"]=$vidid;
		$datas["type"]="youtube";
		$datas["username"]=$data->username;
		$datas["dateadd"]=date("Y-m-d H:i:s");

		$read->upload ( $datas );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/youtube/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/youtube/act/search/?q='.$q );

		} else {
			$this->_redirect ( 'index/embedyoutube/act/search/vidID/'.$vidid );
		}

	}
	
	public function removereadlistAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$vidid = $this->_getParam ( 'vidID', 0 );

		$read = new Application_Model_Readinglist ();

		$link=$vidid;
		$type="youtube";
		$username=$data->username;
		
		$read->delete ( $link, $type, $username );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/youtube/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/youtube/act/search/?q='.$q );

		} else {
			$this->_redirect ( 'index/embedyoutube/act/search/vidID/'.$vidid );
		}

	}
	
	public function googleAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		
		$this->view->username=$data->username;
		$this->view->lastlogin=$data->last_login;
		
		
		$act = $this->_getParam ( 'act', 0 );
		$this->view->act=$act;
		$txtkeyword = $this->_getParam ( 'q', 0 );
		$this->view->txtkeyword=$txtkeyword;
	}
	
	public function addgooglereadlistAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$link = urldecode($this->_getParam ( 'link', 0 ));

		$read = new Application_Model_Readinglist ();

		$datas["link"]=$link;
		$datas["type"]="google";
		$datas["username"]=$data->username;
		$datas["dateadd"]=date("Y-m-d H:i:s");

		$read->upload ( $datas );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/google/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/google/act/search/?q='.$q );

		} 

	}
	
	public function removegooglereadlistAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$link = urldecode($this->_getParam ( 'link', 0 ));

		$read = new Application_Model_Readinglist ();

		$type="google";
		$username=$data->username;
		
		$read->delete ( $link, $type, $username );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/google/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/google/act/search/?q='.$q );

		} 

	}
	
	public function libraryAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$ic = $data->studentID;
		$this->view->ic=$ic;
		$username = $data->username;
		$this->view->username=$username;
		
	}
	
	public function studentportalAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$ic = $data->studentID;
		$this->view->ic=$ic;
		$privilage = $data->privilage;
		$this->view->privilage=$privilage;
		
	}
	
	public function myvlegmailAction ()
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		
		$storage2 = new Zend_Auth_Storage_Session("myvleemail");
		$data2 = $storage2->read();
		
		$username = $data->username;
		$this->view->username=$username;
		$password = $data2->passwd;
		$this->view->password=$password;
		
	}
	
	public function myreadinglistAction() 
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$this->view->username = $data->username;
		$this->view->lastlogin=$data->last_login;
		
		$username = $data->username;
		$privilage=$data->privilage;

		$form = new Application_Form_Searchreadinglist ();
		
		$this->view->form = $form;
		
		
		$read = new Application_Model_Readinglist ();
		$select = $read->fetchAll ($username);
		
		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost())
		{
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost()){
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			//make sure the submitted data is valid and modify select statement
			if ($form->valid()){
				$data = $form->getValues ();
				$select = $read->fetchAll ( $username, $data ['keyword'] );
			} 
			
		} 
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'));

		
	}
	
	public function deletereadinglistAction() 
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$read = new Application_Model_Readinglist ();
		$id = $this->_getParam ( 'rowid', 0 );

		
		//echo $id;
		if ($id > 0)
		{
			$read->deleteid ( $id );
			$this->_redirect ( 'index/myreadinglist' );
		}
	}
}

