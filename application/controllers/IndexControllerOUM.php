<?php
ini_set("soap.wsdl_cache_enabled", "0");
class IndexController extends Zend_Controller_Action
{
	public function init ()
	{
		/* Initialize action controller here */
	}
	public function indexAction ()
	{
		
		$this->_helper->layout->setLayout('login');
		 
		$request = $this->getRequest();
		$form = new Application_Form_Index();
		$db = $this->_getParam('db');
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($request->getPost())) {
		
				//Nie kalau pakai soap
				$client = new Zend_Soap_Client(
				"http://lms.oum.edu.my/myvle/webservice/sso/ssowsdl.php?wsdl",
				array('soap_version' => SOAP_1_1));
				$username = $form->getValue('username');
				$password = $form->getValue('password');
				$result = $client->ssoauthorized($username, $password);
				
				
				if ($result == "Ok") {
					$user = new Users_Model_Users();
					$rs = $user->checkusername($username);
				
					if (! $rs) {
						$infodetail = $client->getuserInfo($username);
						$data['firstname'] = $infodetail->fullname;
						$data['intake'] = $infodetail->intake;
						$data['studentID'] = $infodetail->studentID;
						$data['ulccode'] = $infodetail->ulccode;
						$data['ulcname'] = $infodetail->ulcname;

						$data['username'] = $username;
						$data['password'] = md5($password);
						$data["datecreated"]=date("Y-m-d H:i:s");
						$user->upload($data);


						$rs2 = $user->checkusername($username);
						
						$announcement = new Application_Model_Announcement();
						$rscourse_announcement=$announcement->listannouncement();					
						$this->view->announcement = $rscourse_announcement;
						
						// assign subjek automatically
						//if coordinator
						if ($rs[0]['privilage']=="COURSE COORDINATOR"){

							//delete course eos kalo ada
							$grader = new Course_Model_Grader ();
							$grader->delete($username);

							//delete course student kalo ada
							$student = new Course_Model_Student ();
							$student->delete($username);

						}
						//end coordinator assign subject
						//if eos
						if ($rs[0]['privilage']=="EOS"){

							//delete course coordinator kalo ada
							$coordinator = new Course_Model_Coordinator ();
							$coordinator->delete($username);

							//delete course student kalo ada
							$student = new Course_Model_Student ();
							$student->delete($username);

						}
						//end facilitator assign subject
						//if student
						if ($rs2[0]['privilage']=="STUDENT"){

							//delete course coordinator kalo ada
							$coordinator = new Course_Model_Coordinator ();
							$coordinator->delete($username);

							//delete course eos kalo ada
							$grader = new Course_Model_Grader ();
							$grader->delete($username);


						}
						//end student assign subject


					} else {
						$infodetail = $client->getuserInfo($username);
						$data['firstname'] = $infodetail->fullname;
						$data['intake'] = $infodetail->intake;
						$data['studentID'] = $infodetail->studentID;
						$data['ulccode'] = $infodetail->ulccode;
						$data['ulcname'] = $infodetail->ulcname;

						$data['password'] = md5($password);
						$data["last_login"]=date("Y-m-d H:i:s");
						$user->modify($data,$rs[0]['id']);

						include_once("browserdetection.php");
						$sdata['userID'] = $username;
						$sdata['userIP'] = $_SERVER['REMOTE_ADDR'];
						$sdata['userBrowser'] = browser_detection( 'browser_working' )." ".browser_detection( 'browser_number' );
						$sdata['userOS'] = browser_detection( 'os' )." ".browser_detection( 'os_number' );
						$sdata['userlogin'] = $data["last_login"];
						$user->tracklogin($sdata);


						$announcement = new Application_Model_Announcement();
						$rscourse_announcement=$announcement->listannouncement();					
						$this->view->announcement = $rscourse_announcement;
						print_r($rscourse_announcement);

						//if coordinator
						if ($rs[0]['privilage']=="COURSE COORDINATOR"){

							//delete course eos kalo ada
							$grader = new Course_Model_Grader ();
							$grader->delete($username);

							//delete course student kalo ada
							$student = new Course_Model_Student ();
							$student->delete($username);

						}
						//end coordinator assign subject
						//if eos
						if ($rs[0]['privilage']=="EOS"){

							//delete course coordinator kalo ada
							$coordinator = new Course_Model_Coordinator ();
							$coordinator->delete($username);

							//delete course student kalo ada
							$student = new Course_Model_Student ();
							$student->delete($username);

						}
						//end eos assign subject
						//if student
						if ($rs[0]['privilage']=="STUDENT"){

							//delete course coordinator kalo ada
							$coordinator = new Course_Model_Coordinator ();
							$coordinator->delete($username);

							//delete course eos kalo ada
							$grader = new Course_Model_Grader ();
							$grader->delete($username);


						}
						//end student assign subject
					}
					/*$storage = new Zend_Auth_Storage_Session();*/
					$adapter = new Zend_Auth_Adapter_DbTable($db, 'users',
					'username', 'password');
					$adapter->setIdentity($username);
					$adapter->setCredential(md5($password));
					$auth = Zend_Auth::getInstance();
					$result = $auth->authenticate($adapter);
					if ($result->isValid()) {
						$storage = new Zend_Auth_Storage_Session();
						$storage->write($adapter->getResultRowObject());
						
						$storage2 = new Zend_Auth_Storage_Session("myvleemail");
						$email= new stdClass();
						$email->passwd=base64_encode($password);
						$storage2->write($email);
						
						//$data = $storage2->read();
						//print_r($data);
						
						if ($rs[0]['privilage']=="STUDENT"){
							if ($rs[0]['telephone_number']==null || $rs[0]['school_name']==null || $rs[0]['school_address1']==null || $rs[0]['school_postcode']==null || $rs[0]['school_city']==null || $rs[0]['school_state']==null || $rs[0]['school_number']==null){
								$this->_redirect('users/portfolio/submitdetails');
							}
						}

						//$this->_redirect('index/menu');
						$this->_redirect('index/home');
					}
					
				} else {

					//nie kalo create manual
					$adapter = new Zend_Auth_Adapter_DbTable($db,'users', 'username', 'password');
					$adapter->setIdentity($form->getValue('username'));
					$adapter->setCredential(md5($form->getValue('password')));
					$auth = Zend_Auth::getInstance();
					$result = $auth->authenticate($adapter);
					
					if ($result->isValid()) {
						$storage = new Zend_Auth_Storage_Session();
						$storage->write($adapter->getResultRowObject());
						
						$storage2 = new Zend_Auth_Storage_Session("myvleemail");
						$email= new stdClass();
						$email->passwd=base64_encode($form->getValue('password'));
						$storage2->write($email);
						
						//$data = $storage2->read();
						//print_r($data);
						
						//$auth->getStorage()->write($adapter->getResultRowObject());

						$user = new Users_Model_Users();
						$rs = $user->checkusername($username);
					
						if ($rs) {

							$data["last_login"]=date("Y-m-d H:i:s");
							$user->modify($data,$rs[0]['id']);
							include_once("browserdetection.php");
							
							$sdata['userID'] = $username;
							$sdata['userIP'] = $_SERVER['REMOTE_ADDR'];
							$sdata['userBrowser'] = browser_detection( 'browser_working' )." ".browser_detection( 'browser_number' );
							$sdata['userOS'] = browser_detection( 'os' )." ".browser_detection( 'os_number' );
							$sdata['userlogin'] = $data["last_login"];
							$user->tracklogin($sdata);

							$announcement = new Application_Model_Announcement();
							$rscourse_announcement=$announcement->listannouncement();
							$this->view->announcement = $rscourse_announcement;


							//if coordinator
							if ($rs[0]['privilage']=="COURSE COORDINATOR"){

								//delete course eos kalo ada
								$grader = new Course_Model_Grader ();
								$grader->delete($username);

								//delete course student kalo ada
								$student = new Course_Model_Student ();
								$student->delete($username);

							}
							//end coordinator assign subject
							//if eos
							if ($rs[0]['privilage']=="EOS"){

								//delete course coordinator kalo ada
								$coordinator = new Course_Model_Coordinator ();
								$coordinator->delete($username);

								//delete course student kalo ada
								$student = new Course_Model_Student ();
								$student->delete($username);

							}
							//end eos assign subject
							//if student
							if ($rs[0]['privilage']=="STUDENT"){

								//delete course coordinator kalo ada
								$coordinator = new Course_Model_Coordinator ();
								$coordinator->delete($username);

								//delete course eos kalo ada
								$grader = new Course_Model_Grader ();
								$grader->delete($username);

							}
							//end student assign subject
						}
						if ($rs[0]['privilage']=="STUDENT"){
							if ($rs[0]['telephone_number']==null || $rs[0]['school_name']==null || $rs[0]['school_address1']==null || $rs[0]['school_postcode']==null || $rs[0]['school_city']==null || $rs[0]['school_state']==null || $rs[0]['school_number']==null){
								$this->_redirect('users/portfolio/submitdetails');
							}
						}

						//$this->_redirect('index/menu');
						$this->_redirect('index/home');
					} else
						$this->view->errorMessage = "Invalid username or password. Please try again.";
				}
			}
		}
		$this->view->form = $form;
		$announcement = new Application_Model_Announcement();
		$rscourse_announcement=$announcement->listannouncement();
		$this->view->announcement = $rscourse_announcement;
	}

	public function logoutAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();

		$logindate=$data->last_login;
		$username=$data->username;

		$user = new Users_Model_Users();
		$sdata["userlogout"]=date("Y-m-d H:i:s");
		$user->modifytracklogin($sdata,$username,$logindate);

		$storage = new Zend_Auth_Storage_Session();
		$storage->clear();


		$this->_redirect('index/index');
	}
	
	public function homeAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		
		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();
		$this->view->course = $rscourse;
	}

	public function coursepageAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$this->view->privilage=$data->privilage;

		$this->view->username=$data->username;


		//echo PUBLIC_PATH;
		//exit();
		$id = $this->_getParam('id', 0);



		//check grader ker
		// Kalau sama ngan grader gi page grader
		if($data->privilage=="EOS"){
			$this->_redirect('index/graderpage/id/'.$id);
		}
		//

		$course = new Application_Model_Course();
		$rscourse=$course->listcourse($id);

		$this->view->course = $rscourse;

		$intakeid = "";
		//tambahan for student
		if ($data->privilage=="STUDENT"){
			$assignsubject= new Course_Model_Student();
			$rs=$assignsubject->fetch($data->username,$row['courseid']);
			if ($rs)
			$intakeid=$rs["intakeid"];
			else
			$intakeid='0';
			//echo $intakeid;
			//exit();

			$this->view->semid = $intakeid;
		}
		//end tambahan



		$sample = new Sample_Model_Sample ();
		$rssample=$sample->sampledownload($id,$intakeid);

		$this->view->sampledownload = $rssample;



	}

	public function movieAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}

	public function menuAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$this->view->act = $this->_getParam('act', 0);

		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();

		$announcement = new Application_Model_Announcement();
		$rscourse_announcement=$announcement->listannouncement();

		//echo $data->username;

		$this->view->username=$data->username;
		$this->view->lastlogin=$data->last_login;
		$this->view->course = $rscourse;
		$this->view->announcement = $rscourse_announcement;
		$privilage = $data->privilage;
		$this->view->privilage=$privilage;
		//		echo $rscourse_announcement;
	}

	public function smecontentAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}
	public function activitiesAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}
	public function assignmentAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}
	public function resourcesAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
	}

	public function graderpageAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$this->view->privilage=$data->privilage;
		$this->view->username=$data->username;

		$id = $this->_getParam('id', 0);


		//check assign subjek
		//jika semua expired tak boleh masuk page
		//jika salah satu expired boleh masuk page
		//jika semua start date dan end date sama dengan "0000-00-00 00:00:00" tak boleh masuk page

		//Kesimpulan kalo ada yang ok boleh masuk
		$assignsubject= new Course_Model_Grader();
		$rs=$assignsubject->fetchsemua($data->username,$id);
		$today=date("Y-m-d H:i:s");

		$coursedetails = new Admin_Model_Course();

		$access="not ok";
		foreach($rs as $row){

			$rsdetails=$coursedetails->checkdetails($row['courseid'],$row['intakeid']);
			/*echo $rsdetails[0]['start_date'];
			echo $rsdetails[0]['end_date'];
			echo $today;*/
			if ($today>=$rsdetails[0]['start_date'] && $today<=$rsdetails[0]['end_date']){

				$access="ok";
			}
		}
		/*echo $access;
		exit();*/
		$this->view->access = $access;

		$course = new Application_Model_Course();
		$rscourse=$course->listcourse($id);

		$this->view->course = $rscourse;


	}

	public function feedbackpageAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}

		$username=$data->username;
		$firstname=$data->firstname;
		$telephonenumber=$data->telephone_number;

		$this->view->username = $username;

		$form = new Application_Form_Feedback();

		$this->view->form = $form;

		if ($this->getRequest ()->isPost ()) {
			if ($form->isValid ( $_POST )) {
				$data2 = $form->getValues ();
				$config = array(
				'ssl' => 'ssl',
				'port' => 465,
				'auth' => 'login',
				'username' => 'assignment_admin@oum.edu.my',
				'password' => 'admin123' );


				$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

				// Set From & Reply-To address and name for all emails to send.
				Zend_Mail::setDefaultFrom($username.'@'.MAIL_DOMAIN, $firstname);
				Zend_Mail::setDefaultReplyTo($username.'@'.MAIL_DOMAIN,$firstname);



				$mail = new Zend_Mail();
				$mail->setBodyHtml("Fullname : ".$firstname."<br>Email : ".$username."@oum.edu.my<br> Telephone Number : ".$telephonenumber."<br><br>".$data2['message']);


				//$mail->setFrom($username.'@'.MAIL_DOMAIN, $firstname);
				$mail->addTo('epic_feedback@oum.edu.my');
				$mail->setSubject($data2['subject']);
				//$mail->setReturnPath($username.'@'.MAIL_DOMAIN);
				//$mail->getFrom($username.'@'.MAIL_DOMAIN);
				//$mail->setReplyTo($username.'@'.MAIL_DOMAIN);
				//$mail->send($transport);

				if(!$mail->send($transport)) {
					?>
				    <script>
				    alert("Error Sent Message");
					</script>
					<?php

				} else {
					?>
				    <script>
				    alert("Message has been sent");
					</script>
					<?php


				}

				// Reset defaults
				Zend_Mail::clearDefaultFrom();
				Zend_Mail::clearDefaultReplyTo();


				/*include_once(APPLICATION_PATH . "/../public"."/PHPMailer/class.phpmailer.php");
				include_once(APPLICATION_PATH . "/../public"."/PHPMailer/class.smtp.php");

				$mail=new PHPMailer();

				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
				$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 465;                   // set the SMTP port


				$mail->Username   = "assignment_admin@oum.edu.my";  // GMAIL username
				$mail->Password   = "admin123";           // GMAIL password

				$mail->From       = $username.'@'.MAIL_DOMAIN;
				$mail->FromName   = $firstname;

				$mail->Subject    = $data2['subject'];


				$mail->Body       = "Fullname : ".$firstname."<br>Email : ".$username."@oum.edu.my<br> Telephone Number : ".$telephonenumber."<br><br>".$data2['message'];//HTML Body


				$mail->WordWrap   = 5000; // set word wrap


				$mail->AddAddress("epic_feedback@oum.edu.my","Epic Feedback");
				$mail->AddReplyTo($username.'@'.MAIL_DOMAIN,$firstname);



				$mail->IsHTML(true); // send as HTML

				if(!$mail->Send()) {
				?>
				<script>
				alert("Mailer Error: <?php echo $mail->ErrorInfo;?>");
				</script>
				<?php

				} else {
				?>
				<script>
				alert("Message has been sent");
				</script>
				<?php

				}*/

			}
		}

	}
	public function youtubeAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$act = $this->_getParam ( 'act', 0 );
		$this->view->act=$act;
		$txtkeyword = $this->_getParam ( 'q', 0 );
		$this->view->txtkeyword=$txtkeyword;
	}


	public function embedyoutubeAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$vidid = $this->_getParam ( 'vidID', 0 );
		$this->view->vidid=$vidid;
	}

	public function addreadlistAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$vidid = $this->_getParam ( 'vidID', 0 );

		$read = new Application_Model_Readinglist ();

		$datas["link"]=$vidid;
		$datas["type"]="youtube";
		$datas["username"]=$data->username;
		$datas["dateadd"]=date("Y-m-d H:i:s");

		$read->upload ( $datas );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/youtube/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/youtube/act/search/?q='.$q );

		} else {
			$this->_redirect ( 'index/embedyoutube/act/search/vidID/'.$vidid );
		}

	}
	
	public function removereadlistAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$vidid = $this->_getParam ( 'vidID', 0 );

		$read = new Application_Model_Readinglist ();

		$link=$vidid;
		$type="youtube";
		$username=$data->username;
		
		$read->delete ( $link, $type, $username );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/youtube/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/youtube/act/search/?q='.$q );

		} else {
			$this->_redirect ( 'index/embedyoutube/act/search/vidID/'.$vidid );
		}

	}
	
	public function googleAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$act = $this->_getParam ( 'act', 0 );
		$this->view->act=$act;
		$txtkeyword = $this->_getParam ( 'q', 0 );
		$this->view->txtkeyword=$txtkeyword;
	}
	
	public function addgooglereadlistAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$link = urldecode($this->_getParam ( 'link', 0 ));

		$read = new Application_Model_Readinglist ();

		$datas["link"]=$link;
		$datas["type"]="google";
		$datas["username"]=$data->username;
		$datas["dateadd"]=date("Y-m-d H:i:s");

		$read->upload ( $datas );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/google/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/google/act/search/?q='.$q );

		} 

	}
	
	public function removegooglereadlistAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$link = urldecode($this->_getParam ( 'link', 0 ));

		$read = new Application_Model_Readinglist ();

		$type="google";
		$username=$data->username;
		
		$read->delete ( $link, $type, $username );

		$q = $this->_getParam ( 'q', 0 );
		if ($q){
			$page = $this->_getParam ( 'page', 0 );
			if ($page)
			$this->_redirect ( 'index/google/act/search/?q='.$q.'&page='.$page );
			else
			$this->_redirect ( 'index/google/act/search/?q='.$q );

		} 

	}
	
	public function libraryAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$ic = $data->studentID;
		$this->view->ic=$ic;
		$username = $data->username;
		$this->view->username=$username;
		
	}
	
	public function studentportalAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$ic = $data->studentID;
		$this->view->ic=$ic;
		$privilage = $data->privilage;
		$this->view->privilage=$privilage;
		
	}
	
	public function myvlegmailAction ()
	{
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		
		$storage2 = new Zend_Auth_Storage_Session("myvleemail");
		$data2 = $storage2->read();
		
		$username = $data->username;
		$this->view->username=$username;
		$password = $data2->passwd;
		$this->view->password=$password;
		
	}
}

