<?php
class Application_Model_Readinglist extends Zend_Db_Table
{
    protected $_name = "reading_list";
   
    public function fetch ($id = "",$link="",$username="")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($id != "") {
			$sql->where('id = ?', $id);
			
		}
		if ($link != "") {
			$sql->where('link = ?', $link);

		}
		if ($username != "") {
			$sql->where('username = ?', $username);

		}
		
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	
	public function fetchAll ($username="",$keyword="")
	{
		$sql = $this->_db->select()->from($this->_name);
		
		if ($keyword != "") {
			$sql->where('link LIKE ?', '%' . $keyword . '%');
		}
		if ($username != "") {
			$sql->where('username = ?', $username);
			
		}
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	
    
     public function upload ($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    
    public function delete ($link, $type, $username)
	{
		$sql = $this->_db->select()->from($this->_name);
		$sql->where('link = ?', $link);
		$sql->where('type = ?', $type);
		$sql->where('username = ?', $username);
		
		$result = $this->_db->fetchRow($sql);
		
		$this->_db->delete($this->_name, 'id = ' . (int) $result["id"]);
	}
	
	public function deleteid ($id)
	{
		$this->_db->delete($this->_name, 'id = ' . (int) $id);
	}
}
?>
