<?php
class Application_Model_Firstlogin extends Zend_Db_Table
{
	protected $_name = 'users';
	
    /*
	 * checkStudent
	 */
	public function checkStudent ($id = "")
	{
		if ( $id == '' ) return;
		
        $sql = $this->_db->select()->from($this->_name);
       	$sql->where('studentID = ?', trim(strip_tags($id)) );

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
}
?>
