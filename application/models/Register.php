<?php
class Application_Model_Register extends Zend_Db_Table
{
	protected $_name = 'users';
	
    /*
	 * checkUsername
	 */
	public function checkUsername ($id = "")
	{
		if ( $id == '' ) return;
		
        $sql = $this->_db->select()->from($this->_name);
       	$sql->where('username = ?', trim(strip_tags($id)) );

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
    
	/*
	 * checkEmail
	 */
	public function checkEmail ($id = "")
	{
		if ( $id == '' ) return;
		
        $sql = $this->_db->select()->from($this->_name);
       	$sql->where('email = ?', trim(strip_tags($id)) );

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
    
/*
	 * checkIcno
	 */
	public function checkIcno ($id = "")
	{
		if ( $id == '' ) return;
		
        $sql = $this->_db->select()->from($this->_name);
       	$sql->where('icno = ?', trim(strip_tags($id)) );

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
}
?>
