<?php
class Application_Model_Announcement extends Zend_Db_Table
{
	protected $_name = "announcement";

	function fetch ($id)
	{
		$select = $this->_db->select()
		->from($this->_name)
		->where("id = '".$id."'");



		$result = $this->_db->fetchRow($select);

		return $result;

	}

	function listannouncement ($condition=null)
	{

		//print_r($condition);

		$select = $this->_db->select()->from($this->_name);

		if($condition!=null)
		{
			if($condition["keywords"]!='')
			{
				$select->where("title LIKE '%".$condition['keywords']."%'");
			}

			if($condition["type"]!='')
			{
				$select->where("type = '".$condition['type']."'");
			}
			if($condition["courseid"]!='')
			{
				$select->where("courseid = '".$condition['courseid']."'");
			}
			
			if($condition["tlimit"]!='')
			{
				$select->limit($condition["tlimit"],0);
			}
			
			if($condition["orderby"]!='')
			{
				$select->order($condition["orderby"]);
			}
		}

		if ($condition["target"]!="ADMIN")
		{
			if ($condition["target"]!="PUBLIC")
			{
				if($condition["type"]=='1')
				{
					$select->where("(role = '".$condition["target"]."'");
					$select->orwhere("role = 'All')");
					$select->where("curdate() BETWEEN start_date AND end_date");
				}
				else if($condition["type"]=='2')
				{
					if($condition["target"]!="COURSE COORDINATOR")
					{
						$select->where("(role = '".$condition["target"]."'");
						$select->orwhere("role = 'All')");
						$select->where("curdate() BETWEEN start_date AND end_date");
					}
				}
			} 
			else if ($condition["target"]=="PUBLIC")
			{
				$select->where("role = 'PUBLIC'");
				$select->where("curdate() BETWEEN start_date AND end_date");
			}

		}
		else
		{
			
			if ($condition["showactive"]=="1")
			{
				$select->where("curdate() BETWEEN start_date AND end_date");
			}
		}
		
		//print_R($select);
		
		//echo $select;
		$result = $this->_db->fetchAll($select);

		return $result;

	}


	public function add ($data)
	{
		$this->_db->insert($this->_name, $data);
	}

	public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'id = ' . (int) $id);
	}

	public function delete ($id)
	{
		$this->_db->delete($this->_name, 'id = ' . (int) $id);
	}

	function listallannouncement ($condition=null)
	{
		$select = $this->_db->select()
		->from($this->_name)
		->order('start_date desc');

		if($condition!=null){
			if($condition["title"]!=''){
				$select->where("title LIKE '%".$condition['title']."%'");
			}
			if($condition["type"]!=''){
				$select->where("type = '".$condition['type']."'");
			}
			if($condition["course"]!=''){
				$select->where("courseid LIKE '%".$condition['course']."%'");
			}
			if($condition["start_date"]!=''){
				$select->where("start_date >= '".$condition['start_date']."'");
				$select->where("end_date <= '".$condition['end_date']."'");
			}

		}



		$result = $this->_db->fetchAll($select);

		return $result;

	}




}
?>
