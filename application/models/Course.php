<?php
class Application_Model_Course extends Zend_Db_Table
{
    protected $_name 	= 'coursemain';
   	protected $_coursestudent = "coursestudent";
	protected $_coursecoordinator = "coursecoordinator";
	protected $_coursefacilitator = "coursefacilitator";
	protected $_coursemaindetail = "coursemain_details";
	
	protected $_namesis = 'tbl_subjectmaster';
	
    function listcourse ($id="")
    {
        $select = $this->_db->select()
            ->from($this->_name);
        if ($id != "")  {
            $select->where(
            'id  = ? ', 
             $id );
             
             $result = $this->_db->fetchRow($select);
        }else{
        	 $result = $this->_db->fetchAll($select);
        }
        
        //echo $select;
        return $result;
        
    }
    
    function catFetch( $cat_id )
    {
    	$select = $this->_db->select()->from($this->_name);
      
        $select->where('course_cat  = ?',$cat_id );
        $session = new Zend_Session_Namespace('language');
        $result = $this->_db->fetchAll($select);
            
        return $result;
    }
    
    function fetch($id='',$by='id')
    {
        $select = $this->_db->select()->from($this->_name);
        
        if ($id != "")  
        {
            $select->where($by.'  = ?',$id );
            
            $session = new Zend_Session_Namespace('language');
            $result = $this->_db->fetchRow($select);
            
            if ( $session->lang == 'ar' && $result['coursename_ar'] != '' )
            {
            	$result['coursename'] = $result['coursename_ar'];
            }
             
           
        }
        else
        {
        	 $result = $this->_db->fetchAll($select);
        }
        
        return $result;
        
    }
    
    function fetchSIS($id='',$by='SubCode')
    {
    	$db = Zend_Registry::get('db2');
    	
    	$select = $db->select()->from($this->_namesis);
        
        if ($id != "")  
        {
            $select->where($by.'  = ?',$id );
             
             $result = $db->fetchRow($select);
        }
        else
        {
        	 $result = $db->fetchAll($select);
        }
        
        return $result;
    }
    
    
    
    public function upload ($data)
	{
		$id = $this->_db->insert($this->_name, $data);
		
		return $id;
	}
	
	public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'id = ' . (int) $id);
	}
	
	public function modifycourseinfo ($data, $courseid)
	{
		$this->_db->update($this->_name, $data, array('id = ?' => $courseid) );
	}
	
	public function delete ($id)
	{
		$this->_db->delete($this->_name, 'id = ' . (int) $id);
	}
	
    /*function searchcourse ($keywords="")
    {
        $select = $this->_db->select()
                  ->from($this->_name);
                  
    	if($keywords)
		{
			//$select->where("courseid LIKE '%".$keywords."%'");
    		//$select->orwhere("coursename LIKE '%".$keywords."%'");
                        
            $select->where(' coursecode LIKE ? ','%' . $keywords . '%');
            $select->orWhere(' coursename LIKE ? ','%' . $keywords . '%');
        }
             
        $result = $this->_db->fetchAll($select);                 
        
        return $result;   
    }*/
	
	function searchcourse ($keywords="")
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();



		$select = $this->_db->select()->from(array('cm'=>$this->_name));

		if($data->privilage=="STUDENT")
		{
			//check course yg di assign sahaja
			$select = $this->_db->select()
								->from(array('sc'=>$this->_coursestudent),array())
								->join(array('i'=>intake),'sc.intakeid = i.id',array())
								->join(array('cm'=>$this->_name),'sc.courseid = cm.id',array('*'))
								->where("username = ?", $data->username);
		}
		else if($data->privilage=="EOS")
		{
			//check course yg di assign sahaja
			$select = $this->_db->select()
								->from(array('sc'=>$this->_coursefacilitator),array())
								->join(array('i'=>intake),'sc.intakeid = i.id',array())
								->join(array('cm'=>$this->_name),'sc.courseid = cm.id',array('*'))
								->where("username = ?", $data->username);

		}
		else if($data->privilage=="COURSE COORDINATOR")
		{
			//check course yg di assign sahaja
			$select_mycourse = $this->_db	->select()
											->from(array('sc'=>$this->_coursecoordinator),array('courseid'=>'sc.courseid'))
											->where("username ='".$data->username."'");


			$select->where("courseid IN (".$select_mycourse.")");

		}

		if ($keywords)
		{
			$select->where("(cm.coursecode LIKE '%".$keywords."%'");
			$select->orwhere("cm.coursename LIKE '%".$keywords."%')");
		}
		
		if ($data->privilage=="STUDENT" || $data->privilage=="EOS")
		{
			$select.= " ORDER BY case intakestatus
           WHEN 'CURRENT' THEN 1
           WHEN 'NEXT' THEN 3
           ELSE 2
         END";
		}
		
		//echo $select;

		$result = $this->_db->fetchAll($select);

		return $result;

	}
    
	function myallcourse ()
	{
		

		$today=date("Y-m-d H:i:s");
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		
		if($data->privilage=="STUDENT")
		{
			//check course yg di assign sahaja
			$select = $this->_db->select()
			->from(array('sc'=>$this->_coursestudent),array())
			->join(array('i'=>intake),'sc.intakeid = i.id',array())
			->join(array('cm'=>$this->_name),'sc.courseid = cm.id',array('*'))
			->where("username = ?", $data->username);

			//echo $select;
			// exit();

		}
		else if($data->privilage=="EOS")
		{
//			echo "lala";
			//check course yg di assign sahaja
			$select = $this->_db->select()
			->from(array('sc'=>$this->_coursefacilitator),array())
			->join(array('i'=>intake),'sc.intakeid = i.id',array())
			->join(array('cm'=>$this->_name),'sc.courseid = cm.id',array('*'))
			->where("username = ?", $data->username);

	
		}
		else if($data->privilage=="COURSE COORDINATOR")
		{



			//check course yg di assign sahaja
			$select_mycourse = $this->_db->select()
									->from(array('sc'=>$this->_coursecoordinator),array('courseid'=>'sc.courseid'))
									->where("sc.username = ?", $data->username);


			$select->where("courseid IN (".$select_mycourse.")");
		}
		else if ( $data->privilage =='ADMIN' )
		{
			$select = $this->_db->select()->from($this->_name);
		}
		else
		{
			return array();
		}
		
		if ($data->privilage=="STUDENT" || $data->privilage=="EOS")
		{
			$select.= " ORDER BY case intakestatus
				           WHEN 'CURRENT' THEN 1
				           WHEN 'NEXT' THEN 3
				           ELSE 2
				         END";
		}


//echo $select;
		$result = $this->_db->fetchAll($select);

		return $result;

	}
}
?>
