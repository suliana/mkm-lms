<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initDoctype ()
	{
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
		
		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
		$viewRenderer->setView($view);
		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
		
		//munzir
		$view->doctype('HTML5');
		$view->setEncoding('UTF-8');
	}
	
	/* protected function _initSiteModules ()
	{
	//Don't forget to bootstrap the front controller as the resource may not been created yet...
	$this->bootstrap("frontController");
	$front = $this->getResource("frontController");
	//Add modules dirs to the controllers for default routes...
	$front->addModuleDirectory(APPLICATION_PATH . '/modules');
	}*/
	protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
    }


	protected function _initAuth()
    {
        $auth = Zend_Auth::getInstance(); 
        $auth->setStorage(new Zend_Auth_Storage_Session('ustlms'));
        Zend_Registry::set('auth', $auth);
    }
    
	protected function _initViewHelpers() 
	{
	    $view = new Zend_View();
	    $view->headTitle( APP_TITLE )->setSeparator(' - ');
	}
    
	protected function _initLoadAclIni ()
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/acl.ini');
		Zend_Registry::set('acl', $config);
	}

	protected function _initAclControllerPlugin ()
	{
		
		//$this->bootstrap('frontcontroller');
		$this->bootstrap('loadAclIni');
		$front = Zend_Controller_Front::getInstance();
		$aclPlugin = new Epic_Controller_Acl(new Epic_Acl());
		$front->registerPlugin($aclPlugin);
		$front->registerPlugin(new Epic_LangSelector ());

	}

	protected function _initVariableIni ()
	{
		$varname = new Zend_Config_Ini(APPLICATION_PATH . '/configs/var.ini');
		Zend_Registry::set('varname', $varname);



	}

	protected function setconstants($constants){
		foreach ($constants as $key=>$value){
			if(!defined($key)){
				define($key, $value);
			}
		}
	}

	protected function _initTranslate(){


		$registry = Zend_Registry::getInstance();

		// Create Session block and save the locale
		$session = new Zend_Session_Namespace('session');
		
		//print_R($session->lang);
		
		$locale = new Zend_Locale('en_US');
		$file = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR . "en_US.php";

		$translate = new Zend_Translate('array',
			$file, $locale,
			array(
			'disableNotices' => true,    // This is a very good idea!
			'logUntranslated' => false,  // Change this if you debug
			)
		);


		$registry->set('Zend_Locale', $locale);
		$registry->set('Zend_Translate', $translate);


		return $registry;


	}
	
	
	protected function _initDb()
    {
        $resource = $this->getPluginResource('multidb');
        Zend_Registry::set("multidb", $resource);
        
		
        //$db = Zend_Registry::get("db");
	    $resource = $this->getPluginResource('db');
    	$db = $resource->getDbAdapter();
	    Zend_Registry::set("db", $db);
	    //Zend_Registry::get('db')->query("SET NAMES 'utf8'");
		//Zend_Registry::get('db')->query("SET CHARACTER SET 'utf8'");
    }
    
	protected function _initDb2()
    {
	    /*$parameters = array(
                    'host'     => 'localhost',
                    'username' => 'root',
                    'password' => 'mysql',
                    'dbname'   => 'sisv4'
                   );
		try 
		{
		    $db = Zend_Db::factory('Pdo_Mysql', $parameters);
		    $db->getConnection();
		}
		catch (Zend_Db_Adapter_Exception $e) 
		{
		    echo $e->getMessage();
		    die('Could not connect to SIS database.');
		}
		catch (Zend_Exception $e) 
		{
		    echo $e->getMessage();
		    die('Could not connect to SIS database.');
		}
		
		Zend_Registry::set('db2', $db);
		Zend_Registry::get('db2')->query("SET NAMES 'utf8'");
		Zend_Registry::get('db2')->query("SET CHARACTER SET 'utf8'"); */
    }

}

