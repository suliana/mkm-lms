<?php

class Application_Form_Index extends Zend_Form
{

    public function init()
    {
        // Set the method for the display form to POST
        $this->setMethod('post');

        // Add username element
        $this->addElement('text', 'username', array(
            'label'      => 'Your username:',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));
        
        // Add username element
        $this->addElement('password', 'password', array(
            'label'      => 'Your password:',
            'required'   => true,
        ));

        
        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Login',
        ));

        
    }


}

