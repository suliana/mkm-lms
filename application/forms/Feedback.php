<?php
class Application_Form_Feedback extends Zend_Form
{


public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$subject = $this->createElement('text', 'subject');
        $subject->setLabel($this->getView()->translate('subject').':')->setRequired(true);
        
        $subject->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $message = $this->createElement('textarea', 'message');
        $message->setLabel($this->getView()->translate('message').':')->setRequired(true);
        
        $message->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        

			
        $this->addElements(
        array($subject, $message));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('send').' '.$this->getView()->translate('feedback'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
               )
        ));
        
        
        
        
	    $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       ));
    }
}