<?php

class Application_Form_Index extends Zend_Form
{	
	protected $_redir;

	public function setRedir($value)
	{
		$this->_redir = $value;
	}
    
	public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'element')),
        array('Label', array('tag' => 'span')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div')),
    );

    public $buttonDecorators = array('ViewHelper', 
		array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'submit')), 
		array(array('label' => 'HtmlTag'), 
		array('tag' => 'span', 'placement' => 'prepend')), 
		array(array('row' => 'HtmlTag'), array('tag' => 'div')),
	);
	
    public function init()
    {
    	
        // Set the method for the display form to POST
        $this->setMethod('post');
		
        // Add username element
        //$this->addElement('text', 'StudentID', array(
        $this->addElement('text', 'username', array(
        	'decorators' => $this->elementDecorators,
            'label'      => '',
            'required'   => true,
            'filters'    => array('StringTrim','StripTags'),
        ));
        
        // Add username element
        $this->addElement('password', 'password', array(
        //$this->addElement('password', 'StudentPassword', array(
            'decorators' => $this->elementDecorators,
            'label'      => '',
            'required'   => true,
        	'filters'    => array('StringTrim','StripTags')
        ));
        
       $this->addElement('hidden', 'redirect', array('value'=> $this->_redir));
       
        
        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'decorators' => $this->buttonDecorators,
            'ignore'   => true,
            'label'    => $this->getView()->translate('login'),
        	'class'	   => 'buttonsubmit btn',
        ));

        
    }
    
     public function loadDefaultDecorators()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'class' => 'loginbox')),
            'Form',
        ));
    }
    


}

