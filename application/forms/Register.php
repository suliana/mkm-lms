<?php
class Application_Form_Register extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
	
		
		$Username = new Zend_Form_Element_Text('username');
		$Username->setAttrib('class', 'input')
				 ->setRequired(true)
				 ->setAttrib('required','required')
				 ->setAttrib('onblur','signup.username();')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
				 
		$Password = new Zend_Form_Element_Password('pw1');
		$Password->setAttrib('class', 'input')
				 ->setRequired(true)
				 ->setAttrib('required','required')
				 ->setAttrib('onblur','signup.pass1(); signup.pwmatch();')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
				 
		$Confirm = new Zend_Form_Element_Password('pw2');
		$Confirm->setAttrib('class', 'input')
				 ->setRequired(true)
				 ->setAttrib('required','required')
				 ->setAttrib('onblur','signup.pass2();')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
		
		$Email = new Zend_Form_Element_Text('email');
		$Email->setAttrib('class', 'input')
				 ->setRequired(true)
				 ->setAttrib('required','required')
				 ->setAttrib('onblur','signup.email();')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
		
		
		$Firstname = new Zend_Form_Element_Text('firstname');
		$Firstname->setAttrib('class', 'input')
				 ->setRequired(true)
				 ->setAttrib('required','required')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
				 
		$Lastname = new Zend_Form_Element_Text('lastname');
		$Lastname->setAttrib('class', 'input')
				 ->setRequired(true)
				 ->setAttrib('required','required')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
				 
		$Icno = new Zend_Form_Element_Text('icno');
		$Icno->setAttrib('class', 'input')
				 ->setRequired(true)
				  ->setAttrib('onblur','signup.icno();')
				 ->setFilters(array('StringTrim','StripTags'))
				 ->removeDecorator("DtDdWrapper")
				 ->removeDecorator("Label")
				 ->removeDecorator('HtmlTag');
		
		$Submit = new Zend_Form_Element_Submit('submit');
		$Submit->setAttrib('class', 'submit btn');
		$Submit->removeDecorator("DtDdWrapper");
		$Submit->removeDecorator("Label");
		$Submit->removeDecorator('HtmlTag');
		

		$this->addElements(array($Username, $Email, $Password, $Confirm, $Firstname,$Lastname, $Icno, $Submit ));
	}
}