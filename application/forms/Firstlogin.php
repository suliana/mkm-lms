<?php

class Application_Form_Firstlogin extends Zend_Form
{	
	protected $_info;

    public function __construct($options = null, $info = null)
    {
        $this->_info = $info;
        parent::__construct($options);
    }
    
	public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'element')),
        array('Label', array('tag' => 'span')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div')),
    );

    public $buttonDecorators = array('ViewHelper', 
		array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'submit')), 
		array(array('label' => 'HtmlTag'), 
		array('tag' => 'span', 'placement' => 'prepend')), 
		array(array('row' => 'HtmlTag'), array('tag' => 'div')),
	);
	
    public function init()
    {
    	
        // Set the method for the display form to POST
        $this->setMethod('post');
		
        // student id
        $this->addElement('text', 'student_id', array(
        	'decorators' => $this->elementDecorators,
            'label'      => $this->getView()->translate('studentID'),
            'required'   => true,
            'filters'    => array('StringTrim','StripTags'),
        ));
        
         // email
        $this->addElement('text', 'email', array(
        	'decorators' => $this->elementDecorators,
            'label'      => $this->getView()->translate('email'),
            'required'   => true,
            'filters'    => array('StringTrim','StripTags'),
        ));
        
        // username
        $this->addElement('text', 'username', array(
        	'decorators' => $this->elementDecorators,
            'label'      => $this->getView()->translate('username'),
            'required'   => true,
            'filters'    => array('StringTrim','StripTags'),
        ));
        
        // password
        $this->addElement('password', 'password', array(
            'decorators' => $this->elementDecorators,
            'label'      => $this->getView()->translate('password'),
            'required'   => true,
        	'filters'    => array('StringTrim','StripTags')
        ));
		        
        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'decorators' => $this->buttonDecorators,
            'ignore'   => true,
            'label'    => $this->getView()->translate('create_account'),
        	'class'	   => 'buttonsubmit btn',
        ));

        
    }
    
     public function loadDefaultDecorators()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'class' => 'loginbox')),
            'Form',
        ));
    }
    


}

