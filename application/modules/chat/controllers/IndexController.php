<?php
class Chat_IndexController extends Zend_Controller_Action 
{
	public function init() 
	{
		/* Initialize action controller here */
		
		$this->_helper->layout()->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(true);
	}

	public function indexAction() 
	{
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
	
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->view->privilage = $data->privilage;
		
		$this->privilage = $this->userinfo->privilage;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;
		
		$title = $this->_getParam ( 'title', 0 );
		$this->view->roomname = $title;		
	}
	
	public function userAction()
	{
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
	
		$this->userinfo = $data;
		echo $data->username;
        
	}
}





