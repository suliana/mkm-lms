<?php
class Chat_Model_Chat extends Zend_Db_Table
{
    protected $_name 	= 'block_content';
    protected $_block 	= 'block';
    
    public function getChats()
    {
    	$select = $this->_db->select()->from(array('a' => $this->_name))
    								->joinLeft(array('b'=> $this->_block),'a.blockid=b.id', array('b.coursecode'))
										->where('a.res_type = 7');

		$result = $this->_db->fetchAll($select);
		
		return $result;
    }
 	   
}