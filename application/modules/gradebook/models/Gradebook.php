<?php
class Gradebook_Model_Gradebook extends Zend_Db_Table
{
	protected $_name = "result";
	protected $_block = "block";
	protected $_block_content = "block_content";
	protected $_test = "test";
	

	
	public function delete ($id)
    {
        $this->_db->delete($this->_name, 'result_id = ' . (int) $id);
    }
    
    public function fetch ($courseid,$uid,$keyword="")
	{
		$sql = $this->_db->select()
		->from(array('a'=>$this->_name),
		array('result_id','obtained_percentage','status'))
		->join(array('b'=>$this->_test),'a.tid=b.tid',
		array('tid'))
		->join(array('c'=>$this->_block_content),'b.blockcontentid=c.id',
		array('title','description'))
		->join(array('d'=>$this->_block),'c.blockid=d.id',
		array('courseid'))
		->where('courseid = ?', $courseid)
		->where('res_type = ?', "9")
		->where('uid = ?', $uid);
		
		
		if ($keyword != "") {
			$sql->where(
				'  ( c.title LIKE ? ',
				'%' . $keyword . '%');
			$sql->orwhere(
				'  a.result_id = ? )',
				 $keyword );
		}
		//echo $sql;
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		// exit();
		return $result;
	}
	
	public function fetchrowtest ($tid)
	{
		$sql = $this->_db->select()
		->from(array('a'=>$this->_test),
		array('tid','test_time'))
		->join(array('b'=>$this->_block_content),'a.blockcontentid=b.id',
		array('title','description'))
		->where('tid = ?', $tid);
		
		//echo $sql;
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		// exit();
		return $result;
	}
	
	public function fetchtest ($courseid,$semid,$keyword="")
	{
		$res_type=9;
		
		$sql = $this->_db->select()
		->from(array('a'=>$this->_block_content),
		array('id','title'))
		->join(array('b'=>$this->_block),'a.blockid=b.id',
		array('intakeid'))
		->where('courseid = ?', $courseid)
		->where('intakeid = ?', $semid)
		->where('res_type = ?', $res_type);
		
		if ($keyword!="")
		$sql->where('a.title LIKE ?', '%' . $keyword . '%');
		
		//echo $sql;
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		// exit();
		return $result;
	}
	
	
	public function fetchresult ($blockcontentid,$uid)
	{
		$sql = $this->_db->select()
		->from(array('a'=>$this->_name),
		array('result_id','obtained_percentage','status'))
		->join(array('b'=>$this->_test),'a.tid=b.tid',
		array('tid'))
		->join(array('c'=>$this->_block_content),'b.blockcontentid=c.id',
		array('title','description'))
		->where('blockcontentid = ?', $blockcontentid)
		->where('uid = ?', $uid);
		
		
		
		//echo $sql;
		//exit();
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		// exit();
		return $result;
	}
	


	
	
	
	

}


?>
