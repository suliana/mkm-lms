<?php
class Gradebook_IndexController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */

		//$this->_helper->layout->setLayout('course');
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
	}



	public function indexAction()
	{
		$this->view->username = $this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('gradebook');
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ($courseid);
		$this->view->course = $rscourse;

		$form = new Gradebook_Form_Search ();
		$this->view->form = $form;

		$gradebook = new Gradebook_Model_Gradebook ();

		// get questions list as array
		$select = $gradebook->fetch($courseid,$this->userinfo->id);


		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost())
		{
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost())
			{
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else
			{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			
			//make sure the submitted data is valid and modify select statement
			if ($form->valid())
			{
				$datas = $form->getValues ();
				$select = $gradebook->fetch($courseid,$this->userinfo->id, $datas['keyword'] );
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'));
	}
	
	public function liststudentAction() 
	{
			$semid= $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;
		
		$blockcontentidid= $this->_getParam ( 'blockcontentid', 0 );
		$this->view->blockcontentid = $blockcontentidid;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('gradebook')." > ".$this->view->translate('view'). " ".$this->view->translate('result')." > ".$this->view->translate('student_list');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		
		$student = new Course_Model_Student ();
		$condition=array('courseid'=>$courseid,'intakeid'=>$semid)	;
		$select = $student->coursestudentlist ($condition);

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
	}
	
	public function listtestnameAction()
	{
		
		$this->view->username = $this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('gradebook');
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ($courseid);
		$this->view->course = $rscourse;

		

		$intakeid = $this->_getParam ( 'intakeid', "" );

		//get current intake id
		if($intakeid=="")
		{
			$intake = new Admin_Model_Intake ();
			$result =  $intake->checkstatus('CURRENT');
			$intakeid = $result["id"]	;
		}
		
		$this->view->intakeid = $intakeid;
		
		$form = new Gradebook_Form_SearchIntake (array ('intakeid' => $intakeid));
		$this->view->form = $form;
		
		$gradebook = new Gradebook_Model_Gradebook ();
		
		// get questions list as array
		$select = $gradebook->fetchtest($courseid,$intakeid);

		//capture the input params
		if ($this->_request->getParam('intakeid',"") || $this->_request->isPost())
		{
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost())
			{
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else
			{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			//make sure the submitted data is valid and modify select statement
			if ($form->valid())
			{
				$data = $form->getValues ();
				$select = $gradebook->fetchtest($courseid,$data ['intakeid'],$data ['keyword']);
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));
	}
	
	public function deleteresultAction() 
	{
		
		$test = new Block_Model_Test ();
		$id = $this->_getParam ( 'rowid', 0 );

		$courseid = $this->_getParam ( 'courseid', '' );
		
		$semid= $this->_getParam ( 'semid', 0 );
		
		$blockcontentidid= $this->_getParam ( 'blockcontentid', 0 );
		
		$from= $this->_getParam ( 'from', "" );
		
		//echo $id;
		if ($id > 0) 
		{
			$test->deleteresult ( $id );
			if ($from=="liststudent")
			$this->_redirect ( 'gradebook/index/liststudent/blockcontentid/'.$blockcontentidid.'/semid/'.$semid.'/courseid/'.$courseid );
			else if ($from=="mylist")
			$this->_redirect ( 'gradebook/index/listmyresult/blockcontentid/'.$blockcontentidid.'/semid/'.$semid.'/courseid/'.$courseid );
			else 
			$this->_redirect ( 'gradebook/index/index/courseid/'.$courseid );
		}
	}
	
	public function listmyresultAction() 
	{	
		$this->view->username = $this->userinfo->username;
		
		$this->view->uID = $this->userinfo->id;
		
		$semid= $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;
		
		$blockcontentidid= $this->_getParam ( 'blockcontentid', 0 );
		$this->view->blockcontentid = $blockcontentidid;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('gradebook')." > ".$this->view->translate('view'). " ".$this->view->translate('result');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		
		
	}

	
	
	

}





