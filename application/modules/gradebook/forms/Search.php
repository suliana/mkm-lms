<?php
class Gradebook_Form_Search extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$keyword = $this->createElement('text', 'keyword');
        $keyword->setLabel($this->getView()->translate('test').' '.$this->getView()->translate('name').' :')->setRequired(false);
        
        $keyword->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $submit = $this->createElement('submit', 'submit');
        $submit->setLabel($this->getView()->translate('search').$this->getView()->translate('test').' '.$this->getView()->translate('name'))->setIgnore(true);
        $submit->setOptions(array('class'=>'button'));
        
        $submit->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        
        $this->addElements(
        array($keyword, $submit));
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}