<?php
class Gradebook_Form_SearchIntake extends Zend_Form
{
	protected $_intakeid;


	public function setIntakeid($value)
	{
		$this->_intakeid = $value;
	}

	public function init ()
	{

		$this->setMethod('post');

		$intake = new Admin_Model_Intake ();
		$result =  $intake->fetchAll();
		$intakeid = new Zend_Form_Element_Select('intakeid');

		$intakeid->setLabel($this->getView()->translate('semester_id').':')
		->setRequired(true);
		$intakeid->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($result as $c) {
			if($this->_intakeid != ""){
				$selected = $this->_intakeid;
				$intakeid->setValue(array($c['id'] => $selected));
			}else{
				if($c["intakestatus"]=="CURRENT"){
				    $selected = $c['id'];
					$intakeid->setValue(array($c['id'] => $selected));
				}
			}
			
			$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}

		$intakeid->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$keyword = $this->createElement('text', 'keyword');
        $keyword->setLabel($this->getView()->translate('test').' '.$this->getView()->translate('name').' :')->setRequired(false);
        
        $keyword->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));

		$register = $this->createElement('submit', 'register');
		$register->setLabel($this->getView()->translate('search_assignment'))->setIgnore(true);

		$register->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));


		$this->addElements(
		array($intakeid, $keyword, $register));


		$this->setDecorators(array(

		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'

		));
	}
}