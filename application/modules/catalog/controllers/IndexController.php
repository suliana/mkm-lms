<?php
class Catalog_IndexController extends Zend_Controller_Action 
{
	public function init()
	{
		//$this->_helper->layout->setLayout('course');
		$this->view->course_tools = 1;

		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'catalog';
		$this->catalog = new Catalog_Model_Catalog();
	}
	
	public function indexAction() 
	{
		$cats = $this->catalog->fetchAll();
		$this->cat = $this->cats = array();
		
		foreach ( $cats as $cat )
		{
			$this->cat[$cat['pid']][$cat['id']] = $cat;	
		}
		
		$this->view->cats = $this->cat;
	}
	
	public function viewAction()
	{
		
		$id = $this->_getParam('id', 0);
		
		if ( $id == 0 ) exit;
		
		$cat = $this->catalog->fetch($id);
		$this->view->cat = $cat;
		
		if ( empty($cat) )
		{
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
		
		$course = new Application_Model_Course ();
		
		$this->view->courses = $course->catFetch($id);
		
		
	}
}