<?php
class Catalog_AdminController extends Zend_Controller_Action 
{
	public function init()
	{
		//$this->_helper->layout->setLayout('course');
		$this->view->course_tools = 1;

		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
		
		$this->catalog = new Catalog_Model_Catalog();
	}
	
	public function indexAction() 
	{
		$cats = $this->catalog->fetchAll();
		$this->cat = $this->cats = array();
		
		foreach ( $cats as $cat )
		{
			$this->cat[$cat['pid']][$cat['id']] = $cat;	
		}
		
		$this->view->cats = $this->cat;
	}
	
	public function deleteAction()
	{
		$id = $this->_getParam('id', 0);
		
		if ( $id == 0 ) exit;
		
		$catalog = new Catalog_Model_Catalog();
		
		$catalog->delete($id);
		
		$url = $this->view->url(array('module'=>'catalog', 'controller'=>'admin'),'default',true);
		$this->_redirect($url, array('prependBase' => false));
	}
	
	public function editAction()
	{
		$id = $this->_getParam('id', 0);
		
		if ( $id == 0 ) exit;
		
		$form = new Catalog_Form_Edit();
		$this->view->form = $form;
		$catalog = new Catalog_Model_Catalog();
		//build select
		$cats = $this->catalog->fetchAll();
		$this->cat = $this->cats = array();
		
		foreach ( $cats as $cat )
		{
			$this->cat[$cat['pid']][$cat['id']] = $cat;	
		}
		
		$this->make_cat_options(0,'',1);
		$this->view->form->parent->addMultiOption(0, $this->view->translate('None'));
		
		foreach ( $this->cats as $cat_id => $cat_name )
		{
			$this->view->form->parent->addMultiOption($cat_id, $cat_name);
		}
		
		if ($this->getRequest()->isPost())
		{

			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) 
			{
				$formData = $form->getValues();
				
				//check
				$check = $catalog->fetch($this->export_title($formData['name']),'p_name');
				if ( !empty($check) && $check['id'] == $id )
				{
					$check = array();
				}
				
				if ( empty($check) )
				{
					//process form
					$catalog->update(array(
						'name'			=>	$formData['name'],
						'p_name'		=> 	$this->export_title($formData['name']),
						'description'	=>	$formData['description'],
						'pid'			=>	$formData['parent']
					), $id );
					
					$url = $this->view->url(array('module'=>'catalog', 'controller'=>'admin'),'default',true);
					$this->_redirect($url, array('prependBase' => false));
				}
				else
				{
					$this->view->error = $this->view->translate('catalog_already_exists');
					
					$form->populate($formData);
				}
			}
			else
			{
				$form->populate($formData);
			}
		}
		else
		{
			$data = $catalog->fetch($id);
			$form->parent->setValue($data['pid']);
			
			$form->populate($data);
		}
		
		
		
	}
	
	public function addAction()
	{
		$form = new Catalog_Form_Edit();
		$this->view->form = $form;
		$catalog = $this->catalog;
		
		//build select
		$cats = $this->catalog->fetchAll();
		$this->cat = $this->cats = array();
		
		foreach ( $cats as $cat )
		{
			$this->cat[$cat['pid']][$cat['id']] = $cat;	
		}
		
		$this->make_cat_options(0,'',1);
		$this->view->form->parent->addMultiOption(0, $this->view->translate('None'));
		
		foreach ( $this->cats as $cat_id => $cat_name )
		{
			$this->view->form->parent->addMultiOption($cat_id, $cat_name);
		}
		
		if ($this->getRequest()->isPost())
		{

			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) 
			{
				$formData = $form->getValues();
				
				//check
				$check = $catalog->fetch($this->export_title($formData['name']),'p_name');
				
				
				if ( empty($check) )
				{
					//process form
					$catalog->insert(array(
						'name'			=>	$formData['name'],
						'p_name'		=> 	$this->export_title($formData['name']),
						'description'	=>	$formData['description'],
						'pid'			=>	$formData['parent']
					));
					
					$url = $this->view->url(array('module'=>'catalog', 'controller'=>'admin'),'default',true);
					$this->_redirect($url, array('prependBase' => false));
				}
				else
				{
					$this->view->error = $this->view->translate('catalog_already_exists');
					
					$form->populate($formData);
				}
			}
			else
			{
				$form->populate($formData);
			}
		}
		
		
		
	}
	
	public function export_title($name,$replace='-')
	{
		$untouched = $name;
        $name = strip_tags( trim($name) );
		preg_match_all('/[a-z0-9]+/',strtolower($name), $match);

        $value = implode($replace,$match[0]);

		if ( $value == '' )
		{
			//$value = substr(md5(strlen($value)),0,8);
			$value = $untouched;
		}

		return $value;
    }
	
	private function make_cat_options($pid='0',$selected='',$depth=1)
	{
		$cache = $this->cat;

		if(!isset($cache[$pid])) return;
		
		while (list($parent,$category) = each($cache[$pid]))
		{
			$depths = $depth > 1 ? str_repeat("-",$depth-1)." ":'';

			$this->cats[$category['id']] = $depths.$category['name'];
			
			$this->make_cat_options($category['id'],$selected,$depth+1);
		} 
	}
	
	
}