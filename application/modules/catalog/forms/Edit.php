<?php 
class Catalog_Form_Edit extends Zend_Form
{
 	protected $_info;

    public function __construct($options = null, $info = null)
    {
        $this->_info = $info;
        parent::__construct($options);
    }
	
    
	public function init()
	{
	 	$this->addElement(
		                    'text',
		                    'name',
		                    array(
		                            'class'		  => 'inputtext',
		                    		'filters'    => array('StringTrim','StripTags'),
		                    		'required'    => true,
		                            'label'       => $this->getView()->translate('name'),
		                    		'decorators'  => array
		                    						(
														'ViewHelper',
									 					'Description',
														'Errors',
														
														array('Label', array('tag' => 'strong')),
														array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
											        ),
		                    )
            		);
            			

       
		$this->addElement(
		                    'textarea',
		                    'description',
		                     array(
		                            'required'    => true,
		                            'filters'     => array(),
		                            'label'       => $this->getView()->translate('description'),  
		                            'decorators'  => array
		                    						(
														'ViewHelper',
									 					'Description',
														'Errors',
														
														array('Label', array('tag' => 'strong','escape' => false)),
														array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
											        ),
		                    )
            			);
	          
		$parent = new Zend_Form_Element_Select('parent');
		$parent ->setLabel($this->getView()->translate('parent'));
		$parent->setOptions(array('class'=>'select'));
		$parent->setDecorators(array(
										'ViewHelper',
										'Description',
										'Errors',
										array('Label', array('tag' => 'strong','escape' => false)),
										array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
		));
		
		$this->addElements( array($parent) );
        
        //buttons
		$this->addElement('submit', 'save', array(
          	'label'=> $this->getView()->translate('save'),
          	'class'=>'btn submit',
          	'decorators'=>array('ViewHelper'),
        ));
    	
        $this->addElement('submit', 'cancel', array(
          	'label'=> $this->getView()->translate('cancel'),
          	'class'=>'btn cancel',
          	'decorators'=>array('ViewHelper'),
        	'onclick'=> "window.location ='" . $this->getView()->url(array('module'=>'languageset', 'controller'=>'configurer'),'default',true) . "'; return false;"
        ));
    	
    	$this->addDisplayGroup( array('save', 'cancel'), 'buttons', array(
      		'decorators'=>array('FormElements',array('HtmlTag', array('tag'=>'div', 'class'=>'submit')) )
    	));
        

		$this->setDecorators(array(
               'FormElements',
               array(array('data'=>'HtmlTag'),array('tag'=>'div', 'class'=>'formnice')),
               'Form'
       ));
		
	}
}
?>