<?php 
class Catalog_Model_Catalog extends Zend_Db_Table
{
	protected $_name = 'catalog';
	
	public function fetchAll()
	{	
		$sql = $this->_db->select()->from($this->_name);
		$stmt = $this->_db->query($sql);
        $result = $stmt->fetchAll();
        return $result;
	}
	
	public function fetch ($id = "", $by='id')
	{
        $sql = $this->_db->select()->from($this->_name);
        
        if ($id != "") {
            $sql->where($by.' = ?', $id);
        }

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
	
    public function update($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    
 	/*
	 * 	Insert
	 */
	public function insert($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    
	public function delete ($id)
	{
		$this->_db->delete($this->_name, array( 'id = ?' => $id ) );
		$this->update(array('pid' => $id ), $id );
	}
    
	public function find ($keyword = '')
	{
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != '')
        {
            $sql->where('name LIKE ? ','%' . $keyword . '%');
        }

        $result = $this->_db->query($sql);
        return $result;
    }
    
}
?>