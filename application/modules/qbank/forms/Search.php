<?php
class Qbank_Form_Search extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$keyword = $this->createElement('text', 'keyword', array('class'=>'inputtext'));
        $keyword->setLabel($this->getView()->translate('question') )->setRequired(false);
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_question'))->setIgnore(true);
        $register->setOptions(array('class'=>'btn submit'));

        
        
        $this->addElements(array($keyword, $register));
	}
}