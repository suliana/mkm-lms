<?php
class Qbank_Model_Qbank extends Zend_Db_Table
{
	protected $_qbank_maindb = "questionbank";
	protected $_qbank_testdb = "test";
	protected $_qbank_resultdb = "result";
	protected $_coursemaindb = "coursemain";


	
	public function delete ($qid)
    {
        $this->_db->delete($this->_qbank_maindb, 'qid = ' . (int) $qid);
    }
	


	public function get_question($slug,$courseid,$keyword = "")
	{
		if ($slug>="1")
		{
			$sql = $this->_db->select()->from(array('a'=>$this->_qbank_maindb),
			array('*'))
			->join(array('b' => $this->_coursemaindb),
			'b.id = a.courseid',
			array('coursename'));

			$sql->where('qid = ?', $slug);

			$sql->where('a.courseid = ?', $courseid);

			if ($keyword != "") {
				//echo $keyword;
				$sql->where(
				' question LIKE ? ','%' . $keyword . '%');
			}
			
			$result = $this->_db->fetchRow($sql);
			
			

			return $result;
		}
		else
		{
			$sql = $this->_db->select()->from(array('a'=>$this->_qbank_maindb),
			array('*'))
			->join(array('b' => $this->_coursemaindb),
			'b.id = a.courseid',
			array('coursename'));

			$sql->where('a.courseid = ?', $courseid);
			//echo $sql;
			//exit();

			if ($keyword != "") {
				//echo $keyword;
				$sql->where(
				' question LIKE ? ','%' . $keyword . '%');
			}
			$result = $this->_db->fetchAll($sql);

			return $result;

		}
	}
	
	public function update_question($qid,$data)
	{
		$option=array_filter($data['option']);
		$option=implode(",",$option);
		$answer=$data['answer'];
		foreach($data['option'] as $key => $opt)
		{
			if($key==$answer)
			{
				$ans=$opt;
			}
		}
		// update question

		$data2=array('question'=>$data['question'],'answer'=>$ans,'options'=>$option,'courseid'=>$data['courseid'],'coursecode'=>$data['coursecode']);
		//echo $qid;

		$this->_db->update($this->_qbank_maindb, $data2, 'qid = ' . (int) $qid);
		

	}

	public function add_question($data)
	{
		$option=array_filter($data['option']);
		$option=implode(",",$option);
		$answer=$data['answer'];
		foreach($data['option'] as $key => $opt)
		{
			if($key==$answer)
			{
				$ans=$opt;
			}
		}
		
		// insert question
		$data2=array('question'=>$data['question'],'answer'=>$ans,'options'=>$option,'courseid'=>$data['courseid'],'coursecode'=>$data['coursecode']);
		
		$this->_db->insert($this->_qbank_maindb, $data2);
		
		
	}
	
	public function importques($sub_select,$courseid)
	{
		$sql = $this->_db->select()
              ->from($this->_qbank_maindb)
              ->where("courseid = ?", $courseid)
              ->where("qid NOT IN ?", $sub_select);
           
        //echo $sql;
        //exit();
        $result = $this->_db->fetchAll($sql);
        
        return $result;

	}
	
	
	

}


?>
