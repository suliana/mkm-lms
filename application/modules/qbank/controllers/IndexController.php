<?php
class Qbank_IndexController extends Zend_Controller_Action 
{
	public function init()
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('course');
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
		
		$this->view->course_tools = 1;
	}



	public function indexAction()
	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('qbank');
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$form = new Qbank_Form_Search ();
		$this->view->form = $form;

		$qbank = new Qbank_Model_Qbank ();

		// get questions list as array
		$select = $qbank->get_question('0',$courseid);

		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost())
		{
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost())
			{
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else
			{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			
			//make sure the submitted data is valid and modify select statement
			if ($form->valid())
			{
				$data = $form->getValues ();
				$select = $qbank->get_question('0',$courseid, $data ['keyword'] );
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'));
	}
	

	public function uploadAction() 
	{
		$username = $this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$qbank = new Qbank_Model_Qbank ();

		//$form->getElement('courseid')->setValue($courseid);
		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;
		
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;


		if ($this->getRequest ()->isPost ())
		{
			$data = $this->getRequest()->getPost();
			
			$course = new Application_Model_Course ();
			$rscourse = $course->fetch($data['courseid']);
			$data['coursecode'] = $rscourse['coursecode'];
			
			$qbank->add_question ( $data );
			
			if ($id > 0 && $blockid > 0)
			{
				$lastId=$qbank->getAdapter()->lastInsertId();

				$sdata['qid'] =  $lastId;	
				$sdata['blockcontentid'] =  $id;
				
				$test = new Block_Model_Test();
				
				$sdata['countNo'] = $test->countquestion_db($id);
				
				
				
				$test->uploadquestion_db($sdata);
				
				$this->_redirect ( 'block/test/index/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );
			}
			else
			{
				$this->_redirect ( 'qbank/index/index/courseid/'.$courseid );
			}
		}

	}
	
	public function modifyAction() 
	{
		$username=$this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		
		$qbank = new Qbank_Model_Qbank ();
		
		$qid = $this->_getParam ( 'qid', 0 );
		
		$this->view->qid = $qid;
		
		$questions = $qbank->get_question($qid,$courseid);

		$this->view->question = $questions;
		
		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;
		
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;

		//$form->getElement('courseid')->setValue($courseid);


		if ($this->getRequest ()->isPost ())
		{
			$data = $this->getRequest()->getPost();
			$course = new Application_Model_Course ();
			$rscourse = $course->fetch($data['courseid']);
			$data['coursecode'] = $rscourse['coursecode'];
			
			$qbank->update_question ( $data['qid'],$data );

			if ($id > 0 && $blockid > 0)
			{
				$this->_redirect ( 'block/test/index/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );
			}
			else 
			{
				$this->_redirect ( 'qbank/index/index/courseid/'.$courseid );
			}
			
		} // ispost
	}
	
	public function deleteAction() 
	{
		$qbank = new Qbank_Model_Qbank ();
		
		$id = $this->_getParam ( 'rowid', 0 );
		
		$courseid = $this->_getParam ( 'courseid', 0 );

		if ($id > 0)
		{
			$qbank->delete ( $id );
			$this->_redirect ( 'qbank/index/index/courseid/'.$courseid );
		}
	}
	
	public function viewAction()
	{
		$username = $this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$qbank = new Qbank_Model_Qbank ();
		
		$qid = $this->_getParam ( 'qid', 0 );
		
		$this->view->qid = $qid;
		
		$questions=$qbank->get_question($qid,$courseid);
		
		$this->view->question = $questions;
		
	}
	
	public function importquesAction()
	{
		$username = $this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch ($courseid);
		$this->view->course = $rscourse;
		
		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;
		
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;
		
		$test = new Block_Model_Test();
		
		$sql=$test->fetchquesid($id);
		
		
		
		$qbank = new Qbank_Model_Qbank ();
		
		$select=$qbank->importques($sql,$courseid);
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 2);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		
		
		
	}
	
	public function importquesprocessAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );

		$blockid = $this->_getParam ( 'blockid', 0 );

		$id = $this->_getParam ( 'id', 0 );
		
		$pageitem = $this->_getParam ( 'pageitem', 0 );
		
		$test = new Block_Model_Test();
		
		for ($ctr = 1 ; $ctr <= $pageitem ; $ctr++ )  
		{
			$sdata['qid'] =  $this->_getParam ( 'C'.$ctr, "" );
			$sdata['blockcontentid'] =  $id;
				
			$sdata['countNo'] = $test->countquestion_db($id);
				
			//print_r($sdata);
			if ($sdata['qid']!="")
			{
				//echo "here";
				$test->uploadquestion_db($sdata);
			}
			//exit();
				
			
		}
		
		$this->_redirect ( 'block/test/index/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );
	}
	
	

}





