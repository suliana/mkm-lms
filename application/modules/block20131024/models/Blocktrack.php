<?php
class Block_Model_Blocktrack extends Zend_Db_Table
{
	protected $_name = "block_track_";
	protected $_block_content = "block_content";
	protected $_content_track = "content_track_";
	protected $_block = "block";

	public function addTrack ($data,$courseid)
	{

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$track_table = $this->_name.$courseid;

		$this->_db->insert($track_table, $data);

		return $data;

	}

	public function addcontentTrack ($data,$courseid)
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$track_table = $this->_content_track.$courseid;

		$this->_db->insert($track_table, $data);

		return $data;

	}

	public function updateData ($data, $id , $courseid)
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$track_table = $this->_name.$courseid;
		$this->_db->update($track_table, $data, 'id = ' . (int) $id);
	}

	public function checkBlock($username,$block_id,$courseid,$content_id){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$track_table = $this->_name.$courseid;

		$sql_track = $this->_db->select()
		->from($track_table)
		->where("block_id='$block_id'")
		->where("content_id='$content_id'")
		->where("username='$username'");

		//$total = $this->_db->query($sql_track)->rowCount();
		$result = $this->_db->fetchRow($sql_track);

		return $result["id"];

	}

	/* ============================================================
	* This function to read how many content on the course -> block
	* To calculate reading progress by user
	============================================================ */
	public function getCourseReadingStatus($username,$course_id){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');
		
		//get total content on thecourse block
		$sql_block = $this->_db->select()
		->from(array('bc' => $this->_block_content))
		->joinLeft(array('b'=>$this->_block),'b.id=bc.blockid')
		->where("b.courseid='$course_id'");

		$total_content = $this->_db->query($sql_block)->rowCount();


		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');
		//get how many content read
		$track_table = $this->_name.$course_id;

		$sql_track = $this->_db->select()
		->from($track_table)
		->where("course_id='$course_id'")
		->where("username='$username'");

		$total_read = $this->_db->query($sql_track)->rowCount();


		$percentage = @ceil((abs($total_read)/ abs($total_content))*100);

		return $percentage;
	}


	/* ============================================================
	* This function to read how many content for each block
	* To calculate reading progress by user
	============================================================ */
	public function getReadingStatus($username,$block_id,$courseid){


		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');
		
		//get total content
		$sql_block = $this->_db->select()
		->from($this->_block_content)
		->where("blockid='$block_id'");

		$total_content = $this->_db->query($sql_block)->rowCount();


		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		//get how many content read
		$track_table = $this->_name.$courseid;

		$sql_track = $this->_db->select()
		->from($track_table)
		->where("block_id='$block_id'")
		->where("username='$username'");

		$total_read = $this->_db->query($sql_track)->rowCount();


		$percentage = @ceil((abs($total_read)/ abs($total_content))*100);

		return $percentage;
	}


	public function getContentStatus($courseid,$content_id,$username){

		$multidb = Zend_Registry::get("multidb");
	    $this->_db = $multidb->getDb('db2');
	   
		$sql_content = $this->_db->select()
		->from($this->_content_track.$courseid)
		->where("content_id='$content_id'")
		->where("username='$username'");

		$total_content = $this->_db->query($sql_content)->rowCount();

		if($total_content>0)
		return true;
		else
		return false;

	}


	public function createtable($courseid){

		 $multidb = Zend_Registry::get("multidb");
	   $this->_db = $multidb->getDb('db2');
	   
		$courseid = trim($courseid);

		$sql1 = "CREATE TABLE IF NOT EXISTS ".$this->_name.$courseid."(
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `course_id` varchar(50) DEFAULT NULL,
					  `block_id` int(11) DEFAULT NULL,
					  `content_id` int(11) NOT NULL,
					  `username` varchar(50) DEFAULT NULL,
					  `last_read_date` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					);";


		$this->_db->query($sql1);


		$sql2 = "CREATE TABLE IF NOT EXISTS ".$this->_content_track.$courseid." (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `course_id` varchar(50) DEFAULT NULL,
				  `block_id` int(11) DEFAULT NULL,
				  `content_id` int(11) DEFAULT NULL,
				  `username` varchar(50) DEFAULT NULL,
				  `read_date` datetime DEFAULT NULL,
				  PRIMARY KEY (`id`)
				);";

		$this->_db->query($sql2);

	}
}
?>