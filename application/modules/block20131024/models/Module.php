<?php
class Block_Model_Module extends Zend_Db_Table
{
    protected $_name = "c_module";
        
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    
	public function addData ($data)
    {
        $this->_db->insert($this->_name, $data);
        return $this->_db->lastInsertId();
    }
    
    public function updateData ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    
    public function scorm_get_module_version($moduleid){
    	
    	 $sql = $this->_db->select()
    	  			      ->from($this->_name,array('version'=>'version'))    	  			     
    	  			      ->where("ID='$moduleid'");
    	
    	 $result = $this->_db->fetchRow($sql);   
    	
         return $result["version"];
    }
    
 	public function get_info($moduleid){
    	
    	//$module_type = $oModule->get_info("content","type","c_module","ID='$moduleid'");
    	 $sql = $this->_db->select()
    	  			      ->from($this->_name,array('type'=>'type'))    	  			    
    	  			      ->where("ID='$moduleid'");    	  			        	  			      
    	
    	 $result = $this->_db->fetchRow($sql);   
      
         return $result["type"];
    }
    
   
   
}
?>