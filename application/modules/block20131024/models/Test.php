<?php
class Block_Model_Test extends Zend_Db_Table
{
	protected $_name = "test";
	protected $_test_question_db = "test_question";
	protected $_result_db = "result";



	public function upload ($data)
	{
		$this->_db->insert($this->_name, $data);
	}

	public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'blockcontentid = ' . (int) $id);
	}

	public function fetch ($id = "",$tid="")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($id != "") {
			$sql->where('blockcontentid = ?', $id);
		}
		if ($tid != "") {
			$sql->where('tid = ?', $tid);
		}
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		// exit();
		return $result;
	}

	public function fetchques ($id = "")
	{
		$sql = $this->_db->select()->from($this->_test_question_db);
		if ($id != "") {
			$sql->where('blockcontentid = ?', $id);
		}
		$sql->order('countNo ASC');
		// echo $sql;
		$result = $this->_db->fetchAll($sql);
		// print_r($result);
		// exit();
		return $result;
	}

	//attempt test fetch question
	public function fetchquesfortest ($id = "", $noofques="", $seed="")
	{
		$sql = $this->_db->select()->from($this->_test_question_db);
		if ($id != "") {
			$sql->where('blockcontentid = ?', $id);
		}

		if ($noofques != "") {
			$sql->limit($noofques);
		}

		if ($seed!=""){
			$sql->order(new Zend_Db_Expr("RAND($seed)"));
		}
		else {
			$sql->order("countNo");

		}

		//echo $sql;
		$result = $this->_db->fetchAll($sql);
		// print_r($result);
		// exit();
		return $result;
	}
	//end attempt test

	public function setnoques($data,$id) {

		$this->_db->update($this->_name, $data, 'blockcontentid = ' . (int) $id);

	}

	public function remove ($qid,$blockcontentid)
	{

		//echo $qid;
		//echo $blockcontentid;
		//exit();
		$condition = array(
		'qid = ?' => $qid,
		'blockcontentid = ?' => $blockcontentid
		);


		$this->_db->delete($this->_test_question_db, $condition);

	}

	public function uploadquestion_db ($data)
	{
		$this->_db->insert($this->_test_question_db, $data);
	}

	public function countquestion_db ($blockcontentid)
	{
		$sql = $this->_db->select()
		->from(array('p' => $this->_test_question_db),
		array('countNo'));

		$sql->where('blockcontentid = ?', $blockcontentid);
		$sql->order('countNo DESC');
		$sql->limit(1);

		$result = $this->_db->fetchRow($sql);

		$countno=1;
		if($result) {
			$countno=$result["countNo"]+1;
		}

		return $countno;


	}

	function sortassessques($data,$fldid,$mvno,$bilgelung) {

		$bezantara=$mvno - $data['chgno'.$mvno];


		//#kes 1 kecik --> besar
		if ($bezantara < 0) {
			for ($g=1; $g<=$bilgelung; $g++) {
				if ($g<>$mvno & $g<=$data['chgno'.$mvno]){
					$bezanya=$mvno-$data['chgno'.$g];
					if($bezanya<0){
						$ubah=	$data['chgno'.$g]-1;

						$sdata['countNo']=$ubah;

						$this->_db->update($this->_test_question_db, $sdata, 'asqID = ' . (int) $data['fldid'.$g]);


					}
				}
			}

			$sdata['countNo']=$data['chgno'.$mvno];

			$this->_db->update($this->_test_question_db, $sdata, 'asqID = ' . (int) $data['fldid'.$mvno]);

		}

		//#kes 1 besaq --> kecik
		if ($bezantara > 0) {
			for ($g=1; $g<=$bilgelung; $g++) {
				if ($g<>$mvno & $g>=$data['chgno'.$mvno]){
					$bezanya=$mvno-$data['chgno'.$g];
					if($bezanya>0){
						$ubah=	$data['chgno'.$g]+1;

						$sdata['countNo']=$ubah;

						$this->_db->update($this->_test_question_db, $sdata, 'asqID = ' . (int) $data['fldid'.$g]);



					}
				}
			}

			$sdata['countNo']=$data['chgno'.$mvno];

			$this->_db->update($this->_test_question_db, $sdata, 'asqID = ' . (int) $data['fldid'.$mvno]);


		}

	}

	public function fetchquesid ($id = "")
	{
		$sql = $this->_db->select()->from($this->_test_question_db, array("qid"));

		if ($id != "") {
			$sql->where('blockcontentid = ?', $id);
		}

		return $sql;
	}

	public function insert_result_row($totqs,$uid,$tid,$qids)
	{


		$temp_value=array();
		for($i=0; $i < $totqs; $i++)
		{
			$temp_value[$i]='notrated';
		}
		$temp_value=implode(",",$temp_value);

		$temp_time=array();
		for($j=0; $j < $totqs; $j++)
		{
			$temp_time[$j]='0';

		}
		$temp_time=implode(",",$temp_time);



		// insert row
		$data=array('uid'=>$uid,'tid'=>$tid,'total_question'=>$totqs,'correct_answer' => $temp_value,'time_taken' => $temp_time,	'selected_answers' => $temp_value,'question_ids'=>$qids,'status'=>'2','iniTime'=>time());

		//print_r($data);


		$this->_db->insert($this->_result_db, $data);
		$id=$this->_db->lastInsertId($this->_result_db);

		return $id;


	}

	public function fetchresult ($id = "")
	{
		$sql = $this->_db->select()->from($this->_result_db);
		if ($id != "") {
			$sql->where('result_id = ?', $id);
		}
		//echo $sql;
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		// exit();
		return $result;
	}

	public function submit_answer($resultid,$qid,$courseid,$qno,$posted_answer="",$time1)
	{

		$result=$this->fetchresult($resultid);

		// getting correct answer from question
		$qbank = new Qbank_Model_Qbank();
		$answer=$qbank->get_question($qid,$courseid);


		//compare posted answer with correct answer
		$correct_answer=explode(",",$result['correct_answer']);
		$selected_answers=explode(",",$result['selected_answers']);
		$time_taken=explode(",",$result['time_taken']);
		
		//echo $qno."<br>";
		//echo $answer['answer']."<br>";
		//echo $posted_answer."<br>";
		$time2=(time()-$time1);
		$time_taken[$qno-1]=($time_taken[$qno-1])+$time2;
		
		if($posted_answer!= ""){
			if($posted_answer==$answer['answer']){ $correct_answer[$qno-1]="1"; }else{ $correct_answer[$qno-1]="0"; }
			$selected_answers[$qno-1]=$posted_answer;
		}

		$correct_answer=implode(",",$correct_answer);
		$selected_answers=implode(",",$selected_answers);
		$time_taken=implode(",",$time_taken);

		// Update result row
		$data = array('correct_answer' => $correct_answer,'selected_answers' => $selected_answers,'time_taken' => $time_taken);
		
		$this->_db->update($this->_result_db, $data, 'result_id = ' . (int) $resultid);

	}
	
	public function submit_test($resultid,$qid,$courseid,$qno,$posted_answer="",$time1,$blockcontentid)
	{
	
	//$this->submit_answer($resultid,$qid,$courseid,$qno,$posted_answer,$time1);
		
	$result=$this->fetchresult($resultid);
	
	// calculating result
	$correct_answer=$result['correct_answer'];
	$correct_answer=str_replace("notrated","0",$correct_answer);
	$correct_answer=explode(",",$correct_answer);
	$correctans=array_sum($correct_answer);
	$percentage=($correctans/$result['total_question'])*100;
	
	$test=$this->fetch($blockcontentid);
	
	//print_r($test);
	
	if($percentage>=$test['reqpercentage'])
	{
	$status="1";
	}
	else
	{
	$status="0";
	}
	
	// update result row
	$data = array('obtained_percentage' => $percentage,'status' => $status,'submitTime'=>time());
	
	//print_r($data);
	//exit();
	$this->_db->update($this->_result_db, $data, 'result_id = ' . (int) $resultid);
	//echo "finish";
	

	}




}


?>
