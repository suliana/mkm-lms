<?php
class Block_Form_Editblock extends Zend_Form
{
	
	protected $_courseid;
	
	protected $_blockid;
	
	
	public function setCourseid($value)
	{
		$this->_courseid = $value;
	}
	
	public function setBlockid($value)
	{
		$this->_blockid = $value;
	}
	
	public function init ()
    {
        
    	$this->setMethod('post');
    	$this->setAction($this->getView()->baseUrl().'/block/index/editblock');
    	
    	$title = $this->createElement('text', 'title',array('class'=>'inputtext'));
        $title->setLabel($this->getView()->translate('title').':')->setRequired(true);
        //$title->setAttribs(array('font-size' => '11px'));
        
        
        
        $ccontent = $this->createElement('textarea', 'description');
        $ccontent->setLabel($this->getView()->translate('description').':')->setRequired(false);
        
        
        
        $courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        
        $blockid = $this->createElement('hidden', 'blockid');
        $blockid->setValue($this->_blockid);
        
               
        $intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel($this->getView()->translate('semester_id').':')->setRequired(true);
		foreach ($result as $c) 
		{
			$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
		
		 $this->addElements(
        array($title, $ccontent, $intakeid, $courseid,$blockid));
		
		//button
		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('save'),
		'class'=>'btn submit',
		'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
		'label'=>$this->getView()->translate('cancel'),
		'class'=>'btn',
		'decorators'=>array('ViewHelper'),
		'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'block', 'controller'=>'index','action'=>'block','courseid' => $this->_courseid),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
		'decorators'=>array(
		'FormElements',
		array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
		'DtDdWrapper'
		)
		));
		
		
		
    }
}