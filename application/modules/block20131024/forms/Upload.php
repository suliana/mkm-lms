<?php
class Block_Form_Upload extends Zend_Form
{
		
	protected $_courseid;
	
	
	public function setCourseid($value)
	{
		$this->_courseid = $value;
	}
	
	public function init ()
    {
        
    	$this->setMethod('post');
    	$this->setAction($this->getView()->baseUrl().'/block/index/addblock');
    	
    	$title = $this->createElement('text', 'title',array('class'=>'inputtext'));
        $title->setLabel($this->getView()->translate('title').':')->setRequired(true);
        $title->setAttribs(array('font-size' => '11px'));
 
        $ccontent = $this->createElement('textarea', 'description');
        $ccontent->setLabel($this->getView()->translate('description').':')->setRequired(false);
        
        
        
        $courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        
               
        $intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel($this->getView()->translate('semester_id').':')->setRequired(true);
		foreach ($result as $c)
		{
			$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
		
		 $this->addElements( array($title, $ccontent, $intakeid, $courseid));
		
		//button
		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('upload').' '.$this->getView()->translate('block'),
		'class'=>'btn submit',
		'decorators'=>array('ViewHelper')
		));

		$this->addElement('submit', 'cancel', array(
		'label'=>$this->getView()->translate('cancel'),
		'class'=>'btn',
		'decorators'=>array('ViewHelper'),
		'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'block', 'controller'=>'index','action'=>'block','courseid' => $this->_courseid),'default',true) . "'; return false;"
		));

		$this->addDisplayGroup(array('save','cancel'),'buttons', array(
		'decorators'=>array(
		'FormElements',
		array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
		'DtDdWrapper'
		)
		));
		
		
		
    }
}