<?php
class Block_Form_Viewsemester extends Zend_Form
{
	
	protected $_intakeid;
	
	
	public function setIntakeid($value)
	{
		$this->_intakeid = $value;
	}	
	
		


	public function init ()
    {
        
    	$this->setMethod('post');
    	$this->setAction('/block/index/block');
    	$this->setAttrib('id', 'f2');
    	
    	$intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid',array('class'=>'select', 'onchange' => 'refreshblock();'));
		
		$intakeid->setLabel($this->getView()->translate('view_by').' '.$this->getView()->translate('semester_id').': ')->setRequired(true);
		$intakeid->addMultiOption("", $this->getView()->translate('please_select'));
    	foreach ($result as $c) 
    	{
			if($this->_intakeid != "")
			{
				$selected = $this->_intakeid;
				$intakeid->setValue(array($c['id'] => $selected));
			}
			else
			{
				if($c["intakestatus"]=="CURRENT")
				{
				    $selected = $c['id'];
					$intakeid->setValue(array($c['id'] => $selected));
				}
			}
			
			$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
		
		/*$intakeid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));*/
        
		$this->addElements(array($intakeid, $courseid));
        
        /*$this->setDecorators(array(
               'FormElements',
                array(array('data'=>'HtmlTag'),array('tag'=>'table')),
               'Form'
       ));*/
			
		
    }
}