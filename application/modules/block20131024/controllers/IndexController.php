<?php
class Block_IndexController extends Zend_Controller_Action 
{
	public function init()
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('course');
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
	}


	public function blockAction() 
	{	
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('block');
		
		$username = $this->userinfo->username;
		$privilage = $this->privilage;
		
		$course = new Application_Model_Course ();
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch($courseid);

		$this->view->course = $rscourse;
		
		$form = new Block_Form_Viewsemester ( array ('intakeid' => $intakeid ) );
		$this->view->form = $form;

		//$intakeid = "";
		$intakeid = $this->_getParam ( 'intakeid', "" );
		
		//tambahan for student
		if ( $privilage == "BASIC" )
		{
			$assignsubject= new Course_Model_Student();
			$rs=$assignsubject->fetch($this->userinfo->username,$courseid);
			if ($rs)
			{
				$intakeid=$rs["intakeid"];
			}
			else
			{
				$intakeid='0';	
			}
			
			//echo $intakeid;
			//exit();
		}
		//end tambahan
		
		$block = new Block_Model_Block();
		$vdata = $block->fetch ('',$courseid,$intakeid,$privilage,$username);

		$this->view->vdata = $vdata;
		
		$blocktrackDB = new Block_Model_Blocktrack();
		$blocktrackDB->createtable($courseid);	
		//$this->view->courseReadPercentage=0;
			
		$this->view->courseReadPercentage = $blocktrackDB->getCourseReadingStatus($username,$courseid);
	}

	public function addblockAction()
	{
		
		if( $this->getRequest()->isXmlHttpRequest() )
	  	{
			$this->_helper->layout->disableLayout();
	  	}
	  	
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$block = new Block_Model_Block();
		$form = new Block_Form_Upload ( array ('courseid' => $courseid ) );
		$this->view->form = $form;
		
		$course = new Application_Model_Course();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;
		
		//$form->getElement('courseid')->setValue($courseid);
		if ($this->getRequest ()->isPost ())
		{
			if ($form->isValid ( $_POST ))
		 	{
				$data = $form->getValues ();

				$data['description'] = stripslashes($data['description']);
				$data["datecreated"] = date("Y-m-d H:i:s");
				$data["createby"] = $this->userinfo->username;
				$data['coursecode'] = $rscourse['coursecode'];
				
				
				$block->upload ( $data );
				
				$this->_redirect ( 'block/index/block/id/'.$courseid.'/courseid/'.$courseid );
			}
		}
	}
	
	public function editblockAction() 
	{
		if( $this->getRequest()->isXmlHttpRequest() )
	  	{
			$this->_helper->layout->disableLayout();
	  	}

		$blockid = $this->_getParam ( 'blockid', 0 );
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$block = new Block_Model_Block();
		$form = new Block_Form_Editblock ( array ('courseid' => $courseid,'blockid' => $blockid ) );
		$this->view->form = $form;

		//$form->getElement('courseid')->setValue($courseid);
		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST )) 
			{
				$sdata = $form->getValues ();
				$sdata["datemodified"]=date("Y-m-d H:i:s");
				$sdata["modifyby"]=$this->userinfo->username;
				unset($sdata["blockid"]);
				$block->modify ( $sdata, $blockid );
				$this->_redirect ( 'block/index/block/id/'.$courseid.'/courseid/'.$courseid );
			}
		}
		else
		{
			if ($blockid > 0)
			{
				$form->populate ( $block->fetchRow ( $blockid ) );
			}
		}
	}

	public function removeblockAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$blockid = $this->_getParam ( 'blockid', 0 );
		$block = new Block_Model_Block();

		if ($blockid > 0) 
		{
			$block->delete ( $blockid );
			$this->_redirect ( 'block/index/block/id/'.$courseid.'/courseid/'.$courseid );
		}
	}

	public function addcontentAction() 
	{
		if( $this->getRequest()->isXmlHttpRequest() )
	  	{
			$this->_helper->layout->disableLayout();
	  	}
	  	
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;


		$block = new Block_Model_Block();
		$rs_type=$block->findForSelect();
		$this->view->rs_type = $rs_type;
		
		
		$username = $this->userinfo->username;


		if ($this->getRequest ()->isPost ()) 
		{
			$data = $this->getRequest()->getPost();

			//yati add for scorm
			if ($data["res_type"]==1 || $data["res_type"]==2 || $data["res_type"]==4 || $data["res_type"]==5 || $data["res_type"]==8) 
			{

				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/';

				if (! is_dir ( $uploadDir ))
				mkdir ( $uploadDir, 0775 );

				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/' . $courseid . '/';

				if (! is_dir ( $uploadDir ))
				mkdir ( $uploadDir, 0775 );

				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/' . $courseid . '/' . $blockid . '/';

				if (! is_dir ( $uploadDir ))
				mkdir ( $uploadDir, 0775 );


				//scorm
				if($data["res_type"]==8)
				{

					$uploadDir = DOC_PATH.APP_FOLDER.'/upload/blockcontent/' . $courseid . '/' . $blockid . '/scorm/';

					if (! is_dir ( $uploadDir ))
					mkdir ( $uploadDir, 0775 );

				}
				
				/* Uploading Document File on Server */

				$upload = new Zend_File_Transfer_Adapter_Http();
				$upload->setDestination($uploadDir);
				
				echo $uploadDir;
				exit;
				
				//$files = $upload->getFileInfo();
				
				//print_r($files);

				try 
				{
					// upload received file(s)
					$upload->receive('filename');

					$locationFile = $upload->getFileName('filename');

					$data ["filename"] = date ( 'Ymdhis' ) . "_" . $_FILES["filename"]["name"];

					$fullPathNameFile = $uploadDir . $data ["filename"];


					// Renommage du fichier
					$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
					$filterRename->filter ( $locationFile );


					if($data["res_type"]==8)
					{
						$moduleid = $this->readscormfile($uploadDir,$data ["filename"],$courseid,$blockid);
						
						if ( $moduleid == false )
						{
							return false;
						}
						
						$data ["moduleid"] = $moduleid;
					}



				}
				catch (Zend_File_Transfer_Exception $e)
				{
					$e->getMessage();
				}

			}

			if ($data["res_type"]==3) 
			{
				$data ["url"] = $data ["link"];
			}


			if ($data["res_type"]==6)
			{
				$data ["url"] = $data ["url"];
			}

			//Online Test
			if ($data["res_type"]==9) 
			{
				$sdata['start_time']=strtotime($data['start_time']." ".$data['Shour'].":".$data['Sminute']." ".$data['Sampm']);
				$sdata['end_time']=strtotime($data['end_time']." ".$data['Ehour'].":".$data['Eminute']." ".$data['Eampm']);
				$sdata['test_time']=$data['test_time'];
				$sdata['reqpercentage']=$data['reqpercentage'];
				$sdata['answer_view']=$data['answer_view'];
				$sdata['attempts']=$data['attempts'];
				$sdata['allow_random']=$data['allow_random'];
			}

			//Forum
			if ($data["res_type"]==12)
			{
				$forum = new Forum_Model_Forum ();
				$rsblock=$block->fetch($blockid);

				$rsforum=$forum->getcategory($courseid,$rsblock[0]["intakeid"],$courseid);

				if(!$rsforum)
				{
					//echo "sini";
					$sdata["courseID"]=$courseid;
					$sdata["semid"]=$rsblock[0]["intakeid"];
					$sdata["name"]=$courseid;
					$forum->upload("forumcat",$sdata);

					$rsforum=$forum->getcategory($courseid,$rsblock[0]["intakeid"],$courseid);
				}

				$pdata['title']=$data["title"];
				$pdata['courseid']=$courseid;
				$pdata['description']=$data["description"];
				$pdata['semid']=$rsblock[0]["intakeid"];
				$pdata['catid']=$rsforum[0]["id"];
				$pdata["dtcreated"]=date("Y-m-d H:i:s");
				$pdata["createby"]=$username;
				$forum->upload ( "forummain",$pdata );
				$data['forumid']=$block->getlastinsertid("forummain");

			}
			
			//Assignment
			if ($data["res_type"]==13) 
			{
				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/';
				
				if (! is_dir ( $uploadDir ))
				{
					mkdir ( $uploadDir, 0775 );
				}
	
				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/' . $courseid . '/';
	
				if (! is_dir ( $uploadDir ))
				{
					mkdir ( $uploadDir, 0775 );
				}
	
				// Uploading Document File on Server
	
				$upload = new Zend_File_Transfer_Adapter_Http();
				$upload->setDestination($uploadDir);
				try 
				{
					// upload received file(s)
					$upload->receive('filename2');
		
					$locationFile = $upload->getFileName('filename2');
		
		
					$data ["filename"] = date ( 'Ymdhis' ) . "_" . $_FILES["filename2"]["name"];
		
					$fullPathNameFile = $uploadDir . $data ["filename"];
		
					// Renommage du fichier
					$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
					$filterRename->filter ( $locationFile );
		
				} 
				catch (Zend_File_Transfer_Exception $e) 
				{
					$e->getMessage();
				}

				$rsblock=$block->fetch($blockid);
	
				$sdata["courseid"]=$courseid;
				$sdata["filename"] = $data["filename"];
				$sdata['semid']=$rsblock[0]["intakeid"];
				$sdata['title'] = stripslashes($data['title']);
				$sdata['description'] = stripslashes($data['description']);
				$sdata["createby"]=$username;
				$sdata['start_date']=$data['start_date'];
				$sdata['end_date']=$data['end_date'];
				$sdata['asg_type']=$data['asg_type'];

			}

			unset($data['start_time']);
			unset($data['Shour']);
			unset($data['Sminute']);
			unset($data['Sampm']);
			unset($data['end_time']);
			unset($data['Ehour']);
			unset($data['Eminute']);
			unset($data['Eampm']);
			unset($data['test_time']);
			unset($data['reqpercentage']);
			unset($data['answer_view']);
			unset($data['attempts']);
			unset($data['allow_random']);

			unset($data["courseid"]);
			unset($data["link"]);
			unset($data["save"]);

			unset($data['start_date']);
			unset($data['end_date']);
			unset($data['asg_type']);

			$data["datecreated"]=date("Y-m-d H:i:s");
			$data["createby"] = $username;
			
			//countNo
			$rscount=$block->fetchcontent($blockid,"DESC","1");
			$data["countNo"]=$rscount[0]["countNo"]+1;
			//end countNo
			$block->uploadcontent ( $data );

			//Online Test
			if ($data["res_type"]==9)
			{
				$sdata['blockcontentid'] =  $block->getlastinsertid("block_content");

				$test = new Block_Model_Test();
				$test->upload($sdata);
			}

			//Assignment
			if ($data["res_type"]==13)
			{
				$sdata["datecreated"]=$data["datecreated"];
				$sdata['blockcontentid'] =  $block->getlastinsertid("block_content");

				$assignment = new Assignment_Model_Assignment ();
				$assignment->upload($sdata);
			}
			
			$this->_redirect ( 'block/index/block/id/'.$courseid.'/courseid/'.$courseid );
		}
	}


	public function unZip($file, $path)
	{
		//$cmd="unzip -qq $file -d $path";
		//system($cmd);
		
		$zip = new ZipArchive;
		$zip->open($file);
		$zip->extractTo($path);
		$zip->close();
	}

	public function readscormfile($uploadDir,$filename,$courseid,$blockid)
	{

		$filetounzip=explode(".",$filename);
		$filetounzip=$filetounzip[0];
		$this->unZip($uploadDir.$filename,$uploadDir.$filetounzip);
		
		
		//read manifest file
		$manifest = new Block_Model_Manifest();

		$import_type ='Scorm';
		$strurl = DOC_PATH.APP_FOLDER."/upload/blockcontent/".$courseid."/".$blockid."/scorm/".$filetounzip;
		
		list($files,$newdirectory) = $manifest->dirwalk($uploadDir,$filename);

		$filename = $newdirectory."/imsmanifest.xml";

		if (file_exists($filename))
		{
			//echo "The file exists";
			$moduleid=$manifest->readmanifest($filename,$newdirectory,$courseid,$import_type,$strurl);
			
			return $moduleid;
		} 
		else 
		{
			$this->view->errorMsg = $this->view->translate('not_scorm_file');
			//echo '<br /><input type="button" value="BACK" onclick="history.back()" />';
			return false;
		}
	}



	public function modifycontentAction() 
	{
		if( $this->getRequest()->isXmlHttpRequest() )
	  	{
			$this->_helper->layout->disableLayout();
	  	}
	  	
		$username = $this->view->username;

		$id = $this->_getParam ( 'id', 0 );

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;


		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid( $id );

		$this->view->vdata=$vdata;


		$rs_type=$block->findForSelect();
		$this->view->rs_type = $rs_type;


		if ($this->getRequest ()->isPost ()) 
		{
			$data = $this->getRequest()->getPost();
			if ($id > 0) 
			{
				$sdata ["title"] = $data ["title"];
				$sdata ["description"] = $data ["description"];
				$sdata ["res_type"] = $data ["res_type"];
	
				if ($data["res_type"]==1 || $data["res_type"]==2 || $data["res_type"]==4 || $data["res_type"]==5) 
				{
					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/';
	
					if (! is_dir ( $uploadDir ))
					mkdir ( $uploadDir, 0775 );
	
					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/' . $courseid . '/';
	
					if (! is_dir ( $uploadDir ))
					mkdir ( $uploadDir, 0775 );
	
					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/' . $courseid . '/' . $blockid . '/';
	
					if (! is_dir ( $uploadDir ))
					mkdir ( $uploadDir, 0775 );
	
					/* Uploading Document File on Server */
					
					$upload = new Zend_File_Transfer_Adapter_Http();
					$upload->setDestination($uploadDir);
					
					//$files = $upload->getFileInfo();
				
					//print_r($files);
					
					if ($upload->isValid('filename')) {
						
						// upload received file(s)
						$files=$upload->receive('filename');
						
						$locationFile = $upload->getFileName('filename');
	
	
						$sdata ["filename"] = date ( 'Ymdhis' ) . "_" . $_FILES["filename"]["name"];
						$sdata["url"]="";
						$fullPathNameFile = $uploadDir . $sdata ["filename"];
	
	
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
	
	
					}
	
				}
					
				if ($data["res_type"]==3) 
				{
					$sdata ["filename"]="";
					$sdata ["url"] = $data ["link"];
	
				}
	
				if ($data["res_type"]==6)
				{
					$sdata ["filename"]="";
					$sdata ["url"] = $data ["url"];
				}
	
	
				//Online Test
				if ($data["res_type"]==9) 
				{
					$mdata['start_time']=strtotime($data['start_time']." ".$data['Shour'].":".$data['Sminute']." ".$data['Sampm']);
					$mdata['end_time']=strtotime($data['end_time']." ".$data['Ehour'].":".$data['Eminute']." ".$data['Eampm']);
					$mdata['test_time']=$data['test_time'];
					$mdata['reqpercentage']=$data['reqpercentage'];
					$mdata['answer_view']=$data['answer_view'];
					$mdata['attempts']=$data['attempts'];
					$mdata['allow_random']=$data['allow_random'];
	
				}
	
				//Assignment
				if ($data["res_type"]==13) 
				{
					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/';
	
					if (! is_dir ( $uploadDir ))
					mkdir ( $uploadDir, 0775 );
	
					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/' . $courseid . '/';
	
					if (! is_dir ( $uploadDir ))
					mkdir ( $uploadDir, 0775 );
	
					/* Uploading Document File on Server */
					$upload = new Zend_File_Transfer_Adapter_Http();
					$upload->setDestination($uploadDir);
					
					if ($upload->isValid('filename2')) 
					{
						// upload received file(s)
						$upload->receive('filename2');
	
						//
						$locationFile = $upload->getFileName('filename2');
	
	
						$sdata ["filename"] = date ( 'Ymdhis' ) . "_" . $_FILES["filename2"]["name"];
						$mdata["filename"] = $sdata["filename"];
						$sdata["url"]="";
	
						$fullPathNameFile = $uploadDir . $sdata ["filename"];
	
	
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
	
	
					}
	
					$rsblock=$block->fetch($blockid);
	
					$mdata["courseid"]=$courseid;
					$mdata['semid']=$rsblock[0]["intakeid"];
					$mdata['title'] = stripslashes($data['title']);
					$mdata['description'] = stripslashes($data['description']);
					$mdata["modifyby"]=$username;
					$mdata['start_date']=$data['start_date'];
					$mdata['end_date']=$data['end_date'];
					$mdata['asg_type']=$data['asg_type'];
	
	
				} // assignment
	
				$sdata["datemodified"]=date("Y-m-d H:i:s");
				$sdata["modifyby"]=$username;
				//print_r($sdata);
				//print_r($mdata);
				//exit();
				$block->modifycontent ( $sdata, $id );
	
				//Forum
				if ($data["res_type"]==12) 
				{
					$forum = new Forum_Model_Forum ();
	
					$pdata['title']=$sdata["title"];
					$pdata['description']=$sdata["description"];
					$pdata["dtmodified"]=date("Y-m-d H:i:s");
					$pdata["modifyby"]=$username;
	
					$forum->modify ( "forummain",$pdata, $vdata[0]['forumid'],"forumID" );
				}
	
	
				//Online Test
				if ($data["res_type"]==9) 
				{
					$test = new Block_Model_Test();
					$test->modify($mdata,$id);
				}
	
				//Assignment
				if ($data["res_type"]==13) 
				{
					$mdata["datemodified"]=$sdata["datemodified"];
	
					$assignment = new Assignment_Model_Assignment ();
					$assignment->modifyblockcontent($mdata,$id);
				}
			}
			
			$this->_redirect ( 'block/index/block/id/'.$courseid.'/courseid/'.$courseid );
		}
	}
	
	
	public function deletecontentAction() 
	{
		$block = new Block_Model_Block();
		$id = $this->_getParam ( 'rowid', 0 );

		$courseid = $this->_getParam ( 'courseid', '' );
		//echo $id;
		//exit();
		if ($id > 0) 
		{
			$block->deletecontent ( $id );
			
			$assignment = new Assignment_Model_Assignment ();
			$assignment->delete($id,'blockcontentid');
				
			$this->_redirect ( 'block/index/block/id/'.$courseid.'/courseid/'.$courseid );
		}
	}


	public function displayresourceAction() 
	{
		$this->_helper->layout->disableLayout();
		
		$id = $this->_getParam ( 'id', 0 );

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;
	}

	public function framepage1Action() 
	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('block')." > Microsoft Office Files/Pdf Files";

		$id = $this->_getParam ( 'id', 0 );

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata = $vdata;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

	}

	public function framepageembedAction() 
	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('block')." > Youtube";
	
		$id = $this->_getParam ( 'id', 0 );

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;


		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

	}

	public function displayresourceembedAction() 
	{
		$id = $this->_getParam ( 'id', 0 );

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;
	}

	public function displayresource2Action()
	{
		$this->_helper->layout->disableLayout();
		
		$id = $this->_getParam ( 'id', 0 );

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;
	}

	public function framepage2Action() 
	{
		//$this->_helper->layout->setLayout('course');		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('block')." > Youtube";
		
		$id = $this->_getParam ( 'id', 0 );

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

	}
	
	public function ajaxSaveTrackAction($id=null)
	{
		$block_id = $this->_getParam('block_id', 0);
    	$content_id = $this->_getParam('content_id', 0);
    	$course_id = $this->_getParam('course_id', 0);
     
     	if ($this->getRequest()->isXmlHttpRequest()) 
     	{
            $this->_helper->layout->disableLayout();
        }
        
     	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();
        
        $btrackDB = new Block_Model_Blocktrack();
          
        //create table if not exist
        $btrackDB->createtable($course_id);
        
        
        //get info
        $info["username"] = $this->userinfo->username;
        $info["course_id"]= $course_id;
        $info["block_id"] = $block_id;
        $info["content_id"] = $content_id;
        $info["last_read_date"] = date("Y-m-d H:i:s");
        //$info["content_id"] = $content_id;
        

       
        //check in table track block&username is already exist?
        $id = $btrackDB->checkBlock($this->userinfo->username,$block_id,$course_id,$content_id);
        
		if ($id != '' || $id!=0)
        {
        	$infoupd["last_read_date"] = date("Y-m-d H:i:s");
        	$btrackDB->updateData($infoupd,$id,$course_id);
       	}
       	else
       	{
       		echo '<br />'.$info_data = $btrackDB->addTrack($info,$course_id);
        }
        
        //add trace content
        $cinfo["username"] = $this->userinfo->username;
        $cinfo["course_id"]= $course_id;
        $cinfo["block_id"] = $block_id;
        $cinfo["content_id"] = $content_id;
        $cinfo["read_date"]  = date("Y-m-d H:i:s");
        
        $btrackDB->addcontenttrack($cinfo,$course_id);
                
		$ajaxContext->addActionContext('view', 'html')
                    ->addActionContext('form', 'html')
                    ->addActionContext('process', 'json')
                    ->initContext();

		$json = Zend_Json::encode($info);
		$this->view->json = $json;

    }
}





