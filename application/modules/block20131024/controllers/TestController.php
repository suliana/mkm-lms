<?php
class Block_TestController extends Zend_Controller_Action {

	public function init() 
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('course');
		//$this->_helper->layout->disableLayout();
		$this->view->course_tools = 1;
		
		$storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->view->active = 'admin';
		$this->privilage = $data->privilage;
	}

	public function indexAction() 
	{
		//$this->_helper->layout->setLayout('course');
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$this->view->username = $data->username;

		$id = $this->_getParam ( 'id', 0 );

		$this->view->id=$id;

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$course = new Application_Model_Course ();

		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

		$test = new Block_Model_Test();

		$act = $this->_getParam ( 'act', 0 );

		if ($act=="setnoques")
		{

			if ($this->getRequest ()->isPost ()) 
			{
				$data = $this->getRequest()->getPost();

				$sdata['random_question_no']=$data['pick1'];

				$test->setnoques($sdata,$id);
			}
		}

		if ($act=="sort")
		{
			if ($this->getRequest()->isPost ()) 
			{
				$data = $this->getRequest()->getPost();

				$fldid = $this->_getParam ( 'fldid', 0 );

				$mvno = $this->_getParam ( 'mvno', 0 );

				$bilgelung = $this->_getParam ( 'bilgelung', 0 );

				$test->sortassessques($data,$fldid,$mvno,$bilgelung);

			}

		}

		$this->view->rowtest=$test->fetch( $id );

		$select=$test->fetchques( $id );
		$this->view->allrecord = count($select);


		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

	}

	public function removeAction() 
	{
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$test = new Block_Model_Test();

		$qid = $this->_getParam ( 'qid', 0 );

		$courseid = $this->_getParam ( 'courseid', 0 );

		$blockid = $this->_getParam ( 'blockid', 0 );

		$id = $this->_getParam ( 'id', 0 );

		if ($qid > 0)
		{
			$test->remove ($qid,$id);
			$this->_redirect ( 'block/test/index/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );
		}
	}

	public function beforetestAction() 
	{
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		
		$username=$data->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test');


		$id = $this->_getParam ( 'id', 0 );
		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;


		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid( $id );

		$this->view->vdata=$vdata;

		$test = new Block_Model_Test();


		$rs=$test->fetch( $id );

		if ($rs["random_question_no"]!=null)
		{
			$this->view->noques=$rs["random_question_no"];
		}
		else
		{
			$select=$test->fetchques( $id );
			$this->view->noques = count($select);
		}


	}

	public function attempttestAction() 
	{
		$this->_helper->layout->setLayout('exam');
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		
		$username=$data->username;
		$uid=$data->id;

		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

		$courseid = $this->_getParam ( 'courseid', 0 );


		$test = new Block_Model_Test();

		$rs=$test->fetch( $id );

		$noques="";

		if ($rs["random_question_no"]!=null)
		{
			$noques=$rs["random_question_no"];
			$this->view->totalq = $noques;
		}
		else
	 	{
			$select=$test->fetchques( $id );
			$noques = count($select);
			$this->view->totalq = $noques;
		}

		if ($rs["allow_random"]=="1")
		{
			$select=$test->fetchquesfortest( $id, $noques,$_COOKIE["randvalue"] );
		} 
		else
		{
			$select=$test->fetchquesfortest( $id, $noques );
		}

		if ( isset($_COOKIE['tid']) && isset($_COOKIE['qids']) && isset($_COOKIE['resultid']) && isset($_COOKIE['randvalue']) && isset($_COOKIE['access_token']) )
		{

			$rsresult=$test->fetchresult($_COOKIE['resultid']);

			$this->view->rsresult= $rsresult;
			$balancetime=time()-$rsresult["iniTime"];

			//echo $balancetime ;

			$this->view->timer= ($rs["test_time"]*60) - $balancetime;

		}
		else 
		{

			//if

			if ( isset($_COOKIE['randvalue']) && isset($_COOKIE['access_token']) )
			{
				//echo "sinilah";
				$this->view->timer=$rs["test_time"]*60;

				$qid=array();
				foreach ($select as $quesid) 
				{
					$qid[]=$quesid["qid"];
				}

				$qids=implode(",",$qid);

				$resultid=$test->insert_result_row($noques,$uid,$rs["tid"],$qids);

				setcookie("resultid", $resultid, time()+86400, "/block/test");
				setcookie("qids", $qids, time()+86400, "/block/test");
				setcookie("tid", $rs["tid"], time()+86400, "/block/test");

				//echo "sdasdas".$_COOKIE['resultid'];
				$rsresult=$test->fetchresult($resultid);

				$this->view->rsresult= $rsresult;
			}
			else 
			{
				$this->_redirect ( 'block/test/beforetest/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );
			}


			//$cookie = array('name'=>'qno','value'=>'0','expire'=>'86500');
			//$this->input->set_cookie($cookie);

		}


		//$this->view->allrecord = count($select);


		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 1);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

	}

	public function submitanswerAction() 
	{
		$this->_helper->layout->disableLayout();

		$courseid = $this->_getParam ( 'courseid', 0 );

		$qid = $this->_getParam ( 'qid', 0 );
		$qno = $this->_getParam ( 'qno', 0 );
		$answer = $this->_getParam ( 'answer', "" );
		$time1 = $this->_getParam ( 'time1', 0 );

		//echo "Time 1".$time1."<br>";
		$test = new Block_Model_Test();
		$test->submit_answer($_COOKIE["resultid"],$qid,$courseid,$qno,$answer,$time1);

		$rsresult=$test->fetchresult($_COOKIE['resultid']);
		$time_taken=explode(',',$rsresult['time_taken']);
		$newtime1= (array_sum($time_taken)+$rsresult['iniTime']);
		echo $newtime1;


	}

	public function submittestAction() 
	{
		$this->_helper->layout->disableLayout();

		//echo "sini";
		//print_r($_COOKIE);
		//exit();
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$qid = $this->_getParam ( 'qid', 0 );
		$qno = $this->_getParam ( 'qno', 0 );
		$answer = $this->_getParam ( 'answer', "" );
		$time1 = $this->_getParam ( 'time1', 0 );
		$blockcontentid = $this->_getParam ( 'id', 0 );


		$test = new Block_Model_Test();
		$test->submit_test($_COOKIE["resultid"],$qid,$courseid,$qno,$answer,$time1,$blockcontentid);

		$this->_redirect ( 'block/test/successsubmit/courseid/'.$courseid.'/resultid/'.$_COOKIE["resultid"] );
	}

	public function successsubmitAction()
	{
		//if (isset($_COOKIE["resultid"]) || isset($_COOKIE["qids"]) || isset($_COOKIE["tid"]) || isset($_COOKIE["access_token"]) || isset($_COOKIE["randvalue"])){
		setcookie("resultid", "", time()+1, "/block/test");
		setcookie("qids", "", time()+1, "/block/test");
		setcookie("tid", "", time()+1, "/block/test");
		setcookie("access_token", "", time()+1, "/block/test");
		setcookie("randvalue", "", time()+1, "/block/test");
		//}

		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test');

		$resultid = $this->_getParam ( 'resultid', 0 );
		$this->view->resultid = $resultid;

	}

	public function viewresultAction()
	{

		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$test = new Block_Model_Test();

		$resultid = $this->_getParam ( 'view', 0 );

		$result=$test->fetchresult($resultid);

		$user = new Users_Model_Users();

		$rsuser=$user->fetch($result['uid']);

		if ($rsuser['username'] == $data->username)
		{
			$this->view->user =$rsuser;
			$this->view->result = $result;
		}
		else
		{
			$this->view->result = "You are not allowed to view this result";
		}
	}
}
?>