<?php
class Course_IndexController extends Zend_Controller_Action
{
	public function init() 
	{
		//$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
	}


	public function listcourseAction() 
	{
		$course = new Application_Model_Course ();
		
		if ($this->_request->getParam('keywords') || $this->getRequest()->isPost())
		{
			$keywords = $this->_request->getParam('keywords',"");
			$this->view->keywords = strip_tags(trim($keywords));
			
			$rscourse = $course->searchcourse($keywords);
		}
		else
		{
			$rscourse = $course->listcourse ();
		}

		//echo $data->username;
		
		$this->view->course = $rscourse;
		
		$_page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($rscourse); //instance of the pagination
		$paginator->setItemCountPerPage(50);
		$paginator->setCurrentPageNumber($_page);

		$this->view->paged_results = $paginator;
	
		//Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
	}
	
	public function copyAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course();
		$rscourse = $course->listcourse($courseid);
		$this->view->rscourse = $rscourse;
		
		
		if ( !is_array($rscourse) )
		{
			$this->_redirect('course/index/listcourse');
		}
		
		//sem
		$intake = new Admin_Model_Intake ();
        $intake_results =  $intake->fetchCurrent();
        
        $this->view->intake_results = $intake_results;
        
        $block = new Block_Model_Block();
		$actdata = $block->fetch ('',$courseid);
		
		// COPY STUFF
		if ($this->getRequest()->isPost ()) 
		{
			$formData = $this->getRequest()->getPost();
			
			$semdata = count($formData['intake']);
			
			if ( $semdata == 0 )
			{
				//throw error Exception: sem data cannot be empty
				$this->_redirect('course/index/listcourse');
			}
			
			if ( $formData['content'] == '' )
			{
				//throw error Exception: content cannot be empty.
				$this->_redirect('course/index/listcourse');
			}
			
			$block_ids = explode(',',$formData['content']);
		
			foreach( $actdata as $block_data )
			{
				if ( in_array($block_data['id'], $block_ids) )
				{
					foreach ( $formData['intake'] as $intake_id )
					{
						$data = array(
										'courseid'		=>	$block_data['courseid'],
										'coursecode'	=>	$block_data['coursecode'],
										'title'			=>	$block_data['title'],
										'description'	=>	$block_data['description'],
										'blocktype'		=>	$block_data['blocktype'],
										'intakeid'		=>	$intake_id,
										'datecreated'	=>  new Zend_Db_Expr('NOW()'),
										'datemodified'	=>	'0000-00-00 00:00:00',
										'createby'		=>  'System',
										'audience'		=> 	'public' 
						
								);
								
						$block->upload($data);
					}
				}
			}
			
			$this->_redirect('index/content/courseid/'.$courseid);
		}
	}
	
	public function contentAction()
	{
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		
		$course = new Application_Model_Course();
		$rscourse = $course->listcourse($courseid);
		$this->view->rscourse = $rscourse;
		
		
		if ( !is_array($rscourse) )
		{
			$this->_redirect('course/index/listcourse');
		}
		
		//sem
		$intake = new Admin_Model_Intake ();
        $intake_results =  $intake->fetchCurrent();
        
        $this->view->intake_results = $intake_results;
        
        $block = new Block_Model_Block();
		$actdata = $block->fetch ('',$courseid);
		$this->view->data = array();
		
		if ( is_array($actdata) )
		{
			foreach( $actdata as $data )
			{
				$this->view->data[$data['intakeid']][] = $data;
			}
		}
		
		// COPY STUFF
		if ($this->getRequest()->isPost ()) 
		{
			$formData = $this->getRequest()->getPost();
			$selected =  count($formData['content']);
			
			$this->view->selected_id = array();
			
			if ( $selected == 0 )
			{
				$this->view->errorMsg = $this->view->translate('No content selected');
				return false;
			}
			else
			{
				$selected_id = array();
				foreach ( $formData['content'] as $sel_id )
				{
					$selected_id[] = $sel_id;
				}
				
				$this->view->selected_id = $selected_id;
			}
			
			$this->view->intake_code_name = '';
			if ( is_array($intake_results) )
			{
				foreach( $intake_results as $intake_row )
				{
					if ( $formData['intakeid'] == $intake_row['id'] )
					{
						$this->view->intake_code_name = $intake_row['intakecode'];
					}
				}		
			}
			
			$this->view->formdata = $formData;
			
			$this->_helper->viewRenderer('content-copy');
		}
		
		
        
       // print_R($intake_results);
	}

	public function propertyAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}

	//Coordinator Part//
	/////////////////////////////////////////////////////////////////////////////////

	public function listcoordinatorAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_Search ();
		$this->view->form = $form;
		$coordinator = new Course_Model_Coordinator ();
		
		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid($_POST)) 
			{
				$data = $form->getValues ();
				Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
				$select = $coordinator->findreturnselect ( $courseid, $data ['keyword'] );
				$paginator = Zend_Paginator::factory ( $select );
				$paginator->setItemCountPerPage ( 50 );
				$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
				$this->view->paginator = $paginator;
			}
		} 
		else 
		{
			//$this->view->entries = $activity->fetchAll();
			Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
			$select = $coordinator->returnselect ( $courseid );
			$paginator = Zend_Paginator::factory ( $select );
			$paginator->setItemCountPerPage ( 50 );
			$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
			$this->view->paginator = $paginator;
		}
	}

	public function assigncoordinatorAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_Search ();
		$this->view->form = $form;
		$coordinator = new Course_Model_Coordinator ();

		$coordinator->deletecusertemp();
		$coordinator->copytocusertemp($courseid);

		if ( $this->getRequest()->isPost() ) 
		{
			if ($form->isValid ( $_POST ))
			{
				$data = $form->getValues ();
				Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
				$select = $coordinator->findreturnselect2 ($data ['keyword'] );
				$paginator = Zend_Paginator::factory ( $select );
				$paginator->setItemCountPerPage ( 50 );
				$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
				$this->view->paginator = $paginator;
			}
		} 
		else 
		{
			//$this->view->entries = $activity->fetchAll();
			Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
			$select = $coordinator->returnselect2 ();
			$paginator = Zend_Paginator::factory ( $select );
			$paginator->setItemCountPerPage ( 50 );
			$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
			$this->view->paginator = $paginator;
		}

		//$coordinator->deletecusertemp();
	}

	public function deleteAction() 
	{
		$courseid = $this->_getParam ( 'courseid', '' );
		$coordinator = new Course_Model_Coordinator();

		$username = $this->_getParam ( 'username', 0 );
		if ($username) 
		{
			$coordinator->deletecoordinator($username,$courseid );
			$this->_redirect ( 'course/index/listcoordinator/courseid/'.$courseid );
		}
	}

	public function assignAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$pageitem = $this->_getParam ( 'pageitem', 0 );

		for ($ctr = 1 ; $ctr <= $pageitem ; $ctr++ )  
		{
			$sdata['username']=$this->_getParam ( 'C'.$ctr, 0 );
			$sdata['courseid']=$courseid;

			if ($sdata['username'] != '')
			{
				$coordinator = new Course_Model_Coordinator();
				$coordinator->upload($sdata);
			}
		}
		
		$this->_redirect ( 'course/index/listcoordinator/courseid/'.$courseid);
	}
	
	//End Coordinator Part//
	///////////////////////////////////////////////////////////////////////////////////////////
	//Grader Part//
	////////////////////////////////////////////////////////////////////////////////////////////

	public function listgraderAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SearchplusIntake ();
		$this->view->form = $form;
		$grader = new Course_Model_Grader ();
		$select = $grader->findreturnselect ( $courseid, "xxx" );

		//capture the input params
		if ($this->_request->getParam('intakeid',0) || $this->_request->isPost())
		{
			if ($this->_request->getParam('intakeid')!="xxx")
			{
				//echo "here";
				//if it is a post, populate the form and reset the page to 1
				if ($this->_request->isPost())
				{
					$form->populate($this->_request->getPost());
					$page = 1;
				}
				else
				{
					//'search' showed up via the GET param
					$form->populate($this->_request->getParams());
				}
				//make sure the submitted data is valid and modify select statement
				if ($form->valid())
				{
					$data = $form->getValues ();
					$select = $grader->findreturnselect ( $courseid, $data ['intakeid'],$data ['keyword'] );
				}
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));
	}

	public function assigngraderAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SearchplusIntake ();
		$this->view->form = $form;
		$grader = new Course_Model_Grader ();
		$select = $grader->findreturnselect2 ("xxx");
		//capture the input params
		//echo $this->_request->getParam('intakeid');
		
		if ($this->_request->getParam('intakeid',0) || $this->_request->isPost())
		{
			if ($this->_request->getParam('intakeid')!="xxx")
			{
				//if it is a post, populate the form and reset the page to 1
				if ($this->_request->isPost())
				{
					$form->populate($this->_request->getPost());
					$page = 1;
				}
				else
				{
					//'search' showed up via the GET param
					$form->populate($this->_request->getParams());
				}

				//make sure the submitted data is valid and modify select statement
				if ($form->valid())
				{
					$data = $form->getValues ();
					$grader->deletecusertemp();
					//echo $data ['intakeid'];

					$grader->copytocusertemp($courseid,$data ['intakeid']);
					$select = $grader->findreturnselect2 ($data ['keyword'] );
				}

			}
		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->intakeid = $form->getValue('intakeid');
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));

		//$grader->deletecusertemp();


	}



	public function deletegraderAction() 
	{
		$courseid = $this->_getParam ( 'courseid', '' );

		$grader = new Course_Model_Grader();
		$username = $this->_getParam ( 'username', 0 );
		$semid = $this->_getParam ( 'semid', 0 );
		
		if ($username && $semid) 
		{
			$grader->deletegrader ( $username,$courseid,$semid );
			$this->_redirect ( 'course/index/listgrader/courseid/'.$courseid );
		}
	}

	public function assignggraderAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$pageitem = $this->_getParam ( 'pageitem', 0 );
		$intakeid = $this->_getParam ( 'intakeid', 0 );

		for ($ctr = 1 ; $ctr <= $pageitem ; $ctr++ )  
		{
			$sdata['username']=$this->_getParam ( 'C'.$ctr, 0 );
			$sdata['courseid']=$courseid;
			$sdata['intakeid']=$intakeid;
			
			if ($sdata['username']!="")
			{
				$grader = new Course_Model_Grader();
				$grader->upload($sdata);

			}
		}

		$this->_redirect ( 'course/index/listgrader/courseid/'.$courseid);
	}

	//End Grader Part//
	///////////////////////////////////////////////////////////////////////////////////////////


	//Student Part//
	////////////////////////////////////////////////////////////////////////////////////////////

	public function liststudentAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SearchplusIntake ();
		$this->view->form = $form;
		
		$student = new Course_Model_Student ();
		$select = $student->findreturnselect ( $courseid, "xxx" );

		//capture the input params
		if ($this->_request->getParam('intakeid',0) || $this->_request->isPost())
		{
			if ($this->_request->getParam('intakeid')!="xxx")
			{
				//echo "here";
				//if it is a post, populate the form and reset the page to 1
				if ($this->_request->isPost())
				{
					$form->populate($this->_request->getPost());
					$page = 1;
				}
				else
				{
					//'search' showed up via the GET param
					$form->populate($this->_request->getParams());
				}
				//make sure the submitted data is valid and modify select statement
				if ($form->valid())
				{
					$data = $form->getValues ();
					$select = $student->findreturnselect ( $courseid, $data ['intakeid'],$data ['keyword'] );
				}
			}
		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));

	}

	public function assignstudentAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SearchplusIntake ();
		$this->view->form = $form;
		$student = new Course_Model_Student ();
		$select = $student->findreturnselect2 ("xxx");
		//capture the input params
		//echo $this->_request->getParam('intakeid');
		
		if ($this->_request->getParam('intakeid',0) || $this->_request->isPost())
		{
			if ($this->_request->getParam('intakeid')!="xxx")
			{
				//if it is a post, populate the form and reset the page to 1
				if ($this->_request->isPost())
				{
					$form->populate($this->_request->getPost());
					$page = 1;
				}
				else
				{
					//'search' showed up via the GET param
					$form->populate($this->_request->getParams());
				}

				//make sure the submitted data is valid and modify select statement
				if ($form->valid())
				{
					$data = $form->getValues ();
					$student->deletecusertemp();
					//echo $data ['intakeid'];

					$student->copytocusertemp($courseid,$data ['intakeid']);
					$select = $student->findreturnselect2 ($data ['keyword'] );
				}


			}
		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->intakeid = $form->getValue('intakeid');
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));


		//$student->deletecusertemp();


	}



	public function deletestudentAction() 
	{
		$courseid = $this->_getParam ( 'courseid', '' );

		$student = new Course_Model_Student();

		$username = $this->_getParam ( 'username', 0 );
		$semid = $this->_getParam ( 'semid', 0 );
		
		if ($username && $semid) 
		{
			$student->deletestudent( $username,$courseid,$semid );
			$this->_redirect ( 'course/index/liststudent/courseid/'.$courseid );
		}
	}

	public function assignsstudentAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$pageitem = $this->_getParam ( 'pageitem', 0 );
		$intakeid = $this->_getParam ( 'intakeid', 0 );

		for ($ctr = 1 ; $ctr <= $pageitem ; $ctr++ )  
		{
			$sdata['username']=$this->_getParam ( 'C'.$ctr, 0 );
			$sdata['courseid']=$courseid;
			$sdata['intakeid']=$intakeid;

			if ($sdata['username']!="")
			{
				$student = new Course_Model_Student();
				//print_r($sdata);
				//exit();
				$student->upload($sdata);
			}
		}
		
		$this->_redirect ( 'course/index/liststudent/courseid/'.$courseid);
	}

	//End Student Part//
	///////////////////////////////////////////////////////////////////////////////////////////

	public function taggraderAction ()
	{
		//		$courseid = $this->_getParam ( 'courseid', 0 );
		//		$this->view->courseid = $courseid;
		//
		//		$intake = new Admin_Model_Intake ();
		//		$fetchAllintake=$intake->fetchAll();
		//		$this->view->fetchAllintake = $fetchAllintake;
		//////////

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SelectIntake ();
		$this->view->form = $form;

		$intake = new Admin_Model_Intake ();
		$fetchAllintake=$intake->fetchAll();

		$intakeid = $this->_getParam ( 'intakeid', 0 );
		$this->view->intakeid = $intakeid;

		$grader = new Course_Model_Oraconn ();

		$rs=$grader->fetchAll();

		$this->view->rsgrader=$rs;

		//
		//		//capture the input params
		//		if ($this->_request->getParam('intakeid',0) || $this->_request->isPost()){
		//			if ($this->_request->getParam('intakeid')!="xxx"){
		//				//echo "here";
		//				//if it is a post, populate the form and reset the page to 1
		//				if ($this->_request->isPost()){
		//					$form->populate($this->_request->getPost());
		//					$page = 1;
		//				}
		//				else{
		//					//'search' showed up via the GET param
		//					$form->populate($this->_request->getParams());
		//				}
		//				//make sure the submitted data is valid and modify select statement
		//				if ($form->valid()){
		//					$data = $form->getValues ();
		//					$select = $grader->findreturnselect ( $courseid, $data ['intakeid'],$data ['keyword'] );
		//				}
		//			}
		//
		//		}
		//
		//		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		//		$paginator = Zend_Paginator::factory ( $select );
		//		$paginator->setItemCountPerPage ( 1 );
		//		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		//		$this->view->paginator = $paginator;
		//		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));

	}

	public function addtagAction ()
	{
		$intakeid = $this->_getParam ( 'intakeid', 0 );
		$this->view->intakeid = $intakeid;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$checked = $this->_getParam ( 'chk', 0 );
		$this->view->checked = $checked;

		if ($this->getRequest()->isPost()) 
		{
			$formData = $_POST;
			$tagging=new Course_Model_Tagging();

			//			print_r($formData["assign"]);

			for ($i=0; $i<$checked; $i++)
			{
				//get each grader and add to a group
				$grader=$formData["assign"][$i];

				$vall=explode(":", $grader);

				$graderid = $vall[0];
				$graderic = $vall[1];

				//before insert check existence in table
				$checkgrader=$tagging->checkgrader($graderid,$courseid,$intakeid);

				if ($checkgrader)
				{
					//$checkgrader["tutorname"];
					$idTag = $checkgrader["id"];
				}
				else
				{
					//insert to taggingsetup
					$maindata["courseid"]=$courseid;
					$maindata["tutorname"]=$graderid;
					$maindata["tutoric"]=$graderic;
					$maindata["datecreated"]=date("Y-m-d");
					$maindata["intakeid"]=$intakeid;
					$maindata["taggedby"]=$data->username;
					$idTag = $tagging->addtaggingsetup ( $maindata );
				}

				if($checked==1)
				{
					$graderlist=$idTag;
				}
				else
				{
					if ($i==0)
					{
						$graderlist .=$idTag;
					}
					else
					{
						$graderlist .=",".$idTag;
					}
				}
			}

			//once insert get grader of each intake and assign to students randomly
			$in_take = new Admin_Model_Intake ();
			$result =  $in_take->fetch($intakeid,'');
			$intakecode = $result["intakecode"];

			$allstudent = new Course_Model_Student();
			//    		$allstud=$allstudent->findallstudent($courseid, $intakeid, $intakecode);
			$allstud = $tagging->findallstudent($courseid, $intakeid, $intakecode);

			if (empty($allstud))
			{
				$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
				$this->view->messages =$this->_helper->flashMessenger->addMessage("All Student Tagged OR No student registered for this course $courseid");

				$this->_redirect ( 'course/index/listtag/courseid/'.$courseid);
			}
			else
			{
				$a=1;
				foreach ($allstud as $all)
				{
					//    			$all["id"];$all["courseid"];$all["firstname"];
					$studentid=$all["username"];
					if ($a==1)
					{
						$students .=$studentid;
					}
					else
					{
						$students .=",".$studentid;
					}
					
					$a++;
				}
				
				$student = explode(',', $students);
				$stdCount = count($student);
				//				echo "<br>COUNT_STUDENT=".$stdCount."<br>";

				if ($grader!="")
				{
					$graderlist = explode(',', $graderlist);
					$gradeCount = count($graderlist);
					//			        echo "COUNT_GRADER=".$gradeCount."<br>";

					for($i=0; $i<$stdCount ;$i++)
					{
						$gdrid=$graderlist[$i%$gradeCount];
						$student[$i];
						//						echo "<br>".$gdrid." ".$i." ".$student[$i];

						try
						{
							//insert into tagginguser
							$subdata["groupID"]=$gdrid;
							$subdata["userID"]=$student[$i];
							$subdata["status"]=1;

							$idTag = $tagging->addtagginguser ( $subdata );
						}
						catch (Exception $e)
						{
							$this->_helper->flashMessenger->addMessage("Error While Assigning");
						}


						if($idTag!=null){
							$this->_helper->flashMessenger->addMessage("Successfuly Assigned");
						}
					}
				}
			}
		}

		$this->_redirect ( 'course/index/listtag/courseid/'.$courseid);
	}

	public function ajaxGetIntakeAction($id=null, $courseid=null)
	{
		$id = $this->_getParam('id', 0);
		$courseid = $this->_getParam('courseid', 0);
		//    	echo $courseid." - ".$id;

		if ($this->getRequest()->isXmlHttpRequest()) 
		{
			$this->_helper->layout->disableLayout();
		}

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('view', 'html');
		$ajaxContext->initContext();

		$config = new Zend_Config(
		array(
				'database' => array
							(
								'adapter' => 'oracle',
								'params' => array(
								'dbname'=> '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = mycampusdb.oum.edu.my)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = icems)))',
								'username' => 'icem_mmls',
								'password' => 'icem_mmls')
							)
			)
		);

		$db = Zend_Db::factory($config->database);
		Zend_Db_Table::setDefaultAdapter($db);
		Zend_Registry::set('db', $db);
		$sql = 'SELECT VTG_GRADER_NAME, VTG_GRADER_UNAME FROM VIEW_TUTOR_GRADER where VTG_SEM='.$id.' AND rownum<3';
		$result = $db->fetchAll($sql);

		//	  	$courseDB = new App_Model_Record_DbTable_Course();
		//		$courseData = $courseDB->getFacultyCourse($id);


		$ajaxContext->addActionContext('view', 'html')
		->addActionContext('form', 'html')
		->addActionContext('process', 'json')
		->initContext();

		$json = Zend_Json::encode($result);

		$this->view->json = $json;

	}

	public function listtagAction ()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$msg = $this->_flashMessenger->getMessages();
		if($msg!=null)
		{
			$this->view->noticeMessage = $msg[0];
		}
	}


	public function manualtagAction ()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SearchplusIntake2 ();
		$this->view->form = $form;
		$tagging = new Course_Model_Tagging ();
		$select = $tagging->findreturnselect ( $courseid, "xxx" );

		//capture the input params
		if ($this->_request->getParam('intakeid',0) || $this->_request->getParam('ulc',0) || $this->_request->isPost())
		{
			if ($this->_request->getParam('intakeid')!="xxx" && $this->_request->getParam('ulc')!="xxx")
			{
				$intakeid = $this->_request->getParam('intakeid');
				$this->view->intakeid = $intakeid;

				$ulc = $this->_request->getParam('ulc');
				$this->view->ulc = $ulc;
				//				echo "here".$ulc." ".$intakeid;
				//				exit;
				//if it is a post, populate the form and reset the page to 1
				if ($this->_request->isPost())
				{
					$form->populate($this->_request->getPost());
					$page = 1;
				}
				else
				{
					//'search' showed up via the GET param
					$form->populate($this->_request->getParams());
				}
				
				//make sure the submitted data is valid and modify select statement
				if ($form->valid())
				{
					$data = $form->getValues ();
					$select = $tagging->findreturnselect ( $courseid, $data ['intakeid'],$data ['ulc'],$data ['keyword']);
				}
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));

	}

	public function addtagmanualAction ()
	{
		$intakeid = $this->_getParam ( 'intakeid', 0 );
		$this->view->intakeid = $intakeid;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$checked = $this->_getParam ( 'chk', 0 );
		$this->view->checked = $checked;

		if ($this->getRequest()->isPost()) 
		{
			$formData = $_POST;

			$in_take = new Admin_Model_Intake ();
			$result =  $in_take->fetch($intakeid,'');
			$intakecode=$result["intakecode"];

			for ($i=0; $i<$checked; $i++)
			{
				//get each student
				$studid=$formData["assignstud"][$i];

				if($checked==1)
				{
					$students=$studid;
				}
				else
				{
					if ($i==0)
					{
						$students .=$studid;
					}
					else
					{
						$students .=",".$studid;
					}
				}
			}

			$student = explode(',', $students);
			$stdCount = count($student);


			$grader=$formData["grader"];
			$vall=explode(":", $grader);

			$graderid = $vall[0];
			$graderic = $vall[1];

			//check whether selected grader already in a group or not
			$tagging=new Course_Model_Tagging();
			$findgrader=$tagging->findgrader($graderid,$courseid,$intakeid);

			//insert to taggingsetup: if grader previously not added to epic
			if (empty($findgrader))
			{
				//insert to taggingsetup
				$maindata["courseid"]=$courseid;
				$maindata["tutorname"]=$graderid;
				$maindata["tutoric"]=$graderic;
				$maindata["datecreated"]=date("Y-m-d");
				$maindata["intakeid"]=$intakeid;
				$maindata["taggedby"]=$data->username;

				$idTag = $tagging->addtaggingsetup ( $maindata );

				$groupuserid=$idTag;
			}
			else
			{
				$groupuserid=$findgrader["id"];
			}

			for($i=0; $i<$stdCount ;$i++)
			{
				$student[$i];

				//insert into tagginguser
				$subdata["groupID"]=$groupuserid;
				$subdata["userID"]=$student[$i];
				$subdata["status"]=1;

				try
				{
					$tagging->addtagginguser ( $subdata );
				}
				catch (Exception $e)
				{
					$this->_helper->flashMessenger->addMessage("Error While Assigning");
				}

				if($groupuserid!=null)
				{
					$this->_helper->flashMessenger->addMessage("Successfuly Assigned");
				}
			}
		}

		$this->_redirect ( 'course/index/listtag/courseid/'.$courseid);
	}

	public function getfaciAction ()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$form = new Course_Form_SearchIntake ();
		$this->view->form = $form;
		
		$tagging = new Course_Model_Tagging ();
		$select = $tagging->findreturnselect ( $courseid, "xxx" );

		//capture the input params
		if ($this->_request->getParam('intakeid',0) || $this->_request->getParam('ulc',0) || $this->_request->isPost())
		{
			if ($this->_request->getParam('intakeid')!="xxx" && $this->_request->getParam('ulc')!="xxx")
			{
				$intakeid = $this->_request->getParam('intakeid');
				$this->view->intakeid = $intakeid;

				$ulc = $this->_request->getParam('ulc');
				$this->view->ulc = $ulc;
				//				echo "here".$ulc." ".$intakeid;
				//				exit;
				//if it is a post, populate the form and reset the page to 1
				if ($this->_request->isPost())
				{
					$form->populate($this->_request->getPost());
					$page = 1;
				}
				else
				{
					//'search' showed up via the GET param
					$form->populate($this->_request->getParams());
				}
				//make sure the submitted data is valid and modify select statement
				if ($form->valid())
				{
					$data = $form->getValues ();
					$select = $tagging->findreturnselect ( $courseid, $data ['intakeid'],$data ['ulc'],$data ['keyword']);
				}
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));

	}

	public function addcourseAction()
	{
		$course = new Application_Model_Course ();
		$form = new Course_Form_Addcourse();
		$this->view->form = $form;
		
		//build catalog select
		$catalog = new Catalog_Model_Catalog();
		$cats = $catalog->fetchAll();
		$this->cat = $this->cats = array();
		
		foreach ( $cats as $cat )
		{
			$this->cat[$cat['pid']][$cat['id']] = $cat;	
		}
		
		$this->make_cat_options(0,'',1);
		$form->course_cat->addMultiOption(0, $this->view->translate('None'));
		foreach ( $this->cats as $cat_id => $cat_name )
		{
			$form->course_cat->addMultiOption($cat_id, $cat_name);
		}
		
		//end 
		
		//$form->getElement('courseid')->setValue($courseid);

		if ($this->getRequest ()->isPost ())
		{
			if ($form->isValid ( $_POST )) 
			{
				$data = $form->getValues ();
				$data['courseinfo'] = stripslashes($data['courseinfo']);
				$course->upload ( $data );
				$this->_redirect ( 'course/index/listcourse' );
			}
		}
	}
	
	public function modifycourseAction() 
	{
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;

		$course = new Application_Model_Course ();
		$form = new Course_Form_Modifycourse();
		$this->view->form = $form;
		
		
		
		//build catalog select
		$catalog = new Catalog_Model_Catalog();
		$cats = $catalog->fetchAll();
		$this->cat = $this->cats = array();
		
		foreach ( $cats as $cat )
		{
			$this->cat[$cat['pid']][$cat['id']] = $cat;	
		}
		
		$this->make_cat_options(0,'',1);
		$form->course_cat->addMultiOption(0, $this->view->translate('None'));
		//end 
		
		foreach ( $this->cats as $cat_id => $cat_name )
		{
			$form->course_cat->addMultiOption($cat_id, $cat_name);
		}
		
		if ($this->getRequest ()->isPost ()) 
		{
			if ( $form->isValid($_POST) ) 
			{
				$data = $form->getValues ();
				$data['courseinfo'] = stripslashes($data['courseinfo']);
				$course->modify ( $data,$id );
				$this->_redirect ( 'course/index/listcourse' );
			}
		}
		else
		{
			if ($id > 0)
			{
				$form->populate ( $course->fetch ( $id ) );
			}
		}
	}
	
	private function make_cat_options($pid='0',$selected='',$depth=1)
	{
		$cache = $this->cat;

		if(!isset($cache[$pid])) return;
		
		while (list($parent,$category) = each($cache[$pid]))
		{
			$depths = $depth > 1 ? str_repeat("-",$depth-1)." ":'';

			$this->cats[$category['id']] = $depths.$category['name'];
			
			$this->make_cat_options($category['id'],$selected,$depth+1);
		} 
	}
	
	public function deletecourseAction() 
	{
		$course = new Application_Model_Course ();
		$id = $this->_getParam ( 'rowid', 0 );
		
		//echo $id;
		if ($id > 0)
		{
			$course->delete ( $id );
			$this->_redirect ( 'course/index/listcourse' );
		}
	}	
}