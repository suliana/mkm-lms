<?php
class Course_Model_Grader extends Zend_Db_Table
{
	protected $_name = "coursefacilitator";
	protected $_users = "users";
	protected $_cusertemp = "cusertemp";

	public function fetchAll ()
	{
		$sql = $this->_db->select()->from($this->_name);
		$stmt = $this->_db->query($sql);
		return $stmt;
	}
	public function upload ($data)
	{
		$this->_db->insert($this->_name, $data);
	}
	public function fetch ($id='',$by='username')
	{
		$sql = $this->_db->select()->from($this->_name);
		if ( $id != '' )
		{
			$sql->where($by.' = ?',$id);
		}
		
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	/*public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'id = ' . (int) $id);
	}*/
	public function delete ($username="")
	{
		if ($username != "")
		{
			$condition = array(
			    'username = ?' => $username
			);
		}

		$this->_db->delete($this->_name, $condition);
	}
	
	public function getbynamecourse($name,$coursecode)
	{
		$sql = $this->_db->select()	->from($this->_name)
									->where('username = ?',$name)
									->where('coursecode = ?',$coursecode);
		
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	
	public function deletegrader ($username,$courseid,$intakeid)
	{
		
		$condition = array(
		    'username = ?' => $username,
		    'courseid = ?' => $courseid,
		    'intakeid = ?' => $intakeid
		);
		
		$this->_db->delete($this->_name, $condition);
		
	}
	public function find ($keyword = "")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($keyword != "") {
			//echo $keyword;
			$sql->where(
			' title LIKE ? ','%' . $keyword . '%');
		}
		$result = $this->_db->query($sql);
		return $result;
	}
	public function returnselect ($courseid)
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_name),
		array('username', 'courseid','intakeid'))
		->join(array('l' => $this->_users),
		'p.username = l.username',
		array('firstname'))
		->where('courseid = ?',$courseid);
		
		return $sql;
	}
	public function findreturnselect ($courseid,$intakeid="",$keyword = "")
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_name),
		array('username', 'courseid','intakeid'))
		->join(array('l' => $this->_users),
		'p.username = l.username',
		array('firstname'))
		->where('courseid = ?',$courseid);
		//echo $intakeid;
		if ($intakeid != "") {
			$sql->where('intakeid = ?',$intakeid);
		}
		if ($keyword != "") {
			//echo $keyword;
			$sql->where(
			' ( firstname LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   lastName LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   l.username LIKE ? )',
			'%' . $keyword . '%');
		}
		
		//echo $sql;
		return $sql;
	}
	
	public function returnselect2 ()
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_users),
		array('firstname','username'))
		->joinLEFT(array('l' => $this->_cusertemp),
		'p.username = l.username',
		array())
		->where('privilage = ?','EOS')
		->where('l.username is null');
		
		return $sql;
	}
	
	public function findreturnselect2 ($keyword = "")
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_users),
		array('firstname','username'))
		->joinLEFT(array('l' => $this->_cusertemp),
		'p.username = l.username',
		array())
		->where('privilage = ?','EOS')
		->where('l.username is null');
		if ($keyword != "") {
			//echo $keyword;
			$sql->where(
			' ( firstname LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   lastName LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   l.username LIKE ? )',
			'%' . $keyword . '%');
		}
		//echo $sql;
		return $sql;
	}
	
	public function copytocusertemp ($courseid,$intakeid="")
	{
		$select = $this->_db->select()
		->from(array('p' => $this->_name),
		array('username','courseid'))
		->where('courseid = ?',$courseid);
		if ($intakeid != "") {
			$select->where('intakeid = ?',$intakeid);
		}
		
		$stmt = $this->_db->fetchRow($select);
		//print_r($stmt);
		if ($stmt) {
  		//echo "here";
  		//exit(); 
		$sql  = "INSERT INTO ".$this->_cusertemp . " ";
    	$sql .= $select;
    	
    	$this->_db->query($sql);    
		}	


	}
	
	public function deletecusertemp ()
	{
		$this->_db->delete($this->_cusertemp);    	


	}
	
	public function fetchsemua ($username,$courseid)
	{
		$sql = $this->_db->select()->from($this->_name);
		
			$sql->where('username = ?', $username);
			$sql->where('courseid = ?', $courseid);
		
		$result = $this->_db->fetchAll($sql);
		//echo $sql;
		return $result;
	}
	
}
?>
