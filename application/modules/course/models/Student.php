<?php
class Course_Model_Student extends Zend_Db_Table
{
	protected $_name = "coursestudent";
	protected $_users = "users";
	protected $_cusertemp = "cusertemp";

	public function fetchAll ()
	{
		$sql = $this->_db->select()->from($this->_name);
		$stmt = $this->_db->query($sql);
		return $stmt;
	}
	public function upload ($data)
	{
		$this->_db->insert($this->_name, $data);
	}
	public function fetch ( $username = "",$subject = "",$intake = "")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($username != "") {
			$sql->where('username = ?', $username);
		}
		if ($subject != "") {
			$sql->where('courseid = ?', $subject);
		}
		if ($intake != "") {
			$sql->where('intakeid = ?', $intake);
		}
		$sql->order('intakeid DESC');
		//echo $sql;
		$result = $this->_db->fetchRow($sql);
		
		//exit();
		return $result;
	}
	/*public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'id = ' . (int) $id);
	}*/
	public function delete ($username="")
	{
		
		
		
		if ($username != "") {
		$condition = array(
		    'username = ?' => $username
		);
		}

		$this->_db->delete($this->_name, $condition);
	}
	
	public function deletestudent ($username,$courseid,$intakeid)
	{
		
		$condition = array(
		    'username = ?' => $username,
		    'courseid = ?' => $courseid,
		    'intakeid = ?' => $intakeid
		);
		
		$this->_db->delete($this->_name, $condition);
		
	}
	
	public function returnselect ($courseid)
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_name),
		array('username', 'courseid','intakeid'))
		->join(array('l' => $this->_users),
		'p.username = l.username',
		array('firstname'))
		->where('courseid = ?',$courseid);
		
		return $sql;
	}
	public function findreturnselect ($courseid,$intakeid="",$keyword = "")
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_name),
		array('username', 'courseid','intakeid'))
		->join(array('l' => $this->_users),
		'p.username = l.username',
		array('firstname','studentID'))
		->where('courseid = ?',$courseid);
		//echo $intakeid;
		if ($intakeid != "") {
			$sql->where('intakeid = ?',$intakeid);
		}
		if ($keyword != "") {
			//echo $keyword;
			$sql->where(
			' ( firstname LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   lastName LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   l.username LIKE ? )',
			'%' . $keyword . '%');
		}
		
		//echo $sql;
		return $sql;
	}
	
	public function returnselect2 ()
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_users),
		array('firstname','username'))
		->joinLEFT(array('l' => $this->_cusertemp),
		'p.username = l.username',
		array())
		->where('privilage = ?','STUDENT')
		->where('l.username is null');
		
		return $sql;
	}
	
	public function findreturnselect2 ($keyword = "")
	{
		
		$sql = $this->_db->select()
		->from(array('p' => $this->_users),
		array('firstname','username','studentID','privilage'))
		->joinLEFT(array('l' => $this->_cusertemp),
		'p.username = l.username',
		array())
		->where('privilage = ?','STUDENT')
		->where('l.username is null');
		
		if ($keyword != "") {
			//echo $keyword;
			$sql->where(
			' ( firstname LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   lastName LIKE ? ',
			'%' . $keyword . '%');
			$sql->orwhere(
			'   l.username LIKE ? )',
			'%' . $keyword . '%');
		}
//		echo $sql;
		//exit;
		return $sql;
	}
	
	public function copytocusertemp ($courseid,$intakeid="")
	{
		$select = $this->_db->select()
		->from(array('p' => $this->_name),
		array('username','courseid'))
		->where('courseid = ?',$courseid);
		if ($intakeid != "") {
			$select->where('intakeid = ?',$intakeid);
		}
		
		$stmt = $this->_db->fetchRow($select);
		//print_r($stmt);
		
		if ($stmt) {
  		//echo "here";
  		//exit(); 
		$sql  = "INSERT INTO ".$this->_cusertemp . " ";
    	$sql .= $select;
    	
    	//echo $sql;
  		//exit(); 
    	$this->_db->query($sql);    
		}	


	}
	
	public function deletecusertemp ()
	{
		$this->_db->delete($this->_cusertemp);    	


	}
	
	
	public function findallstudent ($courseid,$intakeid, $intakecode)
	{
		$select = $this->_db->select()
		->from(array('p'=>$this->_name),array('username', 'courseid','intakeid')) 
		->join(array('l'=>$this->_users),'p.username = l.username',array('firstname'))
		->where('courseid = ?',$courseid)
		->where('intakeid = ?',$intakeid)
		->where('intake = ?',$intakecode);

		$result=$this->_db->fetchAll($select);
//		echo $select;
		return $result;
	}
	
	public function getmycourse($username)
	{
		
		$select = $this->_db->select()->where('username = ?',$username);
		$result=$this->_db->fetchAll($select);
		//echo $select;
		return $result;
	}
	
		
	public function coursestudentlist ($condition=null)
	{
			
		$select = $this->_db->select()
						->from(array('cs' => $this->_name))
						->joinleft(array('u'=> $this->_users),'u.username=cs.username',array('fullname'=>'u.firstname','icno'=>'u.icno','studentID'=>'u.studentID','uID'=>'u.id'))
						->joinleft(array('i'=>'intake'),'i.id=cs.intakeid',array('intake'=>'i.intakecode','intake_desc'=>'i.description'));
						
		if ($condition!=null)
		{	
			if ($condition['courseid']!="")
			{		
				$select->where("cs.courseid LIKE '".$condition['courseid']."'");
			}
			
			if ($condition['intakeid']!="")	
			{		
				$select->where("i.id = '".$condition['intakeid']."'");
			}
		}
		
		$result=$this->_db->fetchAll($select);
		return $result;
	}
	
}
?>