<?php
class Course_Model_Tagging extends Zend_Db_Table
{
	protected $_name = "coursestudent";
	protected $_users = "users";
	protected $_cusertemp = "cusertemp";
	protected $_taggingsetup = "taggingsetup";
	protected $_tagginguser = "tagginguser";
	
	public function addtaggingsetup ($data)
    {
//    	print_r($data);
        $this->_db->insert($this->_taggingsetup, $data);
        $id = $this->_db->lastInsertId();
//        echo $id;
        return $id;
    }
    
    public function addtagginguser ($data)
    {
//    	print_r($data);
        $this->_db->insert($this->_tagginguser, $data);
        $id = $this->_db->lastInsertId();
////        echo $id;
        return $id;
    }
    
    public function findgrader ($graderid,$courseid,$intakeid)
	{
		$select = $this->_db->select()
		->from(array($this->_taggingsetup)) 
		->where('courseid = ?',$courseid)
		->where('intakeid = ?',$intakeid)
		->where('tutorname = ?',$graderid);
//					echo $select;
		$result=$this->_db->fetchRow($select);
		
		return $result;
	}
	
	public function checkgrader ($graderid,$courseid,$intakeid)
	{
		$select = $this->_db->select()
		->from(array($this->_taggingsetup)) 
		->where('courseid = ?',$courseid)
		->where('intakeid = ?',$intakeid)
		->where('tutorname = ?',$graderid);
//					echo $select;
		$result=$this->_db->fetchRow($select);
		
		return $result;
	}
	
	public function findreturnselect ($courseid,$intakeid="", $ulc= "",$keyword = "")
	{
		if ($intakeid!="xxx"){
			
			$sqlnot=$this->_db->select()
			->from(array('tu' => $this->_tagginguser),
			array('tu.userID'))
			->join(array('ts' => $this->_taggingsetup),
			'tu.groupID=ts.id',
			array())
			->where('ts.courseid = ?',$courseid)
			->where('ts.intakeid = ?',$intakeid);
//			echo $sqlnot;
			$resultnot=$this->_db->fetchAll($sqlnot);
			
			if (empty($resultnot)){
				$resultnot="";
			}
			
	
			$sql = $this->_db->select()
			->from(array('p' => $this->_name),
			array('id','username', 'courseid'))
			->join(array('l' => $this->_users),
			'p.username = l.username',
			array('firstname'))
			->where('courseid = ?',$courseid)
			->where('l.username NOT IN (?)',$resultnot)
			->where('l.ulccode=?',$ulc);
			//echo $intakeid;
			if ($intakeid != "") {
				$sql->where('intakeid = ?',$intakeid);
			}
			if ($keyword != "") {
				//echo $keyword;
				$sql->where(
				' ( firstname LIKE ? ',
				'%' . $keyword . '%');
				$sql->orwhere(
				'   lastName LIKE ? ',
				'%' . $keyword . '%');
				$sql->orwhere(
				'   l.username LIKE ? )',
				'%' . $keyword . '%');
			}
			
//			echo $sql;
			return $sql;
		
		}else{
		
			$sql = $this->_db->select()
			->from(array('p' => $this->_name),
			array('id','username', 'courseid'))
			->join(array('l' => $this->_users),
			'p.username = l.username',
			array('firstname'))
			->where('courseid = ?',$courseid)
			->where('l.ulccode=?',$ulc);
			//echo $intakeid;
			if ($intakeid != "") {
				$sql->where('intakeid = ?',$intakeid);
			}
			if ($keyword != "") {
				//echo $keyword;
				$sql->where(
				' ( firstname LIKE ? ',
				'%' . $keyword . '%');
				$sql->orwhere(
				'   lastName LIKE ? ',
				'%' . $keyword . '%');
				$sql->orwhere(
				'   l.username LIKE ? )',
				'%' . $keyword . '%');
			}
			
//			echo $sql;
			return $sql;
		}
	}
	
	public function findallstudent ($courseid,$intakeid, $intakecode)
	{
		$sqlnot=$this->_db->select()
		->from(array('tu' => $this->_tagginguser),
		array('tu.userID'))
		->join(array('ts' => $this->_taggingsetup),
		'tu.groupID=ts.id',
		array())
		->where('ts.courseid = ?',$courseid)
		->where('ts.intakeid = ?',$intakeid);
		
		$resultnot=$this->_db->fetchAll($sqlnot);
		//print_r( $resultnot);echo $sqlnot;
		$sql = $this->_db->select()
		->from(array('p' => $this->_name),
		array('id','username', 'courseid'))
		->join(array('l' => $this->_users),
		'p.username = l.username',
		array('firstname'))
		->where('courseid = ?',$courseid)
		->where('l.username NOT IN (?)',$resultnot);
		//echo $intakeid;
		if ($intakeid != "") {
			$sql->where('intakeid = ?',$intakeid)
			->where('intake = ?',$intakecode);
		}
		
//		echo $sql;
		$result=$this->_db->fetchAll($sql);
		return $result;
	}
}
?>