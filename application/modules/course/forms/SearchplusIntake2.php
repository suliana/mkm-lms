<?php
class Course_Form_SearchplusIntake2 extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	
    	$users = new Users_Model_Users();
        $uresult =  $users->fetchULC();     		
		$ulc = new Zend_Form_Element_Select('ulc');
		
		$ulc->setLabel('Learning Center:')
		->setRequired(true);
		$ulc->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($uresult as $user) {
		$ulc->addMultiOption($user["ulccode"],$user['ulcname']);
		}
		
		$ulc->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
    	
    	
    	$intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel($this->getView()->translate('semester_id').':')
		->setRequired(true);
		$intakeid->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($result as $c) {
		$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
		
		$intakeid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
    	$keyword = $this->createElement('text', 'keyword');
        $keyword->setLabel('Username / Fullname :')->setRequired(false);
        
        $keyword->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_user'))->setIgnore(true);
        
        $register->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $this->addElements(
        array($ulc,$intakeid,$keyword, $register));
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}