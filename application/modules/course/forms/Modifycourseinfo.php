<?php
class Course_Form_Modifycourseinfo extends Zend_Form
{
	protected $_courseid;
	
	
	public function setCourseid($value)
	{
		$this->_courseid = $value;
	}
	
	public function init ()
	{

    	$this->setMethod('post');
    	//$this->setAction('/content/index/modifycourseinfo');
    	
    	$courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        
        $courseinfo = $this->addElement(
		                    'textarea',
		                    'courseinfo',
		                    array(
		                            'class'		  => 'textarea',
		                    		
		                    		'required'    => false,
		                            'label'       => '',
		                    		'decorators'  => array
		                    						(
														'ViewHelper',
									 					'Description',
														'Errors',
														
														array('Label', array('tag' => 'strong')),
														array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
											        ),
		                    )
            			);
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('save'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'btn submit')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'btn'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'content', 'controller'=>'index','action'=>'courseinfo' , 'courseid'=> $this->_courseid),'default',true)   . "'; return false;"
        ));
        
        $this->addDisplayGroup( array('save', 'cancel'), 'buttons', array(
      		'decorators'=>array('FormElements',array('HtmlTag', array('tag'=>'div', 'class'=>'submit')) )
    	));
        

		$this->setDecorators(array(
               'FormElements',
               array(array('data'=>'HtmlTag'),array('tag'=>'div', 'class'=>'', 'id'=>'manage_courseinfo')),
               'Form'
       	));
        
        /*$this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));*/
	    
     
    }
}