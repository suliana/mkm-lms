<?php
class Course_Form_SelectIntake extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel('select_intake')
		->setRequired(true);
		$intakeid->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($result as $c) {
		$intakeid->addMultiOption($c['intakecode'], $c['description']." - ".$c['intakecode']);
		}
		
		$intakeid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
//        $this->addElement($intakeid);

		$register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_user'))->setIgnore(true);
        
        $register->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $this->addElements(
        array($intakeid,$register));
        
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}