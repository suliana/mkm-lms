<?php
class Course_Form_Modifycourse extends Zend_Form
{
	
	
	public function init ()
	{

		$this->setMethod('post');

		$coursecode = $this->createElement('text', 'coursecode',array('class'=>'inputtext'));
		$coursecode->setLabel($this->getView()->translate('coursecode').':')->setRequired(true);

		$coursecode->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$coursename = $this->createElement('text', 'coursename', array('class'=>'inputtext'));
		$coursename->setLabel($this->getView()->translate('coursename').':')->setRequired(true);

		$coursename->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$courseinfo = $this->createElement('textarea', 'courseinfo');
		$courseinfo->setLabel($this->getView()->translate('courseinfo').':')->setRequired(false);

		$courseinfo->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$coursetype = new Zend_Form_Element_Select(coursetype);
		$coursetype ->setLabel($this->getView()->translate('coursetype').' :')->addMultiOptions( array('Core' => 'Core','Elective' => 'Elective'));
		$coursetype->setOptions(array('class'=>'select'));
		$coursetype->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$credithour = new Zend_Form_Element_Select(credithour);
		$credithour->setOptions(array('class'=>'select'));
		$credithour ->setLabel($this->getView()->translate('credithour').' :');
		for ($i = 1; $i <= 20; $i++) {
			$credithour->addMultiOption($i, $i);

		}

		$credithour->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));
		
		$id = $this->createElement ( 'hidden', 'id' );
		/*$courseid->setLabel ( 'Course ID:' )->setRequired ( false );
		$courseid->setAttrib ( 'readonly', 'readonly' );*/
		
		$id->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));



		$this->addElements(
		array($coursecode, $coursename, $courseinfo, $coursetype, $credithour,$id)
		);

		//button
		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('course'),
		'Options'=>array('class'=>'btn submit'),
		'decorators'=>array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td',
		'align'=>'left', 'openOnly'=>true)),
		array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
		)
		));

		$this->addElement('submit', 'cancel', array(
		'label'=>$this->getView()->translate('cancel'),
		'Options'=>array('class'=>'btn'),
		'decorators'=>array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
		'closeOnly'=>true)),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
		),
		'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'course', 'controller'=>'index','action'=>'listcourse'),'default',true) . "'; return false;"
		));



		$this->setDecorators(array(

		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'



		));
	}
}