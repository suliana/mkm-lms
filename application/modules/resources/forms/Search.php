<?php
class Resources_Form_Search extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$keyword = $this->createElement('text', 'keyword');
        $keyword->setLabel($this->getView()->translate('title').' / '.$this->getView()->translate('description').' :')->setRequired(false);
        
        $keyword->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $category = new Zend_Form_Element_Select('res_type');
		$category->setLabel($this->getView()->translate('type').' :')
		->setRequired(true);
		$table = new Resources_Model_Resources ();
		$rs=$table->findForSelect();
		$category->addMultiOption("0","All");
		foreach ($rs as $row) {
		    $category->addMultiOption($row['id'], $row['type']);
		}
		
		$category->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_resources'))->setIgnore(true);
        
        $register->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $this->addElements(
        array($keyword, $category, $register));
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}