<?php
class Resources_IndexController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
		
	}
	
	public function indexAction() 
	{
		
		$this->view->username = $data->username;
		
		$username = $data->username;
		$privilage=$data->privilage;

		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$intakeid = "";
		//tambahan for student
		if ($data->privilage=="STUDENT")
		{
			$assignsubject= new Course_Model_Student();
			$rs=$assignsubject->fetch($data->username,$row['courseid']);
			
			if ($rs)
			{
				$intakeid=$rs["intakeid"];
			}
			else
			{
				$intakeid='0';
				//echo $intakeid;
				//exit();
			}
		}
		//end tambahan
		
		$form = new Resources_Form_Search ();
		$this->view->form = $form;
		$resources = new Resources_Model_Resources ();
		
		$select = $resources->returnselect ( $courseid, $intakeid,$privilage,$username );
		
		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost())
		{
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost())
			{
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else
			{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			
			//make sure the submitted data is valid and modify select statement
			if ($form->valid())
			{
				$data = $form->getValues ();
				$select = $resources->findreturnselect ( $courseid, $data ['keyword'], $data ['res_type'], $intakeid,$privilage,$username);
			}
		} 
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'),'res_type'=>$form->getValue('res_type'));
	}
	
	public function uploadAction() 
	{
		$username=$this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$resources = new Resources_Model_Resources ();
		$rs_type=$resources->findForSelect();
		$this->view->rs_type = $rs_type;

		$this->view->courseid = $courseid;

		if ( $this->getRequest()->isPost() ) 
		{
			$data = $this->getRequest()->getPost();

			if ($data["res_type"]==1 || $data["res_type"]==2 || $data["res_type"]==4 || $data["res_type"]==5) 
			{
				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/resources/';

				if (! is_dir ( $uploadDir ))
				{
					mkdir ( $uploadDir, 0775 );
				}

				$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/resources/' . $courseid . '/';

				if (! is_dir ( $uploadDir ))
				{
					mkdir ( $uploadDir, 0775 );
				}

				/* Uploading Document File on Server */
				$upload = new Zend_File_Transfer_Adapter_Http();
				$upload->setDestination($uploadDir);
				try 
				{
					// upload received file(s)
					$upload->receive();

					//
					$locationFile = $upload->getFileName('filename');

					$data ["filename"] = date ( 'Ymdhis' ) . "_" . $_FILES["filename"]["name"];

					$fullPathNameFile = $uploadDir . $data ["filename"];

					//print_r($_FILES);
					//exit();
					// Renommage du fichier
					$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
					$filterRename->filter ( $locationFile );

				} 
				catch (Zend_File_Transfer_Exception $e) 
				{
					$e->getMessage();
				}

			}
			
			if ($data["res_type"]==3)
			{
				$data["url"] = $data["link"];
			}
			
			
			if ($data["res_type"]==6) 
			{
				$data ["url"] = $data ["url"];
			}
			
			unset($data["link"]);
			unset($data["save"]);
			
			$data["datecreated"]=date("Y-m-d H:i:s");
			$data["createby"]=$username;
			$resources->upload ( $data );
			$this->_redirect ( 'resources/index/index/id/'.$courseid.'/courseid/'.$courseid );

		}
	}
	
	public function modifyAction()
	{
		$username = $this->userinfo->username;
		
		$id = $this->_getParam ( 'id', 0 );

		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;

		$rs_type=$resources->findForSelect();
		$this->view->rs_type = $rs_type;

		if ($this->getRequest()->isPost())
		{
			$data = $this->getRequest()->getPost();
			if ($id > 0)
			{
				$sdata ["title"] = $data ["title"];
				$sdata ["description"] = $data ["description"];
				$sdata ["courseid"] = $data ["courseid"];
				$sdata ["res_type"] = $data ["res_type"];
				$sdata ["intakeid"] = $data ["intakeid"];
				//print_r($data);
				//exit();

				if ($sdata["res_type"]==1 || $sdata["res_type"]==2 || $sdata["res_type"]==4 || $sdata["res_type"]==5) 
				{
					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/resources/';

					if (! is_dir ( $uploadDir ))
					{
						mkdir ( $uploadDir, 0775 );
					}

					$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/resources/' . $sdata ["courseid"] . '/';

					if (! is_dir ( $uploadDir ))
					{
						mkdir ( $uploadDir, 0775 );
					}

					/* Uploading Document File on Server */
					$upload = new Zend_File_Transfer_Adapter_Http();
					$upload->setDestination($uploadDir);
					
					if ($upload->isValid()) 
					{
						// upload received file(s)
						$upload->receive();
						
						//
						$locationFile = $upload->getFileName('filename');

						$sdata["filename"] = date ( 'Ymdhis' ) . "_" . $_FILES["filename"]["name"];
						$sdata["url"]="";
						$fullPathNameFile = $uploadDir . $sdata ["filename"];

						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );

					}

				}
				
				
				if ($sdata["res_type"]==3) 
				{
					$sdata ["filename"]="";
					$sdata ["url"] = $data ["link"];

				}
				
				if ($sdata["res_type"]==6) {
					$sdata ["filename"]="";
					$sdata ["url"] = $data ["url"];

				}
				
				$sdata["datemodified"]=date("Y-m-d H:i:s");
				$sdata["modifyby"]=$username;
				//print_r($sdata);
				//exit();
				$resources->modify ( $sdata, $id );
			} 
			else 
			{
				$data["datecreated"]=date("Y-m-d H:i:s");
				$data["createby"]=$username;
				$resources->upload ( $data );
			}
			
			$this->_redirect ( 'resources/index/index/id/'.$sdata ["courseid"].'/courseid/'.$sdata ["courseid"] );

		}

	}
	
	public function deleteAction() 
	{
		$resources = new Resources_Model_Resources();
		$id = $this->_getParam ( 'rowid', 0 );
		
		$courseid = $this->_getParam ( 'courseid', '' );
		//echo $id;
		if ($id > 0) 
		{
			$resources->delete ( $id );
			$this->_redirect ( 'resources/index/index/id/'.$courseid.'/courseid/'.$courseid );
		}
	}

	public function listcourseAction() 
	{
		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();

		//echo $data->username;
		$this->view->course = $rscourse;
	}
	
	public function displayresourceAction() 
	{
		//$resources = new Resources_Model_Resources ();
		$id = $this->_getParam ( 'id', 0 );
		
		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}
	
	public function framepage1Action() 
	{
		$id = $this->_getParam ( 'id', 0 );
		
		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}
	
	public function framepageembedAction()
	{
		$id = $this->_getParam ( 'id', 0 );
		
		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

	}
	
	public function displayresourceembedAction() 
	{
		//$resources = new Resources_Model_Resources ();
		$id = $this->_getParam ( 'id', 0 );
			
		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}
	
	public function displayresource2Action() 
	{
		//$resources = new Resources_Model_Resources ();
		$id = $this->_getParam ( 'id', 0 );
		
		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}
	
	public function framepage2Action() 
	{
		$this->view->username = $data->username;

		$id = $this->_getParam ( 'id', 0 );
		
		$resources = new Resources_Model_Resources ();
		$vdata=$resources->fetch ( $id );

		$this->view->vdata=$vdata;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

	}
	
}





