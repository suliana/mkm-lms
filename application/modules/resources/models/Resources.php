<?php
class Resources_Model_Resources extends Zend_Db_Table
{
    protected $_name = "resources";
    protected $_resoucestype = "resources_type";
    
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    public function upload ($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    public function fetch ($id = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($id != "") {
            $sql->where('id = ?', $id);
        }
        $result = $this->_db->fetchAll($sql);
        //print_r($result);
        //exit();
        return $result;
    }
    public function modify ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    public function delete ($id)
    {
        $this->_db->delete($this->_name, 'id = ' . (int) $id);
    }
    public function find ($keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            '  ( title LIKE ? ', 
            '%' . $keyword . '%');
            $sql->orwhere(
            '   description LIKE ? )', 
            '%' . $keyword . '%');
        }
        $result = $this->_db->query($sql);
        return $result;
    }
    public function returnselect ($courseid,$intakeid = "",$privilage="",$username="")
    {
        $sql = $this->_db->select()
        	->from($this->_name)
        	->where('courseid = ?',$courseid);
            
        ;
        
        if ($privilage=="EOS"){
			$eos = new Course_Model_Grader();
			$rs=$eos->fetchsemua($username,$courseid);
			$i=0;
			if ($rs){
				foreach ($rs as $row) {
						$intake[$i] = $row['intakeid'] ;
					
					$i++;

				}
				
				$sql->where(
				' intakeid IN  (?) ', $intake );
			}


		}
		
		
        if ($intakeid != "") {
            //echo $keyword;
            $sql->where(
            ' intakeid = ? ',$intakeid );
        }
        
        
        $sql->order('title ASC');
        return $sql;
    }
    public function findreturnselect ($courseid,$keyword = "",$typeid=0, $intakeid = "",$privilage="",$username="")
    {
        $sql = $this->_db->select()->from($this->_name)
        ->where('courseid = ?',$courseid);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            '  ( title LIKE ? ', 
            '%' . $keyword . '%');
            $sql->orwhere(
            '   description LIKE ? )', 
            '%' . $keyword . '%');
        }
        if ($typeid != 0) {
         	$sql->where( ' res_type = ? ' , $typeid );
        }
        
        if ($privilage=="EOS"){
			$eos = new Course_Model_Grader();
			$rs=$eos->fetchsemua($username,$courseid);
			$i=0;
			if ($rs){
				foreach ($rs as $row) {
						$intake[$i] = $row['intakeid'] ;
					
					$i++;

				}
				
				$sql->where(
				' intakeid IN  (?) ', $intake );
			}


		}
		
		
        if ($intakeid != "") {
            //echo $keyword;
            $sql->where(
            ' intakeid = ? ',$intakeid );
        }
        $sql->order('title ASC');
        //echo $sql;
        //exit();
        return $sql;
    }
    
    public function findForSelect()
    {
    	$select = $this->_db->select()->from($this->_resoucestype)->order('id ASC');
    	
    	$result = $this->_db->fetchAll($select);
        //print_r($result);
        //exit();
        return $result;
        

    }
}
?>
