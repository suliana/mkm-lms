<?php
class Assignment_Model_Assignment extends Zend_Db_Table
{
    protected $_name = "assignment";
    protected $_submission = "assignment_submission";
    protected $_submissionGroup = "assignment_sub_group";
    protected $_submissionReflective = "assignment_reflective_journal";
    protected $_type = "assignment_type";
    protected $_users = "users";
    protected $_taggingsetup = "taggingsetup";
	protected $_tagginguser = "tagginguser";
	protected $_coursestudent = "coursestudent";
	protected $_coursemain = "coursemain";
	protected $_assignment_files = "assignment_files";
    
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    public function upload ($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    public function fetch ($id = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($id != "") {
            $sql->where('id = ?', $id);
        }
        $result = $this->_db->fetchRow($sql);
        //print_r($result);
        //exit();
        return $result;
    }
    public function modify ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    
    public function delete ($id,$type='id')
    {   
    	$this->_db->delete($this->_name, array( $type.' = ?' => $id ) );
    }
    
    public function find ($keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
		//echo $sql;
        $result = $this->_db->query($sql);
        return $result;
    }
    public function returnselect ($courseid, $semid)
    {
        $sql = $this->_db->select()
        	->where('courseid = ?',$courseid)
        	->where('semid = ?',$semid)
            ->from($this->_name)
            ->order('title ASC');
        ;
//        echo $sql;
        return $sql;
    }
    public function returnselectall ($courseid)
    {
        $sql = $this->_db->select()
        	->where('courseid = ?',$courseid)
            ->from($this->_name)
            ->order('title ASC');
        ;
        return $sql;
    }
    public function findreturnselect ($courseid,$keyword = "", $semid)
    {
        $sql = $this->_db->select()->from($this->_name)
        ->where('courseid = ?',$courseid)
        ->where('semid = ?',$semid);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
        //echo $sql;
        //exit();
        return $sql;
    }
    
    public function findreturnselectall ($courseid,$keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name)
        ->where('courseid = ?',$courseid);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
        //echo $sql;
        //exit();
        return $sql;
    }
    
    public function checksubmission($username,$asgid)
    {
    	$select = $this->_db->select()->from($this->_submission)
    	->where('username = ? ',$username)
    	->where('asgid = ? ',$asgid);
    	//echo $select;
    	$result = $this->_db->fetchRow($select);
        
        return $result;
        

    }
    
    public function check_submission($username,$asgid)
    {
    	$select = $this->_db->select()->from($this->_submission)
    	->where('username = ? ',$username)
    	->where('asgid = ? ',$asgid);
    	//echo $select;
    	$result = $this->_db->fetchAll($select);
        
        return $result;
        

    }
    
    
     public function submitassignment ($data)
    {
//    	print_r($data);
        $this->_db->insert($this->_submission, $data);
        $id = $this->_db->lastInsertId();
        //echo $id;
        return $id;
    }
    
    public function submitreflective ($data)
    {
//    	print_r($data);
        $this->_db->insert($this->_submissionReflective, $data);
        $id = $this->_db->lastInsertId();
        //echo $id;
        return $id;
    }
    
    public function listasg ($courseid)
    {
        $sql = $this->_db->select()
        	->where('courseid = ?',$courseid)
            ->from($this->_name)
            ->order('title ASC');
        ;
        
//        echo $sql;
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
    
    public function countlistasg ($courseid)
    {
        $sql = $this->_db->select()
        	->from(array('asg' => $this->_name),
                    array('*' => 'COUNT(*) as count' ))
        	->where('asg.courseid = ?',$courseid);
            
//        echo $sql;
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
    
    
    public function findForSelect()
    {
    	$select = $this->_db->select()->from($this->_type)->order('id ASC');
    	
    	$result = $this->_db->fetchAll($select);
        //print_r($result);
        //exit();
        return $result;
        

    }
    
      public function checkuser($icstud, $asgid, $courseid, $semid, $username)
    {           	
    	$selectGid = $this->_db->select()->from($this->_tagginguser)
			    	->where('userID = ? ',$username)
			    	->where('courseid = ? ',$courseid)
			    	->where('semid = ? ',$semid);
			    	$resultGid = $this->_db->fetchRow($selectGid);
    	
    				$leaderID=$resultGid["groupID"];
    	
    	$select = $this->_db->select()
		    	->from(array('u' => $this->_users),
				array('*'))
		    	->join(array('c' => $this->_coursestudent),
				'c.username=u.username',
				array('*'))
				->join(array('tu'=>'tagginguser'),'tu.userID=u.username AND tu.courseid=c.courseid AND tu.semid=c.intakeid')
		    	->where('u.studentID = ? ',$icstud)
		    	->where('u.privilage = ? ',"STUDENT")
		    	->where('c.intakeid = ? ',$intakeid)
				->where('c.courseid = ? ',$courseid)
				->where('tu.groupID IN (?) ',$leaderID);
//    			echo $select;
		    	$result = $this->_db->fetchRow($select);
    	
    	if ($result){
    		//check whether have submitted or not
    		$memberid=$result["userID"];
    		$selectMember = $this->_db->select()->from($this->_submissionGroup)
				    	->where('asgid = ? ',$asgid)
				    	->where('username = ? ',$memberid);
				    	$resultMember = $this->_db->fetchRow($selectMember);
				    	
				    	if ($resultMember){
				    		//choose different group member
				    		return $resultMember;
				    	}else{
				    		return $result;
				    	}
    	}else{
    		return $result;
    	}
    	

        
    }
    
    public function submitassignment_sub ($data)
    {
        $this->_db->insert($this->_submissionGroup, $data);
        $id = $this->_db->lastInsertId();
        
        return $id;
    }
    
    public function checksubmissiongroup($username,$asgid)
    {
		$select = $this->_db->select()
					->from(array('subgroup'=>$this->_submissionGroup),array('subname'=>'username')) 
					->join(array('asgsub'=>'assignment_submission'),'asgsub.id=subgroup.asg_subID AND subgroup.asgid=asgsub.asgid')
					->where('subgroup.username = ?', $username)
					->where('subgroup.asgid = ?', $asgid);
//					echo $select;
		$result=$this->_db->fetchRow($select);
		
		return $result;
    }
    
    public function checksubmission_group($username,$asgid)
    {
		$select = $this->_db->select()
					->from(array('subgroup'=>$this->_submissionGroup),array('subname'=>'username','asg_subID'=>'asg_subID')) 
					->join(array('asgsub'=>'assignment_submission'),'asgsub.id=subgroup.asg_subID AND subgroup.asgid=asgsub.asgid')
					->where('subgroup.username = ?', $username)
					->where('subgroup.asgid = ?', $asgid);
//					echo $select;
		$result=$this->_db->fetchAll($select);
		
		return $result;
    }
    
    public function delete_asg($asgid,$username,$filename)
    {
        $this->_db->delete($this->_submission, array('asgid = ' . (int) $asgid, 'username = "' . $username.'"', 'filename = "' . $filename.'"') );
    }
    
    public function delete_asgNew($asgid,$username)
    {
        $this->_db->delete($this->_submission, array('asgid = ' . (int) $asgid, 'username = "' . $username.'"') );
    }
    
    
    
   
    
    public function returnsubmission ($courseid, $semid, $asid, $asgtype, $username)
    {
    	$sql = $this->_db->select()
				->from(array('tu' => $this->_tagginguser),
				array('*'))
				->join(array('ts' => $this->_taggingsetup),
				'tu.groupID = ts.id',
				array('*'))
				->join(array('u' => $this->_users),
				'u.username = tu.userID',
				array('*'))
				->where('ts.courseid = ?',$courseid)
	        	->where('ts.semid = ?',$semid)
	        	->where('ts.tutorname = ?',$username)
	        	->where('tu.userID NOT IN (?)',$username)
	            ->order('u.firstname ASC');
//	            echo $sql;
	            return $sql;
    	

    	
    }
    
    public function checkname($teamleader)
    {
    	$select = $this->_db->select()->from($this->_users)
    	->where('username = ? ',$teamleader);
//    	echo $select;
    	$result = $this->_db->fetchRow($select);
        
        return $result;
        

    }
    
    public function allasg ()
    {
    	$sql = $this->_db->select()
            ->from($this->_name)
            ->order('courseid ASC')
            ->order('title ASC')
            ->group('courseid');
//        echo $sql;
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
    
    public function returngroupsubmission ($courseid, $semid, $asid, $asgtype, $username)
    {
    	$sql = $this->_db->select()
				->from(array('tu' => $this->_tagginguser),
				array('*'))
				->join(array('ts' => $this->_taggingsetup),
				'tu.groupID = ts.id',
				array('*'))
				->join(array('u' => $this->_users),
				'u.username = tu.userID',
				array('*'))
				->joinLeft(array('subgroup' => $this->_submissionGroup),
				'subgroup.username = tu.userID',
				array('subname'=>'subgroup.username', '*'))
				->where('ts.courseid = ?',$courseid)
	        	->where('ts.semid = ?',$semid)
	        	->where('ts.tutorname = ?',$username)
	        	->where('tu.userID NOT IN (?)',$username)
	            ->order('subgroup.asg_subID ASC')
	            ->order('subgroup.leader DESC');
//	            echo $sql;
	            return $sql;
    	
    	
    }
    
    public function countgroupstudent($asg_subID, $asgid){
    	
    	$sql = $this->_db->select()
        	->from(array('subgroup' => $this->_submissionGroup),
                    array('*' => 'COUNT(*) as count' ))
        	->where('subgroup.asgid = ?',$asgid)
        	->where('subgroup.asg_subID = ?',$asg_subID);
//            echo $sql;exit;
        $result = $this->_db->fetchRow($sql);
        
        return $result;
    }
    
    public function listgroupstudent($asg_subID, $asgid){
    	
    	$sql = $this->_db->select()
        	->from(array('subgroup' => $this->_submissionGroup),
                    array('*' => 'COUNT(*) as count' ))
        	->where('subgroup.asgid = ?',$asgid)
        	->where('subgroup.asg_subID = ?',$asg_subID);
//            echo $sql;
//            exit;
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
    
    public function returnlistasg ($courseid, $semid)
    {
        $sql = $this->_db->select()
        	->where('courseid = ?',$courseid)
        	->where('semid = ?',$semid)
            ->from($this->_name)
            ->order('title ASC');
        ;
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
    
    public function checksubmissionreflective($username,$asgid)
    {
		$select = $this->_db->select()
					->from(array('reflective'=>$this->_submissionReflective))
					->where('reflective.username = ?', $username)
					->where('reflective.asgid = ?', $asgid);
//					echo $select;
		$result=$this->_db->fetchRow($select);
		
		return $result;
    }
    
    public function checksubmissionall ($icnum = "")
    {    	
    	$sql = $this->_db->select()->from(array('cstud' => $this->_coursestudent),
				array('*'))
				->join(array('u' => $this->_users),
				'cstud.username = u.username',
				array('*'))
				->join(array('asg' => $this->_name),
				'cstud.courseid = asg.courseid AND cstud.intakeid = asg.semid',
				array('*','asgid' =>'asg.id'))
				->where(' u.studentID = ? ',$icnum);
				


        return $sql;
    }
    
    public function checksubmissionallgroup ($semid, $courseid)
    {    	    	
        $sql = $this->_db->select()->from(array('cmain' => $this->_coursemain),
				array('*','cmainid' =>'cmain.id','ccourseid' =>'cmain.courseid'))
				->joinLeft(array('asg' => $this->_name),
				'cmain.courseid = asg.courseid AND asg.semid = '.$semid,
				array('*','asgid' =>'asg.id'));
        
        if ($courseid != "" && $courseid != "all") {
            //echo $keyword;
            $sql->where(
            '  asg.courseid = ? ', 
            $courseid);
        }
//        echo $sql;
//        exit();
        return $sql;
    }
    
    public function getEOS ($courseid, $semid, $studid)
    {
    	$sql = $this->_db->select()
				->from(array('tu' => $this->_tagginguser),
				array('*'))
				->join(array('ts' => $this->_taggingsetup),
				'tu.groupID = ts.id AND tu.`courseid` = ts.courseid AND tu.`semid` = ts.semid',
				array('*'))
				->where('tu.courseid = ?',$courseid)
	        	->where('tu.semid = ?',$semid)
	        	->where('tu.userID = ? ',$studid);
//	            echo $sql;
		$result=$this->_db->fetchRow($sql);
	    return $result;
    	
    }
    
    public function counttotalstud ($courseid, $semid)
    {
        $sql = $this->_db->select()
        	->from(array('tu' => $this->_tagginguser),
                    array('COUNT(tu.userID) as count' ))
            ->join(array('u' => $this->_users),
			'tu.userID = u.username',array('u.username'))
        	->where('tu.courseid = ?',$courseid)
        	->where('tu.semid = ?',$semid)
        	->where('u.privilage = ?',"STUDENT");

        $result = $this->_db->fetchRow($sql);

        return $result;
    } 
    
    public function totalsubmitindv ($courseid, $semid, $asgid)
    {
        $sql = $this->_db->select()
        	->from(array('tu' => $this->_tagginguser),
                    array('COUNT(tu.userID) as count' ))
            ->join(array('u' => $this->_users),
			'tu.userID = u.username',array('u.username'))
			->join(array('asgsub' => $this->_submission),
			'tu.userID = asgsub.username',array('asgsub.username'))
        	->where('tu.courseid = ?',$courseid)
        	->where('tu.semid = ?',$semid)
        	->where('u.privilage = ?',"STUDENT")
        	->where('asgsub.asgid = ?',$asgid)
        	->where('asgsub.filename IS NOT NULL');
//		echo $sql;
        $result = $this->_db->fetchRow($sql);

        return $result;
    }
    
    public function viewgroupmembers ($username,$asgid,$said)
    {
    	  $sql = $this->_db->select()
        	->from(array('sub' => $this->_submissionGroup),
                    array('*' ))
            ->join(array('u' => $this->_users),
			'sub.username = u.username',array('u.firstname'))
        	->where('sub.asgid = ?',$asgid)
        	->where('sub.asg_subID = ?',$said)
        	->order('sub.leader DESC');

        $result = $this->_db->fetchAll($sql);

        return $result;
    }
    
   public function countgroupmember($asg_subID, $asgid){
    	
    	$sql = $this->_db->select()
        	->from(array('subgroup' => $this->_submissionGroup),
                    array('*' => 'COUNT(*) as count' ))
        	->where('subgroup.asgid = ?',$asgid)
        	->where('subgroup.asg_subID = ?',$asg_subID)
        	->where('subgroup.leader NOT IN (?)',1);
//            echo $sql;
//				exit;
        $result = $this->_db->fetchRow($sql);
        
        return $result;
    }
    
    public function groupmemberlist($asg_subID, $asgid){
    	
    	$sql = $this->_db->select()
        	->from(array('subgroup' => $this->_submissionGroup),
                    array('*' ))
            ->join(array('u' => $this->_users),
			'subgroup.username=u.username',
			array('u.firstname','u.studentID','u.STUD_ID'))
        	->where('subgroup.asgid = ?',$asgid)
        	->where('subgroup.asg_subID = ?',$asg_subID)
        	->order('subgroup.leader  DESC');
//        	->where('subgroup.leader NOT IN (?)',1);
            echo $sql;
//			exit;
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
    
    public function checkmainByID($submid,$asgid,$username)
    {
    	$select = $this->_db->select()->from(array('asgsub'=>$this->_submission))
    	->joinLeft(array('files'=>$this->_assignment_files),'asgsub.asgid=files.asgid AND asgsub.id=asg_subID',array('fid'=>'id','ffilename'=>'filename','fdatesubmit'=>'datesubmit','fasg_subID'=>'asg_subID','fasgid'=>'asgid'))
    	->where('asgsub.id = ? ', (int) $submid)
    	->where('asgsub.asgid = ? ', (int) $asgid)
    	->where('asgsub.username = ? ', $username);
//    	echo $select;
    	$result = $this->_db->fetchRow($select);
        
        return $result;

    } 


	public function checkgroupByID($submid,$asgid,$savegroup)
    {
		$select = $this->_db->select()
		->from(array('subgroup'=>$this->_submissionGroup),array('subgroupid'=>'id','grasgid'=>'asgid','subname'=>'username','asg_subID','subgsavegroup'=>'savegroup','nofile'))
		->join(array('asgsub'=>$this->_submission),'asgsub.id=subgroup.asg_subID AND subgroup.asgid=asgsub.asgid AND asgsub.savegroup=subgroup.savegroup',array('asgsubid'=>'id','asgasgid'=>'asgid','asgsavegroup'=>'savegroup'))
		->where('subgroup.asgid = ?', (int) $asgid)
		->where('subgroup.asg_subID = ?', (int) $submid)
		->where('subgroup.savegroup = ?', $savegroup);
//		echo $select;
		$result=$this->_db->fetchAll($select);
		
		return $result;
    }
    
    public function updatemaintable ($maindata, $submid)
    {
        $this->_db->update($this->_submission, $maindata, 'id = ' . (int) $submid);
    }
    
    public function submitfilestable ($tablefiles)
    {
        $this->_db->insert($this->_assignment_files, $tablefiles);
    }
    
    public function updategrouptable ($subgroup, $subgroupid)
    {
        $this->_db->update($this->_submissionGroup, $subgroup, 'id = ' . (int) $subgroupid);
    }
    
    public function checksubmissionNew($username,$asgid)
    {
    	$select = $this->_db->select()
    	->from(array('asgsub'=>$this->_submission),array('id', 'asgid','username','filename'))
    	->where('asgsub.username = ? ',$username)
    	->where('asgsub.asgid = ? ',$asgid);
    	//echo $select;
    	$result = $this->_db->fetchRow($select);
        
        return $result;

    }
    
    public function check_submissionNew($username,$asgid)
    {
    	$select = $this->_db->select()
    	->from(array('asgsub'=>$this->_submission),array('id', 'asgid','username','filename'))
    	->where('asgsub.username = ? ',$username)
    	->where('asgsub.asgid = ? ',$asgid);
//    	echo $select;
    	$result = $this->_db->fetchAll($select);
        
        return $result;

    }
    
    public function checksubmissiongroupNew($username,$asgid)
    {
		$select = $this->_db->select()
				->from(array('subgroup'=>$this->_submissionGroup),array('subname'=>'username','savegroup')) 
				->join(array('asgsub'=>$this->_submission),'asgsub.id=subgroup.asg_subID AND subgroup.asgid=asgsub.asgid',array('id', 'asgid','username'))
				->joinLeft(array('files'=>$this->_assignment_files),'asgsub.asgid=files.asgid AND asgsub.id=files.asg_subID',array('filename','datesubmit','fid'=>'id'))
				->where('subgroup.username = ?', $username)
				->where('subgroup.asgid = ?', $asgid)
				->limit(1,0);
//				echo $select;
		$result=$this->_db->fetchRow($select);
		
		return $result;
    }
    
    public function check_submissiongroupNew($username,$asgid, $submid)
    {
		$select = $this->_db->select()
				->from(array('subgroup'=>$this->_submissionGroup),array('subname'=>'username','savegroup')) 
				->join(array('asgsub'=>$this->_submission),'asgsub.id=subgroup.asg_subID AND subgroup.asgid=asgsub.asgid',array('id', 'asgid','username'))
				->joinLeft(array('files'=>$this->_assignment_files),'asgsub.asgid=files.asgid AND asgsub.id=files.asg_subID',array('filename','datesubmit','fid'=>'id'))
				->where('subgroup.username = ?', $username)
				->where('subgroup.asgid = ?', $asgid);
				
				if ($submid != "") {
		            $select->where(
		            ' asgsub.id = ? ',$submid);
		        }
		        
//				echo $select;
		$result=$this->_db->fetchAll($select);
		
		return $result;
    }
    
    public function getgrouplist($asgid,$submid)
    {		
		$select = $this->_db->select()
				->from(array('asgsub'=>$this->_submission)) 
				->join(array('subgroup'=>$this->_submissionGroup),'asgsub.id=subgroup.asg_subID AND subgroup.asgid=asgsub.asgid',array('subgroupid'=>'id', 'leader','grsavegroup'=>'savegroup','nofile'))
				->join(array('users'=>$this->_users),'asgsub.username=users.username',array('firstname','studentID'))
				->where('asgsub.asgid = ?', $asgid)
				->where('subgroup.leader = ?', 1)
				->order('users.firstname ASC');
				
				if ($submid != "") {
		            $select->where(
		            ' asgsub.id = ? ',$submid);
		            
		            $result=$this->_db->fetchRow($select);
		        }else{
		        	$result=$this->_db->fetchAll($select);
		        }
//				echo $select;
		
		
		return $result;
    	
    }
    
    public function getgroupmembers ($asgid,$said)
    {
    	  $sql = $this->_db->select()
        	->from(array('sub' => $this->_submissionGroup),
                    array('*' ))
            ->join(array('u' => $this->_users),
			'sub.username = u.username',array('u.firstname','u.studentID'))
        	->where('sub.asgid = ?',$asgid)
        	->where('sub.asg_subID = ?',$said)
        	->where('sub.leader NOT IN (?)',1)
        	->order('u.firstname ASC');

        $result = $this->_db->fetchAll($sql);

        return $result;
    }
    
    public function fetchblockcontent ($blockcontentid = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($blockcontentid != "") {
            $sql->where('blockcontentid = ?', $blockcontentid);
        }
        
        $result = $this->_db->fetchRow($sql);
        
        return $result;
    }
    
    public function modifyblockcontent ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'blockcontentid = ' . (int) $id);
	}

}
?>
