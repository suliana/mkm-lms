<?php
class Assignment_Form_IndividuSearch extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	       
        $icnum = $this->createElement('text', 'icnum');
        $icnum->setLabel('IC Number :')->setRequired(true);
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_user'))->setIgnore(true);
        
        $this->addElements(
        array($icnum, $register));
    }
}