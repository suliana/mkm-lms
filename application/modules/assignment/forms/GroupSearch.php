<?php
class Assignment_Form_GroupSearch extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	       
        $intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel($this->getView()->translate('semester_id').':')
		->setRequired(true);
		$intakeid->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($result as $c) {
		$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
        
        
        $course = new Admin_Model_Course ();
        $result =  $course->allcourse();     		
		$courseid = new Zend_Form_Element_Select('courseid');
		
		$courseid->setLabel($this->getView()->translate('course').' :')
		->setRequired(true);
		$courseid->addMultiOption("all", "---- ".$this->getView()->translate('all')." ----");
		foreach ($result as $c) {
		$courseid->addMultiOption($c['courseid'], $c['courseid']);
		}
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search'))->setIgnore(true);
        
        $this->addElements(
        array($intakeid, $courseid, $register));
    }
}