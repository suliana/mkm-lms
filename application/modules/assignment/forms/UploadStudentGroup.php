<?php
class Assignment_Form_UploadStudentGroup extends ZendX_JQuery_Form
{
	
protected $_courseid;
protected $_username;
protected $_asgid;
protected $_gpmem;


public function setcourseid($value)
{
$this->_courseid = $value;
}
public function setusername($value)
{
$this->_username = $value;
}
public function setasgid($value)
{
$this->_asgid = $value;
}
public function setgpmem($value)
{
$this->_gpmem = $value;
}

public function init ()
    {
    	$this->setMethod('post');
    	  	
    	for ($i=1; $i<=$this->_gpmem; $i++){
    		$title = $this->createElement('text', 'icstud');
	        $title->setLabel($this->getView()->translate('IC Student').':')->setRequired(true);
	        
	        $title->setDecorators(array(
						'ViewHelper',
	 					'Description',
						'Errors',
						array(array('data'=>'HtmlTag'), array('tag' => 'td' , 'colspan' => '3')),
						array('Label', array('tag' => 'td' )),
						array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
	
	        ));
	        
    	}
    	$this->addElements(
        	array($title));
    	   	
    	
        $courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        
        $courseid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $asgid = $this->createElement('hidden', 'asgid');
        $asgid->setValue($this->_asgid);
        
        $asgid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
                
        $element = new Zend_Form_Element_File('filename');
        
        $dirasg=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/';
        
        if(!is_dir($dirasg))
        mkdir($dirasg,0775);
        
        $dirasgcourse=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/'.$this->_courseid;
        
        if(!is_dir($dirasgcourse))
        mkdir($dirasgcourse,0775);
        
        $dirasgcourse=$dirasgcourse.'/'.$this->_asgid;
        
        if(!is_dir($dirasgcourse))
        mkdir($dirasgcourse,0775);
        
        $dirasgcourse=$dirasgcourse.'/'.$this->_username;
        
        if(!is_dir($dirasgcourse))
        mkdir($dirasgcourse,0775);
        
        $element->setLabel($this->getView()->translate('Choose Group').' '.$this->getView()->translate('file').':')->setDestination(
        $dirasgcourse);
        // ensure minimum 1, maximum 3 files
        /*$element->addValidator('Count', false, 
        array('min' => 1, 'max' => 3));*/
        // limit to 100K
        $element->addValidator('Size', false, 2024800);
        // only JPEG, PNG, and GIFs
        $element->addValidator('Extension', false, 'doc,docx,pdf');
        $element->setRequired(true);
        // defines 3 identical file elements
        //$element->setMultiFile(3);
        //$form->addElement($element, 'foo');
        $element->setDecorators(array(
					'File',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td' )),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        
        $this->addElements(
        array($courseid, $asgid, $element));
       
        
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('submit').' '.$this->getView()->translate('assignment'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'assignment', 'controller'=>'index','action'=>'indexstudent','courseid' => $this->_courseid),'default',true) . "'; return false;"
        ));
		
		$this->setDecorators(array(

  

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       )); 
        
    }
    
    
}