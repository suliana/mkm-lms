<?php
class Assignment_Form_UploadStudent extends ZendX_JQuery_Form
{

	protected $_courseid;
	protected $_username;
	protected $_asgid;
	protected $_blockid;
	protected $_blockcontentid;
	

	public function setcourseid($value)
	{
		$this->_courseid = $value;
	}
	public function setusername($value)
	{
		$this->_username = $value;
	}
	public function setasgid($value)
	{
		$this->_asgid = $value;
	}
	public function setblockid($value)
	{
		$this->_blockid = $value;
	}
	public function setblockcontentid($value)
	{
		$this->_blockcontentid = $value;
	}
	public function init ()
	{

		$this->setMethod('post');


		

		$asgid = $this->createElement('hidden', 'asgid');
		$asgid->setValue($this->_asgid);


		$element = new Zend_Form_Element_File('filename');

		$dirasg=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/';

		if(!is_dir($dirasg))
		mkdir($dirasg,0775);

		$dirasgcourse=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/'.$this->_courseid;

		if(!is_dir($dirasgcourse))
		mkdir($dirasgcourse,0775);

		$dirasgcourse=$dirasgcourse.'/'.$this->_asgid;

		if(!is_dir($dirasgcourse))
		mkdir($dirasgcourse,0775);

		$dirasgcourse=$dirasgcourse.'/'.$this->_username;

		if(!is_dir($dirasgcourse))
		mkdir($dirasgcourse,0775);

		$element->setLabel($this->getView()->translate('choose').' '.$this->getView()->translate('file').':')->setDestination(
		$dirasgcourse);
		
		$element->addValidator('Size', false, 20*1024*1024);
		// only JPEG, PNG, and GIFs
		$element->addValidator('Extension', false, 'doc,docx,pdf,xls,xlsx');
		$element->setRequired(true);
		$element->setDecorators(array(
		'File',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td' )),
		array('Label', array('tag' => 'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));


		$this->addElements(
		array( $asgid, $element));


		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('submit').' '.$this->getView()->translate('assignment'),
		'decorators'=>array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td',
		'align'=>'left', 'openOnly'=>true)),
		array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
		)
		));

		if ($this->_blockid!="") {
			$this->addElement('submit', 'cancel', array(
			'label'=>$this->getView()->translate('cancel'),
			'decorators'=>array(
			'ViewHelper',
			'Description',
			'Errors',
			array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
			'closeOnly'=>true)),
			array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
			),
			'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'assignment' , 'controller' => 'index' , 'action' => 'displayassignment', 'courseid' => $this->_courseid, 'blockid' => $this->_blockid, 'blockcontentid' => $this->_blockcontentid),'default', true) . "'; return false;"
			));
		}
		else {
			$this->addElement('submit', 'cancel', array(
			'label'=>$this->getView()->translate('cancel'),
			'decorators'=>array(
			'ViewHelper',
			'Description',
			'Errors',
			array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
			'closeOnly'=>true)),
			array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
			),
			'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'assignment' , 'controller' => 'index' , 'action' => 'displayassignment', 'courseid' => $this->_courseid, 'id' => $this->_asgid),'default', true) . "'; return false;"
			));
		}

		$this->setDecorators(array(



		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'



		));

	}


}