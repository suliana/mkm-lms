<?php
class Assignment_Form_SearchIntake extends Zend_Form
{
	protected $_intakeid;


	public function setIntakeid($value)
	{
		$this->_intakeid = $value;
	}

	public function init ()
	{

		$this->setMethod('post');

		$intake = new Admin_Model_Intake ();
		$result =  $intake->fetchAll();
		$intakeid = new Zend_Form_Element_Select('intakeid');

		$intakeid->setLabel($this->getView()->translate('semester_id').':')
		->setRequired(true);
		$intakeid->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($result as $c) 
		{
			if($this->_intakeid != "")
			{
				$selected = $this->_intakeid;
				$intakeid->setValue(array($c['id'] => $selected));
			}
			else
			{
				if ($c["intakestatus"]=="CURRENT")
				{
				    $selected = $c['id'];
					$intakeid->setValue(array($c['id'] => $selected));
				}
			}
			
			$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}

		$intakeid->setOptions(array('class'=>'select'));

		$keyword = $this->createElement('text', 'keyword');
		$keyword->setOptions(array('class'=> 'inputtext', 'filters' => array('StringTrim','StripTags') ));
		$keyword->setLabel($this->getView()->translate('title') )->setRequired(false);

		$register = $this->createElement('submit', 'register');
		$register->setLabel($this->getView()->translate('search_assignment'))->setIgnore(true);
		$register->setOptions(array('class'=>'btn submit'));


		$this->addElements(array($intakeid, $keyword, $register));
	}
}