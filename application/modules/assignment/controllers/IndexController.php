<?php
class Assignment_IndexController extends Zend_Controller_Action {

	public function init() 
	{
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';

	}
	
	public function indexAction() 
	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('assignment');
			
		$course = new Application_Model_Course();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;

		$intakeid = $this->_getParam ( 'intakeid', "" );

		//get current intake id
		if($intakeid=="")
		{
			$intake = new Admin_Model_Intake ();
			$result =  $intake->checkstatus('CURRENT');
			$intakeid = $result["id"]	;
		}
		
		$this->view->intakeid = $intakeid;

		$form = new Assignment_Form_SearchIntake( array ('intakeid' => $intakeid));
		$this->view->form = $form;


		//tambahan for student and EOS
		if ( $this->userinfo->privilage=="STUDENT" || $this->userinfo->privilage=="EOS")
		{
			if ($this->userinfo->privilage=="STUDENT") 
			{
				$assignsubject= new Course_Model_Student();
				$rs=$assignsubject->fetch($this->userinfo->username,$courseid);
			}
			else if ($this->userinfo->privilage=="EOS") 
			{
				$assignsubject= new Course_Model_Grader();
				$rs=$assignsubject->fetch($this->userinfo->username, $courseid);
			}

			if ($rs)
			{
				$intakeid=$rs["intakeid"];
			}
			else
			{
				$intakeid='0';
				//echo $intakeid;
				//exit();
			}
		}
		//end tambahan

		//echo $intakeid;
		$assignment = new Assignment_Model_Assignment ();
		$select = $assignment->findreturnselect ( $courseid, "", $intakeid );

		//capture the input params
		if ($this->_request->getParam('intakeid',"") || $this->_request->isPost())
		{
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost()){
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			//make sure the submitted data is valid and modify select statement
			if ($form->valid()){
				$data = $form->getValues ();
				$select = $assignment->findreturnselect ( $courseid, $data ['keyword'], $data ['intakeid'] );

			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'paginationsepecial2.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('intakeid'=>$form->getValue('intakeid'),'keyword'=>$form->getValue('keyword'));

	}


	public function deleteAction()
	{	
		$assignment = new Assignment_Model_Assignment ();
		$id = $this->_getParam ( 'rowid', 0 );
		$courseid = $this->_getParam ( 'courseid', '' );

		if ($id > 0)
		{
			$assignment->delete ( $id );
			$this->_redirect ( 'assignment/index/index/id/'.$courseid.'/courseid/'.$courseid );
		}
	}

	public function displayassignmentAction()
	{
		//$this->_helper->layout->disableLayout();
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('assignment');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch($courseid);

		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', "" );
		$this->view->blockid = $blockid;

		$blockcontentid = $this->_getParam ( 'blockcontentid', "" );
		$this->view->blockcontentid = $blockcontentid;

		$id = $this->_getParam ( 'id', 0 );

		$assignment = new Assignment_Model_Assignment ();
		if ($id>0)
		{
			$data=$assignment->fetch($id);
			$this->view->back=1;
		}
		else
		{
			$blockcontentid = $this->_getParam ( 'blockcontentid', "" );
			
			$data=$assignment->fetchblockcontent($blockcontentid);
			$this->view->back=2;
		}
		
		$this->view->data = $data;
	}



	public function uploadstudentAction()
	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('assignment');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$asgid = $this->_getParam ( 'id', 0 );
		$this->view->asgid = $asgid;

		$asgtype = $this->_getParam ( 'asgtype', 0 );
		$this->view->asgtype = $asgtype;


		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$blockid = $this->_getParam ( 'blockid', "" );
		$this->view->blockid = $blockid;

		$blockcontentid = $this->_getParam ( 'blockcontentid', "" );
		$this->view->blockcontentid = $blockcontentid;


		$assignment = new Assignment_Model_Assignment ();

		$form = new Assignment_Form_UploadStudent ( array ('courseid' => $courseid, 'username' => $this->userinfo->username, 'asgid' => $asgid, 'blockid' => $blockid, 'blockcontentid' => $blockcontentid ) );

		$this->view->form = $form;

		if ($this->getRequest ()->isPost ())
		{

			if ($form->isValid ( $_POST )) 
			{
				$sdata = $form->getValues ();
				$sdata["username"] = $this->userinfo->username;
				$sdata["datesubmit"]=date("Y-m-d H:i:s");

				$rs=$assignment->checksubmissionNew($username,$asgid);


				if (!$rs)
				{
					if ($form->filename->isUploaded ())
					{
						$locationFile = $form->filename->getFileName ();
						$sdata ["filename"] = date ( 'Ymdhs' ) . "_" . $sdata ["filename"];
						//echo $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/' .  $courseid . '/' . $sdata['asgid'] . '/' . $this->userinfo->username . '/' .$sdata ["filename"];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );


					} 
					else
					{
						$sdata ["filename"] = null;
					}
					
					$mainid=$assignment->submitassignment ( $sdata );
				}
				else
				{
					if ($form->filename->isUploaded ()) 
					{
						$locationFile = $form->filename->getFileName ();
						$sdata ["filename"] = date ( 'Ymdhs' ) . "_" . $sdata ["filename"];
						//echo $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/' .  $courseid . '/' . $sdata['asgid'] . '/' . $this->userinfo->username . '/' .$sdata ["filename"];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );

						$OldfullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/' .  $courseid . '/' . $sdata['asgid'] . '/' . $this->userinfo->username . '/' .$rs ["filename"];

						unlink($OldfullPathNameFile);


					}
					else
					{
						$sdata ["filename"] = null;
					}
					
					$assignment->updatemaintable($sdata,$rs['id']);
				}


				if ($blockid!="")
				{
					$this->_redirect ( 'assignment/index/displayassignment/courseid/'.$courseid.'/blockid/'.$blockid.'/blockcontentid/'.$blockcontentid );
				}
				else
				{
					$this->_redirect ( 'assignment/index/displayassignment/courseid/'.$courseid.'/id/'.$asgid );
				}
			}
		}

	}

	public function viewsubmissionAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$asgid = $this->_getParam ( 'id', 0 );
		$this->view->asgid = $asgid;

		$username = $this->_getParam ( 'username', 0 );
		$this->view->username = $username;

		$asgtype = $this->_getParam ( 'asgtype', 0 );
		$this->view->asgtype = $asgtype;
	}



	public function removefileAction()
	{
		$assignment = new Assignment_Model_Assignment ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$asgid = $this->_getParam ( 'asgid', 0 );
		$this->view->asgid = $asgid;

		$username = $this->_getParam ( 'username', 0 );
		$this->view->username = $username;

		$asgtype = $this->_getParam ( 'asgtype', 0 );
		$this->view->asgtype = $asgtype;

		if ($asgtype==1)
		{
			$result=$assignment->check_submissionNew($username,$asgid);
		}
		else
		{
			//$result=$assignment->checksubmission_group($username,$asgid);
		}
		
		foreach ($result as $rs)
		{
			$filename= $rs["filename"];
			$path=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/'.$courseid.'/'.$asgid.'/'.$rs["username"].'/';

			$fullpath=$path.$filename;
			unlink ($fullpath);

			if ($asgtype==1)
			{
				//delete assignment_files
				$assignment->delete_asg_group_files($rs["asgid"],$filename,$rs["fid"],$rs["id"]);
				//$assignment->delete_asg($asgid,$username,$filename);
			}
			else
			{
				//				$assignment->delete_asg($asgid,$rs["username"],$filename);
				//				$assignment->delete_asg_group($rs["asgid"],$filename,$rs["asg_subID"]);
			}
		}
		
		$assignment->delete_asgNew($asgid,$username);
		
		//redirect page
		$this->_redirect ( 'assignment/index/uploadstudent/id/'.$asgid.'/courseid/'.$courseid.'/asgtype/'.$asgtype );
	}

	public function submissionlistAction ()
	{
		$userinfo = $this->userinfo->username;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$asid = $this->_getParam ( 'id', 0 );
		$this->view->asid = $asid;

		$asgtype = $this->_getParam ( 'asgtype', 0 );
		$this->view->asgtype = $asgtype;


		$form = new Assignment_Form_Search ();
		$this->view->form = $form;
		$assignment = new Assignment_Model_Assignment ();

		$intakeid = "";
		
		//tambahan for FACILITATOR
		if ($data->privilage=="EOS")
		{
			$assignsubject= new Assignment_Model_Reminder();
			$rs=$assignsubject->fetch("",$username,$courseid);
			if ($rs)
			$intakeid=$rs["intakeid"];
			else
			$intakeid='0';
			//echo $intakeid;
			//exit();
		}
		//end tambahan

		//$this->view->entries = $assignment->fetchAll();
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$select = $assignment->returnsubmission ( $courseid, $intakeid, $asid, $asgtype, $username );
		$paginator = Zend_Paginator::factory ( $select );
		//$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
	}

	public function individulistAction ()
	{
		$username = $this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$asid = $this->_getParam ( 'id', 0 );
		$this->view->asid = $asid;

		$asgtype = $this->_getParam ( 'asgtype', 0 );
		$this->view->asgtype = $asgtype;

		$type = $this->_getParam ( 'type', 0 );
		$this->view->type = $type;


		$form = new Assignment_Form_Search ();
		$this->view->form = $form;
		$assignment = new Assignment_Model_Assignment ();

		$intakeid = "";
		//tambahan for FACILITATOR
		if ($data->privilage=="EOS")
		{
			$assignsubject= new Assignment_Model_Reminder();
			$rs=$assignsubject->fetch("",$username,$courseid);
			if ($rs)
			$intakeid=$rs["intakeid"];
			else
			$intakeid='0';
			//echo $intakeid;
			//exit();
		}
		//end tambahan

		$intake = new Admin_Model_Intake ();
		$rs =  $intake->fetch($intakeid);
		$this->view->intakecode = $rs["intakecode"];
		$this->view->intakeid = $intakeid;

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$select = $assignment->returnsubmission ( $courseid, $intakeid, $asid, $asgtype, $username );
		$paginator = Zend_Paginator::factory ( $select );
		//$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$msg = $this->_flashMessenger->getMessages();
		if($msg!=null){
			$this->view->noticeMessage = $msg[0];
		}
	}

	public function liststudentAction() 
	{
		$semid= $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$asgid= $this->_getParam ( 'asgid', 0 );
		$this->view->asgid = $asgid;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('assignment')." > ".$this->view->translate('view'). " ".$this->view->translate('submission_files')." > ".$this->view->translate('student_list');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$student = new Course_Model_Student ();
		$condition=array('courseid'=>$courseid,'intakeid'=>$semid)	;
		$select = $student->coursestudentlist ($condition);

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

	}

	public function downloadAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );

		$pageitem = $this->_getParam ( 'pageitem', 0 );

		$semid = $this->_getParam ( 'semid', 0 );

		$asgid = $this->_getParam ( 'asgid', 0 );

		$zip = new ZipArchive();

		$dirasgzip=DOC_PATH.'/'.APP_FOLDER.'/upload/zip/';

		if(!is_dir($dirasgzip))
		mkdir($dirasgzip,0775);

		$dirasgzip=DOC_PATH.'/'.APP_FOLDER.'/upload/zip/assignment/';

		if(!is_dir($dirasgzip))
		mkdir($dirasgzip,0775);

		$dirasgzip=DOC_PATH.'/'.APP_FOLDER.'/upload/zip/assignment/'.$courseid.'/';

		if(!is_dir($dirasgzip))
		mkdir($dirasgzip,0775);

		$fileconpress = $dirasgzip.'/assignment.zip';

		$this->view->fileconpress=$fileconpress;

		$conpress = $zip->open($fileconpress, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE );

		if ($conpress === true)
		{
			for ($ctr = 1 ; $ctr <= $pageitem ; $ctr++ )  
			{

				$sdata['filename']=$this->_getParam ( 'C'.$ctr, 0 );
				$sdata['username']=$this->_getParam ( 'CUsername'.$ctr, 0 );

				if ($sdata['username']!="")
				{
					$destination=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/'.$courseid.'/'.$asgid.'/'.$sdata['username'].'/'.$sdata['filename'];
					$zip->addFile($destination,$sdata['filename']);
				}
			}

			$zip->close();

			$this->view->message= $this->translate('success');
		}
		else
		{
			$this->view->message= $this->translate('error');
		}
	}

}
