<?php 
class Languageset_Model_Configurer extends Zend_Db_Table
{
	protected $_name = 'sys_language_configurer';
	protected $_primary = 'ID';
	public function getDataByCode($code)
    {
        $sql = $this->_db->select()
        	->where('Code = ?',$code)
            ->from($this->_name);
        $stmt = $this->_db->query($sql);
        $result = $stmt->fetchAll();
        return $result;
    }
	
	public function fetchAll()
	{	
		$sql = $this->_db->select()->from($this->_name);
        
		$stmt = $this->_db->query($sql);
        $result = $stmt->fetchAll();
        return $result;
	}
	
	public function fetch ($id = "")
	{
        $sql = $this->_db->select()->from($this->_name);
        
        if ($id != "") {
            $sql->where($this->_primary.' = ?', $id);
        }

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
	
    public function modify ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    
 	/*
	 * 	Insert
	 */
	public function insert($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    
	public function delete ($id)
	{
		//delete all terms and delete language
		// delete from sys_language where slc_id = id
		// delete from sys_language_config where ID = id
		if ( $id != 1 )
		{ 
			 $this->_db->delete('sys_language', array( 'slc_id = ?' => $id ) );
			 $this->_db->delete($this->_name, array( 'ID = ?' => $id ) );
		}
		else
		{
			//make this pretty please?
			die('cant delete english');
		}
    }
    
	public function find ($keyword = "")
	{
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }

        $result = $this->_db->query($sql);
        return $result;
    }
    
}
?>
