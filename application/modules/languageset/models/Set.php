<?php
/*
 * 	yoleeeeeeee
 */ 
class Languageset_Model_Set extends Zend_Db_Table
{
	protected $_name = 'sys_language';
	protected $_configurer = 'sys_language_configurer';
	
	/*
	 * 	Fetching Results
	 */
	public function fetchAll($queryonly=true)
	{
		
		$sql = $this->_db->select()->from($this->_name);
    
        
		if ( $queryonly )
		{
			$stmt = $this->_db->query();
        	return $stmt;
		}
		else
		{
			$results = $this->_db->fetchAll($sql);
			
			return $results;
		}
	}
	
	public function getLangs($byid=0)
	{
		//im going to cheat here
		$ls = new Epic_LangSelector();
		$results = $ls->Get_All_Language_List();
		
		if ( $byid )
		{
			$new = array();
			foreach ( $results as $code => $result )
			{
				$new[$result[2]] = array(
											'ID'		=> $result[2],
											'Name' 		=> $result[0],
											'Code'		=> $code,
											'Direction' => $result[1]
										);
			}
			
			return $new;
		}
		else
		{
			return $results;		
		}
	
	}
	
	/*
	 * 	Fetching Terms by set_ID
	 */
	public function fetchByLang($id = '')
	{
        $sql = $this->_db->select()->from($this->_name);
        
        if ($id != "")
        {
            $sql->where('slc_id = ?', $id);
        }

        $results = $this->_db->fetchAll($sql);
        return $results;
    }
    
    // lang id
    public function fetchConfBy($id='',$type='ID')
    {
    	$sql = $this->_db->select()->from($this->_configurer);
    	if ( $id != '' )
    	{
    		$sql->where($type.' = ?', $id);
    	}
    	
    	$result = $this->_db->fetchRow($sql);
    	
    	return $result;
    }
    
    /*
	 * 	Fetching Term by ID
	 */
	public function fetch ($id = "")
	{
        $sql = $this->_db->select()->from($this->_name);
        
        if ($id != "") {
            $sql->where('id = ?', $id);
        }

        $result = $this->_db->fetchRow($sql);
        return $result;
    }
	
     /*
	 * 	fetching default term
	 */
  	public function fetchTerm($varname='',$slc=1)
  	{
  		$sql = $this->_db->select()->from($this->_name);
        
        if ($varname != "") 
        {
            $sql->where('varname = ?', $varname);
            $sql->where('slc_id = ?', $slc); // default is england
        }
	
        
        $result = $this->_db->fetchRow($sql);
        return $result;
  	}
  	
  	  /*
	 * 	fetching term from all languages
	 */
	public function fetchTermAll($varname='')
  	{
  		$sql = $this->_db->select()->from($this->_name);
        
        if ($varname != "") 
        {
            $sql->where('varname = ?', $varname);
 
        }

        $results = $this->_db->fetchAll($sql);
        
        return $results;
  	}
  	
  	
    /*
	 * 	Update all terms by varname.
	 */
  	public function updateTerm($data='')
  	{
  		//get langs
  		$langs = $this->getLangs(1);
  		
  		/*
  		 * Array ( 
  		 * 	[varname] => activity
  		 * 	[meaning_1] => Activity 
  		 * 	[meaning_2] => Aktiviti 
  		 * 	[meaning_4] =>
  		 * 	[save] => Save ) 
  		 */
  		
  		if ( is_array($langs) )
  		{
	  		foreach ( $langs as $lang_id => $lang_row )
	  		{
	  			$check = $this->fetchTerm($data['varname'], $lang_id);
	  		
	  			//exists or not? how to update if doesnt exists
	  			if ( !is_array($check) )
	  			{
	  				$this->insert
		  			( 
			  			array('varname' => $data['varname'], 'slc_id' => $lang_id, 'meaning' => $data['meaning_'.$lang_id] )
		  			);
	  			}
	  			else
		  		{		  			
		  			$this->update
		  			( 
			  			array('meaning' => $data['meaning_'.$lang_id] ),
			  			array('varname = ?' => $data['varname'], 'slc_id = ?' => $lang_id )
		  			);
		  		}
	  			
	  		} // foreach
	  		
  		}
  		
  		// all done
  	}
  	
	/*
	 * 	Update all terms by varname.
	 */
  	public function addTerm($data='')
  	{
  		//get langs
  		$langs = $this->getLangs(1);
  				
  		/*
  		 * Array ( 
  		 * 	[varname] => activity
  		 * 	[meaning_1] => Activity 
  		 * 	[meaning_2] => Aktiviti 
  		 * 	[meaning_4] =>
  		 * 	[save] => Save ) 
  		 */
  		
  		if ( is_array($langs) )
  		{
	  		foreach ( $langs as $lang_id => $lang_row )
	  		{
	  			$check = $this->fetchTerm($data['varname'], $lang_id);
	  		
	  			//exists or not? how to update if doesnt exists
	  			if ( !is_array($check) )
	  			{
	  				$this->insert
		  			( 
			  			array('varname' => $data['varname'], 'slc_id' => $lang_id, 'meaning' => $data['meaning_'.$lang_id] )
		  			);
	  			}
	  			
	  		} // foreach
	  		
  		}
  		
  		// all done
  	}
  	
	
  	 /*
	 * 	Insert
	 */
	public function insert($data)
    {
        $this->_db->insert($this->_name, $data);
    }
  	/*
	 * 	Update
	 */
    public function update($data, $where)
    {
    	
    	//$sql ='UPDATE '.$this->_name.' SET meaning='.$this->_db->quote($data['meaning']).' WHERE slc_id=\''.$where['slc_id'].'\' AND varname='.$this->_db->quote($where['varname']);
    	//$this->_db->query($sql);
    
      	$this->_db->update($this->_name, $data, $where);
    }
    
    /*
	 * 	Delete
	 */
	public function delete ($id)
	{
        $this->_db->delete($this->_name, array( 'varname = ?' => $id ) );
    }
    
    /*
	 * 	Find Results
	 */
	public function find ($keyword = '', $slc_id='')
	{
        $sql = $this->_db->select()->from($this->_name);
        
        if ($keyword != '')
        {
            $sql->where(' varname LIKE ? ','%' . $keyword . '%');
        }
        
        if ( $slc_id != '' )
        {
        	$sql->where('slc_id = ?', $slc_id);
        }

        $results = $this->_db->fetchAll($sql);
        return $results;
       
    }
    
}
?>
