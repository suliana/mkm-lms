<?php 
class Languageset_ConfigurerController extends Zend_Controller_Action
{
    
	public function init ()
	{
    	$this->view->title = 'language_conf_main';
    	
    	$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
    }
    
    public function indexAction ()
    {
    	$session = new Zend_Session_Namespace('language');
    	
    	$doLangConfDB = new Languageset_Model_Configurer();
    	$langConf = $doLangConfDB->getDataByCode($session->lang);
    	$this->view->current_language = $langConf[0]["Name"];
    	
    	$this->view->langsets = $doLangConfDB->fetchAll();
    	//$this->layout()->langLang = $this->view->langsets;
    }
    
    public function addAction ()
    {
    	$this->view->title = "language_conf_add";

		$form = new Languageset_Form_Configurer();
		
		$form->setAction($this->view->baseUrl().'/languageset/configurer/add');
		if ($this->getRequest()->isPost())
		{

			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) 
			{
				$formData = $form->getValues();
				
				//process form
				$doLangConfDB = new Languageset_Model_Configurer();
				$doLangConfDB->insert(array(
					'Name'=>$formData['Name'],
					'Code'=>str_replace(' ', '_', $formData['Code']),
					'Direction'=>$formData['Direction']
				));
				
//								
//				$nfilePath = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR;
//				$fileName = escapeshellcmd(str_replace(' ', '_', $formData['Code']));
//				$fp = fopen($nfilePath.$fileName, 'w');
//				fwrite($fp, );
				
				$this->_redirect($this->view->url(array('module'=>'languageset', 'controller'=>'configurer'),'',false));
			}
			else
			{
				$form->populate($formData);
			}
		}

		$this->view->form = $form;
    }
   	
    public function editAction()
    {
    	
    	$this->view->title = "language_conf_edit";
    	
    	$langconfid = $this->_getParam('id', 0);
    	
    	$form = new Languageset_Form_Configurer();
    	$form->setAction($this->view->baseUrl().'/languageset/configurer/edit/id/'.$langconfid);
    	
    	if ($this->getRequest()->isPost()) 
    	{
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) 
			{
				$formData = $form->getValues();

				//process form
				$doLangConfDB = new Languageset_Model_Configurer();
				$doLangConfDB->update(array(
				'Name'=>$formData['Name'],
				'Code'=>$formData['Code'],
				'Direction'=>$formData['Direction'],
				'IsDefaultStartup'=>$formData['IsDefaultStartup']
				), 'ID='.$langconfid);
				
				//redirect
				//$this->_redirect($this->view->url(array('module'=>'languageset', 'controller'=>'configurer'),'',false));
				$this->_redirect('languageset/configurer');
			}
			else
			{
				$form->populate($formData);
			}
		}
		else
		{
			$doLangConfDB = new Languageset_Model_Configurer();
			$formData = $doLangConfDB->fetch($langconfid);
			
			$form->populate($formData);
		}
		
    	$this->view->form = $form;
    }
    
    
    public function viewAction()
    {
    	$this->view->title = "language_conf_view";
    	
    	$langconfid = $this->_getParam('id', 0);
    	
    	$doLangConfDB = new Languageset_Model_Configurer();
    	$langConf = $doLangConfDB->fetch($langconfid);
    	$this->view->langName = $langConf['Name'];
    	$this->view->langCode = $langConf['Code'];
    	$this->view->langDirSet = $langConf['Direction'];
    	$this->view->langID = $langConf['ID'];
    }
	
	public function deleteAction()
	{
    	$id = $this->_getParam('id', 0);
    	
    	if ($id>0)
    	{
    		$doLangConfDB = new Languageset_Model_Configurer();
    		$doLangConfDB->delete($id);
    	}
    		
    	//$this->_redirect($this->view->url(array('module'=>'languageset', 'controller'=>'configurer'),'default',true));
    	$this->_redirect('languageset/configurer');
    }
}
?>