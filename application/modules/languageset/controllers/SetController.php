<?php
class LanguageSet_SetController extends Zend_Controller_Action
{
    public function init ()
    {
        $storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
    }
    
    public function indexAction ()
    {
    	$this->view->title = 'language_set_title';
    	
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->locale = $locale->toString();
		
        $this->view->current_language = $this->locale;
        
        //$db = Zend_Registry::get("db");
        $model = new Languageset_Model_Set();
 
			
        // false to return results also
        $results = $model->fetchAll(false);
      	
        
		$page = $this->_getParam('page',1);
      	$paginator = Zend_Paginator::factory($results);
     	$paginator->setItemCountPerPage(20);
      	$paginator->setCurrentPageNumber($page);
      	
      	$this->view->paged_results = $paginator;
    }
    
    // godmode function
   /*
    *  disabled. finished import hack 14/5/2013
    *  
    *  public function importAction()
    {
    	$arabic = $this->get_csvdata('arabic.csv');
		
    	$sorted = array();
    	
    	foreach ( $arabic as $data )
    	{
    		$sorted[$data[0]] = $data[1];
    	}
    	
    	print_R($sorted);
    	
    	// too lazy to do this, just fill in data using mass pl0x
    }
    
    private function get_arabic()
    {
   		$arabic = $this->get_csvdata('arabic.csv');
		
    	$sorted = array();
    	
    	foreach ( $arabic as $data )
    	{
    		$sorted[$data[0]] = $data[1];
    	}
    	
    	return $sorted;
    }
    
    private function get_csvdata($file='')
    {
    	setlocale(LC_ALL, 'en_US.UTF-8');
	    
	    if (($handle = fopen($file, "r")) !== FALSE)
	    {
	        $nn = 0;
	        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
	        {
	            $c = count($data);
	            for ($x=0;$x<$c;$x++)
	            {
	                $csvarray[$nn][$x] = $this->get_encode( $data[$x] );
	            }
	            $nn++;
	        }
	        # Close the File.
	        fclose($handle);
	    }
	    
	    return $csvarray;
    }
    private function get_encode($name='')
	{
		$enc = mb_detect_encoding($name, "UTF-8,ISO-8859-1");
		$name = iconv($enc, "UTF-8", $name);
	
		return $name;
	} */
	    

    // godmode function
    public function massAction()
    {
    	//general crap
    	$model = new Languageset_Model_Set();
    	$this->view->title = "language_conf_massedit"; 
    	
    	//$importdata = $this->get_arabic();
    	
    	//process POST data
    	if ($this->getRequest()->isPost())
    	{
			$data = $this->getRequest()->getPost();
			$model = new Languageset_Model_Set();
			
			//ezpz
			if ( is_array($data['meaning']) )
			{
				foreach ( $data['meaning']  as $lang_id => $lang )
				{
					if ( $lang_id > 1 )
					{
						foreach ( $lang as $varname => $meaning )
						{
							
							$check = $model->fetchTerm($varname, $lang_id);
	  		
				  			//exists or not? how to update if doesnt exists
				  			if ( !is_array($check) )
				  			{
				  				//echo 'insert '.$varname.' for '.$lang_id.' -> '.$meaning.' <br />';
				  				$model->insert
					  			( 
						  			array('varname' => $varname, 'slc_id' => $lang_id, 'meaning' => $meaning )
					  			);
				  			}
				  			else
					  		{		
					  			//echo 'update '.$varname.' for '.$lang_id.' -> '.$meaning.' <br />';  			
					  			$model->update
					  			( 
						  			array('meaning' => $meaning ),
						  			array('varname = ?' => $varname, 'slc_id = ?' => $lang_id )
					  			);
					  		}
						}
					}
				}
			}// is_array
			
    	}
    	
    	$langs = $model->getLangs(1);
    	
    	$this->view->lang_info = $langs;
    	$this->view->lang_list = array();
    	$this->view->lang_terms = array();
    	
    	foreach ( $langs as $lang_id => $lang )
    	{
    		$this->view->lang_list[$lang_id] = $lang['Name'];
    		$this->view->lang_terms[$lang_id] = $this->formatByVarname( $model->fetchByLang($lang_id) );
    	}
    	
    	//cheating
    	//$this->view->lang_terms[5] = $importdata;
    }
    
    
	public function viewAction()
    {
    	//general crap
    	$model = new Languageset_Model_Set();
    	$this->view->title = "language_conf_viewset"; 
 		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->locale = $locale->toString();
		
        $this->view->current_language = $this->locale;
        
        
        // Get Language Set ID
    	$set_id = (int) $this->_getParam('id', 0); //
    	$this->view->set_id = $set_id; 
    	
    	if ( $set_id == 0 )
    	{
    		$this->view->paged_results  = '<div class="error">'.$this->view->translate('Invalid Set ID').'</div>';
    		return;
    	}
    	
    	//
    	$lang_config = $model->fetchConfBy($set_id);   	
    	$this->view->lang_config = $lang_config;
    	
        //get default language configurer by its Code (since stored that way?)
        $default_lang = $model->fetchConfBy($this->locale,'Code');
        $this->view->default_lang = $default_lang['Name'];
        
     
        if ( $default_lang['ID'] == $set_id )
        {
        	$this->view->no_reference = 1;
        }
        else
        {
        	//$this->view->no_reference = 0;
        	
        	//get language by id
	        $default_terms = $model->fetchByLang($default_lang['ID']);
	        $this->view->default_terms = $this->formatByVarname( $default_terms );
        }
        
        
           
        //search
        if ($this->_getParam('keyword') )
		{
			$this->view->isSearch = 1;
			
			$keyword = $this->_getParam('keyword');
			$this->view->keyword = $keyword;
			
			$results = $model->find($keyword, $set_id);
		}
		else
		{	
	        //current selected language 
	        $results = $model->fetchByLang($set_id);
		}
		
		$this->view->total = count($results);
		
		if ( $this->view->total > 0 )
		{
	        // paging da results
			$page = $this->_getParam('page',1);
	      	$paginator = Zend_Paginator::factory($results);
	     	$paginator->setItemCountPerPage(20);
	      	$paginator->setCurrentPageNumber($page);
	      	$this->view->paged_results = $paginator;
		}
		else
		{
			$this->view->paged_results = '';
		}
      	
    }
    
	public function editAction()
	{

		//set title
    	$this->view->title = "language_conf_edit_term";
    	
    	//get varname from url
    	$varname = $this->_getParam('id', 0);
    	if ( $this->_getParam('from', 0) )
    	{
    		$from = $this->_getParam('from', 0);
    		$this->view->set_id = $from;
    	}
    	
    	
		//check if varname exists
    	$model = new Languageset_Model_Set();
    	
    	// get default term and check if exists
    	$def_term = $model->fetchTerm($varname);
    	if ( !is_array($def_term) )
    	{
    		$this->view->form  = '<div class="error">'.$this->view->translate('error_invalid_term').'</div>';
    		return;
    	}
    	
    	//form
    	$langs = $model->getLangs();
   
    	
    	$form = new Languageset_Form_Set('', array('langs' => $langs) );
    	$form->setAction($this->view->baseUrl().'/languageset/set/edit/id/'.$varname);
    	
    	
    	//process POST data
    	if ($this->getRequest()->isPost())
    	{
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) 
			{
				$formData = $form->getValues();
				
				//process form
				$model = new Languageset_Model_Set();
				$model->updateTerm($formData);
				
				//redirect
				$this->_redirect('languageset/set/edit/id/'.$varname);
			}
			else
			{
				$form->populate($formData);
			}
		}
		else
		{
			$model = new Languageset_Model_Set();
			
			$formData = $model->fetchTermAll($varname);
			$formData = $this->formatTerm($varname, $formData);
		
			$form->populate($formData);
		}
		
    	$this->view->form = $form;
    }
    
	public function addAction()
	{

		//set title
    	$this->view->title = "language_conf_add_term";
    	
    	//get varname from url
    	$set_id = $this->_getParam('id', 0);
    	
		//check if varname exists
    	$model = new Languageset_Model_Set();
    	
    	//form
    	$langs = $model->getLangs();
   
    	
    	$form = new Languageset_Form_Set('', array('langs' => $langs) );
    	$form->setAction($this->view->baseUrl().'/languageset/set/add/id/'.$set_id);
    	
    	
    	//process POST data
    	if ($this->getRequest()->isPost())
    	{
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid($formData)) 
			{
				$formData = $form->getValues();
				
				//additional checking
				$varname = $formData['varname'];
				
				if ( strlen($varname) < 3 )
				{
					$this->view->form  = '<div class="error">'.$this->view->translate('error_term_tooshort').'</div>';
		    		return;
				}
				
				$check_term = $model->fetchTerm($varname);
		    	if ( is_array($check_term) )
		    	{
		    		$this->view->form  = '<div class="error">'.$this->view->translate('error_term_exists').'</div>';
		    		return;
		    	}
		    	
				//process form
				$model = new Languageset_Model_Set();
				$model->addTerm($formData);
				
				//redirect
				$this->_redirect('languageset/set/view/id/'.$set_id);
			}
			else
			{
				$form->populate($formData);
			}
		}
		else
		{
			$model = new Languageset_Model_Set();			
			$form->populate($_POST);
		}
	
    	$this->view->form = $form;
    }
    
	public function deleteAction()
	{
    	$id = $this->_getParam('id', 0);
		if ( $this->_getParam('from', 0) )
    	{
    		$set_id = $this->_getParam('from', 0);
    		$this->view->set_id = $set_id;
    	}
    	
    	if($id != '')
    	{
    		$model = new Languageset_Model_Set();
    		$model->delete( $id );
    	}
    		
    	if ( $set_id != '')
    	{
    		$this->_redirect('languageset/set/view/id/'.$set_id);
    	}
    	else
    	{
    		$this->_redirect('languageset/configurer');
    	}
    	
    }
    
    public function formatTerm($varname='', $data='')
    {
    	$newdata['varname'] = $varname;
    	

    	
    	foreach ( $data as $term )
    	{
    		$newdata['meaning_' . $term['slc_id'] ] = htmlspecialchars_decode($term['meaning']);
    	}
   	
    	return $newdata;
    }
    
    public function formatByVarname($terms='')
    {
    	if ( !is_array($terms) ) return;
    	
    	$newterms = array();
    	foreach ($terms as $term)
    	{
    		$newterms[$term['varname']] = $term['meaning'];
    	}
    	
    	return $newterms;
    }
    
    
    
    
}





