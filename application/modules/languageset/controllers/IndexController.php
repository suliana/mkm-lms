<?php
class LanguageSet_IndexController extends Zend_Controller_Action
{
    public function init ()
    {
        $this->view->title = 'language_conf_title';
        
        $storage = new Zend_Auth_Storage_Session();
		$data = $storage->read();
		if (! $data) {
			$this->_redirect('index/index');
		}
		$this->view->username=$data->username;
		$this->view->lastlogin=$data->last_login;
    }
    
    public function indexAction ()
    {
    	$this->_redirect($this->view->url(array('module'=>'languageset', 'controller'=>'configurer'),'default',true));
    	
    	/*
		$registry = Zend_Registry::getInstance();
		$locale = $registry->get('Zend_Locale');
		$this->locale = $locale->toString();
		
        $this->view->current_language = $this->locale;
        
        $db = Zend_Registry::get("db");
		$result = $db->fetchAll('SELECT * FROM `sys_language_configurer`');
	    $this->view->langsets = $result; */
    }
}