<?php 
class Languageset_Form_Configurer extends Zend_Form
{
	public function init()
	{
		
		$this->setMethod('post');
		
		$lang_name = $this->createElement('text', 'Name', array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')) );
		$lang_name->setLabel( $this->getView()->translate('language_name') )->setRequired(true);

		$lang_name->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					
					array('Label', array('tag' => 'strong')),
					array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
        ));
		
		
		$lang_code = $this->createElement('text', 'Code', array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')) );

		$lang_code->setLabel($this->getView()->translate('language_code'))->setRequired(true);
		$lang_code->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					
					array('Label', array('tag' => 'strong')),
					array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
        ));
        
        
        /* $lang_default_startup = new Zend_Form_Element_Radio(IsDefaultStartup);
		$lang_default_startup->setLabel($this->getView()->translate('language_default_startup'))
							->addMultiOptions(array(
							1 => 'Yes', 0 => 'No' ))->setSeparator(' ');
		$lang_default_startup->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					
					array('Label', array('tag' => 'strong')),
					array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
        )); */
        
		
		$lang_dir_set = new Zend_Form_Element_Select(Direction);
		$lang_dir_set->setLabel($this->getView()->translate('language_direction') )
					->addMultiOptions( array('ltr' => 'Left to Right','rtl' => 'Right to Left'));
		$lang_dir_set->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					
					array('Label', array('tag' => 'strong', 'closeOnly'=>true)),
					array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row' ))
        ));
        
        $this->addElements(array($lang_name, $lang_code,  $lang_default_startup, $lang_dir_set) );
        
        //buttons
		$this->addElement('submit', 'save', array(
          	'label'=> $this->getView()->translate('save'),
          	'class'=>'btn submit',
          	'decorators'=>array('ViewHelper'),
        ));
    	
        $this->addElement('submit', 'cancel', array(
          	'label'=> $this->getView()->translate('cancel'),
          	'class'=>'btn cancel',
          	'decorators'=>array('ViewHelper'),
        	'onclick'=> "window.location ='" . $this->getView()->url(array('module'=>'languageset', 'controller'=>'configurer'),'default',true) . "'; return false;"
        ));
    	
    	$this->addDisplayGroup( array('save', 'cancel'), 'buttons', array(
      		'decorators'=>array('FormElements',array('HtmlTag', array('tag'=>'div', 'class'=>'submit')) )
    	));
        

		$this->setDecorators(array(
               'FormElements',
               array(array('data'=>'HtmlTag'),array('tag'=>'div', 'class'=>'formnice', 'id'=>'editlangconf')),
               'Form'
       ));
		
	}
}
?>