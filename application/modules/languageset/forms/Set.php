<?php 
class Languageset_Form_Set extends Zend_Form
{
 	protected $_info;

    public function __construct($options = null, $info = null)
    {
        $this->_info = $info;
        parent::__construct($options);
    }
	
    
	public function init()
	{
		
		$this->setMethod('post');
		
		$lang_varname = $this->addElement(
		                    'text',
		                    'varname',
		                    array(
		                            'class'		  => 'inputtext',
		                    		'filters'    => array('StringTrim','StripTags'),
		                    		'required'    => true,
		                            'label'       => $this->getView()->translate('language_varname'),
		                    		'decorators'  => array
		                    						(
														'ViewHelper',
									 					'Description',
														'Errors',
														
														array('Label', array('tag' => 'strong')),
														array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
											        ),
		                    )
            			);
            			

        
        foreach ( $this->_info['langs'] as $lcode => $ldata )
        {
			$this->addElement(
			                    'textarea',
			                    'meaning_'.$ldata[2],
			                     array(
			                            'required'    =>($lcode == 'en' ? true : false),
			                            'filters'    => array(),
			                            'label'       => '<a href="'.$this->getView()->url(array('module' => 'languageset', 'controller' => 'set','action'=>'view','id'=>$ldata[2]),'',true).'">'.$ldata[0].'</a> - '.$this->getView()->translate('language_meaning'),  
			                            'decorators'  => array
			                    						(
															'ViewHelper',
										 					'Description',
															'Errors',
															
															array('Label', array('tag' => 'strong','escape' => false)),
															array(array('row'=>'HtmlTag'),array('tag'=>'div', 'class' => 'row'))
												        ),
			                    )
	            			);
        }
	          
	
        
        //buttons
		$this->addElement('submit', 'save', array(
          	'label'=> $this->getView()->translate('save'),
          	'class'=>'btn submit',
          	'decorators'=>array('ViewHelper'),
        ));
    	
        $this->addElement('submit', 'cancel', array(
          	'label'=> $this->getView()->translate('cancel'),
          	'class'=>'btn cancel',
          	'decorators'=>array('ViewHelper'),
        	'onclick'=> "window.location ='" . $this->getView()->url(array('module'=>'languageset', 'controller'=>'configurer'),'default',true) . "'; return false;"
        ));
    	
    	$this->addDisplayGroup( array('save', 'cancel'), 'buttons', array(
      		'decorators'=>array('FormElements',array('HtmlTag', array('tag'=>'div', 'class'=>'submit')) )
    	));
        

		$this->setDecorators(array(
               'FormElements',
               array(array('data'=>'HtmlTag'),array('tag'=>'div', 'class'=>'formnice', 'id'=>'editlangconf')),
               'Form'
       ));
		
	}
}
?>