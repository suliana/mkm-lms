<?php
class Activities_Form_Upload extends Zend_Form
{
	
protected $_courseid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$title = $this->createElement('text', 'title');
        $title->setLabel($this->getView()->translate('title').':')->setRequired(true);
        
        $title->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $ccontent = $this->createElement('textarea', 'ccontent');
        $ccontent->setLabel($this->getView()->translate('activity').':')->setRequired(true);
        
        $ccontent->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $courseid = $this->createElement('hidden', 'courseid');
        /*$courseid->setLabel('Course ID:')->setRequired(false);
        $courseid->setAttrib('readonly','readonly');*/
        $courseid->setValue($this->_courseid);
        
        $courseid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
                
        $intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel($this->getView()->translate('semester_id').':')
		->setRequired(true);
		foreach ($result as $c) {
		$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
		
		$intakeid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
			
        $this->addElements(
        array($title, $ccontent, $intakeid, $courseid));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('upload').' '.$this->getView()->translate('activity'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'activities', 'controller'=>'index','action'=>'index','courseid' => $this->_courseid),'default',true) . "'; return false;"
        ));
        
        /*$this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));*/
	    
	    $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       ));
    }
}