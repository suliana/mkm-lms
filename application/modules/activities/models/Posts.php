<?php

class Activities_Model_Posts extends Zend_Db_Table
{
	protected $_name = "facebook_posts";
	protected $_like = "facebook_likes";
	protected $_comment = "facebook_posts_comments";
	protected $_ip = "facebook_ip";



	public function checkValues($value)
	{
		//$this->conn->debug=1;
		$value = trim($value);

		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}

		$value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));

		$value = strip_tags($value);
		$value = mysql_real_escape_string($value);
		//$value=$this->conn->qstr($value);
		$value = htmlspecialchars ($value);
		return $value;

	}


	public function clickable_link($text = '')
	{
		$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
		$ret = ' ' . $text;
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);

		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
		$ret = substr($ret, 1);
		return $ret;
	}


	public function postactivity($value,$cid,$username,$userip,$largeimage,$valueurl,$valueyoutubeurl,$thumbimage,$valueresource)
	{

		$sql="INSERT INTO $this->_name (post,courseid,f_name,userip,date_created,username,f_image,f_link,f_youtubelink,f_image_thumb,f_resid) VALUES('".$this->checkValues($value)."','".$this->checkValues($cid)."','".$username."','".$userip."','".strtotime(date("Y-m-d H:i:s"))."','".$username."','".$largeimage."','".$this->checkValues($valueurl)."','".$this->checkValues($valueyoutubeurl)."','".$thumbimage."',".$this->checkValues($valueresource).")";
		
		echo $sql;
		exit();
		$this->_db->query($sql);
		

	}


	public function timespent($cid,$show_more_post="")
	{
		$sql="SELECT *, UNIX_TIMESTAMP() - date_created AS TimeSpent FROM $this->_name where courseid='$cid' order by p_id desc limit 1";
		if ($show_more_post!=""){
			$sql="SELECT *,UNIX_TIMESTAMP() - date_created AS TimeSpent FROM $this->_name where courseid='$cid' order by p_id desc limit ".$show_more_post.", 10";
		}
		$rs=$this->_db->query($sql);
		return $rs;
	}


	public function selectacitivity($cid,$next_records)
	{
		$sql="SELECT * FROM $this->_name where courseid='$cid' order by p_id desc limit ".$next_records.", 10";
		$rs=$this->_db->query($sql);
		return $rs;
	}


	public function commenttimespent($p_id="",$totals="")
	{
		$sql="SELECT *,	UNIX_TIMESTAMP() - date_created AS CommentTimeSpent FROM $this->_comment order by c_id desc limit 1";
		if ($p_id!=""){
			$startlimit=0;
			if ($totals>4)
			$startlimit=$totals-4;
			$sql="SELECT *,	UNIX_TIMESTAMP() - date_created AS CommentTimeSpent FROM $this->_comment where post_id = ".$p_id." order by c_id asc limit $startlimit,$totals";
		}
		//$this->master_conn->debug=1;
		$rs=$this->_db->query($sql);
		return $rs;
	}

	public function commenttimespent2($p_id="",$totals)
	{
		$totals=$totals-4;
		echo $sql="SELECT *,	UNIX_TIMESTAMP() - date_created AS CommentTimeSpent FROM $this->_comment where post_id = ".$p_id." order by c_id asc limit 0,$totals";
		
		$rs=$this->_db->query($sql);
		return $rs;
	}
	

	public function postcomment($post_id,$value,$username,$userip)
	{
		$sql="INSERT INTO $this->_comment (post_id,comments,userip,date_created,username) VALUES('".$_REQUEST['post_id']."','".$this->checkValues($_REQUEST['comment_text'])."','".$userip."','".strtotime(date("Y-m-d H:i:s"))."','".$username."')";
		$this->_db->query($sql);
	}


	public function deleteactivity($post_id,$username)
	{
		$sql="delete from $this->_name where p_id ='".$post_id."' AND username ='".$username."'";
		$this->_db->query($sql);

		$sql="delete from $this->_comment where post_id ='".$post_id."'";
		
		$this->_db->query($sql);

	}


	public function deletecomment($c_id,$username)
	{
		$sql="delete from $this->_comment where c_id ='".$c_id."' AND username ='".$username."'";
		$this->_db->query($sql);
	}

	
	public function countip($p_id="",$username)
	{
		$sql="SELECT count(*) as bil FROM $this->_ip where post_id = ".$p_id." AND username='".$username."'";

		$rs=$this->_db->query($sql);
		if ($rs)
		return $rs[0]["bil"];
		else 
		return 0;
	}
	
	public function countcomment($p_id="")
	{
		$sql="SELECT count(*) as bil FROM $his->_comment where post_id = ".$p_id;

		$rs=$this->_db->query($sql);
		if ($rs)
		return $rs[0]["bil"];
		else 
		return 0;
	}
	
	public function countlikes($p_id="")
	{
		$sql="SELECT likes FROM $this->_like where post_id = ".$p_id;

		$rs=$this->_db->query($sql);
		if ($rs)
		return $rs[0]["likes"];
		else 
		return 0;
	}

	public function updatelikes($post_id)
	{
		$sql="select * from $this->_like where post_id = ".$post_id;
		$rs=$this->master_conn->Execute($sql);
		
		if($rs)
		$sql="update $this->_like set likes=likes+1 where post_id = ".$post_id;
		else
		$sql="INSERT INTO $this->_like (likes,post_id) VALUES (1,$post_id)";
		$this->_db->query($sql);
	}
	
	public function postip($post_id,$userip,$username)
	{
		$sql="INSERT INTO $this->_ip (userip,post_id,username) VALUES('".$userip."','".$post_id."','".$username."')";
		$this->_db->query($sql);
	}
	
	public function updateunlikes($post_id)
	{
		$sql="update $this->_like set likes=likes-1 where post_id = ".$post_id;
		$this->_db->query($sql);
	}
	
	public function deletelikes($post_id,$username)
	{
		$sql="delete from $this->_ip where username='".$username."' AND post_id = ".$post_id;
		$this->_db->query($sql);
	}


	##########################################################################################################
	# IMAGE FUNCTIONS																						 #
	# You do not need to alter these functions																 #
	##########################################################################################################
	public function resizeImage($image,$width,$height,$scale) {
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image);
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image);
				break;
		}
		imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$image);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$image,90);
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$image);
				break;
		}

		chmod($image, 0777);
		return $image;
	}


	public function createThumbnail($thumb_image_name, $image, $thumbWidth)
	{
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);

		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image);
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image);
				break;
		}

		$origWidth = imagesx($source);
		$origHeight = imagesy($source);
		$ratio = $thumbWidth/$origWidth;
		$thumbHeight = $origHeight * $ratio;

		$newImage = imagecreatetruecolor($thumbWidth,$thumbHeight);
		imagecopyresized($newImage, $source, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);

		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$thumb_image_name);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$thumb_image_name,90);
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);
				break;
		}
		chmod($thumb_image_name, 0777);
		return $thumb_image_name;

	}


	//You do not need to alter these functions
	public function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}
	//You do not need to alter these functions
	public function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}

	public function uploadimage($imageupload){

		if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0){
			$_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s')); //assign the timestamp to the session variable
			$_SESSION['user_file_ext']= "";
		}

		#########################################################################################################
		# CONSTANTS																								#
		# You can alter the options below																		#
		#########################################################################################################


		$upload_dir = "upload_pic"; 				// The directory for the images to be saved in
		$upload_path = $upload_dir."/";				// The path to where the image will be saved
		$large_image_prefix = "resize_"; 			// The prefix name to large image
		$thumb_image_prefix = "thumbnail_";			// The prefix name to the thumb image
		$imageename = strtotime(date('Y-m-d H:i:s'));
		$large_image_name = $large_image_prefix.$_SESSION['random_key'];     // New name of the large image (append the timestamp to the filename)
		$thumb_image_name = $thumb_image_prefix.$_SESSION['random_key'];     // New name of the thumbnail image (append the timestamp to the filename)
		$max_file = "3"; 							// Maximum file size in MB
		$max_width = "500";							// Max width allowed for the large image
		$thumb_width = "100";						// Width of thumbnail image
		$thumb_height = "100";						// Height of thumbnail image
		// Only one of these image types should be allowed for upload
		$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
		$allowed_image_ext = array_unique($allowed_image_types); // do not change this
		$image_ext = "";	// initialise variable, do not change this.
		foreach ($allowed_image_ext as $mime_type => $ext) {
			$image_ext.= strtoupper($ext)." ";
		}

		//Image Locations
		$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
		$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION['user_file_ext'];

		//Create the upload directory with the right permissions if it doesn't exist
		if(!is_dir($upload_dir)){
			mkdir($upload_dir, 0777);
			chmod($upload_dir, 0777);
		}

		//Check to see if any images with the same name already exist
		if (file_exists($large_image_location)){
			if(file_exists($thumb_image_location)){
				$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION['user_file_ext']."\" alt=\"Thumbnail Image\"/>";
			}else{
				$thumb_photo_exists = "";
			}
			$large_photo_exists = "<img src=\"".$upload_path.$large_image_name.$_SESSION['user_file_ext']."\" alt=\"Large Image\"/>";
		} else {
			$large_photo_exists = "";
			$thumb_photo_exists = "";
		}

		//Get the file information
		$userfile_name = $imageupload['name'];
		$userfile_tmp = $imageupload['tmp_name'];
		$userfile_size = $imageupload['size'];
		$userfile_type =$imageupload['type'];
		$filename = basename($imageupload['name']);
		$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));

		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
		if((!empty($imageupload)) && ($imageupload['error'] == 0)) {

			foreach ($allowed_image_types as $mime_type => $ext) {
				//loop through the specified image types and if they match the extension then break out
				//everything is ok so go and check file size
				if($file_ext==$ext && $userfile_type==$mime_type){
					$error = "";
					break;
				}else{
					$error = "Only <strong>".$image_ext."</strong> images accepted for upload<br />";
				}
			}
			//check if the file size is above the allowed limit
			if ($userfile_size > ($max_file*1048576)) {
				$error.= "Images must be under ".$max_file."MB in size";
			}

		}else{
			$error= "Select an image for upload";
		}
		//Everything is ok, so we can upload the image.
		if (strlen($error)==0){

			if (isset($imageupload['name'])){
				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
				$large_image_location = $large_image_location.".".$file_ext;
				$thumb_image_location = $thumb_image_location.".".$file_ext;

				//put the file ext in the session so we know what file to look for once its uploaded
				$_SESSION['user_file_ext']=".".$file_ext;

				move_uploaded_file($userfile_tmp, $large_image_location);
				chmod($large_image_location, 0777);

				$width = $this->getWidth($large_image_location);
				$height = $this->getHeight($large_image_location);
				//Scale the image if it is greater than the width set above
				if ($width > $max_width){
					$scale = $max_width/$width;
					$uploaded = $this->resizeImage($large_image_location,$width,$height,$scale);
					$this->createThumbnail($thumb_image_location, $large_image_location, $thumb_width);
				}else{
					$scale = 1;
					$uploaded = $this->resizeImage($large_image_location,$width,$height,$scale);
					$this->createThumbnail($thumb_image_location, $large_image_location, $thumb_width);
				}

			}

		}
	}





}
?>