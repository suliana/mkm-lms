<?php
class Activities_IndexController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
	}
	public function indexAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$this->view->username = $data->username;
		
		$username = $data->username;
		$privilage=$data->privilage;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$intakeid = "";
		//tambahan for student
		if ($data->privilage=="BASIC"){
			$assignsubject= new Course_Model_Student();
			$rs=$assignsubject->fetch($data->username,$courseid);
			if ($rs)
			$intakeid=$rs["intakeid"];
			else
			$intakeid='0';
			//echo $intakeid;
			//exit();
		}
		//end tambahan

		$form = new Activities_Form_Search ();
		
		$this->view->form = $form;
		
		$activity = new Activities_Model_Activities ();
		$select = $activity->returnselect ( $courseid, $intakeid,$privilage,$username);
		
		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost()){
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost()){
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			//make sure the submitted data is valid and modify select statement
			if ($form->valid()){
				$data = $form->getValues ();
				$select = $activity->findreturnselect ( $courseid, $data ['keyword'], $intakeid,$privilage,$username );
			} 
			
		} 
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'));

		
	}
	public function uploadAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$username=$data->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$activity = new Activities_Model_Activities ();
		$form = new Activities_Form_Upload ( array ('courseid' => $courseid ) );
		$this->view->form = $form;

		//$form->getElement('courseid')->setValue($courseid);


		if ($this->getRequest ()->isPost ()) {
			if ($form->isValid ( $_POST )) {
				$data = $form->getValues ();
				$data['ccontent'] = stripslashes($data['ccontent']);
				$data["datecreated"]=date("Y-m-d H:i:s");
				$data["createby"]=$username;
				$activity->upload ( $data );
				$this->_redirect ( 'activities/index/index/id/'.$courseid.'/courseid/'.$courseid );
			}
		}
	}
	public function modifyAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$username=$data->username;

		$id = $this->_getParam ( 'id', 0 );

		$activity = new Activities_Model_Activities ();
		$vdata=$activity->fetch ( $id );

		$form = new Activities_Form_Modify (array ('courseid' => $vdata["courseid"] ));

		$this->view->form = $form;
		if ($this->getRequest ()->isPost ()) {
			if ($form->isValid ( $_POST )) {
				$data = $form->getValues ();
				if ($id > 0) {
					$sdata ["title"] = $data ["title"];
					$sdata ["courseid"] = $data ["courseid"];
					$sdata['ccontent'] = stripslashes($data['ccontent']);
					$sdata ["intakeid"] = $data ["intakeid"];

					$sdata["datemodified"]=date("Y-m-d H:i:s");
					$sdata["modifyby"]=$username;
					$activity->modify ( $sdata, $id );

				} else {
					$data["datecreated"]=date("Y-m-d H:i:s");
					$data["createby"]=$username;
					$activity->upload ( $data );
				}
				$this->_redirect ( 'activities/index/index/id/'.$vdata["courseid"].'/courseid/'.$vdata["courseid"] );
			}
		} else {
			if ($id > 0) {
				$form->populate ( $activity->fetch ( $id ) );
			}
		}
	}
	public function deleteAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$activity = new Activities_Model_Activities ();
		$id = $this->_getParam ( 'rowid', 0 );

		$courseid = $this->_getParam ( 'courseid', '' );

		//echo $id;
		if ($id > 0) {
			$activity->delete ( $id );
			$this->_redirect ( 'activities/index/index/id/'.$courseid.'/courseid/'.$courseid );
		}
	}

	public function listcourseAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$this->view->username = $data->username;

		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();

		//echo $data->username;


		$this->view->course = $rscourse;
	}

	public function displayactivityAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$id = $this->_getParam ( 'id', 0 );
		$activity = new Activities_Model_Activities ();
		$data=$activity->fetch($id);

		$this->view->data = $data;

	}
	
	public function framepage1Action() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$id = $this->_getParam ( 'id', 0 );
		$activity = new Activities_Model_Activities ();
		$data=$activity->fetch($id);

		$this->view->data = $data;

	}
	
	public function activityAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$this->view->username = $data->username;
		
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

	}
	
}





