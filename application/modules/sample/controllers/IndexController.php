<?php
class Sample_IndexController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
	}
	public function indexAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		
		$this->view->username = $data->username;
		
		$username = $data->username;
		$privilage=$data->privilage;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$form = new Sample_Form_Search ();
		$this->view->form = $form;
		
		$sample = new Sample_Model_Sample ();
		
		//$bilsample = $sample->countsample ( $courseid );
		//$this->view->bilsample = $bilsample;
		$select = $sample->returnselect ( $courseid,$privilage,$username );
		
		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost()){
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost()){
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			//make sure the submitted data is valid and modify select statement
			if ($form->valid()){
				$data = $form->getValues ();
				$select = $sample->findreturnselect ( $courseid, $data ['keyword'],$privilage,$username );
			} 
			
		} 
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'));
		
		
	}
	
	public function uploadAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$username=$data->username;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$sample = new Sample_Model_Sample ();
		$form = new Sample_Form_Upload ( array ('courseid' => $courseid ) );
		$this->view->form = $form;
		
		
		//$form->getElement('courseid')->setValue($courseid);
		

		if ($this->getRequest ()->isPost ()) {
			if ($form->isValid ( $_POST )) {
				$data = $form->getValues ();
				$locationFile = $form->filename->getFileName ();
				$data ["filename"] = date ( 'Ymdhis' ) . "_" . $data ["filename"];
				$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/sample/' . $courseid . '/' . $data ["filename"];
				// Renommage du fichier 
				$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
				$filterRename->filter ( $locationFile );
				$data["datecreated"]=date("Y-m-d H:i:s");
				$data["createby"]=$username;
				$sample->upload ( $data );
				$this->_redirect ( 'sample/index/index/id/'.$courseid.'/courseid/'.$courseid );
			}
		}
	}
	public function modifyAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$username=$data->username;
		
		$id = $this->_getParam ( 'id', 0 );
		
		
		$sample = new Sample_Model_Sample ();
		$vdata=$sample->fetch ( $id );
				
		$form = new Sample_Form_Modify (array ('courseid' => $vdata["courseid"] ));
		
		$this->view->form = $form;
		if ($this->getRequest ()->isPost ()) {
			if ($form->isValid ( $_POST )) {
				$data = $form->getValues ();
				if ($id > 0) {
					$sdata ["title"] = $data ["title"];
					$sdata ["courseid"] = $data ["courseid"];
					$sdata ["intakeid"] = $data ["intakeid"];
					//print_r($data);
					//exit();
					if ($form->filename->isUploaded ()) {
						
						$locationFile = $form->filename->getFileName ();
						$sdata ["filename"] = date ( 'Ymdhis' ) . "_" . $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/sample/' . $data ["courseid"] . '/' . $sdata ["filename"];
						// Renommage du fichier 
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );					
					
					}
					$sdata["datemodified"]=date("Y-m-d H:i:s");
					$sdata["modifyby"]=$username;
					$sample->modify ( $sdata, $id );
				} else {
					$data["datecreated"]=date("Y-m-d H:i:s");
					$data["createby"]=$username;
					$sample->upload ( $data );
				}
				$this->_redirect ( 'sample/index/index/id/'.$sdata ["courseid"].'/courseid/'.$sdata ["courseid"] );
			}
		} else {
			if ($id > 0) {
				$form->populate ( $sample->fetch ( $id ) );
			}
		}
	}
	public function deleteAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$sample = new Sample_Model_Sample ();
		$id = $this->_getParam ( 'rowid', 0 );
		
		$courseid = $this->_getParam ( 'courseid', '' );
		
		//echo $id;
		if ($id > 0) {
			$sample->delete ( $id );
			$this->_redirect ( 'sample/index/index/id/'.$courseid.'/courseid/'.$courseid );
		}
	}
	
	public function listcourseAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		
		$this->view->username = $data->username;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();
		
		//echo $data->username;
		

		$this->view->course = $rscourse;
	}
}





