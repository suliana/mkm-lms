<?php
class Sample_Form_Modify extends Zend_Form {
	protected $_courseid;
	
	public function setCourseid($value) {
		$this->_courseid = $value;
	}
	
	public function init() {
		
		
		$this->setMethod ( 'post' );
		
		$title = $this->createElement ( 'text', 'title' );
		$title->setLabel ( $this->getView()->translate('title').':' )->setRequired ( true );
		
		$intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intakeid');
		
		$intakeid->setLabel($this->getView()->translate('semester_id').':')
		->setRequired(true);
		foreach ($result as $c) {
		$intakeid->addMultiOption($c['id'], $c['intakecode']);
		}
		
		$courseid = $this->createElement ( 'text', 'courseid' );
		$courseid->setLabel ( 'Course ID:' )->setRequired ( false );
		$courseid->setAttrib ( 'readonly', 'readonly' );
		
		$element = new Zend_Form_Element_File ( 'filename' );
		
		$dirsample = DOC_PATH.'/'.APP_FOLDER.'/upload/sample/';
		
		if (! is_dir ( $dirsample ))
			mkdir ( $dirsample, 0775 );
		
		$dirsamplecourse = DOC_PATH.'/'.APP_FOLDER.'/upload/sample/' . $this->_courseid;
		//echo $dirsamplecourse;
		if (! is_dir ( $dirsamplecourse ))
			mkdir ( $dirsamplecourse, 0775 );
		
		$element->setLabel ( $this->getView()->translate('choose').' '.$this->getView()->translate('file').':' )->setDestination ( $dirsamplecourse );
		// ensure minimum 1, maximum 3 files
		/*$element->addValidator('Count', false, 
        array('min' => 1, 'max' => 3));*/
		// limit to 100K
		$element->addValidator ( 'Size', false, 20*1024*1024 );
		// only JPEG, PNG, and GIFs
		$element->addValidator ( 'Extension', false, 'doc,docx,pdf' );
		//$element->setRequired(true);
		// defines 3 identical file elements
		//$element->setMultiFile(3);
		//$form->addElement($element, 'foo');
		

		$this->addElements ( array ($title, $intakeid, $courseid, $element ) );
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('sample'),
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'sample', 'controller'=>'index','action'=>'index','courseid' => $this->_courseid),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	}
}