<?php
class Sample_Model_Sample extends Zend_Db_Table
{
    protected $_name = "sample";
    
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    public function upload ($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    public function fetch ($id = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($id != "") {
            $sql->where('id = ?', $id);
        }
        $result = $this->_db->fetchRow($sql);
        //print_r($result);
        //exit();
        return $result;
    }
    public function modify ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    public function delete ($id)
    {
        $this->_db->delete($this->_name, 'id = ' . (int) $id);
    }
    public function find ($keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
        $result = $this->_db->query($sql);
        return $result;
    }
    public function returnselect ($courseid,$privilage="",$username="")
    {
        $sql = $this->_db->select()
        	->from($this->_name)
            ->where('courseid = ?',$courseid);

        ;
        if ($privilage=="EOS"){
			$eos = new Course_Model_Grader();
			$rs=$eos->fetchsemua($username,$courseid);
			$i=0;
			if ($rs){
				foreach ($rs as $row) {
						$intake[$i] = $row['intakeid'] ;
					
					$i++;

				}
				
				$sql->where(
				' intakeid IN  (?) ', $intake );
			}


		}
		$sql->order('title ASC');
		
        return $sql;
    }
    public function findreturnselect ($courseid,$keyword = "",$privilage="",$username="")
    {
        $sql = $this->_db->select()->from($this->_name)
        ->where('courseid = ?',$courseid);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
        
        if ($privilage=="EOS"){
			$eos = new Course_Model_Grader();
			$rs=$eos->fetchsemua($username,$courseid);
			$i=0;
			if ($rs){
				foreach ($rs as $row) {
						$intake[$i] = $row['intakeid'] ;
					
					$i++;

				}
				
				$sql->where(
				' intakeid IN  (?) ', $intake );
			}


		}
        //echo $sql;
        return $sql;
    }
    
    public function sampledownload ($courseid,$intakeid = "")
    {
        $sql = $this->_db->select()
        	->from($this->_name)
            ->where('courseid = ?',$courseid);
            
            
        ;
        if ($intakeid != "") {
            //echo $keyword;
            $sql->where(
            ' intakeid = ? ', $intakeid );
        }
        
        $sql->order('title DESC');
        $result = $this->_db->fetchAll($sql);
        
        return $result;
       
    }
    
    public function countsample ($courseid = "")
    {
        $sql = $this->_db->select()->from($this->_name,
                    array('bil' => 'count(*)'));
        if ($courseid != "") {
            $sql->where('courseid = ?', $courseid);
        }
        //echo $sql;
        $result = $this->_db->fetchRow($sql);
        //echo $result["bil"];
        //print_r($result);
        //exit();
        if ($result){
        	//echo $result["bil"];
        	return $result["bil"];
        	exit();
        }
        else {
        	echo "0";
        return 0;
        }
        
        
    }
}
?>
