<?php
class Users_Model_Users extends Zend_Db_Table
{
    protected $_name = "users";
    protected $_ccoordinator_name = "coursecoordinator";
    protected $_cgrader_name = "coursefacilitator";
    protected $_tablelogin = "tracklogin";
    
    
    
    function checkUnique ($username)
    {
        $select = $this->_db->select()
            ->from($this->_name, array('username'))
            ->where('username=?', $username);
        $result = $this->getAdapter()->fetchOne($select);
        if ($result) {
            return true;
        }
        return false;
    }
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    public function upload ($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    public function fetch ($id = "", $field='id')
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($id != "") {
            $sql->where($field.' = ?', $id);
        }
        $result = $this->_db->fetchRow($sql);
        //print_r($result);
        //exit();
        return $result;
    }
    
    public function modify ($data, $where)
    {
    	if ( !is_array($where) )
    	{
    		$where = array('id = ?' => $where);
    	}
    	
        $this->_db->update($this->_name, $data, $where );
    }
    
    public function delete ($id)
    {
        $this->_db->delete($this->_name, 'id = ' . (int) $id);
    }
    public function find ($keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            '  ( firstName LIKE ? ', 
            '%' . $keyword . '%');
            $sql->orwhere(
            '   lastName LIKE ? ', 
            '%' . $keyword . '%');
            $sql->orwhere(
            '   username LIKE ? )', 
            '%' . $keyword . '%');
            
            
        }
        $result = $this->_db->query($sql);
        return $result;
    }
    public function returnselect ()
    {
        $sql = $this->_db->select()
            ->from($this->_name)
            ->order('username ASC');
        ;
        return $sql;
    }
    public function findreturnselect ($keyword = "", $role = "", $intakeid = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            '  ( firstName LIKE ? ', 
            '%' . $keyword . '%');
            $sql->orwhere(
            '   lastName LIKE ? ', 
            '%' . $keyword . '%');
            $sql->orwhere(
            '   username LIKE ? )', 
            '%' . $keyword . '%');
        }
        
        if ($role != "" && $role != "xxx") {
            //echo $keyword;
            $sql->where(
            '  privilage = ? ', 
            $role);
        }
        
        if ($intakeid != "" && $intakeid != "xxx") {
            //echo $keyword;
            $sql->where(
            '  intake = ? ', 
            $intakeid);
        }
        //echo $sql;
        //exit();
        return $sql;
    }
    
    public function checkusername ($username)
    {
        $select = $this->_db->select()
            ->from($this->_name)
            ->where('username=?', $username);
            
        $result = $this->_db->fetchRow($select);
        
        return $result;
        
    }
    
    public function checkcoordinatorpriviledge($username,$courseid)
    {
    	$select = $this->_db->select()
    	->from($this->_ccoordinator_name)
    	->where('username=?', $username)
    	->where('courseid=?', $courseid);
    		
    	$result = $this->_db->fetchRow($select);
        
    	return $result;
        

    }
    
    public function checkgraderpriviledge($username,$courseid)
    {
    	$select = $this->_db->select()
    	->from($this->_cgrader_name)
    	->where('username=?', $username)
    	->where('courseid=?', $courseid);
    		
    	$result = $this->_db->fetchRow($select);
        return $result;
        

    }
    
    public function fetchULC ()
    {
        $sql = $this->_db->select()->distinct()
        ->from(array('user' => $this->_name),
			array('user.ulcname','user.ulccode'))
        ->where('user.ulcname IS NOT NULL ');
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    
    public function tracklogin ($data)
    {
        $this->_db->insert($this->_tablelogin, $data);
    }
    
    public function modifytracklogin ($data, $username, $logindate)
    {
        //print_r($data);
    	$where['userlogin = ?'] = $logindate;
        $where['userID = ?'] = $username;
        /*print_r($where);
        exit();*/
    	$this->_db->update($this->_tablelogin, $data, $where);
    }
    
    public function checkuserID ($username)
    {
        $select = $this->_db->select()
            ->from($this->_name)
            ->where('username=?', $username);
        $result = $this->_db->fetchRow($select);
        
        return $result;
        
    }
    
    public function checkStud_ID ($student_id)
    {
        $select = $this->_db->select()
            ->from($this->_name)
            ->where('STUD_ID=?', $student_id);
        $result = $this->_db->fetchRow($select);
        
        return $result;
        
    }
}
?>
