<?php
class Users_Model_Monitoring extends Zend_Db_Table
{
	protected $_name = "tracklogin";
	protected $_table_users = "users";


	public function countlogin($userid,$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from(array('a'=>$this->_name),
		array('count(userID) as billogin'));


		$sql->where('userID = ?', $userid);

		if ($useDate==1){
			$sql->where('userlogin >= ?', $start_date_post);
			$sql->where('userlogin < ?', $end_date_post);
		}
		
		
		
		$stmt = $this->_db->fetchRow($sql);

		if ($stmt['billogin']!='')
		return $stmt['billogin'];
		else 
		return 0;


	}


	/**
	 * Search Login
	 *
	 */	
	public function searchlogin($keyworduser){

		$sql = $this->_db->select()
		->from(array('a'=>$this->_name),
		array('userID','userIP','userBrowser','userOS','userlogin','userlogout'))
		->join(array('d' => $this->_table_users),
		'd.username = a.userID',
		array('firstname','lastname','studentID'))
		;

		if ($keyworduser != "") {
			//echo $keyword;
			$sql->where(
			'  ( firstName LIKE ? ',
			'%' . $keyworduser . '%');
			$sql->orwhere(
			'   lastName LIKE ? ',
			'%' . $keyworduser . '%');
			$sql->orwhere(
			'   d.username LIKE ? ',
			'%' . $keyworduser . '%');
			$sql->orwhere(
			'   studentID LIKE ? )',
			'%' . $keyworduser . '%');


		}
		
		$sql->group('userID');
		

		$stmt = $this->_db->fetchAll($sql);

		return $stmt;


	}

	public function viewlogin($userid="",$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from(array('a'=>$this->_name),
		array('userID','userIP','userBrowser','userOS','userlogin','userlogout'));


		$sql->where('userID = ?', $userid);

		if ($useDate==1){
			$sql->where('userlogin >= ?', $start_date_post);
			$sql->where('userlogin < ?', $end_date_post);
		}
		
		
		return $sql;

	}


}
?>