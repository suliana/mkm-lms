<?php
class Users_IndexController extends Zend_Controller_Action
{
	public function init ()
	{
		/* Initialize action controller here */
			
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
		
		//$this->_helper->layout->setLayout('admin');
		
	}
	
	public function indexAction ()
	{
		$form = new Users_Form_Search();
		$this->view->form = $form;

		$user = new Users_Model_Users();
		if ($this->_request->getParam('role',0) || $this->_request->isPost()) 
		{

			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost())
			{
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else
			{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			
			//make sure the submitted data is valid and modify select statement			
			if ($form->valid())
			{
				$data = $form->getValues();
				if ( $data['keyword'] == '' && $data['role'] == '' && $data['intake_id'] == '' )
				{
					$select = $user->returnselect();
				}
				else
				{
					$select = $user->findreturnselect($data['keyword'],$data['role'],$data['intake_id']);
				}
			}
		} 
		else
		{
			$select = $user->returnselect();
		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		
		//print_R($paginator);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('role'=>$form->getValue('role'),'intake_id'=>$form->getValue('intake_id'));
		
	}
	
	
	public function uploadAction ()
	{
		$username = $this->view->username;

		$user = new Users_Model_Users();
		$form = new Users_Form_Upload();
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) 
		{
			if ($form->isValid($_POST)) 
			{
				$data = $form->getValues();
				if ($data['password'] != $data['confirmPassword'])
			 	{
					$this->view->errorMessage = "Password and confirm password don't match.";
					return;
				}
				
				if ($user->checkUnique($data['username'])) 
				{
					$this->view->errorMessage = "Name already taken. Please choose another one.";
					return;
				}
				
				$data['password']=md5( $data['password']);
				unset($data['confirmPassword']);
				$data["datecreated"]=date("Y-m-d H:i:s");
				$data["createby"]=$username;
				$user->upload($data);
				$this->_redirect('users/index/index');
			}
		}
	}
	
	public function modifyAction ()
	{
		$username = $this->view->username;

		$user = new Users_Model_Users();
		$form = new Users_Form_Modify();
		$id = $this->_getParam('id', 0);
		$this->view->form = $form;
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($_POST)) {
				$data = $form->getValues();
				if ($id > 0) {
					$data["datemodified"]=date("Y-m-d H:i:s");
					$data["modifyby"]=$username;
					$user->modify($data, $id);
				} else {
					if ($data['password'] != $data['confirmPassword']) {
						$this->view->errorMessage = "Password and confirm password don't match.";
						return;
					}
					if ($user->checkUnique($data['username'])) {
						$this->view->errorMessage = "Name already taken. Please choose another one.";
						return;
					}
					$data['password']=md5( $data['password']);
					unset($data['confirmPassword']);
					$data["datecreated"]=date("Y-m-d H:i:s");
					$data["createby"]=$username;
					$user->upload($data);
				}
				$this->_redirect('users/index/index');
			}
		} else {
			if ($id > 0) {
				$form->populate($user->fetch($id));
			}
		}
	}
	public function deleteAction ()
	{

		$user = new Users_Model_Users();
		$id = $this->_getParam('id', 0);
		//echo $id;
		if ($id > 0) {
			$user->delete($id);
			$this->_redirect('users/index/index');
		}
	}
	
	public function changepasswordAction ()
	{
		
		$this->_helper->layout->setLayout('layout');
		
		$storage2 = new Zend_Auth_Storage_Session("myvleemail");
		$data2 = $storage2->read();
		
		$username = $data->username;
		
		$oldpassword = $data2->passwd;
		
		
		$user = new Users_Model_Users();
		$form = new Users_Form_ChangePassword();
		
		$id = $data->id;
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) 
		{
			if ($form->isValid($_POST)) 
			{
				$data = $form->getValues();
				if ($id > 0)
				{
					
					$oldpssword=base64_decode(urldecode($oldpassword));
					
					if ($oldpssword != $data['oldpassword']) {
						$this->view->errorMessage = "Old Password don't match.";
						return;
					}
					
					if ($data['newpassword'] != $data['confirmnewpassword']) {
						$this->view->errorMessage = "New Password and Confirm New Password don't match.";
						return;
					}
					
					$sdata['password']=md5( $data['newpassword']);
					$sdata["datemodified"]=date("Y-m-d H:i:s");
					$sdata["modifyby"]=$username;
					
					$user->modify($sdata, $id);
					
					$this->view->successMessage = "Successfully change password. Please remember the new password";
					return;
					
					
				} 
				
			} //isvalid
		} 
	}
}





