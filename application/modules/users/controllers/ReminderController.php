<?php
class Users_ReminderController extends Zend_Controller_Action
{
    public function init ()
    {
        /* Initialize action controller here */
    }
    public function indexAction ()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if (! $data) {
            $this->_redirect('index/index');
        }
        
        $courseid = $this->_getParam('id', 0);
        $this->view->courseid = $courseid;
        
        $user = new Users_Model_Users();
        $username = $data->username;  
        $userprofile=$user->checkusername($data->username);
        $this->view->userprofile = $userprofile;
        $this->view->privilage = $data->privilage;
        
        $ModelReminder = new Admin_Model_Reminder();
        $select = $ModelReminder->selectreminder ($data->privilage);
        $this->view->selectreminder = $select;
       
        
        
    }
    public function uploadphotoAction ()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if (! $data) {
            $this->_redirect('index/index');
        }
        
        $user = new Users_Model_Users();
        
        $userprofile=$user->checkusername($data->username);
        $this->view->userprofile = $userprofile;
        
    }
    public function displaysubmissionAction ()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if (! $data) {
            $this->_redirect('index/index');
        }
        $username=$data->username;
		$this->view->username = $username;
		
		 
        $course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();
		$this->view->course = $rscourse;
        
        
        
        
    }
    
     public function submitdetailsAction ()
    {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if (! $data) {
            $this->_redirect('index/index');
        }
        
        
        $user = new Users_Model_Users();
        $username = $data->username;  
        $userprofile=$user->checkusername($data->username);
        $this->view->userprofile = $userprofile;
        $this->view->privilage = $data->privilage;
        
        $user = new Users_Model_Users();
        $form = new Users_Form_SubmitDetails();
        $id = $userprofile[0]['id'];
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $data = $form->getValues();
                if ($id > 0) {
                	$data["datemodified"]=date("Y-m-d H:i:s");
                	$data["modifyby"]=$username;
                    $user->modify($data, $id);
                } 
                $this->_redirect('index/movie');
            }
        } else {
            if ($id > 0) {
                $form->populate($user->fetch($id));
            }
        }
        
        
    }
    
    
    public function downloadallAction(){
    	$storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if (! $data) {
            $this->_redirect('index/index');
        }
        
        $user = new Users_Model_Users();
        $username = $data->username;
        $userprofile=$user->checkusername($data->username);
        $this->view->userprofile = $userprofile;
        $this->view->privilage = $data->privilage;
        
        $chk = $this->_getParam ( 'chk', 0 );
		$this->view->chk = $chk;
		
		$assignment = new Assignment_Model_Assignment ();
		$arrayfile=array();
		if ($this->getRequest()->isPost()) {
			$assignmentid=$_POST["download"];
			
			for ($i=0; $i<$chk; $i++){
				$asgid=$assignmentid[$i];
				
				$fetchasg=$assignment->fetch($assignmentid[$i]);
				
				if ($fetchasg["asg_type"]==1){
					$result=$assignment->checksubmission($username,$asgid);
				}else{
					$result=$assignment->checksubmissiongroup($username,$asgid);
				}
				
				if ($result) {
					$filename = $result["filename"];
					$uploaduser = $result["username"];
					$courseid = $fetchasg["courseid"];
					
					$destination=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/'.$courseid.'/'.$asgid.'/'.$uploaduser.'/'.$filename;
					
					$arrayfile[$i][0]=$destination;
					
					$arrayfile[$i][1]=$filename;
					
				}
			
			}
			
			$this->view->files_to_zip = $arrayfile;
			

        }
    }
    
    
    
}





