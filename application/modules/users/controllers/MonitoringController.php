<?php
class Users_MonitoringController extends Zend_Controller_Action 
{
	public function init() {
		/* Initialize action controller here */
		//$this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
		$this->_helper->layout->setLayout('admin');

	}

	public function searchloginAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();

		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$monitoring = new Users_Model_Monitoring ();

		$keyworduser = $this->getRequest()->getParam('keyworduser');

		$this->view->keyworduser = $keyworduser;

		if (isset($keyworduser) || $this->getRequest ()->isPost ()) {

			$keyworduser = $this->getRequest()->getParam('keyworduser');
			$useDate = $this->getRequest()->getParam('useDate');
			$start_date = $this->getRequest()->getParam('start_date',0);
			$end_date = $this->getRequest()->getParam('end_date',0);

			if ($this->getRequest ()->isPost ()){
				$keyworduser = $this->getRequest()->getPost('keyworduser');
				$useDate = $this->getRequest()->getPost('useDate');
				$start_date = $this->getRequest()->getPost('start_date',0);
				$end_date = $this->getRequest()->getPost('end_date',0);
			}

			$this->view->keyworduser = $keyworduser;
			$this->view->useDate = $useDate;
			$this->view->start_date = $start_date;
			$this->view->end_date = $end_date;

			$select=$monitoring->searchlogin($keyworduser);
			
			Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'paginationspecial2.phtml' );

			$paginator = Zend_Paginator::factory ( $select );
			$paginator->setItemCountPerPage ( 10 );
			$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
			$this->view->paginator = $paginator;
			$this->view->searchParams = array('keyworduser'=>$keyworduser,'useDate'=>$useDate,'start_date'=>$start_date,'end_date'=>$end_date);
			
		}

	}
	
	public function framepage1Action() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$this->view->username = $data->username;
		
		$userid = $this->_getParam ( 'userid', 0 );
		$this->view->userid = $userid;

		$usedate = $this->_getParam ( 'usedate', 0 );
		$this->view->usedate = $usedate;
		
		$start_date = $this->_getParam ( 'start_date', 0 );
		$this->view->start_date = $start_date;
		
		$end_date = $this->_getParam ( 'end_date', 0 );
		$this->view->end_date = $end_date;
		

	}
	
	public function viewloginAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$this->view->username = $data->username;
		
		$userid = $this->_getParam ( 'userid', 0 );
		$this->view->userid = $userid;

		$usedate = $this->_getParam ( 'usedate', 0 );
		$this->view->usedate = $usedate;
		
		$start_date = $this->_getParam ( 'start_date', 0 );
		$this->view->start_date = $start_date;
		
		$end_date = $this->_getParam ( 'end_date', 0 );
		$this->view->end_date = $end_date;
		
		
		$monitoring = new Users_Model_Monitoring ();
		
		
		$select = $monitoring-> viewlogin($userid,$usedate,$start_date,$end_date);
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		

	}


}





