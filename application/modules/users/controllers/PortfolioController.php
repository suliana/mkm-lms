<?php
class Users_PortfolioController extends Zend_Controller_Action
{
    public function init ()
    {
       	$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
    }
    
    public function indexAction ()
    {
   		$courseid = $this->_getParam('id', 0);
        $this->view->courseid = $courseid;
        
        $user = new Users_Model_Users();
        $username = $this->userinfo->username;  
        $userprofile=$user->checkusername($username);
        $this->view->userprofile = $userprofile;
        
        $user = new Users_Model_Users();
        $form = new Users_Form_ModifySchool( array ('courseid' => $courseid ) );
        
        $id = $userprofile[0]['id'];
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) 
        {
            if ($form->isValid($_POST)) 
            {
                $data = $form->getValues();
                
                if ($id > 0)
                {
                	$data["datemodified"]=date("Y-m-d H:i:s");
                	$data["modifyby"]=$username;
                    $user->modify($data, $id);
                } 
                
                $this->_redirect('users/portfolio/index/id/'.$courseid);
            }
            
        } 
        else
        {
            if ($id > 0) 
            {
                $form->populate($user->fetch($id));
            }
        }   
        
    }
    
    public function uploadphotoAction ()
    {
       	$username = $this->view->username;
        
        $user = new Users_Model_Users();
        
        $userprofile=$user->checkusername($username);
        
        $this->view->userprofile = $userprofile;
        
        phpinfo();
        exit;
    }
    
    public function displaysubmissionAction ()
    {
        $username = $this->view->username;

        $course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();
		$this->view->course = $rscourse;
    }
    
    public function submitdetailsAction ()
    {
        $user = new Users_Model_Users();
        $username = $this->userinfo->username;  
        $userprofile=$user->checkusername($username);
        $this->view->userprofile = $userprofile;
        $this->view->privilage = $data->privilage;
        
        $user = new Users_Model_Users();
        $form = new Users_Form_SubmitDetails();
        $id = $userprofile[0]['id'];
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) 
        {
            if ($form->isValid($_POST)) 
            {
                $data = $form->getValues();
                if ($id > 0) 
                {
                	$data["datemodified"]=date("Y-m-d H:i:s");
                	$data["modifyby"]=$username;
                    $user->modify($data, $id);
                } 
                $this->_redirect('index/movie');
            }
        }
        else 
        {
            if ($id > 0) 
            {
                $form->populate($user->fetch($id));
            }
        }
    }
    
    public function downloadallAction()
    {
        $user = new Users_Model_Users();
        $username = $this->userinfo->username;
        $userprofile=$user->checkusername($username);
        $this->view->userprofile = $userprofile;
        $this->view->privilage = $data->privilage;
        
        $chk = $this->_getParam ( 'chk', 0 );
		$this->view->chk = $chk;
		
		$assignment = new Assignment_Model_Assignment ();
		$arrayfile=array();
		
		if ($this->getRequest()->isPost()) 
		{
			$assignmentid=$_POST["download"];
			
			for ($i=0; $i<$chk; $i++)
			{
				$asgid=$assignmentid[$i];
				
				$fetchasg=$assignment->fetch($assignmentid[$i]);
				
				if ($fetchasg["asg_type"]==1)
				{
					$result=$assignment->checksubmission($username,$asgid);
				}
				else
				{
					$result=$assignment->checksubmissiongroup($username,$asgid);
				}
				
				if ($result) 
				{
					$filename = $result["filename"];
					$uploaduser = $result["username"];
					$courseid = $fetchasg["courseid"];
					
					$destination=DOC_PATH.'/'.APP_FOLDER.'/upload/assignment/'.$courseid.'/'.$asgid.'/'.$uploaduser.'/'.$filename;
					
					$arrayfile[$i][0]=$destination;
					$arrayfile[$i][1]=$filename;
					
				}
			
			}
			
			$this->view->files_to_zip = $arrayfile;
			

        }
    }
    
    
    
    
    
}





