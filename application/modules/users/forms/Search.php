<?php
class Users_Form_Search extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$keyword = $this->createElement('text', 'keyword' );
    	$keyword->setOptions(array('class'=> 'inputtext', 'filters' => array('StringTrim','StripTags') ));
        $keyword->setLabel($this->getView()->translate('name'))->setRequired(false);
        
        $privilage = new Zend_Form_Element_Select('role');
		$privilage ->setLabel($this->getView()->translate('role'))->addMultiOptions( array('' => '---- '.$this->getView()->translate('please_select').' ----', 'STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','EOS' => 'EOS','ADMIN' => 'ADMIN'));
		$privilage->setOptions(array('class'=>'select'));
		
		$intake = new Admin_Model_Intake ();
        $result =  $intake->fetchAll();     		
		$intakeid = new Zend_Form_Element_Select('intake_id');
		
		$intakeid->setLabel($this->getView()->translate('intake_id'))->setRequired(true);
		$intakeid->addMultiOption("", "---- ".$this->getView()->translate('please_select')." ----");
		
		foreach ($result as $c)
		{
			$intakeid->addMultiOption($c['intakecode'], $c['intakecode']);
		}
		
		$intakeid->setOptions(array('class'=>'select'));
		
		
		
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_user'))->setIgnore(true);
        $register->setOptions(array('class'=>'btn submit'));
        
        $this->addElements(
        array($keyword, $privilage, $intakeid, $register));
    }
}