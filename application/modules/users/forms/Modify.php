<?php
class Users_Form_Modify extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$firstname = $this->createElement('text', 'firstname');
        $firstname->setLabel($this->getView()->translate('fullname').' :')->setRequired(true)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $lastname = $this->createElement('text', 'lastname');
        $lastname->setLabel($this->getView()->translate('lastname').' :')->setRequired(false)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $studentID = $this->createElement('text', 'studentID');
        $studentID->setLabel($this->getView()->translate('studentID').' :')->setRequired(false)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $email = $this->createElement('text', 'email');
        $email->setLabel($this->getView()->translate('email').' :')->setRequired(false)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $username = $this->createElement('text', 'username');
        $username->setLabel('Username:')->setRequired(true)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        $username->setAttrib ( 'readonly', 'readonly' );
        
        $intake = $this->createElement('text', 'intake');
        $intake->setLabel($this->getView()->translate('intake').' :')->setRequired(false)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $privilage = new Zend_Form_Element_Select(privilage);
		$privilage ->setLabel($this->getView()->translate('role').' :')->addMultiOptions( array('STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','EOS' => 'EOS','ADMIN' => 'ADMIN'))->setOptions(array('class'=>'select'));
		        
        
        /*$this->addElements(
        array($firstname, $lastname, $studentID, $email, $username, $intake, $privilage));	*/
        
        $this->addElements(
        array($firstname, $studentID, $email, $username, $intake, $privilage));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('user'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'btn submit')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'btn'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'users', 'controller'=>'index','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
    }
}