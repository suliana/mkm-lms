<?php
class Users_Form_ChangePassword extends Zend_Form
{
    public function init ()
    {
        $this->setMethod('post');
        
        $oldpassword = $this->createElement('password', 'oldpassword');
        $oldpassword->setLabel($this->getView()->translate('oldpassword').' :')->setRequired(true)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $newpassword = $this->createElement('password', 'newpassword');
        $newpassword->setLabel($this->getView()->translate('newpassword').' :')->setRequired(true)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
        
        $confirmnewPassword = $this->createElement('password', 'confirmnewpassword');
        $confirmnewPassword->setLabel($this->getView()->translate('confirm_newpassword').' :')->setRequired(true)->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
       
       
        $this->addElements(
        array($oldpassword, $newpassword,$confirmnewPassword, ));
        
         //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('change_password'),
          'Options'=>array('class'=>'btn submit'),
          'decorators'=>array('ViewHelper')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'Options'=>array('class'=>'btn'),
          'decorators'=>array('ViewHelper'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('controller'=>'index','action'=>'home'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
    }
}