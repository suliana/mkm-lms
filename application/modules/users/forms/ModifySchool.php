<?php
class Users_Form_ModifySchool extends Zend_Form
{
	protected $_courseid;


	public function setCourseid($value)
	{
		$this->_courseid = $value;
	}

	public function init ()
	{

		$this->setMethod('post');

		$schoolname = $this->createElement('text', 'school_name');
		$schoolname->setLabel($this->getView()->translate('current_school_name').' :')->setRequired(false);

		$schoolname->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$address1 = $this->createElement('text', 'school_address1');
		$address1->setLabel($this->getView()->translate('address').' 1 :')->setRequired(false);

		$address1->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$address2 = $this->createElement('text', 'school_address2');
		$address2->setLabel($this->getView()->translate('address').' 2 :')->setRequired(false);

		$address2->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$postcode = $this->createElement('text', 'school_postcode');
		$postcode->setLabel($this->getView()->translate('postcode').' :')->setRequired(false);

		$postcode->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$city = $this->createElement('text', 'school_city');
		$city->setLabel($this->getView()->translate('city').' :')->setRequired(false);

		$city->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$state = $this->createElement('text', 'school_state');
		$state->setLabel($this->getView()->translate('state').' :')->setRequired(false);

		$state->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$school_number = $this->createElement('text', 'school_number');
		$school_number->setLabel($this->getView()->translate('school_contact_number').' :')->setRequired(false);

		$school_number->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));


		$this->addElements(
		array($schoolname, $address1,  $address2, $postcode, $city, $state, $school_number));

		//button
		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('school'),
		'decorators'=>array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td',
		'align'=>'left', 'openOnly'=>true)),
		array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
		)
		));

		$this->addElement('submit', 'cancel', array(
		'label'=>$this->getView()->translate('cancel'),
		'decorators'=>array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
		'closeOnly'=>true)),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
		),
		'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'users', 'controller'=>'portfolio','action'=>'index','id' => $this->_courseid),'default',true) . "'; return false;"
		));

		/*$this->addDisplayGroup(array('save','cancel'),'buttons', array(
		'decorators'=>array(
		'FormElements',
		array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
		'DtDdWrapper'
		)
		));*/

		$this->setDecorators(array(

		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'



		));
	}
}