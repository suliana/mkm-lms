<?php
class Users_Form_SubmitDetails extends Zend_Form
{
	
	public function init ()
	{

		$this->setMethod('post');
		
		$mobilenumber = $this->createElement('text', 'telephone_number');
		$mobilenumber->setLabel($this->getView()->translate('telephone_number').' :')->setRequired(true);

		$mobilenumber->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$schoolname = $this->createElement('text', 'school_name');
		$schoolname->setLabel($this->getView()->translate('current_school_name').' :')->setRequired(true);

		$schoolname->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$address1 = $this->createElement('text', 'school_address1');
		$address1->setLabel($this->getView()->translate('current_school')." ".$this->getView()->translate('address').' 1 :')->setRequired(true);

		$address1->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$address2 = $this->createElement('text', 'school_address2');
		$address2->setLabel($this->getView()->translate('current_school')." ".$this->getView()->translate('address').' 2 :')->setRequired(false);

		$address2->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$postcode = $this->createElement('text', 'school_postcode');
		$postcode->setLabel($this->getView()->translate('current_school')." ".$this->getView()->translate('postcode').' :')->setRequired(true);

		$postcode->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$city = $this->createElement('text', 'school_city');
		$city->setLabel($this->getView()->translate('current_school')." ".$this->getView()->translate('city').' :')->setRequired(true);

		$city->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$state = $this->createElement('text', 'school_state');
		$state->setLabel($this->getView()->translate('current_school')." ".$this->getView()->translate('state').' :')->setRequired(true);

		$state->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));

		$school_number = $this->createElement('text', 'school_number');
		$school_number->setLabel($this->getView()->translate('current_school')." ".$this->getView()->translate('telephone_number').' :')->setRequired(true);

		$school_number->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td' )),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));


		$this->addElements(
		array($mobilenumber,$schoolname, $address1,  $address2, $postcode, $city, $state, $school_number));

		//button
		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('submit'),
		'decorators'=>array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td',
		'align'=>'left', 'openOnly'=>true)),
		array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		)
		));

		

		

		$this->setDecorators(array(

		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'



		));
	}
}