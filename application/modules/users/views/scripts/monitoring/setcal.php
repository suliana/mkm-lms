<style type="text/css">
@import url("<?php echo $this->baseUrl()?>/zpcal/themes/bluexp.css");
</style>
<!-- import the calendar script -->
<script type="text/javascript" src="<?php echo $this->baseUrl()?>/zpcal/src/utils.js"></script>
<script type="text/javascript" src="<?php echo $this->baseUrl()?>/zpcal/src/calendar.js"></script>

<!-- import the language module -->
<script type="text/javascript" src="<?php echo $this->baseUrl()?>/zpcal/lang/calendar-en.js"></script>

<!-- other languages might be available in the lang directory; please check
your distribution archive. -->

<!-- import the calendar setup script -->
<script type="text/javascript" src="<?php echo $this->baseUrl()?>/zpcal/src/calendar-setup.js"></script>

<style type="text/css">
/*
Define elements to show start/end dates and the dates in between
*/

/*
* for start/end dates
*/
.edges {
border : 1px solid;
border-color: #adaa9c #fff #fff #adaa9c;
background-color: #fffbee;
}

/*
* for dates between start and end dates
*/
.between {
background-color: #dccdb9;
}

.calendar tbody .disabled { text-decoration: line-through; color:#000}
</style>