<?php
class Content_Model_Content extends Zend_Db_Table
{
    protected $_name = "content";
    
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    public function upload ($data)
    {
        $this->_db->insert($this->_name, $data);
    }
    public function fetch ($id = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($id != "") {
            $sql->where('id = ?', $id);
        }
        $result = $this->_db->fetchRow($sql);
        
        return $result;
    }
    public function modify ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    public function delete ($id)
    {
        $this->_db->delete($this->_name, 'id = ' . (int) $id);
    }
    public function find ($keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
        $result = $this->_db->query($sql);
        return $result;
    }
    public function returnselect ($courseid)
    {
        $sql = $this->_db->select()
        	->where('courseid = ?',$courseid)
            ->from($this->_name)
            ->order('id ASC');
        ;
        return $sql;
    }
    public function findreturnselect ($courseid,$keyword = "")
    {
        $sql = $this->_db->select()->from($this->_name)
        ->where('courseid = ?',$courseid);
        if ($keyword != "") {
            //echo $keyword;
            $sql->where(
            ' title LIKE ? ','%' . $keyword . '%');
        }
        //echo $sql;
        return $sql;
    }
    public function getcontent ($courseid)
    {
        $sql = $this->_db->select()
        	->where('courseid = ?',$courseid)
            ->from($this->_name)
            ->order('id ASC');
        ;
        
        $result = $this->_db->fetchAll($sql);
        
        return $result;
    }
}
?>
