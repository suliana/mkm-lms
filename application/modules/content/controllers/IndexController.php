<?php
class Content_IndexController extends Zend_Controller_Action 
{
	public function init()
	{
		//$this->_helper->layout->setLayout('course');
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
	}
	
	public function indexAction() 
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		
		$this->view->courseid = $courseid;
		
		$form = new Content_Form_Search ();
		$this->view->form = $form;
		$content = new Content_Model_Content ();
		$select = $content->returnselect ( $courseid );

		//capture the input params
		if ($this->_request->getParam('keyword',0) || $this->_request->isPost())
		{
			//echo "here";
			//if it is a post, populate the form and reset the page to 1
			if ($this->_request->isPost())
			{
				$form->populate($this->_request->getPost());
				$page = 1;
			}
			else{
				//'search' showed up via the GET param
				$form->populate($this->_request->getParams());
			}
			
			//make sure the submitted data is valid and modify select statement
			if ($form->valid())
			{
				$data = $form->getValues ();
				$select = $content->findreturnselect ( $courseid, $data ['keyword'] );
			}

		}

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keyword'=>$form->getValue('keyword'));

	}
	
	public function uploadAction() 
	{
		$username = $this->userinfo->username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$registry = Zend_Registry::getInstance();
		// Get our translate object from registry.
		$translate = $registry->get('Zend_Translate');


		$content = new Content_Model_Content ();
		$form = new Content_Form_Upload ( array ('courseid' => $courseid ,'translator' => $translator) );

		$form->setTranslator($translate);

		$this->view->form = $form;

		//$form->getElement('courseid')->setValue($courseid);


		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST )) 
			{
				$data = $form->getValues ();
				$data['ccontent'] = stripslashes($data['ccontent']);
				$data["datecreated"]=date("Y-m-d H:i:s");
				$data["createby"]=$username;

				if ( $form->full_article->isUploaded() )
				 {
					$locationFile = $form->full_article->getFileName ();
					$data ["full_article"] = date ( 'Ymdhs' ) . "_" . $data ["full_article"];
					//echo $data ["filename"];
					$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/sme/fullarticle/' .  $courseid . '/' . $data ["full_article"];
					// Renommage du fichier
					$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
					$filterRename->filter ( $locationFile );
				}
				else
				{
					$data ["full_article"] = null;
				}

				if ($form->multimedia_files->isUploaded ()) 
				{
					$locationFile = $form->multimedia_files->getFileName ();
					$data ["multimedia_files"] = date ( 'Ymdhs' ) . "_" . $data ["multimedia_files"];
					//echo $data ["filename"];
					$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/sme/multimediafiles/' .  $courseid . '/' . $data ["multimedia_files"];
					// Renommage du fichier
					$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
					$filterRename->filter ( $locationFile );
				}
				else
				{
					$data ["multimedia_files"] = null;
				}

				$content->upload ( $data );
				$this->_redirect ( 'content/index/index/id/'.$courseid.'/courseid/'.$courseid );
			}
		}

	}
	
	public function modifyAction() 
	{
		$id = $this->_getParam ( 'id', 0 );

		$content = new Content_Model_Content ();
		$vdata=$content->fetch ( $id );

		$registry = Zend_Registry::getInstance();
		
		// Get our translate object from registry.
		$translate = $registry->get('Zend_Translate');
		$form = new Content_Form_Modify (array ('courseid' => $vdata["courseid"],'translator' => $translator ));

		$this->view->form = $form;
		if ($this->getRequest ()->isPost ())
		{
			if ($form->isValid( $_POST ))
			{
				$data = $form->getValues ();
				
				if ($id > 0)
				{
					$sdata ["title"] = $data ["title"];
					$sdata ["courseid"] = $data ["courseid"];
					$sdata['ccontent'] = stripslashes($data['ccontent']);

					if ($form->full_article->isUploaded ()) 
					{

						$locationFile = $form->full_article->getFileName ();
						$sdata ["full_article"] = date ( 'Ymdhs' ) . "_" . $data ["full_article"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/sme/fullarticle/' . $data ["courseid"] . '/' . $sdata ["full_article"];
						
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
					}

					if ($form->multimedia_files->isUploaded ())
					{

						$locationFile = $form->multimedia_files->getFileName ();
						$sdata ["multimedia_files"] = date ( 'Ymdhs' ) . "_" . $data ["multimedia_files"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/sme/multimediafiles/' . $data ["courseid"] . '/' . $sdata ["multimedia_files"];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );

					}

					
					$rscontent = $content->fetch( $id );
					$forum = new Forum_Model_Forum ();

					$rs=$forum->checkaddcategory($data ["courseid"],"",$rscontent ["title"]);
					if($rs)
					{
						$kdata["courseID"]=$data ["courseid"];
						$kdata["name"]=$data ["title"];
						$forum->modifyfromcontent ("forumcat", $kdata, $rscontent ["title"] , "name");
						
						$ldata["title"]="Discussion about ". $data ["title"];
						$ldata["description"]=$ldata["title"];
						$ldata["courseid"]=$data ["courseid"];
						
						$forum->modifyfromcontent ("forummain", $ldata, str_replace('{title}', $rscontent['title'], $this->translate('discuss_about_title') ), "title");
					}
					
					$sdata["datemodified"] = date("Y-m-d H:i:s");
					$sdata["modifyby"]=$this->userinfo->username;
					$content->modify ( $sdata, $id );
				} 
				else 
				{
					$data["datecreated"] = date("Y-m-d H:i:s");
					$data["createby"] = $this->userinfo->username;
					
					$content->upload ( $data );
				}
				
				$this->_redirect ( 'content/index/index/id/'.$vdata["courseid"].'/courseid/'.$vdata["courseid"] );
			}
		} 
		else 
		{
			if ($id > 0)
			{
				$form->populate ( $content->fetch ( $id ) );
			}
		}
	}
	
	public function deleteAction() 
	{	
		$courseid = $this->_getParam ( 'courseid', '' );

		if ($id > 0)
		{
			$content->delete ( $id );
			$this->_redirect ( 'content/index/index/id/'.$courseid.'/courseid/'.$courseid );
		}
	}

	public function listcourseAction() 
	{
		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();

		//echo $data->username;
		$this->view->course = $rscourse;
	}

	public function displaycontentAction()
	{
		$id = $this->_getParam ( 'id', 0 );
		$content = new Content_Model_Content ();
		$data=$content->fetch($id);

		$this->view->data = $data;
	}

	public function displaymultimediafilesAction() 
	{		
		$id = $this->_getParam ( 'id', 0 );

		$content = new Content_Model_Content ();
		$vdata=$content->fetch ( $id );
		$this->view->vdata=$vdata;
		//print_r($vdata);
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}
	
	public function framepage1Action()
	{
		$id = $this->_getParam ( 'id', 0 );
		$content = new Content_Model_Content ();
		$data=$content->fetch($id);

		$this->view->data = $data;
	}
	
	public function viewcourseAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		
		$course = new Application_Model_Course();
		$rscourse = $course->fetch($courseid,'coursecode');
		
		//print_r($_SESSION);

		if ( is_array($rscourse) )
		{	
			$this->_redirect($this->view->url(array('module'=>'content', 'controller'=>'index','action'=>'courseinfo' , 'courseid'=> $rscourse['id']),'default',true), array('prependBase' => false));
		}
		else
		{
			$sis_course = $course->fetchSIS($courseid);
			
			//so we create
			
			/* Array ( [IdSubject] => 1576 [IdFaculty] => 89 [IdDepartment] => 98 [SubjectName] => ?????? ????? ?????? ?????? ??????? [ShortName] => 916057 [subjectMainDefaultLanguage] => Skills Of Quran Recitation & Tajweed [courseDescription] => [BahasaIndonesia] => [ArabicName] => [SubCode] => 916057 [Active] => 1 [CreditHours] => 1 [AmtPerHour] => 0.00 [CourseType] => 9 [MinCreditHours] => 0 [IdDefaultSyllabus] => [ClassTimeTable] => 0 [ExamTimeTable] => 0 [ReligiousSubject] => 0 [IdReligion] => 0 [UpdDate] => 2013-05-17 13:52:02 [UpdUser] => 1 [SubCodeFormat] => [IdFormat] => ) */
			$arrdata = array(	
								'coursecode'	=>	$sis_course['SubCode'],
								'coursename'	=>	$sis_course['subjectMainDefaultLanguage'],
								'coursename_ar'	=>	$sis_course['SubjectName'],
								'coursetype'	=>	'',
								'credithour'	=>	$sis_course['CreditHours'],
								'courseinfo'	=>	$sis_course['courseDescription']
							);
							
			$course_id = $course->upload($arrdata);
			
			$this->_redirect($this->view->url(array('module'=>'content', 'controller'=>'index','action'=>'courseinfo' , 'courseid'=> $course_id),'default',true), array('prependBase' => false));
		}
		
		exit;
		
		//check id exists here or not
		//if not, create
		//redirect -> /content/index/courseinfo/courseid/<id>
	}
	
	public function courseinfoAction ()
	{
		//$this->_helper->layout->setLayout('course');
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('courseinfo');
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		//echo $data->username;

		$this->view->course = $rscourse;
	}
	
	public function modifycourseinfoAction()
	{
		//$this->_helper->layout->disableLayout();
		$courseid = $this->_getParam ( 'courseid', "" );
		$this->view->courseid = $courseid;

		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;
		
		$form = new Course_Form_Modifycourseinfo(array ('courseid' => $courseid ));
		$this->view->form = $form;

		
		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST ))
			{
				//echo "here";
				//exit();
				$data = $form->getValues ();
				$data['courseinfo'] = stripslashes($data['courseinfo']);
				unset($data['courseid']);
				//print_r($data);
				//exit();
				
				$course->modifycourseinfo ( $data,$courseid );
				$this->_redirect ( 'content/index/courseinfo/courseid/'.$courseid );
			}
		} 
		else
		{
			if ($courseid != "") 
			{
				$form->populate ( $course->fetch( $courseid ) );
			}
		}
	}
}





