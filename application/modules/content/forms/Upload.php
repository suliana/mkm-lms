<?php
class Content_Form_Upload extends ZendX_JQuery_Form
{
	
protected $_courseid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$title = $this->createElement ( 'text', 'title' );
		$title->setLabel ( $this->getView()->translate('title').':' )->setRequired ( true );
		
		$title->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
		
		$ccontent = $this->createElement ( 'textarea', 'ccontent' );
		$ccontent->setLabel ( $this->getView()->translate('content').':' )->setRequired ( true );
		
		$ccontent->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
		
		$element = new Zend_Form_Element_File('full_article');
        
        $dirarticle=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/';
        
        if(!is_dir($dirarticle))
        mkdir($dirarticle,0775);
        
        $dirarticle=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/fullarticle/';
        
        if(!is_dir($dirarticle))
        mkdir($dirarticle,0775);
        
        $dirarticlecourse=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/fullarticle/'.$this->_courseid;
        
        if(!is_dir($dirarticlecourse))
        mkdir($dirarticlecourse,0775);
        
        $element->setLabel($this->getView()->translate('full_article').':')->setDestination(
        $dirarticlecourse);
        // ensure minimum 1, maximum 3 files
        /*$element->addValidator('Count', false, 
        array('min' => 1, 'max' => 3));*/
        // limit to 100K
        $element->addValidator('Size', false, 20*1024*1024);
        // only JPEG, PNG, and GIFs
        $element->addValidator('Extension', false, 'pdf,doc,docx,xls,xlsx');
        $element->setRequired(false);
        // defines 3 identical file elements
        //$element->setMultiFile(3);
        //$form->addElement($element, 'foo');
        $element->setDecorators(array(
					'File',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td' )),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $element2 = new Zend_Form_Element_File('multimedia_files');
        
        $dirmultimedia=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/';
        
        if(!is_dir($dirmultimedia))
        mkdir($dirmultimedia,0775);
        
        $dirmultimedia=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/multimediafiles/';
        
        if(!is_dir($dirmultimedia))
        mkdir($dirmultimedia,0775);
        
        $dirmultimediacourse=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/multimediafiles/'.$this->_courseid;
        
        if(!is_dir($dirmultimediacourse))
        mkdir($dirmultimediacourse,0775);
        
        $element2->setLabel($this->getView()->translate('multimedia_file').':')->setDestination(
        $dirmultimediacourse);
        // ensure minimum 1, maximum 3 files
        /*$element->addValidator('Count', false, 
        array('min' => 1, 'max' => 3));*/
        // limit to 100K
        $element2->addValidator('Size', false, 20*1024*1024);
        // only JPEG, PNG, and GIFs
        $element2->addValidator('Extension', false, 'pdf,flv,wav,ppt,pptx');
        $element2->setRequired(false);
        // defines 3 identical file elements
        //$element->setMultiFile(3);
        //$form->addElement($element, 'foo');
        $element2->setDecorators(array(
					'File',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td' )),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $courseid = $this->createElement ( 'hidden', 'courseid' );
		/*$courseid->setLabel ( 'Course ID:' )->setRequired ( false );
		$courseid->setAttrib ( 'readonly', 'readonly' );*/
		$courseid->setValue($this->_courseid);
		
		$courseid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
		
		$this->addElements(
        array($title, $element, $ccontent, $element2, $courseid));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('upload').' '.$this->getView()->translate('content'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'content', 'controller'=>'index','action'=>'index','courseid' => $this->_courseid),'default',true) . "'; return false;"
        ));
        
        
	    
	    $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       ));
    }
}