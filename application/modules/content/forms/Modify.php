<?php
class Content_Form_Modify extends Zend_Form {
	protected $_courseid;
	
	public function setCourseid($value) {
		$this->_courseid = $value;
	}
	
	public function init()
	{
		$this->setMethod ( 'post' );
		
		$title = $this->createElement ( 'text', 'title' );
		$title->setLabel ( $this->getView()->translate('title') )->setRequired ( true )->setOptions(array('class'=>'inputtext', 'filters' => array('StringTrim','StripTags')));
		
		$ccontent = $this->createElement ( 'textarea', 'ccontent' );
		$ccontent->setLabel ( $this->getView()->translate('content').':' )->setRequired ( true );
	
        
		$element = new Zend_Form_Element_File('full_article');
      	  
        $dirarticle=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/';
        
        if(!is_dir($dirarticle))
        {
        	mkdir($dirarticle,0775);
        }
        
        $dirarticle=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/fullarticle/';
        
        if(!is_dir($dirarticle))
        {
        	mkdir($dirarticle,0775);
        }
        
        $dirarticlecourse=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/fullarticle/'.$this->_courseid;
        
        if(!is_dir($dirarticlecourse))
        {
        	mkdir($dirarticlecourse,0775);
        }
        
        $element->setLabel($this->getView()->translate('full_article').':')->setDestination($dirarticlecourse);
        
        // ensure minimum 1, maximum 3 files
        /*$element->addValidator('Count', false, 
        array('min' => 1, 'max' => 3));*/
        // limit to 100K
        $element->addValidator('Size', false, 20*1024*1024);
        
        // only JPEG, PNG, and GIFs
        $element->addValidator('Extension', false, 'pdf,doc,docx,xls,xlsx');
        $element->setRequired(false);

        // defines 3 identical file elements
        //$element->setMultiFile(3);
        //$form->addElement($element, 'foo');

        
        $element2 = new Zend_Form_Element_File('multimedia_files');
        
        $dirmultimedia=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/';
        
        if(!is_dir($dirmultimedia))
        {
        	mkdir($dirmultimedia,0775);
        }
        
        $dirmultimedia=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/multimediafiles/';
        
        if(!is_dir($dirmultimedia))
        {
        	mkdir($dirmultimedia,0775);
        }
        
        $dirmultimediacourse=DOC_PATH.'/'.APP_FOLDER.'/upload/sme/multimediafiles/'.$this->_courseid;
        
        if(!is_dir($dirmultimediacourse))
        {
        	mkdir($dirmultimediacourse,0775);
        }
        
        $element2->setLabel($this->getView()->translate('multimedia_file').':')->setDestination(
        $dirmultimediacourse);

        // ensure minimum 1, maximum 3 files
        /*$element->addValidator('Count', false, 
        array('min' => 1, 'max' => 3));*/
        // limit to 100K
        $element2->addValidator('Size', false, 20*1024*1024);
        // only JPEG, PNG, and GIFs
        $element2->addValidator('Extension', false, 'pdf,flv,wav,ppt,pptx');
        $element2->setRequired(false);
        // defines 3 identical file elements
        //$element->setMultiFile(3);
        //$form->addElement($element, 'foo');
        $courseid = $this->createElement ( 'hidden', 'courseid' );
                
		$this->addElements ( array($title, $element, $ccontent, $element2, $courseid));
		
		//button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('user'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'btn submit')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'btn'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'content', 'controller'=>'index','action'=>'index','courseid' => $this->_courseid),'default',true)  . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
	    
	}
}