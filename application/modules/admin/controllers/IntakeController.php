<?php
class Admin_IntakeController extends Zend_Controller_Action
{
	public function init ()
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('admin');
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
	}
	
	public function indexAction ()
	{
		$intake = new Admin_Model_Intake ();

		//$this->view->entries = $activity->fetchAll();
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$select = $intake->returnselect ( $courseid );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 50 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$msg = $this->_flashMessenger->getMessages();
		if($msg!=null){
			$this->view->noticeMessage = $msg[0];
		}
	}
	
	public function uploadAction() 
	{
		
		$username=$this->view->username;
	
		$intake = new Admin_Model_Intake ();
		
		$form = new Admin_Form_IntakeUpload ();
		$this->view->form = $form;
		

		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST ))
			{
				$data = $form->getValues ();
				echo $data["intakestatus"];
				
				$data["datecreated"]=date("Y-m-d H:i:s");
				$data["createby"]=$username;
				
				if ($data["intakestatus"]=="CURRENT")
				{
					
					$checkstatus=$intake->checkstatus( $data["intakestatus"] );

					if($checkstatus["intakestatus"]=="CURRENT")
					{
						//can not upload new intake
						$this->_helper->flashMessenger->addMessage("Failed to upload. Please check semester status.");
					}
					else
					{
						//can upload new intake					
						try
						{
							//insert into database
							$intake->upload ( $data );
							
						} 
						catch (Exception $e)
						{
							$this->_helper->flashMessenger->addMessage("Error While uploading");
						}
						
						if($data["intakestatus"]!=null)
						{
							$this->_helper->flashMessenger->addMessage("Successfuly uploaded");
						}
					}
					
				}
				else
				{
					try
					{
						//insert into database
						$intake->upload ( $data );
						
					}
					catch (Exception $e)
					{
						$this->_helper->flashMessenger->addMessage("Error While uploading");
					}
					
					if ($data["intakestatus"]!=null)
					{
						$this->_helper->flashMessenger->addMessage("Successfuly uploaded");
					}
				}
				
				$this->_redirect ( 'admin/intake/index' );
			}
		}
	}
	
	public function modifyAction()
	{
		$username=$this->data->username;
		
		$id = $this->_getParam ( 'id', 0 );
		
		$intake = new Admin_Model_Intake ();
		
		$form = new Admin_Form_IntakeModify ();
		$this->view->form = $form;
		

		if ($this->getRequest ()->isPost ())
		{
			if ($form->isValid ( $_POST )) 
			{
				$data = $form->getValues ();
				
				$data["datemodified"]=date("Y-m-d H:i:s");
				$data["modifyby"]=$username;
				
				if ($data["intakestatus"]=="CURRENT")
				{
					$checkstatus=$intake->checkstatus( $data["intakestatus"] );

					if ($checkstatus["intakestatus"]=="CURRENT")
					{
						//can not upload new intake
						$this->_helper->flashMessenger->addMessage("Failed to update. Please check semester status.");
					}
					else
					{
						//can upload new intake					
						try
						{
							//insert into database
							$intake->modify ( $data, $id );
						}
						catch (Exception $e)
						{
							$this->_helper->flashMessenger->addMessage("Error While Updating");
						}
						
						if($data["intakestatus"]!=null)
						{
							$this->_helper->flashMessenger->addMessage("Successfuly Updated");
						}
					}
				}
				else
				{
					try
					{
						//insert into database
						$intake->modify ( $data, $id );
					}
					catch (Exception $e)
					{
						$this->_helper->flashMessenger->addMessage("Error While Updating");
					}
					
					if ($data["intakestatus"]!=null)
					{
						$this->_helper->flashMessenger->addMessage("Successfuly Updated");
					}
				}
				$this->_redirect ( 'admin/intake/index' );
			}
		} 
		else 
		{
			if ($id > 0) 
			{
				$form->populate ( $intake->fetch ( $id ) );
			}
		}
	}
	
	public function deleteAction()
	{
		$intake = new Admin_Model_Intake ();
		$id = $this->_getParam ( 'rowid', 0 );

		//echo $id;
		if ($id > 0) 
		{
			$intake->delete ( $id );
			$this->_redirect ( 'admin/intake/index' );
		}
	}

}





