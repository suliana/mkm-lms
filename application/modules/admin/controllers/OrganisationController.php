<?php
class Admin_OrganisationController extends Zend_Controller_Action
{
    public function init ()
    {
        $storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
    }
    public function propertyAction ()
    {
    	//$this->_helper->layout->setLayout('admin');
    }
   
}





