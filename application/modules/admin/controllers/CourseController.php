<?php
class Admin_CourseController extends Zend_Controller_Action
{
	public function init() 
	{
		/* Initialize action controller here */
		//$this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
		//$this->_helper->layout->setLayout('admin');
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
	}
	
	public function indexAction ()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$intake = new Admin_Model_Intake ();
		$rsintake = $intake->fetchAll ();
		
		$coursedetails = new Admin_Model_Course ();
		
		foreach ($rsintake as $row) 
		{
			
			$rs=$coursedetails->checkdetails($courseid,$row['id']);
			if(!$rs)
			{
				$sdata["courseid"]=$courseid;
				$sdata["semid"]=$row['id'];
				$sdata["start_date"]="0000-00-00 00:00:00";
				$sdata["end_date"]="0000-00-00 00:00:00";
				$coursedetails->upload($sdata);
			}
		}
		
		$select = $coursedetails->returnselect ($courseid);
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

		
	}

	public function listcourseAction() 
	{
		$this->view->username = $this->userinfo->username;

		$course = new Application_Model_Course ();
		$rscourse = $course->listcourse ();

		//echo $data->username;
		$this->view->course = $rscourse;
	}

	public function modifyAction() 
	{
		$username = $this->userinfo->username;

		$id = $this->_getParam ( 'id', 0 );
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$coursedetails = new Admin_Model_Course ();

		$form = new Admin_Form_CourseModify ( array ('courseid' => $courseid ) );
		$this->view->form = $form;


		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST )) 
			{
				$data = $form->getValues ();

				if ($id > 0) {
					$sdata ["start_date"] = $data ["start_date"];
					$sdata ["end_date"] = $data ["end_date"];
					
					$sdata["update_date"]=date("Y-m-d H:i:s");
					$sdata["updated_by"]=$username;
					$coursedetails->modify ( $sdata, $id );

				} 
				
				$this->_redirect ( 'admin/course/index/courseid/'.$courseid );
			}
			
		} 
		else
		{
			if ($id > 0) 
			{
				$form->populate ( $coursedetails->fetch ( $id ) );
			}
		}
	}
	
	//
	//	public function deleteAction() {
	//		$storage = new Zend_Auth_Storage_Session ();
	//		$data = $storage->read ();
	//		if (! $data) {
	//			$this->_redirect ( 'index/index' );
	//		}
	//		$intake = new Admin_Model_Intake ();
	//		$id = $this->_getParam ( 'rowid', 0 );
	//
	//
	//		//echo $id;
	//		if ($id > 0) {
	//			$intake->delete ( $id );
	//			$this->_redirect ( 'admin/intake/index' );
	//		}
	//	}

}





