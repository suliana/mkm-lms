<?php
class Admin_UpgradeController extends Zend_Controller_Action
{
    public function init ()
    {
        $storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
		
		$this->db =  Zend_Registry::get('db');
    	//$this->_helper->layout->setLayout('admin');
    	
		$this->loadstuff();
    }
    
    private function loadstuff()
    {
    	$courses = $this->db->fetchAll('SELECT id,courseid,coursecode,coursename FROM `coursemain`');
    	
    	$this->coursebycode = array();
    	
    	foreach ( $courses as $course )
    	{
    		$this->coursebycode[$course['coursecode']] = $course['id'];
    	}
    }
    
    public function indexAction ()
    {
    	$do = $this->_getParam ( 'do', 0 );

    	if ( $do != '' )
    	{
    		$this->upgrade($do, 'id, coursecode');	
    	}
    	else
    	{
       		echo 'what do you want to upgrade today?';
       
	       ?>
	       <ul>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'block'),null, true)?>"><strong>Block</strong></a></li>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'activity'),null, true)?>">Activity</a></li>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'announcement'),null, true)?>">Announcement</a></li>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'assignment'),null, true)?>">Assignment</a></li>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'content'),null, true)?>">Content</a></li>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'coursemain'),null, true)?>"><strong>Coursemain</strong></a></li>
	       		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'coursemain_details'),null, true)?>"><strong>Coursemain_details</strong></a></li>
	     		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'forumcat'),null, true)?>"><strong>forumcat</strong></a></li>
	     		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'forummain'),null, true)?>"><strong>forummain</strong></a></li>
	     		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'forumsetup'),null, true)?>"><strong>forumsetup</strong></a></li>
	     		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'questionbank'),null, true)?>"><strong>questionbank</strong></a></li>
	     		<li><a href="<?php echo $this->view->url(array('module'=>'admin' , 'controller' => 'upgrade' , 'action' => 'index', 'do'=> 'resources'),null, true)?>"><strong>resources</strong></a></li>
	       </ul>
	       <?php
    	}
    }
    
    private function upgrade($table='', $select='' )
    {
    	//block
    	
    	$blocks = $this->db->fetchAll('SELECT '.$select.' FROM `'.$table.'`');
    	foreach ( $blocks as $block )
    	{
    		$newid = $this->coursebycode[$block['coursecode']];
    	
    		$this->db->update($table, array('courseid'=>$newid), array('id = ?' => $block['id']));
		}
		
		echo 'all done';
    	
    	//
    	
    	//get all courses
    	
    }
}





