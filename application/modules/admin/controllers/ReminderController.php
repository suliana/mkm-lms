<?php
class Admin_ReminderController extends Zend_Controller_Action
{
	public function init ()
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('admin');
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
	}
	
	public function indexAction ()
	{

		$ModelReminder = new Admin_Model_Reminder();

		//$this->view->entries = $activity->fetchAll();
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$select = $ModelReminder->returnselect (  );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$msg = $this->_flashMessenger->getMessages();
		if($msg!=null)
		{
			$this->view->noticeMessage = $msg[0];
		}
	}
	
	public function uploadAction() 
	{
		$username = $this->userinfo->username;
		
//		$form = new Admin_Form_ReminderUpload ();
//		$this->view->form = $form;
		
		$ModelReminder = new Admin_Model_Reminder();
		
		if ($this->getRequest ()->isPost ()) 
		{
			$courseid= $this->getRequest()->getParam('courseid');
			$role= $this->getRequest()->getParam('role');
			$title= $this->getRequest()->getParam('title');
			$message= $this->getRequest()->getParam('message');
			$start_date= $this->getRequest()->getParam('start_date',0);
			$end_date= $this->getRequest()->getParam('end_date',0);
			$username;
			//if all empty..back
			if ($courseid=='0' || $role=='0' || $message=="" || $start_date=="" || $end_date=="")
			{	
				$this->_helper->flashMessenger->addMessage("Error While uploading");	
			}
			else
			{
				
				try
				{
					$reminder["courseid"]=$courseid;
					$reminder["role"]=$role;
					$reminder["start_date"]=$start_date;
					$reminder["end_date"]=$end_date;
					$reminder["title"]=$title;
					$reminder["message"]=$message;
					$reminder["datecreated"]=date("Y-m-d H:i:s");
					$reminder["createby"]=$username;
					
					$upload=$ModelReminder->upload($reminder);
					
				}
				catch (Exception $e)
				{
					$this->_helper->flashMessenger->addMessage("Error While uploading");
				}
				
				
				if($start_date!=null){
					$this->_helper->flashMessenger->addMessage("Successfuly uploaded");
				}
				
			}
		
			$this->_redirect ( 'admin/reminder/index' );
		}
	}
	
	public function modifyAction()
	{
		$username = $this->userinfo->username;
		
		$id = $this->_getParam ( 'id', 0 );
		$this->view->idreminder = $id;
		
		$ModelReminder = new Admin_Model_Reminder();
		$searchreminder =  $ModelReminder->searchreminder($id); 

		$this->view->id_rem = $searchreminder["id"];
		$this->view->courseid = $searchreminder["courseid"];
		$this->view->role = $searchreminder["role"];
		$this->view->start_date = $searchreminder["start_date"];
		$this->view->end_date = $searchreminder["end_date"];
		$this->view->title = $searchreminder["title"];
		$this->view->message = $searchreminder["message"];
		$this->view->createby = $searchreminder["createby"];
		$this->view->datecreated = $searchreminder["datecreated"];
		$this->view->modifyby = $searchreminder["modifyby"];
		$this->view->datemodified = $searchreminder["datemodified"];
		
		if ($this->getRequest ()->isPost ()) 
		{
			$courseid= $this->getRequest()->getParam('courseid');
			$role= $this->getRequest()->getParam('role');
			$message= $this->getRequest()->getParam('message');
			$title= $this->getRequest()->getParam('title');
			$start_date= $this->getRequest()->getParam('start_date',0);
			$end_date= $this->getRequest()->getParam('end_date',0);
			$username;
			
			//if all empty..back
			if ($courseid=='0' || $role=='0' || $message=="" || $start_date=="" || $end_date=="")
			{	
				$this->_helper->flashMessenger->addMessage("Failed to update.");	
			}
			else
			{
				try
				{
					$reminder["courseid"]=$courseid;
					$reminder["role"]=$role;
					$reminder["start_date"]=$start_date;
					$reminder["end_date"]=$end_date;
					$reminder["title"]=$title;
					$reminder["message"]=$message;
					$reminder["datemodified"]=date("Y-m-d H:i:s");
					$reminder["modifyby"]=$username;
					
					$upload=$ModelReminder->modify($reminder, $id);
					
				}
				catch (Exception $e)
				{
					$this->_helper->flashMessenger->addMessage("Failed to update.");
				}
				
				
				if($start_date!=null)
				{
					$this->_helper->flashMessenger->addMessage("Successfuly updated.");
				}
				
			}
		
			$this->_redirect ( 'admin/reminder/index' );
		}
	}
	
	public function deleteAction() 
	{	
		$ModelReminder = new Admin_Model_Reminder();
		$id = $this->_getParam ( 'rowid', 0 );
			
		//echo $id;
		if ($id > 0)
		{
			$ModelReminder->delete ( $id );
			$this->_redirect ( 'admin/reminder/index' );
		}
	}
	
	public function framepage1Action() 
	{
		$id = $this->_getParam ( 'id', 0 );
		$ModelReminder = new Admin_Model_Reminder();
		$data=$ModelReminder->searchreminder($id);

		$this->view->data = $data;

	}
	
	public function viewreminderAction() 
	{
		$id = $this->_getParam ( 'id', 0 );
		$ModelReminder = new Admin_Model_Reminder();
		$data=$ModelReminder->searchreminder($id);

		$this->view->data = $data;
	}

}





