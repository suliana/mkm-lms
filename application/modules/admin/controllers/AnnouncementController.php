<?php
class Admin_AnnouncementController extends Zend_Controller_Action
{
	public function init ()
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('admin');
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
	}
	
	
	public function indexAction ()
	{
		$ModelAnnouncement = new Admin_Model_Announcement();

		//$this->view->entries = $activity->fetchAll();
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$select = $ModelAnnouncement->returnselect (  );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 5 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
	}
	
	public function uploadAction()
	{
		$username= $this->userinfo->username;
		
//		$form = new Admin_Form_ReminderUpload ();
//		$this->view->form = $form;
		
		$ModelAnnouncement = new Admin_Model_Announcement();
		
		if ($this->getRequest ()->isPost ()) {
			$courseid= $this->getRequest()->getParam('courseid');
			$role= $this->getRequest()->getParam('role');
			$title= $this->getRequest()->getParam('title');
			$message= $this->getRequest()->getParam('message');
			$start_date= $this->getRequest()->getParam('start_date',0);
			$end_date= $this->getRequest()->getParam('end_date',0);
			$username;
			//if all empty..back
			if ($courseid=='0' || $role=='0' || $message=="" || $start_date=="" || $end_date==""){
				
				$this->_helper->flashMessenger->addMessage("Error While uploading");
				
			}
			else
			{
				try
				{
					$ann["courseid"]=$courseid;
					$ann["role"]=$role;
					$ann["start_date"]=$start_date;
					$ann["end_date"]=$end_date;
					$ann["title"]=$title;
					$ann["message"]=$message;
					$ann["datecreated"]=date("Y-m-d H:i:s");
					$ann["createby"]=$username;
					
					$upload=$ModelAnnouncement->upload($ann);
					
				}
				catch (Exception $e)
				{
					$this->_helper->flashMessenger->addMessage("Error While uploading");
				}
				
				if($start_date!=null)
				{
					$this->_helper->flashMessenger->addMessage("Successfuly uploaded");
				}
				
			}
		
			$this->_redirect ( 'admin/announcement/index' );
		}
	}
	
	public function modifyAction() 
	{
		$username = $this->userinfo->username;
		
		$id = $this->_getParam ( 'id', 0 );
		$this->view->idann = $id;
		
		$ModelAnnouncement = new Admin_Model_Announcement();
		$searchann =  $ModelAnnouncement->searchann($id); 

		$this->view->id_rem = $searchann["id"];
		$this->view->courseid = $searchann["courseid"];
		$this->view->role = $searchann["role"];
		$this->view->start_date = $searchann["start_date"];
		$this->view->end_date = $searchann["end_date"];
		$this->view->title = $searchann["title"];
		$this->view->message = $searchann["message"];
		$this->view->createby = $searchann["createby"];
		$this->view->datecreated = $searchann["datecreated"];
		$this->view->modifyby = $searchann["modifyby"];
		$this->view->datemodified = $searchann["datemodified"];
		
		if ($this->getRequest ()->isPost ()) 
		{
			$courseid= $this->getRequest()->getParam('courseid');
			$role= $this->getRequest()->getParam('role');
			$message= $this->getRequest()->getParam('message');
			$title= $this->getRequest()->getParam('title');
			$start_date= $this->getRequest()->getParam('start_date',0);
			$end_date= $this->getRequest()->getParam('end_date',0);
			$username;
			
			//if all empty..back
			if ($courseid=='0' || $role=='0' || $message=="" || $start_date=="" || $end_date=="")
			{	
				$this->_helper->flashMessenger->addMessage("Failed to update.");
				
			}
			else
			{	
				try
				{
					$ann["courseid"]=$courseid;
					$ann["role"]=$role;
					$ann["start_date"]=$start_date;
					$ann["end_date"]=$end_date;
					$ann["title"]=$title;
					$ann["message"]=$message;
					$ann["datemodified"]=date("Y-m-d H:i:s");
					$ann["modifyby"]=$username;
					
					$upload=$ModelAnnouncement->modify($ann, $id);
					
				}
				catch (Exception $e)
				{
					$this->_helper->flashMessenger->addMessage("Failed to update.");
				}
				
				if($start_date!=null)
				{
					$this->_helper->flashMessenger->addMessage("Successfuly updated.");
				}
				
			}
		
			$this->_redirect ( 'admin/announcement/index' );
		}
	}
	
	public function deleteAction() 
	{
		$ModelAnnouncement = new Admin_Model_Announcement();
		$id = $this->_getParam ( 'rowid', 0 );
		
		//echo $id;
		if ($id > 0) {
			$ModelAnnouncement->delete ( $id );
			$this->_redirect ( 'admin/announcement/index' );
		}
	}
	
	public function framepage1Action() 
	{
		$id = $this->_getParam ( 'id', 0 );
		$ModelAnnouncement = new Admin_Model_Announcement();
		$data=$ModelAnnouncement->searchann($id);

		$this->view->data = $data;
	}
	
	public function viewannouncementAction() 
	{
		$id = $this->_getParam ( 'id', 0 );
		$ModelAnnouncement = new Admin_Model_Announcement();
		$data=$ModelAnnouncement->searchann($id);

		$this->view->data = $data;

	}

}





