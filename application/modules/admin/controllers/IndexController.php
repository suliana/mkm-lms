<?php
class Admin_IndexController extends Zend_Controller_Action
{
    public function init ()
    {
       $storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
		
		
    	//$this->_helper->layout->setLayout('admin');
    }
    public function indexAction ()
    {
        $id = $this->_getParam('id', 0);
        $this->view->courseid = $id;     
        
    }
    
    public function reportAction ()
    {

		        
    }
   
}





