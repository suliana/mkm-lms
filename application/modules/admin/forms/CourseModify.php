<?php
class Admin_Form_CourseModify extends ZendX_JQuery_Form
{
	
protected $_courseid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$elem = $this->createElement('text', 'start_date');
        $elem->setLabel($this->getView()->translate('start_date').':')->setRequired(true);
        
        $elem->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))

        ));
        
        $this->addElement($elem);
        
        
        $elem2 = $this->createElement('text', 'end_date');
        $elem2->setLabel($this->getView()->translate('end_date').':')->setRequired(true);
        
        $elem2->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))

        ));
        
        $this->addElement($elem2);
        
    	
    	
		
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('submit'),
          'Options'=>array('class'=>'button'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'colspan'=>'3', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'Options'=>array('class'=>'button'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'admin', 'controller'=>'course','action'=>'index','courseid' => $this->_courseid),'default',true) . "'; return false;"
        ));
	    
	    $this->setDecorators(array(

  

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       )); 
    }
}