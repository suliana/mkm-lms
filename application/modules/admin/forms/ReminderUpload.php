<?php
class Admin_Form_ReminderUpload extends ZendX_JQuery_Form
{
	
protected $_courseid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function init ()
    {
       	$this->setMethod('post');
    	
    	$course = new Admin_Model_Course ();
        $result =  $course->allcourse();     		
		$courseid = new Zend_Form_Element_Select('courseid');
		
		$courseid->setLabel($this->getView()->translate('course').' :')
		->setRequired(true);
		$courseid->addMultiOption("xxx", "---- ".$this->getView()->translate('please_select')." ----");
		foreach ($result as $c) {
		$courseid->addMultiOption($c['courseid'], $c['courseid']);
		}
		
		$courseid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $privilage = new Zend_Form_Element_Select('role');
		$privilage ->setLabel($this->getView()->translate('role').' :')->setRequired(true)->addMultiOptions( array('xxx' => '---- '.$this->getView()->translate('please_select').' ----', 'STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','EOS' => 'EOS','ADMIN' => 'ADMIN'));
		
		$privilage->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
		
    	
//    	$message = $this->createElement('textarea', 'message');
//        $message->setLabel($this->getView()->translate('message').' :')->setRequired(false);
        
        $message=$this->createElement(
		    'Textarea',
		    'message',
		    array(
		        'value'      => '',
		        'label'      => $this->getView()->translate('message').' :',
		        'required'	=> true,
		        'style'    => 'height: 150px; width:400px',
		        //'propercase' => true,
		    )
		);
        
        
        $message->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('add_reminder'))->setIgnore(true);
        $register->setOptions(array('class'=>'button'));
        
        $register->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        
        
        
        	
		$elem = new ZendX_JQuery_Form_Element_DatePicker('start_date', array(
				   'decorators'=>array(
				          'UiWidgetElement',  // it necessary to include for jquery elements 
						   'Description',
			               'Errors', 
				   		   array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               			   array('Label', array('tag' => 'td')),	
			               array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
			               ),
	
        			'label' 		=> $this->getView()->translate('start_date').':',
        			'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' => 'yy-mm-dd',
						'minDate'	 => '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "minDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#end_date").datepicker( "option", option, date );
									}')
		)));
		
		

//		$this->addElement($elem);

		$elem2 = new ZendX_JQuery_Form_Element_DatePicker('end_date', array(
					'decorators'=>array(
					   'UiWidgetElement',  // it necessary to include for jquery elements 
					   'Description',
		               'Errors', 
		               array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               		  array('Label', array('tag' => 'td')),
		               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
		               ),
					'label' 		=> $this->getView()->translate('end_date').':',
					'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' 	=> 'yy-mm-dd',
						'minDate'	 	=> '+1',
						'defaultDate' 	=> '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "maxDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#start_date").datepicker( "option", option, date );
									}')

		)));

//		$this->addElement($elem);
		
		$this->addElements(
        array($courseid, $privilage, $message, $elem, $elem2, $register));
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}