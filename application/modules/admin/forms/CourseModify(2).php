<?php
class Admin_Form_CourseModify extends ZendX_JQuery_Form
{
	
protected $_courseid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$elem = new ZendX_JQuery_Form_Element_DatePicker('start_date', array(
				   'decorators'=>array(
				          'UiWidgetElement',  // it necessary to include for jquery elements 
						   'Description',
			               'Errors', 
				   		   array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               			   array('Label', array('tag' => 'td')),	
			               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
			               ),
	
        			'label' 		=> $this->getView()->translate('start_date').':',
        			'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' => 'yy-mm-dd',
						'minDate'	 	=> '+0',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "minDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#end_date").datepicker( "option", option, date );
									}')
		)));

		$this->addElement($elem);

		$elem2 = new ZendX_JQuery_Form_Element_DatePicker('end_date', array(
					'decorators'=>array(
					   'UiWidgetElement',  // it necessary to include for jquery elements 
					   'Description',
		               'Errors', 
		               array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               		  array('Label', array('tag' => 'td')),
		               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
		               ),
					'label' 		=> $this->getView()->translate('end_date').':',
					'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' 	=> 'yy-mm-dd',
						'minDate'	 	=> '+0',
						'defaultDate' 	=> '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "maxDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#start_date").datepicker( "option", option, date );
									}')

		)));

		$this->addElement($elem2);
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('submit'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'colspan'=>'3', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'admin', 'controller'=>'course','action'=>'index','courseid' => $this->_courseid),'default',true) . "'; return false;"
        ));
	    
	    $this->setDecorators(array(

  

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       )); 
    }
}