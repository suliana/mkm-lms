<?php
class Admin_Form_IntakeUpload extends Zend_Form
{
	
protected $_courseid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$intakeid = $this->createElement('text', 'intakecode');
        $intakeid->setLabel($this->getView()->translate('semester_id').':')->setRequired(true);
        
        $description = new Zend_Form_Element_Textarea('description');
		$description->setLabel($this->getView()->translate('description').':')
		    ->setRequired(true)
		    ->setAttrib('COLS', '40')
		    ->setAttrib('ROWS', '4');
		
        $intakestatus = new Zend_Form_Element_Select(intakestatus);
		$intakestatus ->setLabel($this->getView()->translate('semester_status').' :')->addMultiOptions( array('PAST' => 'PAST','CURRENT' => 'CURRENT','NEXT' => 'NEXT'))->setRequired(true);
               
        $this->addElements(
        array($intakeid, $description, $intakestatus));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('add').' '.$this->getView()->translate('semester'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'button')
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array('ViewHelper'),
          'Options'=>array('class'=>'button'),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'admin', 'controller'=>'intake','action'=>'index'),'default',true) . "'; return false;"
        ));
        
        $this->addDisplayGroup(array('save','cancel'),'buttons', array(
	      'decorators'=>array(
	        'FormElements',
	        array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
	        'DtDdWrapper'
	      )
	    ));
    }
}