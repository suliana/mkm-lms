<?php
class Admin_Model_Reminder extends Zend_Db_Table
{
	protected $_remindersetup = "reminder_setup";
	

	public function upload ($data)
    {
        $this->_db->insert($this->_remindersetup, $data);
    }
    
    public function returnselect ()
	{	
		$sql = $this->_db->select()->from($this->_remindersetup);
		
		return $sql;
	}
	
	public function searchreminder($idreminder){
		$sql = $this->_db->select()->from($this->_remindersetup);
		if ($idreminder != "") {
			$sql->where('id = ?', $idreminder);
		}
		
		$result = $this->_db->fetchRow($sql);

		return $result;
	}
	
	public function modify ($data, $id)
	{
		$this->_db->update($this->_remindersetup, $data, 'id = ' . (int) $id);
	}
	
	public function delete ($id)
	{
		$this->_db->delete($this->_remindersetup, 'id = ' . (int) $id);
	}
	
	public function selectreminder($privilage){
		$datenow=date("Y-m-d");
		
		$sql = $this->_db->select()->from($this->_remindersetup);
		if ($privilage != "") {
			$sql->where('start_date <= ?', $datenow)
             	->where('end_date >= ?', $datenow)
//             	->where('role = ?', $privilage)
             	->where('role IN (?) ', array($privilage, 'BOTH'));
		}

		$result = $this->_db->fetchAll($sql);

		return $result;
	}
}