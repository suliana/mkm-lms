<?php
class Admin_Model_Course extends Zend_Db_Table
{
	protected $_name = "coursemain_details";
	protected $_coursemain = "coursemain";
	

	public function fetchAll ()
	{
		$sql = $this->_db->select()->from($this->_name);
		$stmt = $this->_db->query($sql);
		return $stmt;
	}
	public function upload ($data)
	{
		$this->_db->insert($this->_name, $data);
	}
	public function fetch ($id = "",$intake = "")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($id != "") {
			$sql->where('id = ?', $id);
		}
		if ($intake != "") {
			$sql->where('intakecode = ?', $intake);
		}
		//echo $sql;
		//exit();
		$result = $this->_db->fetchRow($sql);
		
		//print_r($result);
		//exit();
		return $result;
	}
	public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'id = ' . (int) $id);
	}
	public function delete ($id)
	{
		$this->_db->delete($this->_name, 'id = ' . (int) $id);
	}
	public function find ($keyword = "")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($keyword != "") {
			//echo $keyword;
			$sql->where(
			' title LIKE ? ','%' . $keyword . '%');
		}
		$result = $this->_db->query($sql);
		return $result;
	}
	public function returnselect ($courseid)
	{
		
		$sql = $this->_db->select()->from($this->_name)
				->where('courseid = ?', $courseid);
		
		return $sql;
	}
	
	
	public function checkdetails ($courseid,$semid)
	{
		
		$sql = $this->_db->select()->from($this->_name)
					->where('courseid = ?', $courseid);
		$sql->where('semid = ?', $semid);
		
		//echo $sql;
		$result = $this->_db->fetchAll($sql);
		
		return $result;
	}

	public function allcourse ()
	{
		$sql = $this->_db->select()->from($this->_coursemain);
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	
}
?>
