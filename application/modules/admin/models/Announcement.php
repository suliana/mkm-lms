<?php
class Admin_Model_Announcement extends Zend_Db_Table
{
	protected $_annsetup = "announcement";
	

	public function upload ($data)
    {
        $this->_db->insert($this->_annsetup, $data);
    }
    
    public function returnselect ()
	{	
		$sql = $this->_db->select()->from($this->_annsetup);
		
		return $sql;
	}
	
	public function searchann($idann){
		$sql = $this->_db->select()->from($this->_annsetup);
		if ($idann != "") {
			$sql->where('id = ?', $idann);
		}
		
		$result = $this->_db->fetchRow($sql);

		return $result;
	}
	
	public function modify ($data, $id)
	{
		$this->_db->update($this->_annsetup, $data, 'id = ' . (int) $id);
	}
	
	public function delete ($id)
	{
		$this->_db->delete($this->_annsetup, 'id = ' . (int) $id);
	}
	
	public function selectann($privilage){
		$datenow=date("Y-m-d");
		
		$sql = $this->_db->select()->from($this->_annsetup);
		if ($privilage != "") {
			$sql->where('start_date <= ?', $datenow)
             	->where('end_date >= ?', $datenow)
//             	->where('role = ?', $privilage)
             	->where('role IN (?) ', array($privilage, 'BOTH'));
		}

		$result = $this->_db->fetchAll($sql);

		return $result;
	}
}