<?php
class Block_Model_Blockcomment extends Block_Model_Block
{


	public function upload ($data)
	{
		$this->_db->insert($this->_block_content_comments, $data);
	}
	public function fetch ($id = "")
	{
		$sql = $this->_db->select()->from($this->_block_content_comments);
		if ($id != "") {
			$sql->where('c_id = ?', $id);
		}



		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	public function modify ($data, $id)
	{
		$this->_db->update($this->_block_content_comments, $data, 'c_id = ' . (int) $id);
	}
	public function delete ($id)
	{
		$this->_db->delete($this->_block_content_comments, 'c_id = ' . (int) $id);
	}
	public function returnselect ()
	{
		$sql = $this->_db->select()
		->from($this->_block_content_comments);

		;

		$sql->order('c_id ASC');

		return $sql;
	}


	public function fetchAll ($where="",$orderby="date_created ASC",$limit="",$offset="0")
	{
		$sql = $this->_db->select()->from($this->_block_content_comments,	array('*','UNIX_TIMESTAMP() - date_created AS CommentTimeSpent'));

		

		if ($where != "") {
			$sql->where($where);
		}
		
		$sql->order($orderby);
		
		//echo "limit : ".$limit;
		if ($limit != "") {

			$sql->limit($limit,$offset);

		}

		//echo $sql;
		//exit();
		$result = $this->_db->fetchAll($sql);

		//print_r($result);
		//exit();
		return $result;
	}
	
	public function getlastinsertid ()
	{
		$id=$this->_db->lastInsertId($this->_block_content_comments);
		return $id;
	}
	
	
	
	public function fetchlikes ($where="")
	{
		$sql = $this->_db->select()->from($this->_block_content_likes);

		

		if ($where != "") {
			$sql->where($where);
		}
		
		$result = $this->_db->fetchRow($sql);

		return $result;
	}
	
	public function fetchip ($postid="",$username="")
	{
		
		$sql = $this->_db->select()->from($this->_block_content_ip,	array('count(*)'));

		

		if ($postid != "") {
			$sql->where('post_id = ?', $postid);
		}
		
		if ($username != "") {
			$sql->where('username = ?', $username);
		}
		
		//echo $sql;
		$result = $this->_db->fetchRow($sql);

		return $result;
	}
	
	public function modifylikes ($data, $postid)
	{
		$this->_db->update($this->_block_content_likes, $data, 'post_id = ' . (int) $postid);
	}
	
	public function deleteip ($postid,$username)
	{
		$condition = array(
		'post_id = ?' => $postid,
		'username = ?' => $username
		);
		$this->_db->delete($this->_block_content_ip,  $condition);
		
	}
	
	public function uploadlikes ($data)
	{
		$this->_db->insert($this->_block_content_likes, $data);
	}
	
	public function uploadip ($data)
	{
		$this->_db->insert($this->_block_content_ip, $data);
	}
	
	
	
}
?>
