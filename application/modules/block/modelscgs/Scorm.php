<?php
class Block_Model_Scorm extends Zend_Db_Table
{
	
	protected $_scotrack = "c_sco_track";
	 
	public function scorm_get_tracks($userid,$cmid,$scoid)
	{	
		
			//$where = "user_id='$userid' AND module_id='$cmid' AND sco_id = '$tocid'";
			//$sql = "select * from `$content_db`.c_sco_track where $where";
			//$this->dbconn(`$content_db`);
			//$this->conn->debug=1;
			//$rs  = $this->conn->Execute($sql);			
			//$tracks = $this->cleanArray($rs);
			
		     $sql = $this->_db->select()
    	  			      ->from($this->_scotrack) 
    	  			      ->where("user_id='$userid'")   	  			    
    	  			      ->where("module_id='$cmid'")
    	  			      ->where("sco_id='$scoid'");
    	  			        	  			      
    	
	    	 $tracks = $this->_db->fetchAll($sql);   	      
	        
				if(is_array($tracks))
				{			
					$usertrack->userid = $userid;
			        $usertrack->scoid = $scoid; 	
			        // Defined in order to unify scorm1.2 and scorm2004		       
			        $usertrack->score_raw = '';
			        $usertrack->status = '';
			        $usertrack->total_time = '00:00:00';
			        $usertrack->session_time = '00:00:00';
			        $usertrack->timemodified = 0;
	        	
					foreach($tracks as $track){	
					$usertrack->$track["element"]=$track["value"];	
					
							switch ($track["element"]) {
			                case 'cmi.core.lesson_status':
			                case 'cmi.completion_status':
			                    if ($track["value"] == 'not attempted') {
			                        $track["value"] = 'notattempted';
			                    }       
			                    $usertrack->status = $track["value"];
			                break;  
			                case 'cmi.core.score.raw':
			                case 'cmi.score.raw':
			                    $usertrack->score_raw = $track["value"];
			                break;  
			                case 'cmi.core.session_time':
			                case 'cmi.session_time':
			                    $usertrack->session_time = $track["value"];
			                break;  
			                case 'cmi.core.total_time':
			                case 'cmi.total_time':
			                    $usertrack->total_time = $track["value"];
			                break;  
				            }       
				            if (isset($track["timemodified"]) && ($track["timemodified"] > $usertrack->timemodified)) {
				                $usertrack->timemodified = $track["timemodified"];
				            }       
					
				    }	
				    return $usertrack;	
				}else {
			     return false;
			    } 
	}	
		
	
	public function scorm_get_sco($scoid) 
	{	
		//global $content_db,$scoid;	
						
		//$sqls   = "select * from $content_db.c_lesson  where ID = '$scoid'";
		//$this->dbconn($content_db);
		
		//$scos  = $this->conn->Execute($sqls);			
		//$scodt = $this->cleanArray($scos);
		
		 $sql = $this->_db->select()
    	  			      ->from('c_lesson') 
    	  			      ->where("sco_id='$scoid'");  	  			    
    	  			    	  			      
	     $scodt = $this->_db->fetchAll($sql);   
				
		if(is_array($scodt)){				
		
			//$where = "scoid = '$scoid'";
			//$sql   = "select * from $content_db.mdl_scorm_scoes_data  where $where";
			//$this->dbconn($content_db);
			//$this->conn->debug=1;
			//$sco  = $this->conn->Execute($sql);			
			//$scodatas = $this->cleanArray($sco);
			
			 $sql = $this->_db->select()
    	  			      ->from('mdl_scorm_scoes_data') 
    	  			      ->where("scoid='$scoid'");  	  			    
    	  			    	  			      
	     	$scodatas = $this->_db->fetchAll($sql);  
			
				if(is_array($scodatas)){			
					 foreach ($scodatas as $scodata) {		                
				         $sco->$scodata["name"]=$scodata["value"];				        
				     }				     
				}else{
					$sco->parameters = ''; 
				}//end if
			     
			return $sco;     
		} else {
		    return false;
		} 
	
	}
				

	public function scorm_get_last_attempt($username,$cmid,$scoid) 
	{	  
	   //global $cmid,$cid,$scoid,$userid,$content_db;

		//$sql="select max(attempt) as  attempted from `$content_db`.c_sco_track where sco_id='$scoid' AND user_id='$userid' AND module_id='$cmid'";
		
		//$this->dbconn(`$content_db`);
		
		//$rs=$this->conn->Execute($sql);
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = "select max(attempt) as  attempted from c_sco_track 
				where sco_id='$scoid' AND user_id='$username' AND module_id='$cmid'";  			    
    	  			    	  			      
	    $rs = $this->_db->fetchRow($sql);  
		
		
		
		if($rs)
		{
		 $lastattempt=$rs["attempted"];
			 if ($lastattempt) {
		        if (empty($lastattempt)) {
		            return '1';
		        } else {
		            return $lastattempt;
		        }
		    }
		} 			
	}
	
	
	
	
}
?>