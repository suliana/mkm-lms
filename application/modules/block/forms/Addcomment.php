<?php
class Block_Form_Addcomment extends Zend_Form
{

	protected $_courseid;
	protected $_blockid;
	protected $_postid;


	public function setCourseid($value)
	{
		$this->_courseid = $value;
	}
	
	public function setBlockid($value)
	{
		$this->_blockid = $value;
	}
	public function setPostid($value)
	{
		$this->_postid = $value;
	}

	public function init ()
	{

		$this->setMethod('post');
		$this->setAction('/block/index/addfeedback/courseid');
		$this->setAttrib('id', 'formaddfeedback');


		$comments = new Zend_Form_Element_Textarea('comments');
		$comments->setAttrib('cols', '60')
   		->setAttrib('rows', '1');
		$comments->setAttrib('size', '500');
		$comments->setLabel($this->getView()->translate('comment'));
		$comments->setRequired(true);


		$element = new Zend_Form_Element_File('filename');

		$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/';

		if (! is_dir ( $uploadDir ))
		mkdir ( $uploadDir, 0775 );

		$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/' . $this->_courseid . '/';

		if (! is_dir ( $uploadDir ))
		mkdir ( $uploadDir, 0775 );

		$uploadDir = DOC_PATH.'/'.APP_FOLDER.'/upload/blockcontent/' . $this->_courseid . '/' . $this->_blockid . '/';

		if (! is_dir ( $uploadDir ))
		mkdir ( $uploadDir, 0775 );

		$element->setLabel($this->getView()->translate('upload').' '.$this->getView()->translate('file').':')->setDestination(
		$uploadDir);
		// ensure minimum 1, maximum 3 files
		/*$element->addValidator('Count', false,
		array('min' => 1, 'max' => 3));*/
		// limit to 100K
		$element->addValidator('Size', false, 20*1024*1024);
		// only JPEG, PNG, and GIFs
		$element->addValidator('Extension', false, 'pdf,doc,docx,xls,xlsx,gif,jpg,jpeg,png');
		$element->setRequired(false);
		// defines 3 identical file elements
		//$element->setMultiFile(3);
		//$form->addElement($element, 'foo');
		
		$courseid = $this->createElement ( 'hidden', 'courseid' );
		$courseid->setValue($this->_courseid);
		
		$blockid = $this->createElement ( 'hidden', 'blockid' );
		$blockid->setValue($this->_blockid);
		
		$post_id = $this->createElement ( 'hidden', 'post_id' );
		$post_id->setValue($this->_postid);
		
		$this->addElements(
		array($comments,$element,$courseid,$blockid,$post_id));

		//button
		$this->addElement('submit', 'save', array(
		'label'=>$this->getView()->translate('submit'),
		'class'=>'submitfeedback',
		'decorators'=>array('ViewHelper')
		));


	}
}