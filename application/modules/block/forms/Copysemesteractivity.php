<?php
class Block_Form_Copysemesteractivity extends Zend_Form
{

	protected $_intakeid;


	public function setIntakeid($value)
	{
		$this->_intakeid = $value;
	}


	public function init ()
	{

		$this->setMethod('post');
		$this->setAction('/block/index/block');
		$this->setAttrib('id', 'f3');

		$intake = new Admin_Model_Intake ();
		$result =  $intake->fetchAll();
		$copysemid = new Zend_Form_Element_Select('copysemid',array('onchange' => 'refreshblock2();'));

		$copysemid->setLabel($this->getView()->translate('copy').' '.$this->getView()->translate('activity').' '.$this->getView()->translate('from').' '.$this->getView()->translate('semester_id').':')
		->setRequired(true);
		$copysemid->addMultiOption("", $this->getView()->translate('please_select'));
		foreach ($result as $c) {
			if($this->_intakeid != $c['id']){


				$copysemid->addMultiOption($c['id'], $c['intakecode']);
			}
		}

		$copysemid->setDecorators(array(
		'ViewHelper',
		'Description',
		'Errors',
		array(array('data'=>'HtmlTag'), array('tag' => 'td')),
		array('Label', array('tag' => 'td')),
		array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

		));
		
		$intakeid = $this->createElement ( 'hidden', 'intakeid' );
		/*$courseid->setLabel ( 'Course ID:' )->setRequired ( false );
		$courseid->setAttrib ( 'readonly', 'readonly' );*/
		$intakeid->setValue($this->_intakeid);
		
		$intakeid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));

		$this->addElements(
		array($copysemid, $intakeid));

		$this->setDecorators(array(

		'FormElements',

		array(array('data'=>'HtmlTag'),array('tag'=>'table')),

		'Form'



		));


	}
}