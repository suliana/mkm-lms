<?php
class Block_Form_Search extends Zend_Form
{
    public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$keyword = $this->createElement('text', 'keyword');
        $keyword->setLabel($this->getView()->translate('title').' :')->setRequired(false);
        
        $keyword->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('search_activity'))->setIgnore(true);
        
        $register->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $this->addElements(
        array($keyword, $register));
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}