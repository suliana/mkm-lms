<?php
class Block_ContentController extends Zend_Controller_Action {
	
	public function init() 
	{
		/* Initialize action controller here */
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
	}
	
	public function indexAction()
	{
		
		$idtype = $this->_getParam ( 'idtype', 0 );
		$this->view->idtype = $idtype;
		
		if($idtype == 8){
//			$this->_helper->layout->disableLayout();
			$this->_helper->_layout->setLayout('other-layout');
		}
		
		
		
		Zend_Layout::getMvcInstance()->assign('nosidebar',1);
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('block');
						
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$moduleid= $this->_getParam ( 'moduleid', 0 );
		$this->view->moduleid = $moduleid;
		
		
		$course = new Application_Model_Course ();		
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;
		
		$lessonDB = new Block_Model_Lesson();
		$scodata = $lessonDB->get_info($moduleid);
		$this->view->scodata =$scodata;	
		
		$moduleDB =  new Block_Model_Module();
		$module_type = $moduleDB->get_info($moduleid);
	}
	
	
	public function previewAction()
	{
		// disable layouts for this action:
        $this->_helper->layout->disableLayout();
        
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$moduleid= $this->_getParam ( 'moduleid', 0 );
		$this->view->moduleid = $moduleid;
		
		$tocid = $this->_getParam ( 'tocid', 0 );
		$this->view->tocid = $tocid;
		
		$module_type='Scorm';
		
		/* ========================================
		TO GET SCORM MODULE INFORMATION
		========================================*/
		$scoid=$tocid;
		
		$mode = $this->_getParam ( 'mode', 0 );
		$this->view->mode = $mode;
		
		$scormDB = new Block_Model_Scorm();
				
		$attempt  = $scormDB->scorm_get_last_attempt($this->userinfo->username,$moduleid,$scoid);
		$userinfo = $scormDB->scorm_get_sco($scoid);
		
		if ($mode != 'browse') 
		{	
			if ($trackdata = $scormDB->scorm_get_tracks($attempt)) 
			{
				if (($trackdata->{'cmi.completion_status'} == 'completed') || ($trackdata->{'cmi.completion_status'}  == 'passed') || ($trackdata->{'cmi.completion_status'}  == 'failed'))
				{
					$mode = 'review';
				}
				else
				{
					$mode = 'normal';
				}
			} 
			else 
			{
				$mode = 'normal';
			}
		}		
	
		$this->view->attemptstr = $attempt;
		$this->view->modestr    = $mode;
		
		/* ========================================
		END GET SCORM MODULE INFORMATION
		========================================*/
		
		
		
		/* ===================================
		TO TRACK SCO CONTENT (NON-SCORM)
		=================================== */
		
		if ($module_type!='Scorm')
		{ 			
			// 1st : Check dulu dah ada lom		
			$trackDB =  new Block_Model_Track();
			$id = $trackDB->check($courseid,$this->userinfo->username,$moduleid,$tocid);
			
			if($id)
			{
				//update
				//$oTrack->update("$cread_db.cread_$cid","last_read='$date'","id=$id");
				//$rinfo["last_read"]=date("Y-m-d h:m:s");
				$trackDB->updateData('cread_'.$courseid,$rinfo,$id);
			
			}
			else
			{		
				//add
				$scoinfo["userid"]    = $this->userinfo->username;
				$scoinfo["cmid"]      = $moduleid;
				$scoinfo["tocid"]     = $tocid;
				$scoinfo["last_read"] = date("Y-m-d h:m:s");
					
				//$oTrack->add("$cread_db.cread_$cid",$scoinfo);				
				$trackDB->addData('cread_'.$courseid,$scoinfo);
			}
		}
		
		/* ===================================
		END TRACK SCO CONTENT (NON-SCORM)
		=================================== */
		
		
		//require_once($docrootbeta."/modules/c-content/i-scorm/module.class.php");
		//$oModule = new module();
		$lessonDB = new Block_Model_Lesson();
		//$scodata=$oModule->get_info('content',"*","c_lesson","cmID='$cmid' Order by level ASC",'list');
		$scodata = $lessonDB->get_info($moduleid);
		$this->view->scodata = $scodata;
		
		//require_once($docrootbeta."/modules/c-content/lesson.class.php");
		//$oLesson = new Lesson(); 
		//$rs_lesson = $oLesson->get_lesson($tocid);
		$rs_lesson = $lessonDB->get_lesson($tocid);
		$this->view->rs_lesson = $rs_lesson;
		
		//include_once("$docrootbeta/modules/course/cruser.class.php");
		//$oCUser=new CCruser();
				
		//$crsname=$oCUser->getcourseinfo("courseName","courseID='".$cid."'");
		$courseDB =  new Application_Model_Course();
		$course = $courseDB->listcourse($courseid);
		$this->view->crsname = $course["coursename"];
				
	}
	
	public function apiAction()
	{	 
	    // disable layouts for this action:
        $this->_helper->layout->disableLayout();
        
      
		$userid = $this->userinfo->id;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$moduleid= $this->_getParam ( 'moduleid', 0 );
		$this->view->moduleid = $moduleid;
		
		$tocid = $this->_getParam ( 'tocid', 0 );
		$this->view->tocid = $tocid;
		$scoid=$tocid;
		$this->view->scoid = $tocid;
		
		$mode= $this->_getParam ( 'mode', 0 );
		$this->view->mode = $mode;
		
		$attempted= $this->_getParam ( 'attempt2', 0 );
		$this->view->attempted = $attempted;
		
		$student_name = addslashes($data->userinfo->firstname); 	
	    $this->view->student_name = $student_name;
			
        
	}//end api
	
	
	public function storedataAction()
	{
		//$this->_helper->layout->disableLayout();
		 
		if ($this->getRequest()->isXmlHttpRequest()) 
		{
            $this->_helper->layout->disableLayout();
        }

       
       	$getData = $this->getRequest()->getParams();
       
       	$moduleid=$getData["moduleid"];
       	$scoid=$getData["scoid"];

       	$trackDB =  new Block_Model_Track();
		$trackDB->AddModule($moduleid,$this->userinfo->username);
        
            
		foreach($getData as $element => $value)
	   	{
	   		$element = str_replace('__','.',$element);
		    
	   		if (substr($element,0,3) == 'cmi') 
	   		{
		    	$netelement = preg_replace('/\.N(\d+)\./',"\.\$1\.",$element);           
		        list($cmi, $elemen)=split('[/.-]', $netelement);           
		        $mixconsonants = str_replace(".", "_", $netelement);            
		        echo $element.'-'.$value;

		        //insert dalam dbase                  
		        $mdata["element"]  =$netelement;
		        $mdata["value"]    =$value;   
		        $mdata["user_id"]  =$this->userinfo->username;
				$mdata["module_id"]=$moduleid;
				$mdata["sco_id"]   =$scoid; 
						
				$trackDB->addElement(&$mdata,$netelement);
			}
		        
		    if (substr($element,0,15) == 'adl.nav.request') 
		    {
		    	// SCORM 2004 Sequencing Request
		        require_once('sequencing.php');
		            
		        $search  = array('@continue@', '@previous@', '@\{target=(\S+)\}choice@', '@exit@', '@exitAll@', '@abandon@', '@abandonAll@');
		        $replace = array('continue_', 'previous_', '\1', 'exit_', 'exitall_', 'abandon_', 'abandonall');
		        $action  = preg_replace($search, $replace, $value);
							
		        //stop sini
		        if ($action != $value) 
		        {
		        	// Evaluating navigation request
		            $valid = scorm_seq_overall($scoid,$this->userinfo->username,$action);
		            $valid = 'true';
		
                    // Set valid request
                    $search = array('@continue@', '@previous@', '@\{target=(\S+)\}choice@');
                    $replace = array('true', 'true', 'true');
                    $matched = preg_replace($search, $replace, $value);
                    
                    if ($matched == 'true')
                    {
                    	$request = 'adl.nav.request_valid["'.$action.'"] = "'.$valid.'";';
                    }
                }
        	}
		} // foreach
	}
}
?>

