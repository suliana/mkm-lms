<?php
class Block_TestController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('course');
		//$this->_helper->layout->disableLayout();
		
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
	}

	public function indexAction() 
	{
		//$this->_helper->layout->setLayout('course');
		
		$this->view->username = $this->userinfo->username;

		$id = $this->_getParam ( 'id', 0 );

		$this->view->id=$id;

		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid ( $id );

		$this->view->vdata=$vdata;

		
		
		$course = new Application_Model_Course ();
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test');


		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

		$test = new Block_Model_Test();

		$act = $this->_getParam ( 'act', 0 );

		if ($act=="setnoques"){

			if ($this->getRequest ()->isPost ()) {
				$data = $this->getRequest()->getPost();

				$sdata['random_question_no']=$data['pick1'];


				$test->setnoques($sdata,$id);

			}

		}

		if ($act=="sort"){

			if ($this->getRequest ()->isPost ()) {
				$data = $this->getRequest()->getPost();

				$fldid = $this->_getParam ( 'fldid', 0 );

				$mvno = $this->_getParam ( 'mvno', 0 );

				$bilgelung = $this->_getParam ( 'bilgelung', 0 );

				$test->sortassessques($data,$fldid,$mvno,$bilgelung);

			}

		}

		$this->view->rowtest=$test->fetch( $id );

		$select=$test->fetchques( $id );
		$this->view->allrecord = count($select);


		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;




	}

	public function removeAction()
	{
		$test = new Block_Model_Test();

		$qid = $this->_getParam ( 'qid', 0 );
		$courseid = $this->_getParam ( 'courseid', 0 );
		$blockid = $this->_getParam ( 'blockid', 0 );
		$id = $this->_getParam ( 'id', 0 );

		if ($qid > 0) 
		{
			$test->remove ($qid,$id);

			$this->_redirect ( 'block/test/index/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );

		}
	}

	public function beforetestAction() 
	{
		$username=$this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test');


		$id = $this->_getParam ( 'id', 0 );

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;


		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid( $id );

		$this->view->vdata=$vdata;

		$test = new Block_Model_Test();


		$rs=$test->fetch( $id );
		
		if ($rs["random_question_no"]!=null){
			$this->view->noques=$rs["random_question_no"];
		} else {
			$select=$test->fetchques( $id );
			$this->view->noques = count($select);
		}
		
		$bilattempt=$test->countattempttest($rs["tid"],$this->userinfo->id);
		$this->view->bilattempt=$bilattempt;


	}

	public function attempttestAction() 
	{
		//$this->_helper->layout->setLayout('exam');
		$username=$this->userinfo->username;
		$uid=$this->userinfo->id;

		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;
		
		
		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;

		$blockid = $this->_getParam ( 'blockid', 0 );
		$this->view->blockid = $blockid;

		$courseid = $this->_getParam ( 'courseid', 0 );


		$test = new Block_Model_Test();

		$rs=$test->fetch( $id );
		
		
		$block = new Block_Model_Block();
		$vdata=$block->fetchcontentid( $id );
		
		//print_r($vdata);
		//$this->view->testname=$rs["title"];
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test')." > ".$vdata[0]["title"];
		
		$this->view->test_title = $vdata[0]["title"];

		$noques="";

		if ($rs["random_question_no"]!=null){
			$noques=$rs["random_question_no"];
			$this->view->totalq = $noques;
		} else {
			$select=$test->fetchques( $id );
			$noques = count($select);
			$this->view->totalq = $noques;
		}



		if ($rs["allow_random"]=="1"){
			$select=$test->fetchquesfortest( $id, $noques,$this->getcookie('randvalue') );
		} else {
			$select=$test->fetchquesfortest( $id, $noques );
		}

		if ( 
				$this->getcookie('tid') != '' 
			&& 	$this->getcookie('qids') != ''
			&&  $this->getcookie('resultid') != ''
			&& 	$this->getcookie('randvalue') != ''
			&& 	$this->getcookie('access_token') != '' 
			)
		{

			$rsresult=$test->fetchresult($this->getcookie('resultid'));

			$this->view->rsresult= $rsresult;

			$balancetime=time()-$rsresult["iniTime"];

			//echo $balancetime ;

			$this->view->timer= ($rs["test_time"]*60) - $balancetime;





		} else {

			//if

			if( $this->getcookie('randvalue') != '' && $this->getcookie('access_token') != '' )
			{
				//echo "sinilah";
				$this->view->timer=$rs["test_time"]*60;

				$qid=array();
				foreach ($select as $quesid) {

					$qid[]=$quesid["qid"];

				}

				$qids=implode(",",$qid);

				$resultid=$test->insert_result_row($noques,$uid,$rs["tid"],$qids);

				$this->setcookie('resultid',$resultid, time()+86400 );
				$this->setcookie('qids', $qids, time()+86400);
				$this->setcookie('tid', $rs['tid'], time()+86400);

				//echo "sdasdas".$_COOKIE['resultid'];
				$rsresult=$test->fetchresult($resultid);

				$this->view->rsresult= $rsresult;
			}
			else 
			{
				$this->_redirect ( 'block/test/beforetest/courseid/'.$courseid.'/blockid/'.$blockid.'/id/'.$id );
			}

			//$cookie = array('name'=>'qno','value'=>'0','expire'=>'86500');
			//$this->input->set_cookie($cookie);
		}


		//$this->view->allrecord = count($select);


		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 1);
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;

	}
	
	public function setcookie($name, $val, $time)
	{
		$prefix = 'lms_';
		setcookie($prefix.$name, $val, $time, $this->view->baseUrl().'/block/test');
	}
	
	public function getcookie($what='')
	{
		return $_COOKIE['lms_'.$what];
	}

	public function submitanswerAction() {
		$this->_helper->layout->disableLayout();

		$courseid = $this->_getParam ( 'courseid', 0 );

		$qid = $this->_getParam ( 'qid', 0 );

		$qno = $this->_getParam ( 'qno', 0 );

		$answer = $this->_getParam ( 'answer', "" );

		$time1 = $this->_getParam ( 'time1', 0 );

		//echo "Time 1".$time1."<br>";
		$test = new Block_Model_Test();
		$test->submit_answer($this->getcookie('resultid'),$qid,$courseid,$qno,$answer,$time1);

		$rsresult=$test->fetchresult($this->getcookie('resultid'));
		$time_taken=explode(',',$rsresult['time_taken']);
		$newtime1= (array_sum($time_taken)+$rsresult['iniTime']);
		echo $newtime1;


	}

	public function submittestAction() {
		$this->_helper->layout->disableLayout();

		//echo "sini";
		//print_r($_COOKIE);
		//exit();
		$courseid = $this->_getParam ( 'courseid', 0 );

		$qid = $this->_getParam ( 'qid', 0 );

		$qno = $this->_getParam ( 'qno', 0 );

		$answer = $this->_getParam ( 'answer', "" );

		$time1 = $this->_getParam ( 'time1', 0 );

		$blockcontentid = $this->_getParam ( 'id', 0 );


		$test = new Block_Model_Test();
		$test->submit_test($this->getcookie('resultid'),$qid,$courseid,$qno,$answer,$time1,$blockcontentid);

		$this->_redirect ( 'block/test/successsubmit/courseid/'.$courseid.'/resultid/'.$this->getcookie('resultid') );


	}

	public function successsubmitAction() {


		//if (isset($_COOKIE["resultid"]) || isset($_COOKIE["qids"]) || isset($_COOKIE["tid"]) || isset($_COOKIE["access_token"]) || isset($_COOKIE["randvalue"])){
		$this->setcookie("resultid", "", time()+1);
		$this->setcookie("qids", "", time()+1);
		$this->setcookie("tid", "", time()+1);
		$this->setcookie("access_token", "", time()+1);
		$this->setcookie("randvalue", "", time()+1);
		//}


		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;


		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test');

		$resultid = $this->_getParam ( 'resultid', 0 );
		$this->view->resultid = $resultid;

	}

	public function viewresultAction() {

	
		
		$from = $this->_getParam ( 'from', "" );
		$this->view->from = $from;
		
		$blockcontentid = $this->_getParam ( 'blockcontentid', "" );
		$this->view->blockcontentid = $blockcontentid;
		
		$semid = $this->_getParam ( 'semid', "" );
		$this->view->semid = $semid;
		
		
		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test')." > ".$this->view->translate('result');

		
		$resultid = $this->_getParam ( 'view', 0 );

		$test = new Block_Model_Test();
		
		$result = $test->fetchresult($resultid);
		
		$gradebook = new Gradebook_Model_Gradebook();
		
		
		$rstest = $gradebook->fetchrowtest($result['tid']);

		$user = new Users_Model_Users();

		$rsuser=$user->fetch($result['uid']);

		if (($rsuser['username'] == $this->userinfo->username) || $data->privilage=="staff" || $data->privilage=="system support exam" || $data->privilage=="system support" || $data->privilage=="system admin"){

			$this->view->user =$rsuser;
			$this->view->result = $result;
			$this->view->rstest = $rstest;
		}
		else {
			$this->view->result = 0;
		}


	}
	
	public function viewanswersAction() {

		

		$from = $this->_getParam ( 'from', "" );
		$this->view->from = $from;
		
		$blockcontentid = $this->_getParam ( 'blockcontentid', "" );
		$this->view->blockcontentid = $blockcontentid;
		
		$semid = $this->_getParam ( 'semid', "" );
		$this->view->semid = $semid;
		
		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->listcourse ($courseid);

		$this->view->course = $rscourse;
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('test')." > ".$this->view->translate('result');

		
		$resultid = $this->_getParam ( 'view', 0 );

		$test = new Block_Model_Test();
		
		$result = $test->fetchresult($resultid);
		
		$gradebook = new Gradebook_Model_Gradebook();
		
		
		$rstest = $gradebook->fetchrowtest($result['tid']);

		$user = new Users_Model_Users();

		$rsuser=$user->fetch($result['uid']);
		
		

		if ($rsuser['username'] == $this->userinfo->username || $data->privilage=="staff" || $data->privilage=="system support exam" || $data->privilage=="system support" || $data->privilage=="system admin"){

			$this->view->user =$rsuser;
			$this->view->result = $result;
			$this->view->rstest = $rstest;
		}
		else {
			$this->view->result = 0;
		}


	}
	
	public function deleteresultAction() 
	{
		
		$test = new Block_Model_Test ();
		$id = $this->_getParam ( 'rowid', 0 );

		$courseid = $this->_getParam ( 'courseid', '' );

		//echo $id;
		if ($id > 0) {
			$test->deleteresult ( $id );
			$this->_redirect ( 'gradebook/index/index/courseid/'.$courseid );
		}
	}



}
?>

