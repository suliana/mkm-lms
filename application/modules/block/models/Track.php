<?php
class Block_Model_Track extends Zend_Db_Table
{
	
	public function check($courseid,$username,$cmid,$tocid)
	{
		
		  //$id = $oTrack->check("id","$cread_db.cread_$cid","cmid='$cmid' AND tocid='$tocid' AND userid='$ses_username'");
		  $sql = $this->_db->select()
    	  			       ->from("cread_".$courseid)     	  			       	  			    
    	  			       ->where("cmid='$cmid'")
    	  			       ->where("tocid='$tocid'")
    	  			       ->where("userid='$username'");    	  			        	  			      
    	
	      $result = $this->_db->fetchRow($sql);   
		
		  return $result["id"];
	}
	
	public function addData ($table,$data)
    {
        $this->_db->insert($table, $data);
        return $this->_db->lastInsertId();
    }
	
	public function updateData ($table,$data, $id)
    {
        $this->_db->update($table,$data, 'id = ' . (int) $id);
    }
    
	public function updateInfo ($table,$data,$where)
    {
        $this->_db->update($table,$data,$where);
    }
    
    
    
	public function AddModule($moduleid,$username)
	{
		//global $mydb_master,$courseid,$userid,$moduleid,$scoid,$info,$content_db;	
		
		$progress=0;
		
		//to get progress calculate total SCO from c_lesson
	 	$total_lesson=$this->get_total_sco($moduleid);
	 	
	
	 	//get total lesson open
	 	$total_open_lesson=$this->get_sco_info($moduleid,$username);	
		
		$progress  = round(($total_open_lesson/$total_lesson)*100);
			
		//if($check_module=$this->get_module("*","c_module_view","module_id='$moduleid' AND user_id='$userid'","count")){
		if($check_module=$this->get_module($moduleid,$username)){
			//update
			$info["progress"] = $progress;	    		
			$this->updateInfo("c_module_view",$info,"module_id= '$moduleid' AND user_id='$username'");
	
		}else{
	
			$theinfo["user_id"]=$username;
			$theinfo["module_id"]=$moduleid;
			$theinfo["progress"]=$progress;
			
			$this->addData("c_module_view", $theinfo);			
			
		}		
	}
	
	public function get_total_sco($moduleid){
		//global $mydb,$content_db,$moduleid;
		
		//$sql = "SELECT count(*) as bil FROM  $content_db.c_lesson
		//		WHERE cmID = '$moduleid'";		
	   
		//$rs=$mydb->Execute($sql);	
		//return $rs->fields['bil'];
		
		$sql = $this->_db->select()
					     ->from("c_lesson")     	  			       	  			    
    	  			     ->where("cmID='$moduleid'");    	  			     	  			        	  			      
			    	
		$count = $this->_db->query($sql)->rowCount(); 
		return $count;
	}
	
	
	public function get_sco_info($moduleid,$username){
		//global $mydb,$courseid,$moduleid,$ses_username,$content_db;
		
		/*$sql = "SELECT * FROM `$content_db`.c_sco_track 
		        WHERE module_id = '$moduleid' and user_id ='$ses_username'
				GROUP BY sco_id ORDER BY sco_id";
		
		$rs=$mydb->Execute($sql);
		$jumlah =$rs->recordcount();	
		return $jumlah;	*/		
		
		$sql = $this->_db->select()
					     ->from("c_sco_track")     	  			       	  			    
    	  			     ->where("module_id='$moduleid'")    	  			  
    	  			     ->where("user_id='$username'")
    	  			     ->group('sco_id'); 	     	  			        	  			      
			    	
		$jumlah = $this->_db->query($sql)->rowCount(); 
		return $jumlah;				
	}
	
	public function get_module($moduleid,$username){
		//global $mydb,$courseid,$content_db;
		
		/*$sql = "SELECT $field FROM  `$content_db`.$table WHERE $cond";
		$rs=$mydb->Execute($sql);		
		$module["module_id"]=$rs->fields("module_id");
		$module["user_id"]=$rs->fields("user_id");	
		$module["progress"]=$rs->fields("progress");
		if($type=="list"){	
			return $module;
		}elseif($type=="count"){
			return $rs->recordcount();
		}else{
			return $rs->fields[$field];
		}	*/
		//$this->get_module("*","c_module_view","module_id='$moduleid' AND user_id='$userid'","count")
		$sql = $this->_db->select()
					     ->from("c_module_view")     	  			       	  			    
    	  			     ->where("module_id='$moduleid'")    	  			  
    	  			     ->where("user_id='$username'");    	  			     	     	  			        	  			      
			    	
		$jumlah = $this->_db->query($sql)->rowCount(); 
		return $jumlah;		
	}
	
	
	public function addElement(&$data,$netelement)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		//global $mydb,$courseid,$userid,$moduleid,$scoid;
		
		//$bil = $this->get_track("*","c_sco_track","user_id='$data[username]' AND module_id='$moduleid' AND sco_id = '$scoid' AND element='$netelement'" ,"count");
				
		$bil= $this->get_track_count($data,$netelement);
		
	    if($bil>0){
	    	//update("c_sco_track",$data,"sco_id = '$scoid' AND module_id= '$moduleid' AND user_id='$userid' AND element='$netelement'");
	    				 
			$this->updateInfo("c_sco_track",$data,"sco_id = '$data[scoid]' AND module_id= '$data[moduleid]' AND user_id='$data[username]' AND element='$netelement'");
					
	    }else{
	    	//add("c_sco_track",$data); 
	    	$this->addData("c_sco_track",$data);
	    }
		  	        
	}
	
	public function get_track_count($data,$netelement){
		
		//global $mydb,$courseid,$content_db;
		
		//$sql = "SELECT $field FROM `$content_db`.$table WHERE $cond";			
				
		//$mydb->debug=1;
		//$rs=$mydb->Execute($sql);
		
		 $sql = $this->_db->select()
					     ->from("c_sco_track")     	  			       	  			    
    	  			     ->where("module_id='$data[moduleid]'")
    	  			     ->where("sco_id='$data[scoid]'")
    	  			     ->where("user_id='$data[username]'")
    	  			     ->where("element='$netelement'");    	  			        	  			      
			    	
	 $count = $this->_db->query($sql)->rowCount(); 
		
		return $count;
		
	}
	
	function get_scorm_status($cmid,$scoid){
					
		/*$sql   = "SELECT value FROM `$content_db`.c_sco_track 
		          WHERE user_id = '".$ses_username."' AND module_id=".$cmid."
		          AND sco_id = ".$scoid." AND (element='cmi.completion_status' OR element='cmi.core.lesson_status')";	
		$rs    = $this->conn->Execute($sql);*/
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->view->_redirect('index/index');
		}
		
		$sql = $this->_db->select()
    	  			     ->from("c_sco_track",array('value'=>'value')) 
    	  			     ->where("user_id='$data->username'")
    	  			     ->where("module_id='$cmid'")
    	  			     ->where("sco_id='$scoid'")
    	  			     ->where("element='cmi.completion_status'")
    	  			     ->orwhere ("element='cmi.core.lesson_status'");  	
    	  			       			    
    	 $rs = $this->_db->fetchRow($sql);   	
		
		return $rs["value"];
		
	}
	
	public function get_nonscorm_status($cmid,$tocid,$cid)
	{		
	   
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->view->_redirect('index/index');
		}
		
		$sql = $this->_db->select()
    	  			     ->from("cread_$cid",array('id'=>'id')) 
    	  			     ->where("user_id='$data->username'")
    	  			     ->where("module_id='$cmid'")
    	  			     ->where("toc_id='$scoid'");    	  			       	
    	  			       			    
    	 $rs = $this->_db->fetchRow($sql);   
		
		if($rs["id"]){ 
			$status='completed';			
		}

		return $status;
	}
	
	
    
	
}
?>