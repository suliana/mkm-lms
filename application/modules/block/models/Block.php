<?php
class Block_Model_Block extends Zend_Db_Table
{
	protected $_name = "block";
	protected $_block_content = "block_content";
	protected $_block_content_type = "block_content_type";
	protected $_block_content_comments = "block_content_comments";
	protected $_block_content_ip = "block_content_ip";
	protected $_block_content_likes = "block_content_likes";
	
	public function fetchAll ()
	{
		$sql = $this->_db->select()->from($this->_name);
		$stmt = $this->_db->query($sql);
		return $stmt;
	}
	
	public function upload ($data)
	{
		$this->_db->insert($this->_name, $data);
	}
	
	public function fetch ($id = "",$courseid="",$intakeid="",$privilage="",$username="")
	{
		$sql = $this->_db->select()->from($this->_name);
		
		if ($id != "") 
		{
			$sql->where('id = ?', $id);
		}
		
		if ($courseid != "") 
		{
			//$sql->where('courseid = ?', $courseid);
			$sql->where('courseid = ?', $courseid);	
		}

		if ($privilage=="EOS"){
			$eos = new Course_Model_Grader();
			$rs=$eos->fetchsemua($username,$courseid);
			$i=0;
			if ($rs){
				foreach ($rs as $row) {
					$intake[$i] = $row['intakeid'] ;

					$i++;

				}

				$sql->where(
				' intakeid IN  (?) ', $intake );
			}


		}
		if ($intakeid != "") {
			//echo $keyword;
			$sql->where(
			' intakeid = ? ', $intakeid );
		}

		$sql->order('id ASC');

		$result = $this->_db->fetchAll($sql);
//		echo $sql;
		//print_r($result);
		//exit();
		return $result;
	}
	public function modify ($data, $id)
	{
		$this->_db->update($this->_name, $data, 'id = ' . (int) $id);
	}
	public function delete ($id)
	{
		$this->_db->delete($this->_name, 'id = ' . (int) $id);


		$result=$this->fetchcontent($id);

		foreach ($result as $row){
			$this->deletecontent($row[id]);
		}


	}

	public function findForSelect()
	{
		$select = $this->_db->select()->from($this->_block_content_type)->order('id ASC');

		$result = $this->_db->fetchAll($select);
		//print_r($result);
		//exit();
		return $result;


	}

	public function uploadcontent ($data)
	{
		$this->_db->insert($this->_block_content, $data);
	}

	public function fetchcontent ($blockid = "")
	{
		$sql = $this->_db->select()->from($this->_block_content);
		if ($blockid != "") {
			$sql->where('blockid = ?', $blockid);
		}
		//echo $sql;
		$sql->order('datecreated DESC');
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		//exit();
		return $result;
	}

	public function fetchcontentid ($id = "")
	{
		$sql = $this->_db->select()->from($this->_block_content);
		if ($id != "") {
			$sql->where('id = ?', $id);
		}
		$result = $this->_db->fetchAll($sql);
		//print_r($result);
		//exit();
		return $result;
	}

	public function deletecontent ($id)
	{
		$this->_db->delete($this->_block_content, 'id = ' . (int) $id);
		$this->_db->delete($this->_block_content_comments, 'post_id = ' . (int) $id);
		$this->_db->delete($this->_block_content_ip, 'post_id = ' . (int) $id);
		$this->_db->delete($this->_block_content_likes, 'post_id = ' . (int) $id);
	}

	public function modifycontent ($data, $id)
	{
		$this->_db->update($this->_block_content, $data, 'id = ' . (int) $id);
	}

	public function getlastinsertid ($tablename)
	{
		$id=$this->_db->lastInsertId($tablename);
		return $id;
	}

	public function fetchRow ($id = "")
	{
		$sql = $this->_db->select()->from($this->_name);
		if ($id != "") {
			$sql->where('id = ?', $id);
		}

		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		//exit();
		return $result;
	}
	
function sortcountnocontent($data,$fldid,$mvno,$bilgelung) {

		
		$bezantara=$mvno - $data['chgno'.$mvno];
		

		//#kes 1 kecik --> besar
		if ($bezantara < 0) {
			for ($g=1; $g<=$bilgelung; $g++) {
				if ($g<>$mvno & $g<=$data['chgno'.$mvno]){
					$bezanya=$mvno-$data['chgno'.$g];
					if($bezanya<0){
						$ubah=	$data['chgno'.$g]-1;

						$sdata['countNo']=$ubah;
						
						//echo "Ubah :".$sdata['countNo']."<br>";
						//echo "id :".$data['fldid'.$g]."<br>";

						$this->_db->update($this->_block_content, $sdata, 'id = ' . (int) $data['fldid'.$g]);


					}
				}
			}

			$sdata['countNo']=$data['chgno'.$mvno];

			$this->_db->update($this->_block_content, $sdata, 'id = ' . (int) $data['fldid'.$mvno]);

		}

		//#kes 1 besaq --> kecik
		if ($bezantara > 0) {
			for ($g=1; $g<=$bilgelung; $g++) {
				if ($g<>$mvno & $g>=$data['chgno'.$mvno]){
					$bezanya=$mvno-$data['chgno'.$g];
					if($bezanya>0){
						$ubah=	$data['chgno'.$g]+1;

						$sdata['countNo']=$ubah;

						$this->_db->update($this->_block_content, $sdata, 'id = ' . (int) $data['fldid'.$g]);



					}
				}
			}

			$sdata['countNo']=$data['chgno'.$mvno];

			$this->_db->update($this->_block_content, $sdata, 'id = ' . (int) $data['fldid'.$mvno]);


		}

	}
	
	function updatecountnocontent($bil,$id){
		
		$sdata['countNo']=$bil;
		
		$this->_db->update($this->_block_content, $sdata, 'id = ' . (int) $id);

	}
	
	
	function sortcountnoblock($data,$fldid,$mvno,$bilgelung) {

		
		$bezantara=$mvno - $data['chgnoblk'.$mvno];
		

		//#kes 1 kecik --> besar
		if ($bezantara < 0) {
			for ($g=1; $g<=$bilgelung; $g++) {
				if ($g<>$mvno & $g<=$data['chgnoblk'.$mvno]){
					$bezanya=$mvno-$data['chgnoblk'.$g];
					if($bezanya<0){
						$ubah=	$data['chgnoblk'.$g]-1;

						$sdata['countNo']=$ubah;
						
						echo "Ubah :".$sdata['countNo']."<br>";
						echo "id :".$data['fldidblk'.$g]."<br>";

						$this->_db->update($this->_name, $sdata, 'id = ' . (int) $data['fldidblk'.$g]);


					}
				}
			}

			$sdata['countNo']=$data['chgnoblk'.$mvno];

			$this->_db->update($this->_name, $sdata, 'id = ' . (int) $data['fldidblk'.$mvno]);

		}

		//#kes 1 besaq --> kecik
		if ($bezantara > 0) {
			for ($g=1; $g<=$bilgelung; $g++) {
				if ($g<>$mvno & $g>=$data['chgnoblk'.$mvno]){
					$bezanya=$mvno-$data['chgnoblk'.$g];
					if($bezanya>0){
						$ubah=	$data['chgnoblk'.$g]+1;

						$sdata['countNo']=$ubah;

						$this->_db->update($this->_name, $sdata, 'id = ' . (int) $data['fldidblk'.$g]);



					}
				}
			}

			$sdata['countNo']=$data['chgnoblk'.$mvno];

			$this->_db->update($this->_name, $sdata, 'id = ' . (int) $data['fldidblk'.$mvno]);


		}
		
		

	}
	
	function updatecountnoblock($bil,$id){
		
		$sdata['countNo']=$bil;
		
		$this->_db->update($this->_name, $sdata, 'id = ' . (int) $id);

	}
	
	//susun balik sort sekiranya delete
	function removeandsortcountnoblock($id, $courseid){
		
		$rs =$this->fetchRow($id);
		
		$sql = $this->_db->select()->from($this->_name);
		$sql->where('courseid = ?', $courseid);
		$sql->where('countNo > ?', $rs["countNo"]);
		
		//echo $sql;
		$result = $this->_db->fetchAll($sql);	
		return $result;	
			
	}
	
	//susun balik sort sekiranya delete
	function removeandsortcountnoblockcontent($id, $blockid){
		
		$rs =$this->fetchcontentid($id);
		
		$sql = $this->_db->select()->from($this->_block_content);
		$sql->where('blockid = ?', $blockid);
		$sql->where('countNo > ?', $rs[0]["countNo"]);
		
		//echo $sql;
		$result = $this->_db->fetchAll($sql);	
		return $result;	
			
	}

}
?>
