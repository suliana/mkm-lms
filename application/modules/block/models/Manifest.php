<?php 

class Block_Model_Manifest extends Zend_Db_Table
{
	
    protected $_levelutama;
    
    function dirwalk($directory,$filename)
	{   
		global $newdirectory;

		$folder=explode(".",$filename);
		$newdirectory = $directory."/".$folder[0];
		$files = scandir($newdirectory);	
      
		return array($files,$newdirectory);
	}
	
	
    
	
	function readmanifest($manifestfile,$path,$cid,$import_type,$strurl)
	{

		global $lcms_db,$ses_username;

		$xmltext = file_get_contents($manifestfile);

		$pattern = '/&(?!\w{2,6};)/';
		$replacement = '&amp;';
		$xmltext = preg_replace($pattern, $replacement, $xmltext);

		$objXML = new xml2Array();
		$manifests = $objXML->parse($xmltext);

		$scoes = new stdClass();
		$scoes->version = '';
		$scoes = $this->scorm_get_manifest($manifests,$scoes);
	
       //  print_r($scoes);
       
		if (count($scoes->elements) > 0) {
			$x=1;
			$previd=0;
			foreach ($scoes->elements as $manifest => $organizations) {
				foreach ($organizations as $organization => $items) {
					foreach ($items as $identifier => $item) {

						if ($x == 1) {
							$moduleid=$this->CreateModule($item->title,$scoes->version,$import_type);

						}

						// This new db mngt will support all SCORM future extensions
						$newitem = new stdClass();
						$newitem->scorm = $moduleid;
						$newitem->manifest = $manifest;
						$newitem->organization = $organization;
						$standarddatas = array('parent', 'identifier', 'launch', 'scormtype', 'title');
						foreach ($standarddatas as $standarddata) {
							//$newitem->$standarddata = addslashes($item->$standarddata);
							$newitem->$standarddata = $item->$standarddata;
						}



						$newitem=$this->objToArray($newitem);

						if ($newitem["parent"]!="/")
						$id=$this->CreateSCO($newitem,$moduleid);


						$newitem["idref"]=$item->idref;
						$newitem["parameters"]=$item->parameters;


						if ($newitem["parent"]!="/"){
							
							$newitem["title"];
							$curid=$this->checklessonid($newitem,$id,$moduleid,$strurl);

							if (!empty($newitem['launch'])) {

								$this->updatePrevToc($curid,$previd);

								//update next toc for the PREVIOUS toc as current TOC
								$this->updateNextToc($curid,$previd);

								//$this->add_resources_to_array($newitem,$id,$path);
								$previd=$curid;
							}
						}




						if ($optionaldatas = $this->scorm_optionals_data($item,$standarddatas)) {

							$data = array();
							$data["scoid"] = $id;

							foreach ($optionaldatas as $optionaldata) {


								if (isset($item->$optionaldata)) {
									$data["name"] =  $optionaldata;
									$data["value"] = addslashes($item->$optionaldata);
								}
							}
						}

						if (isset($item->sequencingrules)) {
							foreach($item->sequencingrules as $sequencingrule) {
								$rule = new stdClass();
								$rule->scoid = $id;
								$rule->ruletype = $sequencingrule->type;
								$rule->conditioncombination = $sequencingrule->conditioncombination;
								$rule->action = $sequencingrule->action;


								$rule=$this->objToArray($rule);


								//$this->master_conn->AutoExecute("mdl_scorm_seq_ruleconds",$rule,"INSERT");
								//$ruleid=$this->master_conn->Insert_ID();

								if (isset($sequencingrule->ruleconditions)) {
									foreach($sequencingrule->ruleconditions as $rulecondition) {
										$rulecond = new stdClass();
										$rulecond->scoid = $id;
										$rulecond->ruleconditionsid = $ruleid;
										$rulecond->referencedobjective = $rulecondition->referencedobjective;
										$rulecond->measurethreshold = $rulecondition->measurethreshold;
										$rulecond->cond = $rulecondition->cond;


										$rulecond=$this->objToArray($rulecond);


										//$this->master_conn->AutoExecute("mdl_scorm_seq_rulecond",$rulecond,"INSERT");
										//$rulecondid=$this->master_conn->Insert_ID();

									}
								}
							}
						}

						if (isset($item->rolluprules)) {
							foreach($item->rolluprules as $rolluprule) {
								$rollup = new stdClass();
								$rollup->scoid =  $id;
								$rollup->childactivityset = $rolluprule->childactivityset;
								$rollup->minimumcount = $rolluprule->minimumcount;
								$rollup->minimumpercent = $rolluprule->minimumpercent;
								$rollup->rollupruleaction = $rolluprule->rollupruleaction;
								$rollup->conditioncombination = $rolluprule->conditioncombination;

								//$this->print_object($rollup);
								$rollup=$this->objToArray($rollup);
								//print_r($rollup);

								//$this->master_conn->AutoExecute("mdl_scorm_seq_rolluprule",$rollup,"INSERT");
								//$rollupruleid=$this->master_conn->Insert_ID();

								if (isset($rollup->conditions)) {
									foreach($rollup->conditions as $condition){
										$cond = new stdClass();
										$cond->scoid = $rollup->scoid;
										$cond->rollupruleid = $rollupruleid;
										$cond->operator = $condition->operator;
										$cond->cond = $condition->cond;

										//$this->print_object($cond);
										$cond=$this->objToArray($cond);
										//print_r($cond);

									//	$this->master_conn->AutoExecute("mdl_scorm_seq_rolluprulecond",$cond,"INSERT");
									//	$conditionid=$this->master_conn->Insert_ID();

									}
								}
							}
						}

						if (isset($item->objectives)) {
							foreach($item->objectives as $objective) {
								$obj = new stdClass();
								$obj->scoid = $id;
								$obj->primaryobj = $objective->primaryobj;
								$obj->satisfiedbumeasure = $objective->satisfiedbymeasure;
								$obj->objectiveid = $objective->objectiveid;
								$obj->minnormalizedmeasure = $objective->minnormalizedmeasure;

								//$this->print_object($obj);
								$obj=$this->objToArray($obj);
								//print_r($obj);

								//$this->master_conn->AutoExecute("mdl_scorm_seq_objective",$obj,"INSERT");
								//$objectiveid=$this->master_conn->Insert_ID();

								if (isset($objective->mapinfos)) {
									//print_object($objective->mapinfos);
									foreach($objective->mapinfos as $objmapinfo) {
										$mapinfo = new stdClass();
										$mapinfo->scoid = $id;
										$mapinfo->objectiveid = $objectiveid;
										$mapinfo->targetobjectiveid = $objmapinfo->targetobjectiveid;
										$mapinfo->readsatisfiedstatus = $objmapinfo->readsatisfiedstatus;
										$mapinfo->writesatisfiedstatus = $objmapinfo->writesatisfiedstatus;
										$mapinfo->readnormalizedmeasure = $objmapinfo->readnormalizedmeasure;
										$mapinfo->writenormalizedmeasure = $objmapinfo->writenormalizedmeasure;

										$this->print_object($mapinfo);
										$mapinfo=$this->objToArray($mapinfo);
										//print_r($mapinfo);

										//$this->master_conn->AutoExecute("mdl_scorm_seq_mapinfo",$mapinfo,"INSERT");
										//$mapinfoid=$this->master_conn->Insert_ID();

									}
								}
							}
						}
						$x++;

					}

				}

			}


		}

		//to auto assign to course for outside lcms import scorm
		//$this->assignlp($cid,$moduleid,$import_type);
		//exit;
       	return $moduleid;

	}


	/**
	 * To read manifest file gathered to an array
	 *
	 * Database table : no database used
	 */

	function scorm_get_manifest($blocks,$scoes) {
		static $parents = array();
		static $resources;

		static $manifest;
		static $organization;

		if (count($blocks) > 0) {
			foreach ($blocks as $block) {
				
				switch ($block['name']) {
					case 'METADATA':
						if (isset($block['children'])) {
							foreach ($block['children'] as $metadata) {
								if ($metadata['name'] == 'SCHEMAVERSION') {
									if (empty($scoes->version)) {
										if (isset($metadata['tagData']) && (preg_match("/^(1\.2)$|^(CAM )?(1\.3)$/",$metadata['tagData'],$matches))) {
											$scoes->version = 'SCORM_'.$matches[count($matches)-1];
										} else {
											if (isset($metadata['tagData']) && (preg_match("/^2004 3rd Edition$/",$metadata['tagData'],$matches))) {
												$scoes->version = 'SCORM_1.3';
											} else {
												$scoes->version = 'SCORM_1.2';
											}
										}
									}
								}
							}
						}
						break;
					case 'MANIFEST':
						$manifest = addslashes($block['attrs']['IDENTIFIER']);
						$organization = '';
						$resources = array();
						$resources = $this->scorm_get_resources($block['children']);
						$scoes = $this->scorm_get_manifest($block['children'],$scoes);
						if (count($scoes->elements) <= 0) {
							foreach ($resources as $item => $resource) {
								if (!empty($resource['HREF'])) {
									$sco = new stdClass();
									$sco->identifier = $item;
									$sco->title = $item;
									$sco->parent = '/';
									$sco->launch = addslashes($resource['HREF']);
									$sco->scormtype = addslashes($resource['ADLCP:SCORMTYPE']);
									$scoes->elements[$manifest][$organization][$item] = $sco;
								}
							}
						}
						break;
					case 'ORGANIZATIONS':
						if (!isset($scoes->defaultorg)) {
							$scoes->defaultorg = addslashes($block['attrs']['DEFAULT']);
						}
						$scoes = $this->scorm_get_manifest($block['children'],$scoes);
						break;
					case 'ORGANIZATION':
						$identifier = addslashes($block['attrs']['IDENTIFIER']);
						$organization = '';
						$scoes->elements[$manifest][$organization][$identifier]->identifier = $identifier;
						$scoes->elements[$manifest][$organization][$identifier]->parent = '/';
						$scoes->elements[$manifest][$organization][$identifier]->launch = '';
						$scoes->elements[$manifest][$organization][$identifier]->scormtype = '';

						$parents = array();
						$parent = new stdClass();
						$parent->identifier = $identifier;
						$parent->organization = $organization;
						array_push($parents, $parent);
						$organization = $identifier;

						$scoes = $this->scorm_get_manifest($block['children'],$scoes);

						array_pop($parents);
						break;
					case 'ITEM':
						$parent = array_pop($parents);
						array_push($parents, $parent);

						$identifier = addslashes($block['attrs']['IDENTIFIER']);
						$scoes->elements[$manifest][$organization][$identifier]->identifier = $identifier;
						$scoes->elements[$manifest][$organization][$identifier]->parent = $parent->identifier;
						if (!isset($block['attrs']['ISVISIBLE'])) {
							$block['attrs']['ISVISIBLE'] = 'true';
						}
						$scoes->elements[$manifest][$organization][$identifier]->isvisible = addslashes($block['attrs']['ISVISIBLE']);
						if (!isset($block['attrs']['PARAMETERS'])) {
							$block['attrs']['PARAMETERS'] = '';
						}
						$scoes->elements[$manifest][$organization][$identifier]->parameters = addslashes($block['attrs']['PARAMETERS']);
						if (!isset($block['attrs']['IDENTIFIERREF'])) {
							$scoes->elements[$manifest][$organization][$identifier]->launch = '';
							$scoes->elements[$manifest][$organization][$identifier]->scormtype = 'asset';
						} else {
							$idref = addslashes($block['attrs']['IDENTIFIERREF']);
							$base = '';
							if (isset($resources[$idref]['XML:BASE'])) {
								$base = $resources[$idref]['XML:BASE'];
							}
							$scoes->elements[$manifest][$organization][$identifier]->idref = addslashes($idref);
							$scoes->elements[$manifest][$organization][$identifier]->launch = addslashes($base.$resources[$idref]['HREF']);
							if (empty($resources[$idref]['ADLCP:SCORMTYPE'])) {
								$resources[$idref]['ADLCP:SCORMTYPE'] = 'asset';
							}
							$scoes->elements[$manifest][$organization][$identifier]->scormtype = addslashes($resources[$idref]['ADLCP:SCORMTYPE']);
						}

						$parent = new stdClass();
						$parent->identifier = $identifier;
						$parent->organization = $organization;
						array_push($parents, $parent);

						$scoes = $this->scorm_get_manifest($block['children'],$scoes);

						array_pop($parents);
						break;
					case 'TITLE':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						//$scoes->elements[$manifest][$parent->organization][$parent->identifier]->title = addslashes($block['tagData']);
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->title = $block['tagData'];
						break;
					case 'ADLCP:PREREQUISITES':
						if ($block['attrs']['TYPE'] == 'aicc_script') {
							$parent = array_pop($parents);
							array_push($parents, $parent);
							if (!isset($block['tagData'])) {
								$block['tagData'] = '';
							}
							$scoes->elements[$manifest][$parent->organization][$parent->identifier]->prerequisites = addslashes($block['tagData']);
						}
						break;
					case 'ADLCP:MAXTIMEALLOWED':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->maxtimeallowed = addslashes($block['tagData']);
						break;
					case 'ADLCP:TIMELIMITACTION':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->timelimitaction = addslashes($block['tagData']);
						break;
					case 'ADLCP:DATAFROMLMS':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->datafromlms = addslashes($block['tagData']);
						break;
					case 'ADLCP:MASTERYSCORE':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->masteryscore = addslashes($block['tagData']);
						break;
					case 'ADLCP:COMPLETIONTHRESHOLD':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!isset($block['tagData'])) {
							$block['tagData'] = '';
						}
						$scoes->elements[$manifest][$parent->organization][$parent->identifier]->threshold = addslashes($block['tagData']);
						break;
					case 'ADLNAV:PRESENTATION':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!empty($block['children'])) {
							foreach ($block['children'] as $adlnav) {
								if ($adlnav['name'] == 'ADLNAV:NAVIGATIONINTERFACE') {
									foreach ($adlnav['children'] as $adlnavInterface) {
										if ($adlnavInterface['name'] == 'ADLNAV:HIDELMSUI') {
											if ($adlnavInterface['tagData'] == 'continue') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hidecontinue = 1;
											}
											if ($adlnavInterface['tagData'] == 'previous') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideprevious = 1;
											}
											if ($adlnavInterface['tagData'] == 'exit') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideexit = 1;
											}
											if ($adlnavInterface['tagData'] == 'exitAll') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideexitall = 1;
											}
											if ($adlnavInterface['tagData'] == 'abandon') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideabandon = 1;
											}
											if ($adlnavInterface['tagData'] == 'abandonAll') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hideabandonall = 1;
											}
											if ($adlnavInterface['tagData'] == 'suspendAll') {
												$scoes->elements[$manifest][$parent->organization][$parent->identifier]->hidesuspendall = 1;
											}
										}
									}
								}
							}
						}
						break;
					case 'IMSSS:SEQUENCING':
						$parent = array_pop($parents);
						array_push($parents, $parent);
						if (!empty($block['children'])) {
							foreach ($block['children'] as $sequencing) {
								if ($sequencing['name']=='IMSSS:CONTROLMODE') {
									if (isset($sequencing['attrs']['CHOICE'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->choice = $sequencing['attrs']['CHOICE'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['CHOICEEXIT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->choiceexit = $sequencing['attrs']['CHOICEEXIT'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['FLOW'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->flow = $sequencing['attrs']['FLOW'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['FORWARDONLY'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->forwardonly = $sequencing['attrs']['FORWARDONLY'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['USECURRENTATTEMPTOBJECTINFO'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->usecurrentattemptobjectinfo = $sequencing['attrs']['USECURRENTATTEMPTOBJECTINFO'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['USECURRENTATTEMPTPROGRESSINFO'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->usecurrentattemptprogressinfo = $sequencing['attrs']['USECURRENTATTEMPTPROGRESSINFO'] == 'true'?1:0;
									}
								}
								if ($sequencing['name']=='ADLSEQ:CONSTRAINEDCHOICECONSIDERATIONS') {
									if (isset($sequencing['attrs']['CONSTRAINCHOICE'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->constrainChoice = $sequencing['attrs']['CONSTRAINCHOICE'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['PREVENTACTIVATION'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->preventactivation = $sequencing['attrs']['PREVENTACTIVATION'] == 'true'?1:0;
									}
								}
								if ($sequencing['name']=='IMSSS:OBJECTIVES') {
									$objectives = array();
									foreach ($sequencing['children'] as $objective) {
										$objectivedata = new stdClass();
										$objectivedata->primaryobj = 0;
										switch ($objective['name']) {
											case 'IMSSS:PRIMARYOBJECTIVE':
												$objectivedata->primaryobj = 1;
											case 'IMSSS:OBJECTIVE':
												$objectivedata->satisfiedbymeasure = 0;
												if (isset($objective['attrs']['SATISFIEDBYMEASURE'])) {
													$objectivedata->satisfiedbymeasure = $objective['attrs']['SATISFIEDBYMEASURE']== 'true'?1:0;
												}
												$objectivedata->objectiveid = '';
												if (isset($objective['attrs']['OBJECTIVEID'])) {
													$objectivedata->objectiveid = $objective['attrs']['OBJECTIVEID'];
												}
												$objectivedata->minnormalizedmeasure = 1.0;
												if (!empty($objective['children'])) {
													$mapinfos = array();
													foreach ($objective['children'] as $objectiveparam) {
														if ($objectiveparam['name']=='IMSSS:MINNORMALIZEDMEASURE') {
															$objectivedata->minnormalizedmeasure = $objectiveparam['tagData'];
														}
														if ($objectiveparam['name']=='IMSSS:MAPINFO') {
															$mapinfo = new stdClass();
															$mapinfo->targetobjectiveid = '';
															if (isset($objectiveparam['attrs']['TARGETOBJECTIVEID'])) {
																$mapinfo->targetobjectiveid = $objectiveparam['attrs']['TARGETOBJECTIVEID'];
															}
															$mapinfo->readsatisfiedstatus = 1;
															if (isset($objectiveparam['attrs']['READSATISFIEDSTATUS'])) {
																$mapinfo->readsatisfiedstatus = $objectiveparam['attrs']['READSATISFIEDSTATUS'] == 'true'?1:0;
															}
															$mapinfo->writesatisfiedstatus = 0;
															if (isset($objectiveparam['attrs']['WRITESATISFIEDSTATUS'])) {
																$mapinfo->writesatisfiedstatus = $objectiveparam['attrs']['WRITESATISFIEDSTATUS'] == 'true'?1:0;
															}
															$mapinfo->readnormalizemeasure = 1;
															if (isset($objectiveparam['attrs']['READNORMALIZEDMEASURE'])) {
																$mapinfo->readnormalizemeasure = $objectiveparam['attrs']['READNORMALIZEDMEASURE'] == 'true'?1:0;
															}
															$mapinfo->writenormalizemeasure = 0;
															if (isset($objectiveparam['attrs']['WRITENORMALIZEDMEASURE'])) {
																$mapinfo->writenormalizemeasure = $objectiveparam['attrs']['WRITENORMALIZEDMEASURE'] == 'true'?1:0;
															}
															array_push($mapinfos,$mapinfo);
														}
													}
													if (!empty($mapinfos)) {
														$objectivesdata->mapinfos = $mapinfos;
													}
												}
												break;
										}
										//print_object($objectivedata);
										array_push($objectives,$objectivedata);
									}
									$scoes->elements[$manifest][$parent->organization][$parent->identifier]->objectives = $objectives;
								}
								if ($sequencing['name']=='IMSSS:LIMITCONDITIONS') {
									if (isset($sequencing['attrs']['ATTEMPTLIMIT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->attemptLimit = $sequencing['attrs']['ATTEMPTLIMIT'];
									}
									if (isset($sequencing['attrs']['ATTEMPTABSOLUTEDURATIONLIMIT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->attemptAbsoluteDurationLimit = $sequencing['attrs']['ATTEMPTABSOLUTEDURATIONLIMIT'];
									}
								}
								if ($sequencing['name']=='IMSSS:ROLLUPRULES') {
									if (isset($sequencing['attrs']['ROLLUPOBJECTIVESATISFIED'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->rollupobjectivesatisfied = $sequencing['attrs']['ROLLUPOBJECTIVESATISFIED'] == 'true'?1:0;;
									}
									if (isset($sequencing['attrs']['ROLLUPPROGRESSCOMPLETION'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->rollupprogresscompletion = $sequencing['attrs']['ROLLUPPROGRESSCOMPLETION'] == 'true'?1:0;
									}
									if (isset($sequencing['attrs']['OBJECTIVEMEASUREWEIGHT'])) {
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->objectivemeasureweight = $sequencing['attrs']['OBJECTIVEMEASUREWEIGHT'];
									}

									if (!empty($sequencing['children'])){
										$rolluprules = array();
										foreach ($sequencing['children'] as $sequencingrolluprule) {
											if ($sequencingrolluprule['name']=='IMSSS:ROLLUPRULE' ) {
												$rolluprule = new stdClass();
												$rolluprule->childactivityset = 'all';
												if (isset($sequencingrolluprule['attrs']['CHILDACTIVITYSET'])) {
													$rolluprule->childactivityset = $sequencingrolluprule['attrs']['CHILDACTIVITYSET'];
												}
												$rolluprule->minimumcount = 0;
												if (isset($sequencingrolluprule['attrs']['MINIMUMCOUNT'])) {
													$rolluprule->minimumcount = $sequencingrolluprule['attrs']['MINIMUMCOUNT'];
												}
												$rolluprule->minimumpercent = 0.0000;
												if (isset($sequencingrolluprule['attrs']['MINIMUMPERCENT'])) {
													$rolluprule->minimumpercent = $sequencingrolluprule['attrs']['MINIMUMPERCENT'];
												}
												if (!empty($sequencingrolluprule['children'])) {
													foreach ($sequencingrolluprule['children'] as $rolluproleconditions) {
														if ($rolluproleconditions['name']=='IMSSS:ROLLUPCONDITIONS') {
															$conditions = array();
															$rolluprule->conditioncombination = 'all';
															if (isset($rolluproleconditions['attrs']['CONDITIONCOMBINATION'])) {
																$rolluprule->conditioncombination = $rolluproleconditions['attrs']['CONDITIONCOMBINATION'];
															}
															foreach ($rolluproleconditions['children'] as $rolluprulecondition) {
																if ($rolluprulecondition['name']=='IMSSS:ROLLUPCONDITION') {
																	$condition = new stdClass();
																	if (isset($rolluprulecondition['attrs']['CONDITION'])) {
																		$condition->cond = $rolluprulecondition['attrs']['CONDITION'];
																	}
																	$condition->operator = 'noOp';
																	if (isset($rolluprulecondition['attrs']['OPERATOR'])) {
																		$condition->operator = $rolluprulecondition['attrs']['OPERATOR'];
																	}
																	array_push($conditions,$condition);
																}
															}
															$rolluprule->conditions = $conditions;
														}
														if ($rolluproleconditions['name']=='IMSSS:ROLLUPACTION') {
															$rolluprule->rollupruleaction = $rolluproleconditions['attrs']['ACTION'];
														}
													}
												}
												array_push($rolluprules, $rolluprule);
											}
										}
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->rolluprules = $rolluprules;
									}
								}

								if ($sequencing['name']=='IMSSS:SEQUENCINGRULES') {
									if (!empty($sequencing['children'])) {
										$sequencingrules = array();
										foreach ($sequencing['children'] as $conditionrules) {
											$conditiontype = -1;
											switch($conditionrules['name']) {
												case 'IMSSS:PRECONDITIONRULE':
													$conditiontype = 0;
													break;
												case 'IMSSS:POSTCONDITIONRULE':
													$conditiontype = 1;
													break;
												case 'IMSSS:EXITCONDITIONRULE':
													$conditiontype = 2;
													break;
											}
											if (!empty($conditionrules['children'])) {
												$sequencingrule = new stdClass();
												foreach ($conditionrules['children'] as $conditionrule) {
													if ($conditionrule['name']=='IMSSS:RULECONDITIONS') {
														$ruleconditions = array();
														$sequencingrule->conditioncombination = 'all';
														if (isset($conditionrule['attrs']['CONDITIONCOMBINATION'])) {
															$sequencingrule->conditioncombination = $conditionrule['attrs']['CONDITIONCOMBINATION'];
														}
														foreach ($conditionrule['children'] as $rulecondition) {
															if ($rulecondition['name']=='IMSSS:RULECONDITION') {
																$condition = new stdClass();
																if (isset($rulecondition['attrs']['CONDITION'])) {
																	$condition->cond = $rulecondition['attrs']['CONDITION'];
																}
																$condition->operator = 'noOp';
																if (isset($rulecondition['attrs']['OPERATOR'])) {
																	$condition->operator = $rulecondition['attrs']['OPERATOR'];
																}
																$condition->measurethreshold = 0.0000;
																if (isset($rulecondition['attrs']['MEASURETHRESHOLD'])) {
																	$condition->measurethreshold = $rulecondition['attrs']['MEASURETHRESHOLD'];
																}
																$condition->referencedobjective = '';
																if (isset($rulecondition['attrs']['REFERENCEDOBJECTIVE'])) {
																	$condition->referencedobjective = $rulecondition['attrs']['REFERENCEDOBJECTIVE'];
																}
																array_push($ruleconditions,$condition);
															}
														}
														$sequencingrule->ruleconditions = $ruleconditions;
													}
													if ($conditionrule['name']=='IMSSS:RULEACTION') {
														$sequencingrule->action = $conditionrule['attrs']['ACTION'];
													}
													$sequencingrule->type = $conditiontype;
												}
												array_push($sequencingrules,$sequencingrule);
											}
										}
										$scoes->elements[$manifest][$parent->organization][$parent->identifier]->sequencingrules = $sequencingrules;
									}
								}
							}
						}
						break;
				}
			}
		}
		return $scoes;
	}


	function scorm_get_resources($blocks) {
		$resources = array();
		foreach ($blocks as $block) {
			if ($block['name'] == 'RESOURCES') {
				foreach ($block['children'] as $resource) {
					if ($resource['name'] == 'RESOURCE') {
						$resources[addslashes($resource['attrs']['IDENTIFIER'])] = $resource['attrs'];
					}
				}
			}
		}
		return $resources;
	}

	function scorm_optionals_data($item, $standarddata) {
		$result = array();
		$sequencingdata = array('sequencingrules','rolluprules','objectives');
		foreach ($item as $element => $value) {
			if (! in_array($element, $standarddata)) {
				if (! in_array($element, $sequencingdata)) {
					$result[] = $element;
				}
			}
		}
		return $result;
	}

	function print_object($object) {
		echo '<pre class="notifytiny">' . htmlspecialchars(print_r($object,true)) . '</pre>';
	}

	

	function CreateModule($title,$version,$import_type)
	{

		//$storage = new Zend_Auth_Storage_Session ();
		//$data = $storage->read ();
		//if (! $data) {
		//	$this->_redirect ( 'index/index' );
		//}
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->view->_redirect('index/index');
		}
		
		$module["name"]=$title;
		$module["description"]="Imported Scorm Package";
		$module["version"]=$version;
		$module["createdby"]=$data->username;;
		$module["createddt"]=date("Y-m-d h:i:s");
		$module["type"]=$import_type;	
		$module["status"]=1;

		$moduleDB = new Block_Model_Module();
		$moduleid = $moduleDB->addData($module);

		//print_r($module);
		//assignCon
		//$catalog["catID"]=0;
		//$catalog["cnID"]=$moduleid;
		//$catalog["userID"]=$ses_username;
		//$catalog["dtcreated"]=date("Y-m-d h:i:s");

		//$this->master_conn->AutoExecute("c_modcat",$catalog,"INSERT");
		//end assignCon

		return $moduleid;

	}



	/**
	 * To gathered sco information add insert into database.
	 *
	 * Database table : sco
	 * @param sring  $toc[title]
	 * @param string $toc[identifier]
	 * @param string $toc[scormtype]
	 * @param string $ses_username
	 * @param string $moduleid
	 * @return int
	 */

	function CreateSCO($toc,$moduleid)
	{
		global $ses_username;

		$scoinfo["sco_title"]=$toc[title];
		$scoinfo["sco_subject"]=$toc[title];
		$scoinfo["sco_desc"]=$toc[identifier];
		$scoinfo["sco_type"]=$toc[scormtype];
		$scoinfo["sco_language"]="ENGLISH";
		$scoinfo["sco_status"]="NOTPUBLISH";
		$scoinfo["createdby"]=$ses_username;
		$scoinfo["createddt"]=date("Y-m-d h:i:s");
		$scoinfo["cmID"]=$moduleid;
		
		//print_r($scoinfo);
			
		$scoDB = new Block_Model_Sco();
		$scoid = $scoDB->addData($scoinfo);
		
		return $scoid;

	}



	/**
	 * To gathered toc information add insert into database.
	 *
	 * Database table : c_lesson
	 * @param sring  $toc[title]
	 * @param string $moduleid
	 * @param int    $id
	 * @param int    $currid
	 * @param int    $level
	 * @param string $ses_username
	 * @param int    $toc["identifier"]
	 * @param int    $toc["idref"]
	 * @param int    $toc["parent"]
	 * @return int
	 */


		
	function CreateTOC($toc,$id,$moduleid,$currid,$level,$strurl)
	{
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->view->_redirect('index/index');
		}
		
		$filename = "$toc[launch]$toc[parameters]";	
		
		if($filename)	
			$location="$strurl/$filename";
		else 
			$location="";
			
		$info["toc"]=$toc[title];
		$info["cmID"]=$moduleid;
		$info["sco_id"]=$id;
		$info["threadparent"]=$currid;
		$info["level"]=$level;
		$info["location"]=$location;		
		$info["createdBy"]=$data->username;;
		$info["createdDt"]=date("Y-m-d h:i:s");
		$info["modifiedBy"]=$data->username;;
		$info["modifiedDt"]=date("Y-m-d h:i:s");
		$info["scormid"]=$toc["identifier"];
		$info["scormidref"]=$toc["idref"];
		$info["scormparentid"]=$toc["parent"];

		$lessonDB = new Block_Model_Lesson();
		$tocid = $lessonDB->addData($info);

		return $tocid;

	}
	

	function checklessonid($newitem,$id,$moduleid,$strurl)
	{
		//untuk dapatkan sco_id parent tu.
		$parent_sco=$newitem["parent"];
		
		$scoDB = new Block_Model_Sco();
		$rs = $scoDB->getData($parent_sco,$moduleid);
		
		
		//$sql="Select sco_id from sco where sco_desc='$parent_sco' and cmID='$moduleid'";
		//$rs=$this->master_conn->Execute($sql);

		//sekiranya tiada level dia 1 else selain dari 1.
		if (!$rs){
		    $this->_levelutama= 1 + $this->_levelutama;
			$levelutama=$this->_levelutama;
			//$toc,$id,$moduleid,$currid,$level,$strurl
			$curid=$this->CreateTOC($newitem,$id,$moduleid,0,$levelutama,$strurl);  //add TOC


		}else{

			//echo '<br>++CHILD++<br>';
			//dapatkan ID untuk sco_id tersebut
			$sco_id=$rs["sco_id"];
			//$sql="Select ID from c_lesson  where sco_id='$sco_id'";
			//$rs2=$this->master_conn->Execute($sql);
			$lessonDB = new Block_Model_Lesson();
			$rs2 = $lessonDB->getParentSco($sco_id);
			//print_r($rs2);

			//tgk bilangan anak dalam tu
			echo '<br>parent:'.$threadparent=$rs2["ID"].'<br>';
			//$sql="Select count(*) as bil from c_lesson  where threadparent ='$threadparent'";
			//$rs3=$this->master_conn->Execute($sql);
			echo 'rs3 bil child count:'.$rs3 = $lessonDB->getChild($threadparent);
			//print_r($rs3);
			echo '<br>';

			if ($rs3>0){
				//$level=$rs3->fields["bil"];
				$level=$rs3;
				$level++;

			}else {
				$level=1;
			}
          
			$curid=$this->CreateTOC($newitem,$id,$moduleid,$threadparent,$level,$strurl);  //add TOC


		}
		return $curid;

	}


	function updatePrevToc($curid,$previd)
	{
		//$this->master_conn->debug=1;
		$info['previous']=$previd;
		$cond="ID=$curid";

		//$this->master_conn->AutoExecute("c_lesson",$info,"UPDATE",$cond);
		$lessonDB = new Block_Model_Lesson();
		$lessonDB->updateData($info,$curid);

	}

	function updateNextToc($curid,$previd)
	{
		//$this->master_conn->debug=1;
		$info['next']=$curid;
		$cond="ID=$previd";

		//$this->master_conn->AutoExecute("c_lesson",$info,"UPDATE",$cond);
		$lessonDB = new Block_Model_Lesson();
		$lessonDB->updateData($info,$previd);
	}

	/**
	 * Object to Array
	 *
	 * @param object $z
	 * @return aray
	 */
	function objToArray(&$z)
	{
		if( is_array( $z ) )
		{
			foreach( $z as $k=>$v )
			{
				if( is_object( $z[$k] ) )
				{
					$z[$k] =(array)$v;
					$z[$k] = $this->objToArray( $z[$k] );
				}
			}
		}
		else
		{
			settype( $z, 'array' );
			$this->objToArray( $z );
		}
		return $z;
	}

	function add_resources_to_array($newitem,$id,$path)
	{
		$link = "$newitem[launch]$newitem[parameters]";

		//echo $id;
		$contentid=$this->CreateContent($newitem["title"],$link,$path); //add content
		$this->CreateLessonContent($id,$contentid);  //addlessoncontent



	}

	function CreateContent($title,$link,$path)
	{
		global $ses_username;

		$scormpath="/$path/$link";

		$newscopath = $link;
		$dtcont["title"]=$title;
		$dtcont["url"]=$scormpath;
		$dtcont["template"]=30;
		$dtcont["ctype"]="url";
		$dtcont["dtcreated"]=date("Y-m-d h:i:s");
		$dtcont["createdby"]=$ses_username;

		$this->master_conn->AutoExecute("c_scorm",$dtcont,"INSERT");
		$contid=$this->master_conn->Insert_ID();

		return $contid;
	}


	function CreateLessonContent($id,$contentid)
	{
		global $ses_username,$docroot;
		include_once("$docroot/modules/lcms/lomodule/lo_toc.class.php");

		$ctoc = new CLOProp();

		$ctoc->cnid=$contentid;
		$ctoc->dtype=30; //scorm type
		$ctoc->sco_id=$id;
		$ctoc->author=tosql($ses_username,"Text");;
		$ctoc->dtcreated="NOW()";
		$ctoc->dtmodified="NOW()";
		$ctoc->lesid=0;
		$ctoc->addLessonContent();
	}
	

	/**
	 * To gethered Learning Material infor to be assigned to course
	 * Can be used for multiple scoid
	 *
	 * @param string $cid	course id
	 */
	function assignlp($cid,$cmid,$import_type)
	{
		global $ses_username,$docroot;
		include_once("$docroot/modules/lcms/assign/assign.class.php");
		$oResource = new general();


		$module["courseID"]  = $cid;
		$module["userID"]    = $ses_username;;
		$module["dtCreated"] = date("Y-m-d h:m:s");
		$module["cnID"]      = $cmid;

		$oResource->add("c_modcourse",$module);

		/*if($import_type=='Scorm')
		header("Location: /modules/lcms/lomodule/index_scorm.php?v=y&cid=$cid&refresh=1");
		else{
		header("Location: /modules/lcms/lomodule/index.php?ccat=m&courseid=$cid&refresh=1");}*/

		header("Location: /modules/lcms/import/import.php?cid=$cid&refresh=1");

		exit();

	}


}

class xml2Array {

	var $arrOutput = array();
	var $resParser;
	var $strXmlData;

	/**
   * Convert a utf-8 string to html entities
   *
   * @param string $str The UTF-8 string
   * @return string
   */
	function utf8_to_entities($str) {
		global $CFG;

		$entities = '';
		$values = array();
		$lookingfor = 1;

		return $str;
	}

	/**
   * Parse an XML text string and create an array tree that rapresent the XML structure
   *
   * @param string $strInputXML The XML string
   * @return array
   */
	function parse($strInputXML) {
		$this->resParser = xml_parser_create ('UTF-8');
		xml_set_object($this->resParser,$this);
		xml_set_element_handler($this->resParser, "tagOpen", "tagClosed");

		xml_set_character_data_handler($this->resParser, "tagData");

		$this->strXmlData = xml_parse($this->resParser,$strInputXML );
		if(!$this->strXmlData) {
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->resParser)),
			xml_get_current_line_number($this->resParser)));
		}

		xml_parser_free($this->resParser);

		return $this->arrOutput;
	}

	function tagOpen($parser, $name, $attrs) {
		$tag=array("name"=>$name,"attrs"=>$attrs);
		array_push($this->arrOutput,$tag);
	}

	function tagData($parser, $tagData) {
		if(trim($tagData)) {
			if(isset($this->arrOutput[count($this->arrOutput)-1]['tagData'])) {
				$this->arrOutput[count($this->arrOutput)-1]['tagData'] .= $this->utf8_to_entities($tagData);
			} else {
				$this->arrOutput[count($this->arrOutput)-1]['tagData'] = $this->utf8_to_entities($tagData);
			}
		}
	}

	function tagClosed($parser, $name) {
		$this->arrOutput[count($this->arrOutput)-2]['children'][] = $this->arrOutput[count($this->arrOutput)-1];
		array_pop($this->arrOutput);
	}




}

?>