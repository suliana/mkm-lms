<?php
class Block_Model_Lesson extends Zend_Db_Table
{
    protected $_name = "c_lesson";
    protected $_primary = "ID";   
    protected $_show=3;
    
        
    public function fetchAll ()
    {
        $sql = $this->_db->select()->from($this->_name);
        $stmt = $this->_db->query($sql);
        return $stmt;
    }
    
    public function getParentSco($sco_id)
    {
        $sql = $this->_db->select()->from($this->_name)->where("sco_id = '$sco_id'");
        $result = $this->_db->fetchRow($sql); 
        return $result;
    }
    
    public function getChild($id)
    {
       echo $sql = $this->_db->select()
    	  			     ->from($this->_name)    	  			      
    	  			     ->where("threadparent='$id'");
       
        $count = $this->_db->query($sql)->rowCount();
      
        return $count;
    }
    
    public function getData($parent_sco,$moduleid){
    	
    	 $sql = $this->_db->select()
    	  			      ->from($this->_name)
    	  			      ->where("sco_desc='$parent_sco'")
    	  			      ->where("cmID='$moduleid'");
    	  			      
    	// $result = $this->_db->fetchRow($sql); 
    	 $result = $this->_db->query($select);
    	
         return $result;
    	  			      
    }
    
    public function get_info($moduleid){
    	
    	////$scodata=$oModule->get_info('content',"*","c_lesson","cmID='$moduleid' Order by level ASC",'list');
    	  $sql = $this->_db->select()
    	  			      ->from($this->_name)    	  			    
    	  			      ->where("cmID='$moduleid'")
    	  			      ->order('level asc');    	  			      
    	
    	 $result = $this->_db->fetchAll($sql);   
      
         return $result;
    }
    
 	public function get_lesson($id)
    {
        $sql = $this->_db->select()->from($this->_name)->where("ID = '$id'");
        $result = $this->_db->fetchRow($sql); 
        return $result;
    }
    
	public function addData ($data)
    {
        $this->_db->insert($this->_name, $data);
        return $this->_db->lastInsertId();
    }
    
    public function updateData ($data, $id)
    {
        $this->_db->update($this->_name, $data, 'id = ' . (int) $id);
    }
    
    
     public function displaychild($show,$layer,$parent_id,$showid,$clevel,$moduleid,$courseid,$tocid=null) 
     {		
     	
	
     	 $sql = $this->_db->select()
    	  			      ->from($this->_name)    
    	  			      ->where("threadparent<>'0'")	  			    
    	  			      ->where("cmID='$moduleid'")
    	  			      ->order('level asc');    	  			      
    	
    	 $rsc = $this->_db->fetchAll($sql);  
		
    	
		if ($rsc) {
			$idx=0;
			foreach ($rsc as $rsorder) {
				$idx++;
				$arVal[0][$idx]=0;
				$arVal[1][$idx]=$rsorder["ID"];
				$arVal[2][$idx]=$rsorder["threadparent"];
				$arVal[3][$idx]=$rsorder["toc"];			
				$arVal[4][$idx]=$rsorder["level"];
				$arVal[5][$idx]=0;
				$arVal[6][$idx]=$rsorder["sco_id"];
				$arVal[7][$idx]=$rsorder["cmID"];
			}
			
			echo '<ul class="child">';
			$this->displaychild_s1($show,1,$idx,$arVal,$parent_id,$clevel,$courseid,$tocid);
			echo '</ul>';
		}
	}
	
	public function displaychild_s1($show,$layer,$index,$arValues,$pid,$clevel,$courseid,$curtocid){	
					
		$sh_indent=1; // 1: utk show indent 0: no indent
		$bilpost=$bilpost+1;
		
		for($t=1;$t<=$index;$t++){
			if($arValues[2][$t]==$pid){
				$tid=$arValues[0][$t];
				$tocid=$arValues[1][$t];
				$toc=$arValues[3][$t];
				$scoid=$arValues[6][$t];
				$cmid=$arValues[7][$t];
				$level=$clevel.".".$arValues[4][$t];
								
				$spaceing="";
				$indent=0;
				for ($mspace=1;$mspace<=$layer;$mspace++){
					$spaceing=$spaceing;
					$indent=$indent+3;
				}
				
				
				$rs_lesson  = $this->get_lesson($tocid);
			    $url        = $rs_lesson["location"];
						   				
				if($url){					
					$urllink = '<li><a href="#" onClick="previewContent('.$tocid.'); return false; ">'.$toc.'</a></li>';
					$bgclr   = 'class="bgoff" onmouseover="this.className=\'bgon\'" onmouseout="this.className=\'bgoff\'';
				}else{
					$urllink = $toc;
					$bgclr = 'bgcolor="#FFFF66"';
				}
										
				
			    //==================
				//DISPLAY OPTIONS 1 (SELECT)
				//==================						
				if($show==1){
				$param = "cid=$courseid&cmid=$cmid&tocid=$tocid";						
				if($curtocid==$tocid) $selected='selected'; else $selected='';				
				echo '<option value="'.$tocid.'" '.$selected.'>'.$toc.'....</option>';		
				}				
								
				
				
			    //==================
				//DISPLAY OPTIONS 3 (TRACKING COLUMN)
				//==================	
				if($show==3){		

				   /*==========================================
					TRACKING - TO CHECK COMPLETED OR INCOMPLETED
					===========================================*/	
				
					$img='&nbsp;';
					$moduleDB = new Block_Model_Module();
					$module_type = $moduleDB->get_info($cmid);	
								
					$trackDB = new Block_Model_Track();
						
					if($module_type=='Scorm'){ 												
						$status=$trackDB->get_scorm_status($cmid,$tocid);	
					}else{						
						$status=$trackDB->get_nonscorm_status($cmid,$tocid,$courseid);
					}
					
					
				 
					if($status=="completed")  {$img='<img src="/images/icon_correct.jpg" width="19" height="19" />';}
					if($status=="incompleted"){$img='<img src="/images/icon_false.jpg" width="19" height="19" />';}					
					
					/*==============================================
					END TRACKING - TO CHECK COMPLETED OR INCOMPLETED
					================================================*/
					
					
				/*echo '<tr '.$bgclr.'">				      
					  <td id="data_border" class="line">&nbsp;'.$spaceing.$img.'&nbsp;'.$urllink.'</td>
					  </tr>';*/
					echo $urllink;
				}
				
				
				
				$this->displaychild_s1($show,$layer+1,$index, $arValues, $tocid,$level,$courseid,$curtocid);
			}
			
		}
	}
	
	
	
}
?>