<?php
class Forum_MonitoringController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
		//$this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
		$this->_helper->layout->setLayout('admin');

	}

	public function searchmessageAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();

		if (! $data) {
			$this->_redirect ( 'index/index' );
		}

		$monitoring = new Forum_Model_Monitoring ();

		$semid = $this->getRequest()->getParam('semid');

		$this->view->semid = $semid;

		if (isset($semid) || $this->getRequest ()->isPost ()) {

			$semid = $this->getRequest()->getParam('semid');
			$cid = $this->getRequest()->getParam('cid');
			$keyworduser = $this->getRequest()->getParam('keyworduser');
			$filter = $this->getRequest()->getParam('filter');
			$useDate = $this->getRequest()->getParam('useDate');
			$start_date = $this->getRequest()->getParam('start_date',0);
			$end_date = $this->getRequest()->getParam('end_date',0);

			if ($this->getRequest ()->isPost ()){
				$semid = $this->getRequest()->getPost('semid');
				$cid = $this->getRequest()->getPost('cid');
				$keyworduser = $this->getRequest()->getPost('keyworduser');
				$filter = $this->getRequest()->getPost('filter');
				$useDate = $this->getRequest()->getPost('useDate');
				$start_date = $this->getRequest()->getPost('start_date',0);
				$end_date = $this->getRequest()->getPost('end_date',0);
			}

			$this->view->semid = $semid;
			$this->view->cid = $cid;
			$this->view->keyworduser = $keyworduser;
			$this->view->filter = $filter;
			$this->view->useDate = $useDate;
			$this->view->start_date = $start_date;
			$this->view->end_date = $end_date;


			

			$select=$monitoring->searcheos($semid,$cid,$keyworduser);
			
			if ($filter==""){
			Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'paginationspecial2.phtml' );

			$paginator = Zend_Paginator::factory ( $select );
			$paginator->setItemCountPerPage ( 3 );
			$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
			$this->view->paginator = $paginator;
			$this->view->searchParams = array('keyworduser'=>$keyworduser,'semid'=>$semid,'cid'=>$cid,'filter'=>$filter,'useDate'=>$useDate,'start_date'=>$start_date,'end_date'=>$end_date);
			}
			else {
			$this->view->select = $select; 
			}
			


		}

	}
	
	public function framepage1Action() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$this->view->username = $data->username;
		
		$userid = $this->_getParam ( 'userid', 0 );
		$this->view->userid = $userid;

		$groupid = $this->_getParam ( 'groupid', 0 );
		$this->view->groupid = $groupid;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$usedate = $this->_getParam ( 'usedate', 0 );
		$this->view->usedate = $usedate;
		
		$start_date = $this->_getParam ( 'start_date', 0 );
		$this->view->start_date = $start_date;
		
		$end_date = $this->_getParam ( 'end_date', 0 );
		$this->view->end_date = $end_date;
		
		$act = $this->_getParam ( 'act', 0 );
		$this->view->act = $act;

		

	}
	
	public function viewmessageAction() {
		$storage = new Zend_Auth_Storage_Session ();
		$data = $storage->read ();
		if (! $data) {
			$this->_redirect ( 'index/index' );
		}
		$this->view->username = $data->username;
		
		$userid = $this->_getParam ( 'userid', 0 );
		$this->view->userid = $userid;

		$groupid = $this->_getParam ( 'groupid', 0 );
		$this->view->groupid = $groupid;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$usedate = $this->_getParam ( 'usedate', 0 );
		$this->view->usedate = $usedate;
		
		$start_date = $this->_getParam ( 'start_date', 0 );
		$this->view->start_date = $start_date;
		
		$end_date = $this->_getParam ( 'end_date', 0 );
		$this->view->end_date = $end_date;
		
		$act = $this->_getParam ( 'act', 0 );
		$this->view->act = $act;
		
		$monitoring = new Forum_Model_Monitoring ();
		
		
		$forumid = $monitoring-> viewforummesg($courseid,$userid,$groupid,"",$usedate,$start_date,$end_date);
		
		
		//print_r($forumid);
		
		if ($act=="viewmessagetutor")
		$select = $monitoring-> viewforummesg_sub($userid,$forumid,$usedate,$start_date,$end_date);
		else if ($act=="viewmessageall")
		$select = $monitoring-> viewforummesg_sub("",$forumid,$usedate,$start_date,$end_date);
		else if ($act=="viewmessagestudent")
		$select = $monitoring-> viewforummesg_stud($userid,$forumid,$usedate,$start_date,$end_date);
		
		

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 10 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		

	}


}





