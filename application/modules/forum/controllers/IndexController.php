<?php
class Forum_IndexController extends Zend_Controller_Action
{
	public function init() 
	{
		/* Initialize action controller here */
		//$this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
		//$this->_helper->layout->setLayout('course');
		
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		$this->view->active = 'admin';
	}
	
	public function listforumAction() 
	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;
		
		$data = $this->userinfo;
		$privilage=$data->privilage;

		$this->view->privilage = $privilage;

		$intake_id = $this->_getParam ( 'intake_id', "" );
		//echo "Sebelum ".$intake_id;
		//get current intake id
		if($intake_id=="")
		{
			$intake = new Admin_Model_Intake ();
			$result =  $intake->checkstatus('CURRENT');
			$intake_id = $result["id"]	;
		}


		//tambahan for student and EOS
		if ($data->privilage=="STUDENT" || $data->privilage=="EOS")
		{
			if ($data->privilage=="STUDENT") 
			{
				$assignsubject= new Course_Model_Student();
				$rs=$assignsubject->fetch($data->username,$courseid);
			}
			else if ($data->privilage=="EOS") 
			{
				$assignsubject= new Course_Model_Grader();
				$rs=$assignsubject->fetch($data->username, $courseid);
			}

			if ($rs)
			{
				$intake_id=$rs["intakeid"];
			}
			else
			{
				$intake_id='0';
			}
			
			//echo $intakeid;
			//exit();
		}
		//end tambahan

		//echo "Selepas ".$intake_id;

		$this->view->semid = $intake_id;

		$form = new Forum_Form_Search (array ('intakeid' => $intake_id ));
		$this->view->form = $form;

		//print_r($rstitle);
		$forum = new Forum_Model_Forum();
		$forum->checktableforumread($courseid);

		if($intake_id!="")
		{
			$rs=$forum->checkaddcategory($courseid,$intake_id,$rscourse['coursecode']);
			if(empty($rs))
			{
				//echo "sini";
				$sdata["courseID"]=$courseid;
				$sdata['coursecode']=$rscourse['coursecode'];
				$sdata["semid"]=$intake_id;
				$sdata["name"]=$rscourse['coursecode'].' - '.$rscourse['coursename'];
				$forum->upload("forumcat",$sdata);
				/*$catid =  $forum->getlastinsertid("forumcat");
				//echo $catid;

				$vdata["title"]="Discussion about General";
				$vdata["description"]=$vdata["title"];
				$vdata["courseid"]=$courseid;
				$vdata["catid"]=$catid;
				$vdata["semid"]=$intake_id;
				$vdata["dtcreated"]=date("Y-m-d H:i:s");
				$vdata["createby"]=$data->username;
				$forum->upload ( "forummain",$vdata );*/
			}

			$rscategory=$forum->getcategory($courseid,$intake_id);
		}
		
		$this->view->category = $rscategory;
	}

	public function addfrmcategoryAction() 
	{
		//$this->_helper->layout->disableLayout();

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;


		$act= $this->_getParam ( 'act', "none" );
		$this->view->act = $act;

		$addnewcategory= $this->_getParam ( 'addnewcategory', 0 );
		$this->view->addnewcategory = $addnewcategory;

	}

	public function delfrmcategoryAction() 
	{
		//$this->_helper->layout->disableLayout();
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$catid= $this->_getParam ( 'catid', 0 );
		$this->view->catid = $catid;

		$act= $this->_getParam ( 'act', "none" );
		$this->view->act = $act;

	}

	public function edfrmcategoryAction()
	{
		$this->_helper->layout->disableLayout();

		$storage = new Zend_Auth_Storage_Session ();

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$catid= $this->_getParam ( 'catid', 0 );
		$this->view->catid = $catid;

		$forum = new Forum_Model_Forum ();
		$this->view->catname=$forum->getcatname($catid);

		$act= $this->_getParam ( 'act', "none" );
		$this->view->act = $act;

		$category= $this->_getParam ( 'category', 0 );
		$this->view->category = $category;


	}

	public function addtitleAction() 
	{
		$username=$this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum')." > ".$this->view->translate('add')." ".$this->view->translate('title');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forum = new Forum_Model_Forum ();
		$form = new Forum_Form_Addtitle ( array ('courseid' => $courseid,'semid' => $semid ) );
		$this->view->form = $form;

		//$form->getElement('courseid')->setValue($courseid);
		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST )) 
			{
				$data 				= $form->getValues ();
				$data["dtcreated"]	= date("Y-m-d H:i:s");
				$data["createby"]	= $this->userinfo->username;
				$data['createby_id'] = $this->userinfo->id;
				$data['coursecode'] = $rscourse['coursecode'];
				
				$forum->upload ( "forummain",$data );
				$this->_redirect ( 'forum/index/listforum/courseid/'.$courseid.'/semid/'.$semid );
			}
		}
	}

	public function modifytitleAction() 
	{
		$username=$this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum')." > ".$this->view->translate('modify')." ".$this->view->translate('title');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forumID = $this->_getParam ( 'forumID', 0 );
		//echo $forumID;

		$forum = new Forum_Model_Forum ();
		$form = new Forum_Form_Modifytitle ( array ('courseid' => $courseid,'semid' => $semid ) );
		$this->view->form = $form;

		if ($this->getRequest ()->isPost ())
		{
			if ($form->isValid ( $_POST )) 
			{
				$data = $form->getValues ();
				if ($forumID > 0) 
				{
					$data["dtmodified"]=date("Y-m-d H:i:s");
					$data["modifyby"]=$username;
					$forum->modify ( "forummain",$data, $forumID,"forumID" );

				}
				else
				{
					$data["dtcreated"]=date("Y-m-d H:i:s");
					$data["createby"]=$username;
					$forum->upload ( "forummain",$data );
				}
				
				$this->_redirect ( 'forum/index/listforum/courseid/'.$courseid.'/semid/'.$semid );
			}
		} 
		else 
		{
			if ($forumID > 0) 
			{
				//echo "here";
				$form->populate ( $forum->fetch ("forummain",$forumID,"forumID" ) );
			}
		}
	}

	public function listmsgforumAction() 
	{
		$this->view->username = $this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;


		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forumID = $this->_getParam ( 'forumID', 0 );
		$this->view->forumID = $forumID;

		$forum = new Forum_Model_Forum ();
		$select=$forum->getfirstlist($forumID);

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
	}

	public function showmesgAction() 
	{
		$this->view->username = $this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum')." > ".$this->view->translate('show').' '.$this->view->translate('message');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forumID = $this->_getParam ( 'forumID', 0 );
		$this->view->forumID = $forumID;

		$fmid = $this->_getParam ( 'fmid', 0 );
		$this->view->fmid = $fmid;

		$tid = $this->_getParam ( 'tid', 0 );
		$this->view->tid = $tid;
	}

	public function editmessageAction() 
	{
		$username=$this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum')." > ".$this->view->translate('edit').' '.$this->view->translate('message');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );

		$forumID = $this->_getParam ( 'forumID', 0 );

		$fmid = $this->_getParam ( 'fmid', 0 );

		$tid = $this->_getParam ( 'tid', 0 );

		$forum = new Forum_Model_Forum ();
		$form = new Forum_Form_Modifymesg ( array ('courseid' => $courseid,'semid' => $semid,'forumID' => $forumID,'fmid' => $fmid,'tid' => $tid ) );
		$this->view->form = $form;

		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST )) 
			{
				$data = $form->getValues ();
				
				if(strlen($fmid)<>0)
				{
					$data['message'] = stripslashes($data['message']);
					unset($data["courseid"]);
					unset($data["semid"]);
					unset($data["tid"]);
					$forum->modify ( "forummesg",$data, $fmid,"fmesgID" );

				} 
				else 
				{
					$forum->upload ( "forummesg",$data );
				}
				
				if ($tid==0)
				{
					$this->_redirect ( 'forum/index/showmesg/courseid/'.$courseid.'/semid/'.$semid.'/forumID/'.$forumID.'/fmid/'.$fmid.'/tid/0' );
				}
				else 
				{
					$this->_redirect ( 'forum/index/showmesg/courseid/'.$courseid.'/semid/'.$semid.'/forumID/'.$forumID.'/fmid/'.$tid.'/tid/0' );
				}
			}
		} 
		else 
		{
			if (strlen($fmid)<>0)
			{
				$form->populate ( $forum->getmesg ($forumID,$fmid) );
			}
			else
			{
				$form->populate ( $forum->getforummain ($forumID ) );

			}

		}
	}

	public function postmessageAction()
	{
		$username = $this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum')." > ".$this->view->translate('post').' '.$this->view->translate('message');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forumID = $this->_getParam ( 'forumID', 0 );
		$this->view->forumID = $forumID;

		$fmid = $this->_getParam ( 'fmid', 0 );
		$this->view->fmid = $fmid;

		$tid = $this->_getParam ( 'tid', 0 );
		$this->view->tid = $tid;

		$forum = new Forum_Model_Forum ();
		$form = new Forum_Form_Replymesg ( array ('courseid' => $courseid,'semid' => $semid,'forumID' => $forumID,'fmid' => $fmid,'tid' => $tid ) );
		$this->view->form = $form;

		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST ))
			{
				$data = $form->getValues ();

				if ($data['tid']==0) $data['threadID']=0;
				else $data['threadID']=$data['tid'];
				if ($data['fmid']==0) $data['threadparent']=0;
				else $data['threadparent']=$data['fmid'];
				if ($data['tid']==0 && $data['fmid']<>0) $data['threadID']=$data['fmid'];

				
				$data['userid']=$this->userinfo->id;
				$data['username']=$this->userinfo->username;
				$data["dateposted"]=date("Y-m-d H:i:s");
				$data['message'] = stripslashes($data['message']);
				$data['coursecode']= $rscourse['coursecode'];

				unset($data['tid']);
				unset($data['fmid']);
				unset($data['courseid']);
				unset($data["semid"]);


				$forum->upload ( "forummesg",$data );
				/*$fmesgid =  $forum->getlastinsertid("forumesg");

				if ($fmid!=0)
				$rsreplyto=$forum->getmesg($forumID,$fmid);
				else
				$rsreplyto=$forum->gettutor($semid);

				$rs=$forum->getmesg($forumID,$fmesgid);*/

				//stop dulu

				/*$config = array(
				'ssl' => 'ssl',
				'port' => 465,
				'auth' => 'login',
				'username' => 'epicnotification@oum.edu.my',
				'password' => 'aabbccddee' );



				$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

				$mail = new Zend_Mail();
				$mail->setBodyHtml("<b>".$data['subject']."</b><br>".$data['message']);



				$mail->addHeader('courseid', $courseid);
				$mail->addHeader('semid', $semid);
				$mail->addHeader('forumID', $forumID);
				$mail->addHeader('fmid', $rs['fmesgID']);
				$mail->addHeader('tid', $rs['threadID']);
				$mail->addHeader('subjectss', $data['subject']);

				$mail->setMessageId(uniqid());


				$mail->setFrom('epicnotification@oum.edu.my', 'Epic Forum Notification');
				//$mail->addTo('zahili@oum.edu.my', 'Mohamad Zahili');
				if ($fmid!=0){
				if ($username!=$rsreplyto['userid']){
				$mail->addTo($rsreplyto['userid'].'@oum.edu.my');
				$cansend=1;
				}
				}
				else{
				if ($username!=$rsreplyto['username']){
				$mail->addTo($rsreplyto['username'].'@oum.edu.my');
				$cansend=1;
				}
				}
				$mail->setSubject($data['userid']." posted on ".$courseid. " forum");

				if ($cansend==1)
				$mail->send($transport);*/


				/*include_once(APPLICATION_PATH . "/../public"."/PHPMailer/class.phpmailer.php");
				include_once(APPLICATION_PATH . "/../public"."/PHPMailer/class.smtp.php");

				$mail=new PHPMailer();

				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
				$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 465;                   // set the SMTP port


				$mail->Username   = "assignment_admin@oum.edu.my";  // GMAIL username
				$mail->Password   = "admin123";           // GMAIL password

				$mail->From       = "mylms_admin@oum.edu.my";
				$mail->FromName   = "myVLE Administrator";

				$mail->Subject    = $data['userid']." posted on ".$courseid. " forum";


				$mail->Body       = "<b>".$data['subject']."</b><br>".$data['message'];//HTML Body


				$mail->WordWrap   = 5000; // set word wrap


				$mail->AddAddress("zahili@oum.edu.my","Mohamad Zahili");
				//$mail->AddCC("safiah_mdyusof@".$mail_domain, "Dr Safiah");

				//$mail->AddAttachment($_FILES['atfile']['tmp_name'],$_FILES['atfile']['name']);

				$mail->IsHTML(true); // send as HTML

				if(!$mail->Send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
				} else {
				echo "Message has been sent";

				}*/


				if ( $data['threadID']==0 )
				{
					$this->_redirect ( 'forum/index/listmsgforum/courseid/'.$courseid.'/semid/'.$semid.'/forumID/'.$forumID );
				}
				else
				{
					$this->_redirect ( 'forum/index/showmesg/courseid/'.$courseid.'/semid/'.$semid.'/forumID/'.$forumID.'/fmid/'.$data['threadID'].'/tid/0' );
				}
			}
		}
	}

	public function printforumAction() 
	{
		$username = $this->userinfo->username;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('forum')." > ".$this->view->translate('print_message');

		$course = new Application_Model_Course ();

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$rscourse = $course->fetch ($courseid);

		$this->view->course = $rscourse;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forumID = $this->_getParam ( 'forumID', 0 );
		$this->view->forumID = $forumID;

		$fmid = $this->_getParam ( 'fmid', 0 );
		$this->view->fmid = $fmid;

		$tid = $this->_getParam ( 'tid', 0 );
		$this->view->tid = $tid;
	}

	public function deletemessageAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forumID = $this->_getParam ( 'forumID', 0 );
		$this->view->forumID = $forumID;

		$fmid = $this->_getParam ( 'fmid', 0 );
		$this->view->fmid = $fmid;

		$tid = $this->_getParam ( 'tid', 0 );
		$this->view->tid = $tid;
	}
	////



	public function deleteAction()
 	{
		$activity = new Activities_Model_Activities ();
		$id = $this->_getParam ( 'rowid', 0 );

		$courseid = $this->_getParam ( 'courseid', '' );

		//echo $id;
		if ($id > 0) 
		{
			$activity->delete ( $id );
			$this->_redirect ( 'activities/index/index/id/'.$courseid.'/courseid/'.$courseid );
		}
	}

	public function deletetitleAction() 
	{
		$username = $this->userinfo->username;
		$courseid = $this->_getParam ( 'courseid', 0 );
		$semid = $this->_getParam ( 'semid', 0 );
		$forumID = $this->_getParam ( 'forumID', 0 );

		$forum = new Forum_Model_Forum ();
		$forum->deletetitle($forumID,$courseid,$semid);

		$this->_redirect ( 'forum/index/listforum/courseid/'.$courseid.'/semid/'.$semid );
	}



	public function listuserAction() 
	{
		$this->_helper->layout->disableLayout();

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$forum = new Forum_Model_Forum ();

		$select = $forum->listuser($semid);

		//$select = $forum->fetchAll("tagginguser","",$semid);

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $select );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
	}

	public function readmailAction()
	{
		/*$config = array('host'     => 'pop.gmail.com',
		'user'     => 'grader1@oum.edu.my',
		'password' => 'aabbccddee',
		'port' => 995,
		'ssl'      => 'SSL');*/

		$config = array('host'     => '74.125.53.108',
		'user'     => 'grader1@oum.edu.my',
		'password' => 'aabbccddee',
		'port' => 993,
		'ssl'      => 'SSL');

		//$mail = new Zend_Mail_Storage_Pop3($config);
		$mail = new Zend_Mail_Storage_Imap($config);


		// find unread messages
		echo "Unread mails:\n";
		//$mail->setFlags(2, array(Zend_Mail_Storage::FLAG_SEEN));
		foreach ($mail as $messageNum => $message) 
		{
			if ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN))
			{
				continue;
			}

			if ($message->hasFlag(Zend_Mail_Storage::FLAG_RECENT))
			{
				echo "message num : " .$messageNum;
				//$mail->setFlags($messageNum, array(Zend_Mail_Storage::FLAG_SEEN));
				if( isset($message->references))
				{
					/*if($message->headerExists("message-id")){
					//echo $message_id = str_replace(array("<", ">"), "", $message->getHeader("message-id"));
					}else{
					//echo $message_id = uniqid();
					}*/

					/*$config2 = array('host'     => '74.125.53.108',
					'user'     => 'grader1@oum.edu.my',
					'password' => 'aabbccddee',
					'port' => 993,
					'ssl'      => 'SSL');

					//$mail = new Zend_Mail_Storage_Pop3($config);
					$mail2 = new Zend_Mail_Protocol_Imap('74.125.53.108');*/

					/* connect to gmail */
					$hostname = '{74.125.53.108:993/imap/ssl/novalidate-cert}[Gmail]/Sent Mail';
					$username = 'grader1@oum.edu.my';
					$password = 'aabbccddee';

					/* try to connect */
					/* try to connect */
					$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

					//Imap List
					/*$list = imap_list($inbox, "{74.125.53.108}", "*");
					if (is_array($list)) {
					foreach ($list as $val) {
					echo imap_utf7_decode($val) . "\n";
					}
					} else {
					echo "imap_list failed: " . imap_last_error() . "\n";
					}*/

					/*imap_close($inbox);
					exit();*/

					/* grab emails */
					echo $message->references;
					$emails = imap_search($inbox, 'TEXT "'.$message->references.'"',SE_UID);

					print_r($emails);

					foreach ($emails as $email)
					{
						echo $msgNO = $mail->getNumberByUniqueId($email);
						$message2 = $mail->getMessage($msgNO);
						echo "Content: " . $message2->getContent() . "<br><br>";
						$message->getContent();
					}

					/* close the connection */
					imap_close($inbox);
					//echo $mail->getUniqueId($message->getHeader("message-id"));
					//$message2 = $mail->getMessage($currentMessageId);

					/*foreach ($message->getHeaders() as $name => $value) {
					if (is_string($value)) {
					echo "$name: $value\n";
					continue;
					}
					foreach ($value as $entry) {
					echo "$name: $entry\n";
					}
					}*/
				}
				else 
				{
					continue;
				}
			
			}
			else
			{
				echo "message num : " .$messageNum;
				
				//$mail->setFlags($messageNum, array(Zend_Mail_Storage::FLAG_SEEN));
				
				if( isset($message->references))
				{
					$part = $message;
					//print_r($part);
					while ($part->isMultipart()) 
					{
						$part = $message->getPart(1);
					}
					
					echo 'Type of this part is ' . strtok($part->contentType, ';') . "\n";
					echo "Content:\n";
					$replytoa=$part->getContent();

					// output first text/plain part
					/*$foundPart = null;
					foreach (new RecursiveIteratorIterator($mail->getMessage($messageNum)) as $part) {
					try {
					if (strtok($part->contentType, ';') == 'text/plain') {
					$foundPart = $part;
					break;
					}
					} catch (Zend_Mail_Exception $e) {
					// ignore
					}
					}
					if (!$foundPart) {
					echo 'no plain text part found';
					} else {
					$replytoa= $foundPart;
					}*/

					//$replytoa =$message->getContent();
					/*if($message->headerExists("message-id")){
					//echo $message_id = str_replace(array("<", ">"), "", $message->getHeader("message-id"));
					}else{
					//echo $message_id = uniqid();
					}*/

					/*$config2 = array('host'     => '74.125.53.108',
					'user'     => 'grader1@oum.edu.my',
					'password' => 'aabbccddee',
					'port' => 993,
					'ssl'      => 'SSL');

					//$mail = new Zend_Mail_Storage_Pop3($config);
					$mail2 = new Zend_Mail_Protocol_Imap('74.125.53.108');*/

					/* connect to gmail */
					$hostname = '{74.125.53.108:993/imap/ssl/novalidate-cert}[Gmail]/Sent Mail';
					$username = 'grader1@oum.edu.my';
					$password = 'aabbccddee';

					/* try to connect */
					/* try to connect */
					$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

					//Imap List
					/*$list = imap_list($inbox, "{74.125.53.108}", "*");
					if (is_array($list)) {
					foreach ($list as $val) {
					echo imap_utf7_decode($val) . "\n";
					}
					} else {
					echo "imap_list failed: " . imap_last_error() . "\n";
					}*/

					/*imap_close($inbox);
					exit();*/

					/* grab emails */
					//echo "bawah =".$message->references;
					$emails = imap_search($inbox, 'TEXT "'.$message->references.'"',SE_UID);

					//$emails = imap_search($inbox, 'ALL',SE_UID);


					//print_r($emails);
					foreach ($emails as $email)
					{
						//echo $email;
						$msgno = imap_msgno( $inbox, $email );

						echo imap_qprint(imap_body($inbox, $msgno))."<hr>";
						echo "<pre>".$replytoa."</pre>";
					}
					
					//imap_close($inbox);
				}
				else 
				{
					continue;
				}

			}
		}
	}

	public function framepage1Action()
	{
		$this->view->username = $this->userinfo->username;

		$semid = $this->_getParam ( 'semid', 0 );
		$this->view->semid = $semid;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
	}


}





