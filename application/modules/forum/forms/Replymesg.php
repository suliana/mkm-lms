<?php
class Forum_Form_Replymesg extends Zend_Form
{
	
protected $_courseid;
protected $_semid;
protected $_forumID;
protected $_fmid;
protected $_tid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function setSemid($value)
{
$this->_semid = $value;
}

public function setForumID($value)
{
$this->_forumID = $value;
}
public function setFmid($value)
{
$this->_fmid = $value;
}
public function setTid($value)
{
$this->_tid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$title = $this->createElement('text', 'subject', array('class' => 'inputtext' ));
        $title->setLabel($this->getView()->translate('title').':')->setRequired(true);
        
        
		$forum = new Forum_Model_Forum ();
        
		if ($this->_fmid!=0){
		$rsorder=$forum->getmesg($this->_forumID,$this->_fmid);		
        $title->setValue(" Re : ". $rsorder["subject"]);
		}
        
        $title->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $message = $this->createElement('textarea', 'message');
        $message->setLabel($this->getView()->translate('message').':')->setRequired(true);
        
        $message->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        $courseid->setDecorators(array('ViewHelper'));
        
        $semid = $this->createElement('hidden', 'semid');
        $semid->setValue($this->_semid);
        $semid->setDecorators(array('ViewHelper'));
        
        
        $forumID = $this->createElement('hidden', 'forumID');
        $forumID->setValue($this->_forumID);
        $forumID->setDecorators(array('ViewHelper'));
        

        $fmid = $this->createElement('hidden', 'fmid');
        $fmid->setValue($this->_fmid);
        $fmid->setDecorators(array('ViewHelper'));

        
        $tid = $this->createElement('hidden', 'tid');
        $tid->setValue($this->_tid);
		$tid->setDecorators(array('ViewHelper'));
			
        $this->addElements(
        array($title,$message,$courseid,$semid,$forumID,$fmid,$tid));
        
        //button
		$this->addElement('submit', 'save', array(
		  'class' => 'btn submit',	
          'label'=>$this->getView()->translate('post').' '.$this->getView()->translate('message'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        if ($this->_tid==0) {
        	$ffmid=$this->_fmid;
        } else {
        	$ffmid=$this->_tid;
        }
        
        $this->addElement('submit', 'cancel', array(
          'class' => 'btn',
          'label'=>$this->getView()->translate('cancel'),
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"history.back(); return false;"
        ));
        
        
	    
	    $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       ));
    }
}