<?php
class Forum_Form_Modifytitle extends Zend_Form
{
	
protected $_courseid;
protected $_semid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function setSemid($value)
{
$this->_semid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$title = $this->createElement('text', 'title', array('class' => 'inputtext') );
        $title->setLabel($this->getView()->translate('title').':')->setRequired(true);
        
        $title->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $description = $this->createElement('textarea', 'description');
        $description->setLabel($this->getView()->translate('description').':')->setRequired(true);
        
        $description->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        
        $courseid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $semid = $this->createElement('hidden', 'semid');
        $semid->setValue($this->_semid);
        
        $semid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
                
        $forum = new Forum_Model_Forum ();
        $result =  $forum->getcategory($this->_courseid,$this->_semid);     		
		$catid = new Zend_Form_Element_Select('catid');
		
		$catid->setLabel($this->getView()->translate('category').':')
		->setRequired(true);
		foreach ($result as $c) {
		$catid->addMultiOption($c['id'], $c['name']);
		}
		
		$catid->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
			
        $this->addElements(
        array($title, $description, $catid, $courseid, $semid));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('title'),
		  'class' => 'btn submit',
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'class' => 'btn',
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'forum', 'controller'=>'index','action'=>'listforum','courseid' => $this->_courseid, 'semid' => $this->_semid),'default',true) . "'; return false;"
        ));
        
        
	    
	    $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       ));
    }
}