<?php
class Forum_Form_Modifymesg extends Zend_Form
{
	
protected $_courseid;
protected $_semid;
protected $_forumID;
protected $_fmid;
protected $_tid;


public function setCourseid($value)
{
$this->_courseid = $value;
}

public function setSemid($value)
{
$this->_semid = $value;
}

public function setForumID($value)
{
$this->_forumID = $value;
}
public function setFmid($value)
{
$this->_fmid = $value;
}
public function setTid($value)
{
$this->_tid = $value;
}

public function init ()
    {
        
    	$this->setMethod('post');
    	
    	$title = $this->createElement('text', 'subject', array('class'=>'inputtext'));
        $title->setLabel($this->getView()->translate('title').':')->setRequired(true);
        
        $title->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $message = $this->createElement('textarea', 'message');
        $message->setLabel($this->getView()->translate('message').':')->setRequired(true);
        
        $message->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $courseid = $this->createElement('hidden', 'courseid');
        $courseid->setValue($this->_courseid);
        
        $semid = $this->createElement('hidden', 'semid');
        $semid->setValue($this->_semid);
        
        $forumID = $this->createElement('hidden', 'forumID');
        $forumID->setValue($this->_forumID);
    
        $fmid = $this->createElement('hidden', 'fmesgID');
        $fmid->setValue($this->_fmid);
     
        $tid = $this->createElement('hidden', 'tid');
        $tid->setValue($this->_tid);
      
                
        
			
        $this->addElements(array($title,$message,$courseid,$semid,$forumID,$fmid,$tid));
        
        //button
		$this->addElement('submit', 'save', array(
          'label'=>$this->getView()->translate('modify').' '.$this->getView()->translate('message'),
          'class' => 'btn submit',
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td',
               'align'=>'left', 'openOnly'=>true)),
               array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'openOnly'=>true))
               )
        ));
        
        if ($this->_tid==0) {
        	$ffmid=$this->_fmid;
        } else {
        	$ffmid=$this->_tid;
        }
        $this->addElement('submit', 'cancel', array(
          'label'=>$this->getView()->translate('cancel'),
          'class' => 'btn',
          'decorators'=>array(
	          'ViewHelper',
               'Description',
               'Errors', 
               array(array('data'=>'HtmlTag'), array('tag' => 'td' ,
               'closeOnly'=>true)),
               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
               ),
          'onClick'=>"window.location ='" . $this->getView()->url(array('module'=>'forum', 'controller'=>'index','action'=>'showmesg','courseid' => $this->_courseid, 'semid' => $this->_semid, 'forumID' => $this->_forumID, 'fmid' => $ffmid, 'tid' => 0),null,true) . "'; return false;"
        ));
        
        
	    
	    $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

  

       ));
    }
}