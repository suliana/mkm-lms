<?php
class Forum_Model_Forum extends Zend_Db_Table
{
	protected $_forum_cat = "forumcat";
	protected $_forum_setup = "forumsetup";
	protected $_forum_main = "forummain";
	protected $_forum_user = "tagginguser";
	protected $_forum_mesg = "forummesg";
	protected $_forum_read = "forumread";
	protected $_table_users = "users";

	/**
	 * Dapatkan category. Create category kalo tak ada
	 *
	 * @return $result
	 */
	public function getcategory($courseid="",$semid="",$name=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()->from($this->_forum_cat);
		if ($courseid != "") {
			$sql->where('courseid = ?', $courseid);
		}
		if ($semid != "") {
			$sql->where('semid = ?', $semid);
		}
		if ($name != "") {
			$sql->where('name = ?', $name);
		}
		$sql->order('id ASC');
		$result = $this->_db->fetchAll($sql);

		return $result;
	}


	/**
	 * Get Forum. 
	 *
	 * @return $result
	 */
	public function getforum($catid,$courseid="",$semid="")
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('p' => $this->_forum_main),
		array('forumID','title', 'description','createby','dtcreated', 'counter','password','newthread', 'lockforum','StartStatus','DateStart', 'EndStatus','DateEnd','target', 'starget'));
		$sql->where('catid = ?', $catid);

		if ($courseid != "") {
			$sql->where('courseid = ?', $courseid);
		}
		if ($semid != "") {
			$sql->where('semid = ?', $semid);
		}
		$sql->order('forumID ASC');
		$result = $this->_db->fetchAll($sql);

		//echo $sql;
		return $result;


	}

	/**
	 * Add Forum Category
	 *
	 */
	public function checkaddcategory($courseid="",$semid="",$addnewcategory=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');


		$sql = $this->_db->select()->from($this->_forum_cat);
		if ($courseid != "") {
			$sql->where('courseID = ?', $courseid);
		}
		if ($semid != "") {
			$sql->where('semid = ?', $semid);
		}
		if ($addnewcategory != "") {
			$sql->where('name LIKE ?', $addnewcategory.'%');
		}
		
	
		
		$result = $this->_db->fetchAll($sql);

		return $result;

	}

	/**
	 * Edit Forum Category
	 *
	 */
	public function checkeditcategory($courseid="",$semid="",$catid="",$category=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()->from($this->_forum_cat);
		if ($courseid != "") {
			$sql->where('courseid = ?', $courseid);
		}
		if ($semid != "") {
			$sql->where('semid = ?', $semid);
		}
		if ($category != "") {
			$sql->where('name = ?', $category);
		}
		if ($catid != "") {
			$sql->where('id <> ?', $catid);
		}
		$result = $this->_db->fetchAll($sql);

		//echo $sql;
		//exit();
		return $result;

	}

	public function upload ($tablename,$data)
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$this->_db->insert($tablename, $data);
	}

	public function delcategory ($courseid,$semid,$catid)
	{

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_main),
		array('forumID'))
		->join(array('b'=>$this->_forum_cat),'a.catID=b.id');
		if ($courseid != "") {
			$sql->where('b.courseID = ?', $courseid);
		}
		if ($semid != "") {
			$sql->where('b.semid = ?', $semid);
		}
		if ($catid != "") {
			$sql->where('b.id = ?', $catid);
		}

		$result = $this->_db->fetchAll($sql);

		if ($result){
			//echo "here";

			foreach ($result as $row) {

				$multidb = Zend_Registry::get("multidb");
				$this->_db = $multidb->getDb('db1');

				$sql2 = $this->_db->select()->from(array('a'=>$this->_forum_mesg),
				array('fmesgID'));
				$sql2->where('a.forumID = ?', $row["forumID"]);

				$result2 = $this->_db->fetchAll($sql2);


				if ($result2){

					foreach ($result2 as $row2) {

						$multidb = Zend_Registry::get("multidb");
						$this->_db = $multidb->getDb('db2');


						$sql3 = $this->_db->select()->from(array('a'=>$this->_forum_read."_".$courseid),
						array('fmesgid'));
						$sql3->where('a.fmesgid = ?', $row2["fmesgID"]);

						$result3 = $this->_db->fetchAll($sql3);


						if ($result3){

							foreach ($result3 as $row3) {


								$this->delete(DBASE_NAME_TRACK.".".$this->_forum_read."_".$courseid,$row3["fmesgid"],"fmesgid");

							}
						}

						$this->delete($this->_forum_mesg,$row2["fmesgID"],"fmesgID");



					}
				}

				$this->delete($this->_forum_main,$row["forumID"],"forumID");

			}


		}
		$condition = array(
		'courseid = ?' => $courseid,
		'semid = ?' => $semid,
		'id = ?' => $catid
		);
		$this->_db->delete($this->_forum_cat,  $condition);

	}

	public function delete ($tablename,$id,$fields="id")
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		if ($id != "") {
			$condition = array(
			$fields .' = ?' => $id
			);
			$this->_db->delete($tablename,  $condition);
		}


	}

	public function fetchAll ($tablename,$courseid="",$semid="")
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()->from($tablename);
		if ($courseid != "") {
			$sql->where('courseid = ?', $courseid);
		}
		if ($semid != "") {
			$sql->where('semid = ?', $semid);
		}

		$stmt = $this->_db->fetchAll($sql);
		/*echo $sql;
		exit();*/
		return $stmt;
	}

	/**
	 * Get Category Name
	 *
	 * @return $stmt["name"]
	 */
	public function getcatname($catid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_cat),
		array('name'));
		$sql->where('id = ?', $catid);
		$stmt = $this->_db->fetchRow($sql);
		return $stmt["name"];
	}

	public function modify ($tablename,$data, $id, $fields="id")
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$this->_db->update($tablename, $data, $fields .' = ' . (int) $id);
	}


	public function fetch ($tablename,$id = "",$fields="id")
	{

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()->from($tablename);
		if ($id != "") {
			$sql->where($fields .' = ?', $id);
		}
		//echo $sql;
		$result = $this->_db->fetchRow($sql);
		//print_r($result);
		//exit();
		return $result;
	}



	/**
	 * Get First List
	 *
	 * @return $stmt
	 */
	public function getfirstlist($forumID=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('message','fmesgID','threadID','subject','dateposted','forumID','threadparent','userid','counter'))
		->joinLeft(array('b' => $this->_table_users),
		'b.id = a.userid',
		array('firstname','lastname'));

		$sql->where('forumID = ?', $forumID);
		$sql->where('threadparent = ?', 0);
		$sql->order('fmesgID DESC');
		
	
		
		$stmt = $this->_db->fetchAll($sql);


		return $stmt;

	}

	/**
	 * Get Last Sender
	 *
	 */
	public function getlastsender($threadID){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('username'));
		$sql->joinLeft(array('b' => $this->_table_users),'b.id=a.userid',array('b.firstname', 'b.lastname'));
			
		$sql->where('fmesgID = ?', $threadID);
		$result = $this->_db->fetchRow($sql);
		return $result["firstname"]." ".$result['lastname'];

	}

	/**
	 * Get Last Post
	 *
	 */
	public function getlastpost($threadID){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('MAX(dateposted) as lastpost'));
		$sql->where('threadID = ?', $threadID);
		$result = $this->_db->fetchRow($sql);
		return $result["lastpost"];

	}

	/**
	 * Dapatkan Last Post ID
	 *
	 */
	public function getlastpostid($ID,$fields="threadID"){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('MAX(fmesgid) as lastpostid'));
		$sql->where($fields.' = ?', $ID);
		$result = $this->_db->fetchRow($sql);
		return $result["lastpostid"];
	}


	/**
	 * Masukkan dalam $this->table_read
	 *
	 */
	public function inserttoforumread($courseid,$ftid,$username){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_read."_".$courseid),
		array('id'));
		$sql->where('fmesgid = ?', $ftid);
		$sql->where('userid = ?', $username);


		$result = $this->_db->fetchRow($sql);


		if(!$result){
			$data['userid']=$username;
			$data['fmesgid']=$ftid;
			$this->upload(DBASE_NAME_TRACK.".".$this->_forum_read."_".$courseid,$data);

		}
	}

	/**
	 * Check New
	 *
	 */
	public function checknew($courseid,$ftid,$username){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_read."_".$courseid),
		array('id'));
		$sql->where('fmesgid = ?', $ftid);
		$sql->where('userid = ?', $username);

		//echo $sql;
		//exit();
		$result = $this->_db->fetchRow($sql);


		if($result){
			return false;
		}else{
			return true;
		}


	}


	/**
	 * Nak Display Child Yang bawah Parent Message. Tetapi buat looping dulu
	 *
	 */
	public function displaychild($forumID)
	{

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('fmesgID','subject','message','userid','dateposted','forumID','threadID','threadparent','counter','filename','pfilename'));
		$sql->joinLeft(array('b' => $this->_table_users),'b.id=a.userid',array('b.photo','b.firstname', 'b.lastname'));
		
		$sql->where('threadparent <> ?', 0);
		$sql->where('forumID = ?', $forumID);
		$sql->order('fmesgID ASC');

		//echo $sql;
		$result = $this->_db->fetchAll($sql);

		return $result;


	}


	/**
	 * Count Message Based on forumID dan $mysql_forum_read
	 *
	 */
	public function getreadforum($forumID,$courseid,$userid,$chkthread=false){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('count(a.fmesgID) as bil'));
		$sql->join(array('b' => DBASE_NAME_TRACK.".".$this->_forum_read."_".$courseid),
		'b.fmesgid = a.fmesgID');
		if($chkthread){
			/*$sql->where('threadID = ?', $forumID);
			$sql->orwhere('a.fmesgID = ?', $forumID);*/
			$sql->where("threadID = $forumID OR a.fmesgID = $forumID");
		}
		else {
			$sql->where('forumID = ?', $forumID);
		}
		$sql->where('b.userid = ?', $userid);

		//echo $sql;
		//exit();
		$result = $this->_db->fetchRow($sql);



		return $result['bil'];
	}

	/**
	 * Get Forum Main
	 *
	 */
	public function getforummain($fid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from($this->_forum_main);
		$sql->where('forumID = ?', $fid);

		//echo $sql;
		$result = $this->_db->fetchAll($sql);

		return $result;
	}

	/**
	 * Get Message
	 *
	 */
	public function getmesg($fid,$fmid,$tid=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		if ($tid==""){
			$sql = $this->_db->select()
			->from(array('a' => $this->_forum_mesg));
			$sql->joinLeft(array('b' => $this->_table_users),'b.id=a.userid',array('b.photo','b.username', 'b.firstname', 'b.lastname'));
			$sql->where('a.forumID = ?', $fid);
			$sql->where('a.fmesgID = ?', $fmid);
			$sql->order('a.fmesgID ASC');


		}
		else {

			$sql = $this->_db->select()
			->from(array('a' => $this->_forum_mesg));
			$sql->joinLeft(array('b' => $this->_table_users),'b.id=a.userid',array('b.photo', 'b.username', 'b.firstname', 'b.lastname'));
			$sql->where('a.forumID = ?', $fid);
			$sql->where('a.fmesgID = ?', $tid);

		}
		//echo $sql;
		//exit();
		$result = $this->_db->fetchRow($sql);
	
		
		return $result;
	}

	/**
	 * Reply Kepada siapa
	 *
	 * @param int $reply
	 */
	public function getreplyfor($reply){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('userid','dateposted'));
		$sql->where('fmesgID = ?', $reply);

		$result = $this->_db->fetchRow($sql);

		return $result;

	}

	/**
	 * Count First List
	 *
	 */
	public function countfirstlist($forumID){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('count(fmesgID) as bil'));
		$sql->where('forumID = ?', $forumID);
		$sql->where('threadparent = ?', 0);

		$result = $this->_db->fetchRow($sql);
		return $result['bil'];

	}

	/**
	 * Count Message Based on forumID
	 *
	 */
	public function getmainpost($forumID){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('count(fmesgID) as bil'));
		$sql->where('forumID = ?', $forumID);

		$result = $this->_db->fetchRow($sql);
		return $result['bil'];

	}

	/**
	 * Get Last List
	 *
	 */
	public function getlastlist($fmesgid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('message','fmesgID','threadID','subject','dateposted','forumID','threadparent','userid','counter'))
		->joinLeft(array('b' => $this->_table_users),
		'b.id = a.userid',
		array('firstname','lastname'));

		$sql->where('fmesgID = ?', $fmesgid);

		$result = $this->_db->fetchRow($sql);
		return $result;

	}

	

	/**
	 * Update Counter
	 *
	 */
	public function updatecounter($fmid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$condition = array(
		'counter' => new Zend_Db_Expr( 'counter+1' )

		);
		$this->_db->update($this->_forum_mesg, $condition, ' fmesgID = ' . (int) $fmid);


	}

	/**
	 * Check Whether Under This Forum Got Message Under It
	 *
	 * @return true/false
	 */
	public function checkmessage($fmid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_mesg),
		array('fmesgID'));
		$sql->where('threadparent = ?', $fmid);

		$result = $this->_db->fetchRow($sql);



		if ($result) return true;
		else return false;
	}

	/**
	 * Delete Message
	 *
	 */
	public function delmesg($fmid,$courseid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		//$this->conn->debug=1;

		$this->delete($this->_forum_mesg,$fmid,"fmesgid");
		$this->delete(DBASE_NAME_TRACK.".".$this->_forum_read."_".$courseid,$fmid,"fmesgid");



	}

	public function deletetitle ($forumID,$courseid,$semid)
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql2 = $this->_db->select()->from(array('a'=>$this->_forum_mesg),
		array('fmesgID'));
		$sql2->where('a.forumID = ?', $forumID);

		$result2 = $this->_db->fetchAll($sql2);


		if ($result2){

			foreach ($result2 as $row2) {

				$multidb = Zend_Registry::get("multidb");
				$this->_db = $multidb->getDb('db2');

				$sql3 = $this->_db->select()->from(array('a'=>$this->_forum_read."_".$courseid),
				array('fmesgid'));
				$sql3->where('a.fmesgid = ?', $row2["fmesgID"]);

				$result3 = $this->_db->fetchAll($sql3);


				if ($result3){

					foreach ($result3 as $row3) {


						$this->delete(DBASE_NAME_TRACK.".".$this->_forum_read."_".$courseid,$row3["fmesgid"],"fmesgid");

					}
				}

				$this->delete($this->_forum_mesg,$row2["fmesgID"],"fmesgID");



			}
		}

		$this->delete($this->_forum_main,$forumID,"forumID");




	}

	/**
	 * Check Forum User
	 *
	 * @return true/false
	 */
	public function checkforumuser_ori($userid,$courseid,$intakeid="",$privilage=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		if ($privilage=="COURSE COORDINATOR" OR $privilage=="ADMIN"){
			$sql = $this->_db->select()
			->from($this->_forum_setup);

			$sql->where('courseid = ?', $courseid);
		}
		else {
			$sql = $this->_db->select()
			->from(array('a'=>$this->_forum_user),
			array('semID'))
			->join(array('b'=>$this->_forum_setup),'a.semID=b.id');



			$sql->where('userID = ?', $userid);

			$sql->where('courseid = ?', $courseid);

			if ($intakeid != "") {
				//echo $keyword;
				$sql->where(
				' intakeid = ? ', $intakeid );
			}

		}
		//echo $sql;
		//exit();
		$result = $this->_db->fetchAll($sql);

		return $result;
	}

	/**
	 * Check Table ForumRead
	 *
	 * @return $rs->fields["status"]/false
	 */
	public function checktableforumread($courseid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db2');

		$forumreadtable = $this->_forum_read."_$courseid";
		$sql = "create table IF NOT EXISTS $forumreadtable (
		id bigint(20) NOT NULL auto_increment,
		userid varchar(100) NOT NULL default '',
		fmesgid bigint(9) NOT NULL default '0',
		PRIMARY KEY  (id),
		KEY username (userid),
		KEY fmesgid (fmesgid)
		)";
		$this->_db->query($sql);
	}

	public function getlastinsertid ($tablename)
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$id=$this->_db->lastInsertId($tablename);
		return $id;
	}

	public function modifyfromcontent ($tablename,$data, $id, $fields="id")
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$this->_db->update($tablename, $data, $fields ." = '" . $id . "'");
	}

	/*public function gettutor($semid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_setup),
		array('id'))
		->join(array('b' => $this->_table_users),
		'b.studentID = a.tutoric',
		array('username'));

		$sql->where('a.id = ?', $semid);

		$result = $this->_db->fetchRow($sql);
		return $result;

	}*/

	/**
	 * Check Forum User
	 *
	 * @return true/false
	 */
	public function checkforumuser($userid,$courseid,$intakeid="",$privilage=""){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from($this->_forum_setup);

		$sql->where('courseid = ?', $courseid);

		if ($intakeid != "") {
			//echo $keyword;
			$sql->where(
			' intakeid = ? ', $intakeid );
		}


		$sql->order('id ASC');


		//echo $sql;
		//exit();
		//$result = $this->_db->fetchAll($sql);

		return $sql;
	}

	public function listuser ($semid)
	{
		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_user),
		array('semID','userID'))
		->join(array('b'=>$this->_table_users),'a.userID=b.username',
		array('firstname','lastname','privilage','telephone_number'));


		$sql->where('semID = ?', $semid);
		$sql->order(array('privilage DESC',
		'firstname ASC'));


		$stmt = $this->_db->fetchAll($sql);
		/*echo $sql;
		exit();*/
		return $stmt;
	}

	/**
	 * Count Forum USer
	 *
	 * @param int $semid.
	 * @return $rs
	 */
	public function countforumuser($semid){

		$multidb = Zend_Registry::get("multidb");
		$this->_db = $multidb->getDb('db1');

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_user),
		array('count(distinct userID) as biluser'));

		$sql->where('semID = ?', $semid);


		$rs = $this->_db->fetchRow($sql);

		return $rs;
	}




	////////




}
?>
