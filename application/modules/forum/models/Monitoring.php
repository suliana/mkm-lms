<?php
class Forum_Model_Monitoring extends Zend_Db_Table
{
	protected $_forum_cat = "forumcat";
	protected $_forum_setup = "taggingsetup";
	protected $_forum_main = "forummain";
	protected $_forum_user = "tagginguser";
	protected $_forum_mesg = "forummesg";
	protected $_forum_read = "forumread";
	protected $_table_users = "users";
	protected $_table_eos = "coursefacilitator";


	public function countforummesg($cid,$userid,$tutorgroupid="",$forumid="",$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from($this->_forum_main);

		$sql->where('courseid = ?', $cid);

		if ($tutorgroupid != "") {
			$sql->where('groupid = ?', $tutorgroupid);
		}
		if ($forumid != "") {
			$sql->where('forumID = ?', $forumid);
		}

		//exit();
		$stmt = $this->_db->fetchAll($sql);

		$i=0;
		if($stmt){
			foreach ($stmt as $row)
			{
				$sql2 = $this->_db->select()
				->from(array('p' => $this->_forum_mesg),
				array('count(fmesgID) as bil'));

				$sql2->where('forumID = ?', $row['forumID']);

				$sql2->where('userid = ?', $userid);

				if ($useDate==1){
					$sql2->where('dateposted >= ?', $start_date_post);
					$sql2->where('dateposted < ?', $end_date_post);
				}

				$stmt2 = $this->_db->fetchRow($sql2);
				$i = $i +$stmt2['bil'];


			}
		}


		return $i;


	}

	public function viewforummesg($cid,$userid,$tutorgroupid="",$forumid="",$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from($this->_forum_main);

		$sql->where('courseid = ?', $cid);

		if ($tutorgroupid != "") {
			$sql->where('groupid = ?', $tutorgroupid);
		}
		if ($forumid != "") {
			$sql->where('forumID = ?', $forumid);
		}

		$rs = $this->_db->fetchAll($sql);
		
		//echo $sql;
		
		$i=0;
		foreach ($rs as $row){
			$forumid[$i]=$row['forumID'];
			
			$i++;
		}
		
		return $forumid;

		

	}




	/**
	 * Search EOS
	 *
	 */	
	public function searcheos($semid,$cid,$keyworduser){

		$sql = $this->_db->select()
		->from(array('a'=>$this->_forum_user),
		array('userID'))
		->join(array('b' => $this->_forum_setup),
		'b.id = a.groupID',
		array('courseid','b.id'))
		->join(array('c' => $this->_table_eos),
		'c.username = a.userID',
		array('intakeid'))
		->join(array('d' => $this->_table_users),
		'd.username = a.userID',
		array('firstname','lastname','studentID'))
		;

		if ($keyworduser != "") {
			//echo $keyword;
			$sql->where(
			'  ( firstName LIKE ? ',
			'%' . $keyworduser . '%');
			$sql->orwhere(
			'   lastName LIKE ? ',
			'%' . $keyworduser . '%');
			$sql->orwhere(
			'   d.username LIKE ? ',
			'%' . $keyworduser . '%');
			$sql->orwhere(
			'   studentID LIKE ? )',
			'%' . $keyworduser . '%');


		}

		$sql->where('c.intakeid = ?', $semid);
		if ($cid!='')
		$sql->where('b.courseid = ?', $cid);

		$stmt = $this->_db->fetchAll($sql);

		return $stmt;


	}

	public function viewforummesg_sub($userid="",$forumid="",$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from($this->_forum_mesg);

		if ($userid != "") {
			$sql->where('userid = ?', $userid);
		}
		if ($forumid != "") {
			$sql->where(
				' forumID IN  (?) ', $forumid );
		}

		
		if ($useDate==1){
				$sql->where('dateposted >= ?', $start_date_post);
				$sql->where('dateposted <= ?', $end_date_post);


		}
		
		$sql->order('forumID ASC');
		
		//echo $sql;
		//exit();
		
		return $sql;
		
	}
	
	public function viewforummesg_stud($userid="",$forumid="",$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from($this->_forum_mesg);

		if ($userid != "") {
			$sql->where('userid <> ?', $userid);
		}
		if ($forumid != "") {
			$sql->where(
				' forumID IN  (?) ', $forumid );
		}

		
		if ($useDate==1){
				$sql->where('dateposted >= ?', $start_date_post);
				$sql->where('dateposted <= ?', $end_date_post);


		}
		
		$sql->order('forumID ASC');
		
		//echo $sql;
		//exit();
		
		return $sql;
		
	}


	public function countforummesgbystudent($cid,$userid,$tutorgroupid="",$forumid="",$useDate="",$start_date_post="",$end_date_post=""){


		$sql = $this->_db->select()
		->from($this->_forum_main);

		$sql->where('courseid = ?', $cid);

		if ($tutorgroupid != "") {
			$sql->where('groupid = ?', $tutorgroupid);
		}
		if ($forumid != "") {
			$sql->where('forumID = ?', $forumid);
		}

		//exit();
		$stmt = $this->_db->fetchAll($sql);

		$i=0;
		if($stmt){
			foreach ($stmt as $row)
			{
				$sql2 = $this->_db->select()
				->from(array('p' => $this->_forum_mesg),
				array('count(fmesgID) as bil'));

				$sql2->where('forumID = ?', $row['forumID']);

				$sql2->where('userid != ?', $userid);

				if ($useDate==1){
					$sql2->where('dateposted >= ?', $start_date_post);
					$sql2->where('dateposted < ?', $end_date_post);
				}

				$stmt2 = $this->_db->fetchRow($sql2);
				$i = $i +$stmt2['bil'];


			}
		}


		return $i;

	}

	
	public function countforummesgtotal($cid,$userid,$tutorgroupid="",$forumid="",$useDate="",$start_date_post="",$end_date_post=""){

		$sql = $this->_db->select()
		->from($this->_forum_main);

		$sql->where('courseid = ?', $cid);

		if ($tutorgroupid != "") {
			$sql->where('groupid = ?', $tutorgroupid);
		}
		if ($forumid != "") {
			$sql->where('forumID = ?', $forumid);
		}

		//exit();
		$stmt = $this->_db->fetchAll($sql);

		$i=0;
		if($stmt){
			foreach ($stmt as $row)
			{
				$sql2 = $this->_db->select()
				->from(array('p' => $this->_forum_mesg),
				array('count(fmesgID) as bil'));

				$sql2->where('forumID = ?', $row['forumID']);


				if ($useDate==1){
					$sql2->where('dateposted >= ?', $start_date_post);
					$sql2->where('dateposted < ?', $end_date_post);
				}

				$stmt2 = $this->_db->fetchRow($sql2);
				$i = $i +$stmt2['bil'];


			}
		}


		return $i;


	}

	


}
?>