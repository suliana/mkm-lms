<?php
class Announcement_CourseannController extends Zend_Controller_Action {

	public function init() 
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('course');
		
		$this->view->course_tools = 1;
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
		//activate tab
		$this->view->active = 'admin';
	}


	public function listAction()
	{
		$username = $this->userinfo->username;
		$this->view->username= $username;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$keywords = $this->_getParam ( 'keywords', '' );
		$this->view->keywords = $keywords;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('announcement')." > ".$rscourse['coursecode'];

		$annDB =  new Application_Model_Announcement();
		$condition = array('keywords'=>$keywords,'type'=>2,'courseid'=>$courseid,'target'=>$this->privilage,'order'=>'start_date desc'); //course annoucement
		$list_announcement = $annDB->listannouncement($condition);
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $list_announcement );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		
		
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('keywords'=>$keywords,'type'=>2,'courseid'=>$courseid,'order'=>'start_date desc');

	}


	public function addAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('announcement')." > ".$rscourse['coursecode'];


		$form = new Announcement_Form_Announcement ( array ('courseid' => $courseid ) );
		$this->view->form = $form;

		if ($this->getRequest ()->isPost ()) 
		{
			if ($form->isValid ( $_POST ))
			{
				$formdata = $form->getValues ();

				$formdata["type"]=2;
				$formdata["datecreated"]=date('Y-m-d h:i:s');
				$formdata["createby"]=$this->userinfo->username;
				$formdata['coursecode'] = $rscourse['coursecode'];

				//print_r($formdata);

				if (is_array($formdata ["filename"])){

					//print_r($formdata ["filename"]);

					$i=1;
					foreach ($formdata ["filename"] as $key => $value){

						$locationFile = $form->filename->getFileName ($key);

						$formdata ["filename".$i] = date ( 'Ymdhs' ) . "_" . $value;
						//echo $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $formdata ["filename".$i];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
						$i++;

					}
				} else {

					if ($formdata ["filename"]!=""){
						$locationFile = $form->filename->getFileName ();
						$formdata ["filename1"] = date ( 'Ymdhs' ) . "_" . $formdata ["filename"];
						//echo $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $formdata ["filename1"];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
					}

				}

				unset($formdata ["filename"]);
				//exit();
				$annDB =  new Application_Model_Announcement();
				$annDB->add($formdata);

				$this->_redirect ( 'announcement/courseann/list/courseid/'.$formdata["courseid"]);
			}
		}
	}

	public function modifyAction()
	{	
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		$course = new Application_Model_Course ();
		$rscourse = $course->fetch($courseid);
		$this->view->course = $rscourse;
		
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('announcement')." > ".$rscourse['coursecode'];

		$annDB =  new Application_Model_Announcement();
		$datann = $annDB->fetch($id);

		$form = new Announcement_Form_Modifyannouncement ( array ('id' => $id,'courseid' => $courseid ) );
		$this->view->form = $form;

		if ($this->getRequest ()->isPost ())
		{
			if ($form->isValid ( $_POST )) 
			{
				$formdata = $form->getValues ();

				$formdata["datemodified"]=date('Y-m-d h:i:s');
				$formdata["modifyby"]=$this->userinfo->username;

				//print_r($formdata);

				if (is_array($formdata ["filename"])){

					$formdata ["filename1"] = null;
					$formdata ["filename2"] = null;
					$formdata ["filename3"] = null;
						
					$fullPathNameFile1 = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $datann ["filename1"];

					if (is_file($fullPathNameFile1) == TRUE)
					unlink($fullPathNameFile1);

					$fullPathNameFile2 = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $datann ["filename2"];

					if (is_file($fullPathNameFile2) == TRUE)
					unlink($fullPathNameFile2);

					$fullPathNameFile3 = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $datann ["filename3"];

					if (is_file($fullPathNameFile3) == TRUE)
					unlink($fullPathNameFile3);

					//print_r($formdata ["filename"]);

					$i=1;
					foreach ($formdata ["filename"] as $key => $value)
					{
						$locationFile = $form->filename->getFileName ($key);

						$formdata ["filename".$i] = date ( 'Ymdhs' ) . "_" . $value;
						//echo $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $formdata ["filename".$i];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
						$i++;

					}
				} 
				else
				{
					if ($formdata ["filename"]!="")
					{

						$formdata ["filename1"] = null;
						$formdata ["filename2"] = null;
						$formdata ["filename3"] = null;
						
						$fullPathNameFile1 = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $datann ["filename1"];

						if (is_file($fullPathNameFile1) == TRUE)
						unlink($fullPathNameFile1);

						$fullPathNameFile2 = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $datann ["filename2"];

						if (is_file($fullPathNameFile2) == TRUE)
						unlink($fullPathNameFile2);

						$fullPathNameFile3 = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $datann ["filename3"];

						if (is_file($fullPathNameFile3) == TRUE)
						unlink($fullPathNameFile3);
						

						$locationFile = $form->filename->getFileName ();
						$formdata ["filename1"] = date ( 'Ymdhs' ) . "_" . $formdata ["filename"];
						//echo $data ["filename"];
						$fullPathNameFile = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/' . $formdata["courseid"]. '/' . $formdata ["filename1"];
						// Renommage du fichier
						$filterRename = new Zend_Filter_File_Rename ( array ('target' => $fullPathNameFile, 'overwrite' => true ) );
						$filterRename->filter ( $locationFile );
					}

				}

				unset($formdata ["filename"]);

				$annDB =  new Application_Model_Announcement();
				$annDB->modify($formdata,$formdata["id"]);

				$this->_redirect ( 'announcement/courseann/list/courseid/'.$formdata["courseid"]);

			}
		}else{

			if ($id > 0) {
				$form->populate ($datann   );
			}
		}
	}



	public function previewAction()
	{
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('announcement')." > ".$courseid;

		$annDB =  new Application_Model_Announcement();
		$announcement = $annDB->fetch($id);
		$this->view->announcement = $announcement;

	}


	public function listmoreAction()
	{
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('announcement')." > ".$courseid;

		//course announcement
		$annDB =  new Application_Model_Announcement();
		$condition = array('type'=>2,'courseid'=>$courseid,'target'=>$data->privilage,'orderby'=>'start_date desc'); //course annoucement
		$list_announcement = $annDB->listannouncement($condition);

		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $list_announcement );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('type'=>2,'courseid'=>$courseid,'orderby'=>'start_date asc');

	}


	public function anndetailsAction()
	{
		// disable layouts for this action:
		if( $this->getRequest()->isXmlHttpRequest() )
	  	{
			$this->_helper->layout->disableLayout();
	  	}

		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;

		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;

		$annDB =  new Application_Model_Announcement();
		$datann = $annDB->fetch($id);
		$this->view->announcement = $datann;
	}

	public function deleteAction()
	{

		$id = $this->_getParam ( 'id', 0 );

		$courseid = $this->_getParam ( 'courseid', 0 );

		if ($id > 0) 
		{
			$annDB =  new Application_Model_Announcement();
			$annDB->delete ( $id );
			$this->_redirect ( 'announcement/courseann/list/courseid/'.$courseid );
		}
	}
}
?>