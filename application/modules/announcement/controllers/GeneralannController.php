<?php
class Announcement_GeneralannController extends Zend_Controller_Action {
	
	public function init()
	{
		/* Initialize action controller here */
		//$this->_helper->layout->setLayout('layout');
		
		$storage = Zend_Registry::get('auth')->getStorage();
		$data = $storage->read();
		if (! $data) 
		{
			$this->_redirect('index/index');
		}
		
		$this->userinfo = $data;
		$this->view->username = $data->username;
		$this->view->lastlogin = $data->last_login;
		$this->privilage = $this->userinfo->privilage;
		
	}	
	
  	public function listmoreAction()
  	{
		$this->view->title = $this->view->translate('e_learning')." > ".$this->view->translate('announcement');

		//course announcement
		$annDB =  new Application_Model_Announcement();
		$condition = array('type'=>1,'target'=>$data->privilage,'orderby'=>'start_date desc','showactive'=>'1'); //general annoucement
		$list_announcement = $annDB->listannouncement($condition);
		
		Zend_View_Helper_PaginationControl::setDefaultViewPartial ( 'pagination.phtml' );
		$paginator = Zend_Paginator::factory ( $list_announcement );
		$paginator->setItemCountPerPage ( 20 );
		$paginator->setCurrentPageNumber ( $this->_getParam ( 'page', 1 ) );
		$this->view->paginator = $paginator;
		$this->view->searchParams = array('type'=>1,'target'=>$data->privilage,'orderby'=>'start_date asc');

	}
	
	
	public function anndetailsAction()
	{
		// disable layouts for this action:
        $this->_helper->layout->disableLayout();
			
		
		$id = $this->_getParam ( 'id', 0 );
		$this->view->id = $id;
		
		$courseid = $this->_getParam ( 'courseid', 0 );
		$this->view->courseid = $courseid;
		
		$annDB =  new Application_Model_Announcement();
	    $datann = $annDB->fetch($id);
		$this->view->announcement = $datann;
	}
}
?>