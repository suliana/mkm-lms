<?php
class Announcement_Form_Announcement extends ZendX_JQuery_Form
{
	
	protected $_courseid;
	
	
	public function setCourseid($value)
	{
		$this->_courseid = $value;
	}
	
	public function init ()
    {
       	$this->setMethod('post');
    	

        
        $privilage = new Zend_Form_Element_Select('role');
		$privilage->setOptions(array('class'=>'select'));
        $privilage ->setLabel($this->getView()->translate('Show').' '.$this->getView()->translate('to').' :')->setRequired(true)->addMultiOptions( array('xxx' => '---- '.$this->getView()->translate('please_select').' ----', 'STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','EOS' => 'EOS','ADMIN' => 'ADMIN'));
		//$privilage ->setLabel($this->getView()->translate('Show').' '.$this->getView()->translate('to').' :')->setRequired(true)->addMultiOptions( array('xxx' => '---- '.$this->getView()->translate('please_select').' ----', 'STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','ADMIN' => 'ADMIN'));
		
    	$title = $this->createElement('text', 'title',array('class'=>'inputtext'));
        $title->setLabel($this->getView()->translate('title').' :')->setRequired(true);
     
        $message=$this->createElement(
		    'Textarea',
		    'message',
		    array(
		        'value'      => '',
		        'label'      => $this->getView()->translate('message').' :',
		        'required'	=> true,
		        'style'    => '',
		        //'propercase' => true,
		    )
		);
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('save'))->setIgnore(true);
        $register->setOptions(array('class'=>'btn submit'));
          	
		/*$elem = new ZendX_JQuery_Form_Element_DatePicker('start_date', array(),
	
        			'label' 		=> $this->getView()->translate('start_date').':',
        			'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' => 'yy-mm-dd',
						'minDate'	 => '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "minDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#end_date").datepicker( "option", option, date );
									}')
		)));
		
		

//		$this->addElement($elem);

		$elem2 = new ZendX_JQuery_Form_Element_DatePicker('end_date', array(
					'decorators'=>array(
					   'UiWidgetElement',  // it necessary to include for jquery elements 
					   'Description',
		               'Errors', 
		               array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               		  array('Label', array('tag' => 'td')),
		               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
		               ),
					'label' 		=> $this->getView()->translate('end_date').':',
					'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' 	=> 'yy-mm-dd',
						'minDate'	 	=> '+1',
						'defaultDate' 	=> '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "maxDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#start_date").datepicker( "option", option, date );
									}')

		)));*/
        
        $start_date = $this->createElement('text', 'start_date');
		$start_date->addValidator(new Zend_Validate_Date(array('format' => 'yyyy-MM-dd')));
		$start_date->setValue(Zend_Date::now()->toString('yyyy-MM-dd'));
		$start_date->setLabel($this->getView()->translate('start_date'));
		
		
		$end_date = $this->createElement('text', 'end_date');
		$end_date->addValidator(new Zend_Validate_Date(array('format' => 'yyyy-MM-dd')));
		//$end_date->setValue(Zend_Date::now()->toString('yyyy-MM-dd'));
		$end_date->setLabel($this->getView()->translate('end_date'));
		
		$element = new Zend_Form_Element_File('filename');
        $dirann = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/';

        if(!is_dir($dirann))
        {
        	mkdir($dirann,0775);	
        }
        
        
        $dirann = DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/'.$this->_courseid;
        
        if(!is_dir($dirann))
        {
        	mkdir($dirann,0775);
        }
        
        
        
        $element->setLabel($this->getView()->translate('attachment').':')->setDestination(
        $dirann);
        // ensure minimum 1, maximum 3 files
        /**/$element->addValidator('Count', false, 
        array('min' => 0, 'max' => 3));
        // limit to 100K
        $element->addValidator('Size', false, 20*20*1024);
        // only JPEG, PNG, and GIFs
        $element->addValidator('Extension', false, 'doc,docx,pdf,xls,xlsx,ppt');
        $element->setRequired(false);
        // defines 3 identical file elements
        $element->setMultiFile(3);
        //$form->addElement($element, 'foo');
        
        
        $courseid = $this->createElement('hidden', 'courseid');
		$courseid->setValue($this->_courseid);
		
		$this->addElements(array($privilage, $title, $message, $element, $start_date, $end_date, $courseid, $register,$cancel));
        
        
    }
}