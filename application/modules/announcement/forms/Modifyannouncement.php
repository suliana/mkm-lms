<?php
class Announcement_Form_Modifyannouncement extends ZendX_JQuery_Form
{
	
	protected $_courseid;
	protected $_id;
	
	
	public function setCourseid($value)
	{
	$this->_courseid = $value;
	}
	
	public function setId($value)
	{
	$this->_id = $value;
	}
	
	
	public function init ()
    {
       	$this->setMethod('post');
    	
    	$id = $this->createElement('hidden', 'id');
		$id->setValue($this->_id);
		
		$courseid = $this->createElement('hidden', 'courseid');
		$courseid->setValue($this->_courseid);
        
        $privilage = new Zend_Form_Element_Select('role');
		$privilage->setOptions(array('class'=>'select'));
        $privilage ->setLabel($this->getView()->translate('Show').' '.$this->getView()->translate('to').' :')->setRequired(true)->addMultiOptions( array('xxx' => '---- '.$this->getView()->translate('please_select').' ----', 'STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','EOS' => 'EOS','ADMIN' => 'ADMIN'));
		//$privilage ->setLabel($this->getView()->translate('Show').' '.$this->getView()->translate('to').' :')->setRequired(true)->addMultiOptions( array('xxx' => '---- '.$this->getView()->translate('please_select').' ----', 'STUDENT' => 'STUDENT','COURSE COORDINATOR' => 'COURSE COORDINATOR','ADMIN' => 'ADMIN'));
		
		$privilage->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
		
    	
    	$title = $this->createElement('text', 'title',array('class'=>'inputtext'));
        $title->setLabel($this->getView()->translate('title').' :')->setRequired(true);
        $title->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        
        $message=$this->createElement(
		    'Textarea',
		    'message',
		    array(
		        'value'      => '',
		        'label'      => $this->getView()->translate('message').' :',
		        'required'	=> true,
		        'style'    => 'height: 150px; width:500px',
		        //'propercase' => true,
		    )
		);
        
        
        $message->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td' )),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        $register = $this->createElement('submit', 'register');
        $register->setLabel($this->getView()->translate('save'))->setIgnore(true);
        $register->setOptions(array('class'=>'btn submit'));
        
        $register->setDecorators(array(
					'ViewHelper',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array(array('emptyrow'=>'HtmlTag'), array('placement' => Zend_Form_Decorator_Abstract::PREPEND, 'tag'=>'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
        
        
        
        	
		$elem = new ZendX_JQuery_Form_Element_DatePicker('start_date', array(
				   'decorators'=>array(
				          'UiWidgetElement',  // it necessary to include for jquery elements 
						   'Description',
			               'Errors', 
				   		   array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               			   array('Label', array('tag' => 'td')),	
			               array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
			               ),
	
        			'label' 		=> $this->getView()->translate('start_date').':',
        			'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' => 'yy-mm-dd',
						'minDate'	 => '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "minDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#end_date").datepicker( "option", option, date );
									}')
		)));
		
		

//		$this->addElement($elem);

		$elem2 = new ZendX_JQuery_Form_Element_DatePicker('end_date', array(
					'decorators'=>array(
					   'UiWidgetElement',  // it necessary to include for jquery elements 
					   'Description',
		               'Errors', 
		               array(array('data'=>'HtmlTag'), array('tag' => 'td')),
               		  array('Label', array('tag' => 'td')),
		               array(array('row'=>'HtmlTag'),array('tag'=>'tr', 'closeOnly'=>true))
		               ),
					'label' 		=> $this->getView()->translate('end_date').':',
					'required'		=> true,
					'jQueryParams'	=> array(
						'dateFormat' 	=> 'yy-mm-dd',
						'minDate'	 	=> '+1',
						'defaultDate' 	=> '+1',
						'onSelect'		=> new Zend_Json_Expr('
									function( selectedDate )
									{
										var option = "maxDate";
										instance = $( this ).data( "datepicker" );
										date = $.datepicker.parseDate(
											instance.settings.dateFormat,
											selectedDate, instance.settings );
										$("#start_date").datepicker( "option", option, date );
									}')

		)));

//		$this->addElement($elem);

		$element = new Zend_Form_Element_File('filename');
        
        $dirann=DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/';
        
        if(!is_dir($dirann))
        mkdir($dirann,0775);
        
        
        $dirann=DOC_PATH.'/'.APP_FOLDER.'/upload/announcement/'.$this->_courseid;
        
        if(!is_dir($dirann))
        mkdir($dirann,0775);
        
        
        
        $element->setLabel($this->getView()->translate('attachment').':')->setDestination(
        $dirann);
        // ensure minimum 1, maximum 3 files
        /**/$element->addValidator('Count', false, 
        array('min' => 0, 'max' => 3));
        // limit to 100K
        $element->addValidator('Size', false, 20*20*1024);
        // only JPEG, PNG, and GIFs
        $element->addValidator('Extension', false, 'doc,docx,pdf,xls,xlsx,ppt');
        $element->setRequired(false);
        // defines 3 identical file elements
        $element->setMultiFile(3);
        //$form->addElement($element, 'foo');
        $element->setDecorators(array(
					'File',
 					'Description',
					'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td' )),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))

        ));
		
		
		$this->addElements(
        array($id, $courseid, $privilage, $title, $message, $element, $elem, $elem2, $register,$cancel));
        
        $this->setDecorators(array(

               'FormElements',

               array(array('data'=>'HtmlTag'),array('tag'=>'table')),

               'Form'

       )); 
    }
}