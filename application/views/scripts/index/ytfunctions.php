<?php 
$yt_authkey="AI39si6iZzxXrZZ2mi_eI4smPAQ2hsLFW0kp4lL3PHlvFaQHdrw0J5pm7oC-gIdeyY60S-okO6zyQx6v_EN9bLLUZlMpzqNusA";

Zend_Loader::loadClass('Zend_Gdata_YouTube');
$yt = new Zend_Gdata_YouTube();

Zend_Loader::loadClass('Zend_Gdata_AuthSub');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin'); 

// start a new session 
session_start();

function getAuthSubRequestUrl()
{
    $next = 'http://www.example.com/welcome.php';
    $scope = 'http://gdata.youtube.com';
    $secure = false;
    $session = true;
    return Zend_Gdata_AuthSub::getAuthSubTokenUri($next, $scope, $secure, $session);
}

function getAuthSubHttpClient()
{
    if (!isset($_SESSION['sessionToken']) && !isset($_GET['token']) ){
        echo '<a href="' . getAuthSubRequestUrl() . '">Login!</a>';
        return;
    } else if (!isset($_SESSION['sessionToken']) && isset($_GET['token'])) {
      $_SESSION['sessionToken'] = Zend_Gdata_AuthSub::getAuthSubSessionToken($_GET['token']);
    }

    $httpClient = Zend_Gdata_AuthSub::getHttpClient($_SESSION['sessionToken']);
    return $httpClient;
}

function getAndPrintVideoFeed($location = Zend_Gdata_YouTube::VIDEO_URI)
{
  $yt = new Zend_Gdata_YouTube();
  // set the version to 2 to receive a version 2 feed of entries
  $yt->setMajorProtocolVersion(2);
  $videoFeed = $yt->getVideoFeed($location);
  printVideoFeed($videoFeed);
}
 
function printVideoFeed($videoFeed)
{
  $count = 1;
  foreach ($videoFeed as $videoEntry) {
    echo "Entry # " . $count . "\n";
    printVideoEntry($videoEntry);
    echo "\n";
    $count++;
  }
}

function printVideoEntry($videoEntry) 
{
  // the videoEntry object contains many helper functions
  // that access the underlying mediaGroup object
  /*echo 'Video: ' . $videoEntry->getVideoTitle() . "<hr>";
  echo 'Video ID: ' . $videoEntry->getVideoId() . "<hr>";
  echo 'Updated: ' . $videoEntry->getUpdated() . "<hr>";
  echo 'Description: ' . $videoEntry->getVideoDescription() . "<hr>";
  echo 'Category: ' . $videoEntry->getVideoCategory() . "<hr>";
  echo 'Tags: ' . implode(", ", $videoEntry->getVideoTags()) . "<hr>";
  echo 'Watch page: ' . $videoEntry->getVideoWatchPageUrl() . "<hr>";
  echo 'Flash Player Url: ' . $videoEntry->getFlashPlayerUrl() . "<hr>";
  echo 'Duration: ' . $videoEntry->getVideoDuration() . "<hr>";
  echo 'View count: ' . $videoEntry->getVideoViewCount() . "<hr>";
  echo 'Rating: ' . $videoEntry->getVideoRatingInfo() . "<hr>";
  echo 'Geo Location: ' . $videoEntry->getVideoGeoLocation() . "<hr>";
  echo 'Recorded on: ' . $videoEntry->getVideoRecorded() . "<hr>";*/
  
  // see the paragraph above this function for more information on the 
  // 'mediaGroup' object. in the following code, we use the mediaGroup
  // object directly to retrieve its 'Mobile RSTP link' child
  foreach ($videoEntry->mediaGroup->content as $content) {
    if ($content->type === "video/3gpp") {
      echo 'Mobile RTSP link: ' . $content->url . "<hr>";
    }
  }
  
  echo "Thumbnails:\n";
  $videoThumbnails = $videoEntry->getVideoThumbnails();

  foreach($videoThumbnails as $videoThumbnail) {
    echo $videoThumbnail['time'] . ' - <img src="' . $videoThumbnail['url'].'">';
    echo ' height=' . $videoThumbnail['height'];
    echo ' width=' . $videoThumbnail['width'] . "<hr>";
  }
}

function getVideoId($embedstring){
	preg_match('~youtube.com/v/([0-9a-z_-]+)~i', $embedstring, $matches);
	if ($match[1]=="")
	preg_match('~youtube.com/embed/([0-9a-z_-]+)~i', $embedstring, $matches);
	return $matches[1];	
}
function searchAndPrint($searchTerms = 'puppy')
{
  $yt = new Zend_Gdata_YouTube();
  //var_dump($yt);
  $yt->setMajorProtocolVersion(2);
  $query = $yt->newVideoQuery();
  $query->setOrderBy('viewCount');
  $query->setSafeSearch('none');
  $query->setVideoQuery($searchTerms);

  // Note that we need to pass the version number to the query URL function
  // to ensure backward compatibility with version 1 of the API.
  $videoFeed = $yt->getVideoFeed($query->getQueryUrl(2));
  printVideoFeed($videoFeed, 'Search results for: ' . $searchTerms);
}

function searchAndPrintVideosByKeywords($searchTermsArray,$index=1,$max=10)
{
  $yt = new Zend_Gdata_YouTube(); 
  $query = $yt->newVideoQuery();
  //var_dump($query);
  $query->setMaxResults($max);
  $query->setOrderBy('viewCount');
  $query->setRacy('include');
  //$query->setCategory('Education');
  $query->setStartIndex($index); 

  
   //* The following commented-out code block demonstrates how to generate 
   //* the value that is passed to $query->setCategory
    $searchTermsArray = explode(" ",$searchTermsArray);
    $keywordQuery = '';
    if(is_array($searchTermsArray)){
   	 foreach ($searchTermsArray as $searchTerm) {
      $keywordQuery .= strtolower($searchTerm) . '/';
   	 }
	}
//	echo $keywordQuery."<hr>";

    $query->setCategory($keywordQuery);
 

  $videoFeed = $yt->getVideoFeed($query);
  return $videoFeed;
  //printVideoFeed($videoFeed, 'Search results for keyword search:');
}

  function sec2hms ($sec, $padHours = false) 
  {

    // start with a blank string
    $hms = "";
    
    // do the hours first: there are 3600 seconds in an hour, so if we divide
    // the total number of seconds by 3600 and throw away the remainder, we're
    // left with the number of hours in those seconds
    $hours = intval(intval($sec) / 3600); 

    // add hours to $hms (with a leading 0 if asked for)
    $hms .= ($padHours) 
          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":"
          : $hours. ":";
    
    // dividing the total seconds by 60 will give us the number of minutes
    // in total, but we're interested in *minutes past the hour* and to get
    // this, we have to divide by 60 again and then use the remainder
    $minutes = intval(($sec / 60) % 60); 

    // add minutes to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";

    // seconds past the minute are found by dividing the total number of seconds
    // by 60 and using the remainder
    $seconds = intval($sec % 60); 

    // add seconds to $hms (with a leading 0 if needed)
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hms;
    
  }

?>